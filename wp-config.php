<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'I6aS6hw50F6LFtsYIj4UFy5q/9CU2gCWH5S6bFOtq6seKA0nhXi+CqHTMC+VNJiCdu0YeJUQXwut4nJaJypevA==');
define('SECURE_AUTH_KEY',  'ilAaP7rm3Z5KgAe1qooP6f9DsSm5fGVgPHhRXY/2f+PxA8ry9t6O3nmtSDW16rT53qkHZjxtOS1pMWrHSw74Bg==');
define('LOGGED_IN_KEY',    'MCTsCr5j07DgSIK5aeq7EQYdy6HWocTPSqjFY0yfLfTgodhJatD7mkbMPZiN06npSR2QEDSd0Cb/LpS6r8yLWQ==');
define('NONCE_KEY',        'wowV8jIKavI4NunNmAlovYXs0CQet4qh5CYuIAtCTX3SfZN6DlF+ncpA0/PkMQ/U1+osSv5a6hOmCdtn9JKZxw==');
define('AUTH_SALT',        '0lns16NJn77M544HM0M3OasxT4ywMHZxjcNtacPl8j+NZmrStHBi6Cr5VR6HVniAfA1sO4RqJLPxuHy6OpBdSg==');
define('SECURE_AUTH_SALT', 'oIXfE20afH6nzhEVKZOiA439ECSHk5seCffKRhJVR09rdY6YznoVf4QGT5EnrYy5qITUPQU9jHFwzeSkWOgRaA==');
define('LOGGED_IN_SALT',   'rYXrI+LfRnDRLW1tXlMc0pXpN3NkwidwNK8PQPdbUUMlXLY+CBwn0dDrSsv5pRkBFkqN4hc2ahErChVAXmRAOw==');
define('NONCE_SALT',       'hnUV5M6enhdI903iwEmFys8q/QsKKxJBmSyWzZAjdXU2JWvj8trDV9yudxw3w1zcm+JMokPFCLq+GtEb1zToLw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';