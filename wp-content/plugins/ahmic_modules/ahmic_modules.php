<?php

    /*
    Plugin Name: Ahmic Modules
    Plugin URI: #
    Description: Ahmic Modules Visual Composer Extension
    Author: Bilal Khan
    Version: 1.0
    Author URI: #
    */
	
	function ahmic_addons() {
		/* Yet To Be Implemented */
	}

	register_activation_hook(__FILE__, 'ahmic_addons');

	add_action( 'admin_print_scripts', 'ahmic_modules_css' );

	function ahmic_modules_css(){
		wp_enqueue_style( 'ahmic_modules_vc', plugin_dir_url( __FILE__ ) . 'css/ahmic_modules_icons.css');
	}

	class ahmic_modules{

		var $module_dir;

		function __construct()
		{
			$this->module_dir = plugin_dir_path( __FILE__ ).'modules/';
			add_action('after_setup_theme',array($this,'ahmic_modules_init'));
			add_action( 'init', array($this,'contact_ajax_request') );
		}


		function ahmic_modules_init(){

			$ultimate_modules = get_option('ultimate_modules');

			/* Theme Elements Goes Here */
			$ultimate_modules[] = 'ahmic_homebanner';
			$ultimate_modules[] = 'ahmic_discount';
			$ultimate_modules[] = 'ahmic_collage';
			$ultimate_modules[] = 'ahmic_shop';
			$ultimate_modules[] = 'ahmic_experience';
			$ultimate_modules[] = 'ahmic_membership_text';
			$ultimate_modules[] = 'ahmic_about_banner';
			$ultimate_modules[] = 'ahmic_boys_cruz';
			$ultimate_modules[] = 'ahmic_lifestyle';
			$ultimate_modules[] = 'ahmic_timeline';
			$ultimate_modules[] = 'ahmic_textblock_slogan';
			$ultimate_modules[] = 'ahmic_gear_banner';
			$ultimate_modules[] = 'ahmic_weeklydeals_banner';
			$ultimate_modules[] = 'ahmic_weekly_deals_images';
			$ultimate_modules[] = 'ahmic_contact';
			$ultimate_modules[] = 'ahmic_blog';






			if(get_option('ultimate_row') == "enable")
				$ultimate_modules[] = 'ultimate_parallax';

			foreach(glob($this->module_dir."/*.php") as $module)
			{

				$ultimate_file = basename($module);
				$ultimate_fileName = preg_replace('/\\.[^.\\s]{3,4}$/', '', $ultimate_file);

				if(is_array($ultimate_modules) && !empty($ultimate_modules)){

					if(in_array(strtolower($ultimate_fileName),$ultimate_modules) ){

						require_once($module);
					}
				}
			}

			if(in_array("woocomposer",$ultimate_modules) ){
				if(defined('WOOCOMMERCE_VERSION'))
				{
					if(version_compare( '2.1.0', WOOCOMMERCE_VERSION, '<' )) {
						foreach(glob(plugin_dir_path( __FILE__ ).'woocomposer/modules/*.php') as $module)
						{
							require_once($module);
						}
					} else {
						/* Yet To Be Implemented */
					}
				} else {
					/* Yet To Be Implemented */
				}
			}
		}

		function contact_ajax_request(){
			require_once( 'module_functions.php' );
		}

	}

	new ahmic_modules;