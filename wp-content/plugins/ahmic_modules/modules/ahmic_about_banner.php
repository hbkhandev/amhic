<?php
/*
* Add-on Name: Ahmic Modules
* Add-on URI: #
*/
if(!class_exists("ahmic_about_banner")){
	class ahmic_about_banner{
		static $add_plugin_script;
		function __construct(){
			add_action("init",array($this,"ahmic_banner_init"));
			add_shortcode('ahmic_about_banner',array($this,'ahmic_about_banner_shortcode'));
		}
		function ahmic_banner_init(){

			if(function_exists("vc_map")){
				
				vc_map( array(
					"base" => "amhic_tabs",
					"name" => __( "AMHIC Tabs", "js_composer" ),
					"class" => "amhic_tabs_container",
					"category" => esc_html__( 'AMHIC', 'js_composer' ),
					"icon" => "amhic_vc_icon",
					"params" => array(

                            array(
                                'type' => 'param_group',
                                'value' => '',
                                'param_name' => 'amhic_tab',
                                // Note params is mapped inside param-group:
                                'params' => array(
                                    array(
                                        'type' => 'textfield',
                                        'value' => '',
                                        'heading' => 'Enter your title',
                                        'param_name' => 'title',
                                    ),

                                    array(
                                        'type' => 'textarea_html',
                                        'value' => '',
                                        'heading' => 'Enter your description',
                                        'param_name' => 'description',
                                    )
                                )
                            ),

						)
					) 
				);
			}
		}
		
		
		function ahmic_about_banner_shortcode( $atts, $content = null ) {
		
			$result = shortcode_atts( array(
				
				'background_image' => '',
				'element_heading' => '',
				'element_description' => '',
				'amhic_tab' => '',

				

			), $atts );
				
			
			extract( $result );
			
			/* Image Addition */
			
			$image = wp_get_attachment_image_src($background_image,'full');
			
			/*$html.='<section class="banner-section">
					    <div class="banner-img-container bg-image" data-image="'.esc_url($image[0]).'">
					        <div class="container">
					            <div class="banner-content-container">
					                <div class="banner-content vertical-top">
					                    <div class="banner-title-with-border">
					                        <h2>'.esc_attr($element_heading).'</h2>
					                    </div>
					                    <div class="heading-lg">
					                        <h3 class="color_white">'.esc_attr($element_description).'</h3>
					                    </div>
					                </div>
					            </div>
					        </div>
					        <span class="mask mask-bottom white"></span>
					    </div>
					</section>';*/
            $html = $amhic_tab;
			return $html;

		} /* end of function */
		
		
	}
	
	new ahmic_about_banner;
	
	if(class_exists('WPBakeryShortCode'))
	{
		class WPBakeryShortCode_ahmic_about_banner extends WPBakeryShortCode {
		}
	}
}