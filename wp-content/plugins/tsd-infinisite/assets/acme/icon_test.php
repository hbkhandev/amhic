<?
// where there's testing, there's acme!
use TSD_Infinisite\Acme;

$icons = TSD_Infinisite\Font_Awesome::get_icons();
$brands = TSD_Infinisite\Font_Awesome::get_brands();

$weights = ['fas', 'far', 'fal'];

?>


<div class="grid-container" data-editor-style="">

    <div class="grid-x grid-padding-y">

        <div class="cell small-12">
            <? Kint::dump($icons) ?>
        </div>

        <div class="cell shrink space-right">
            <i class="fas fa-alarm-clock secondary_light-text"></i>
        </div>

    </div>

    <div class="grid-x small-up-1 medium-up-3 large-up-6 grid-padding-x">

        <? foreach ($icons as $icon => $label): ?>
            <div class="cell">
                <div class="card">
                    <div class="card-section">
                        <? foreach ($weights as $weight): ?>
                            <i class="<?= $weight ?> fa-<?= $icon ?>"></i>
                        <? endforeach ?>

                    </div>

                    <div class="card-divider">
                        <h6>
                            <?= $icon ?><br>
                            <small>
                                ( <?= $label ?> )
                            </small>
                        </h6>
                    </div>
                </div>

            </div>
        <? endforeach ?>
    </div>

    <div class="grid-x">
        <div class="cell">
            <div class="spacer xlarge"></div>
            <h2>Brands</h2>
            <div class="spacer"></div>
        </div>
    </div>

    <div class="grid-x small-up-1 medium-up-3 large-up-6 grid-padding-x">

        <? foreach ($brands as $icon => $label): ?>
            <div class="cell">
                <div class="card">
                    <div class="card-section">
                        <i class="fab fa-<?= $icon ?>"></i>
                    </div>

                    <div class="card-divider">
                        <h6>
                            <?= $icon ?><br>
                            <small>
                                ( <?= $label ?> )
                            </small>
                        </h6>
                    </div>
                </div>

            </div>
        <? endforeach ?>
    </div>

</div>
