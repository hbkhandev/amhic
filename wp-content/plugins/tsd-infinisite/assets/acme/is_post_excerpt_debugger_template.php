<div class="grid-container" data-editor-style>
    <div class="grid-x">
        <div class="cell">
            <h2>Template Debugger Page</h2>
        </div>
    </div>

    <div class="grid-x">

        <div class="cell medium-shrink">
            <ul class="vertical tabs" data-tabs id="example-tabs">
                <? foreach ($post_types as $c => $post_type): ?>
                    <? $c++ ?>
                    <? $active = $c == 1 ? 'is-active' : ''; ?>
                    <li class="tabs-title <?= $active ?>"><a
                                href="#panel<?= $c ?>v"><?= $post_type['name'] ?></a>
                    </li>
                <? endforeach ?>
            </ul>
        </div>
        <div class="cell medium-auto">
            <div class="tabs-content" data-tabs-content="example-tabs">
                <? foreach ($post_types as $c => $post_type): ?>
                    <? $c++ ?>
                    <? $active = $c == 1 ? 'is-active' : ''; ?>
                    <div class="tabs-panel <?= $active ?>" id="panel<?= $c ?>v">

                        <? $pt_posts = $posts[$post_type['post_type']]; ?>


                        <? foreach ($pt_posts as $pt_post): ?>

                            <h4><?= $pt_post->post_title ?></h4>


                            <? foreach ($excerpt_templates as $filepath => $excerpt_template): ?>

                                <h6 class="gray_dark-background white-text no-margin text-center" style="line-height: 2"><?= $excerpt_template ?></h6>
                                <div class="callout gray_light">

                                    <div class="spacer small"></div>
                                    <?= \TSD_Infinisite\IS_Post_Archive::build_twig_template($pt_post, $filepath) ?>
                                    <div class="spacer small"></div>
                                </div>


                            <? endforeach ?>

                        <? endforeach ?>


                    </div>
                <? endforeach ?>

            </div>
        </div>

    </div>


    <div class="grid-x grid-padding-x">
        <div class="cell auto">
            <? \Kint::dump($post_types);
            ?>
        </div>
        <div class="cell auto">
            <? \Kint::dump($excerpt_templates); ?>
        </div>
        <div class="cell auto">
            <? \Kint::dump($posts); ?>
        </div>
    </div>


</div>
