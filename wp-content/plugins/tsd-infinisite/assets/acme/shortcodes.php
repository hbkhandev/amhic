<?


function is_post_create_form_shortcode_fn($atts) {

    // not sure we need this
    $defaults = ['post_type'   => 'post',
                 'post_status' => 'draft',];

    $post_type = $atts['post_type'] ? $atts['post_type'] : $defaults['post_type'];
    $uc_post_type = ucwords($post_type);
    $defaults['form_submit_value'] = "Submit {$uc_post_type} Post";


    $config = wp_parse_args($atts, $defaults);

    ob_start();
    acf_form(['post_title'           => 'true',
              'post_id'              => 'new_post',
              'html_updated_message' => '<h2>Thank You!</h2><p>We will be in touch regarding your event.</p>',
              'new_post'             => ['post_type'   => $config['post_type'],
                                         'post_status' => 'draft'],
              'submit_value'         => $config['form_submit_value']]);
    return ob_get_clean();

}

function spacer_fn($size = '') {

    $class = $size === '' ? '' : join(' ', $size);
    return "<div class='spacer {$class}'></div>";

}

add_shortcode("is_post_create_form", "is_post_create_form_shortcode_fn");
add_shortcode("spacer", "spacer_fn");

\TSD_Infinisite\Shortcode::create('menu_accordion');