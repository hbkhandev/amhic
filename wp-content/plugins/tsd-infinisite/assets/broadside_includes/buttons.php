<? $palette = new TSD_Infinisite\Palette() ?>

<div class="grid-x grid-padding-x">
    <div class="cell">
        <a class="success button" href="#">Save</a>
        <a class="alert button" href="#">Delete</a>
        <a class="warning button" href="#">Delete</a>
        <br>
        <a class="tiny button" href="#">So Tiny</a>
        <a class="small button" href="#">So Small</a>
        <a class="large button" href="#">So Large</a>
        <a class="expanded button" href="#">Such Expand</a>
    </div>


    <div class="cell">


        <? $styles = [
            'Default' => '',
            'Hollow' => 'hollow',
            'Ghost' => 'ghost',
            'Arrow Link' => 'arrow_link'
        ];
        ?>

        <? foreach ($palette->roles as $role): ?>
            <? $bg = $role == 'white' ? 'gray-background' : '' ?>
            <div class="cell <?= $bg ?>">

                <h3 class="<?= $role ?>-text" style="text-transform: capitalize;"><?= $role ?></h3>

                <? foreach ($styles as $label => $style) : ?>
                    <? $bg = in_array($style, ['hollow', 'ghost']) ? 'gray_xlight-background' : '' ?>
                    <div class="grid-x align-middle <?= $bg ?>">
                        <div class="cell">
                            <p class="no-margin"><?= $label ?></p>
                        </div>
                        <div class="cell">

                            <? if ($role == 'white' || $role == 'black'): ?>

                                <p class="white-background">The role "<?= $role ?>" doesn't have shades</p>

                            <? endif ?>

                            <? if ($role != 'white' && $role != 'black'): ?>
                                <? foreach ($palette->mods as $mod): ?>
                                    <a href="#" class="button <?= "{$role}_{$mod}" ?> <?= $style ?>">
                                        <?= $mod ?>
                                    </a>
                                <? endforeach ?>
                            <? endif ?>

                            <a href="#" class="button <?= $role ?> tiny <?= $style ?>">Tiny</a>
                            <a href="#" class="button <?= $role ?> small <?= $style ?>">Small</a>
                            <a href="#" class="button <?= $role ?> <?= $style ?>">Default</a>
                            <a href="#" class="button <?= $role ?> large <?= $style ?>">Large</a>
                        </div>
                        <div class="cell">
                            <a href="#" class="button <?= $role ?> expanded">Expanded</a>
                        </div>
                    </div>
                <? endforeach ?>


            </div>
            <div class="cell">
                <div class="spacer"></div>
            </div>
        <? endforeach ?>

    </div>

    <div class="cell">

        <h2>Button Group</h2>
        <div class="button-group">
            <a class="button">One</a>
            <a class="button">Two</a>
            <a class="button">Three</a>
        </div>
    </div>
</div>