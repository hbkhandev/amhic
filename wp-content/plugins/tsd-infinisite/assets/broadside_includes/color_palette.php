<?
$palette = new \TSD_Infinisite\Palette();
$colors = $palette->shades;
$c = 0;
?>

<div class="grid-x grid-padding-x">
    <? foreach ($colors as $role => $value): ?>

        <div class="cell <?= $role ?>-background <?= $c % 7 != 0 ? 'auto' : ''; ?>">
            <h4 class="white-text"><?= $role ?></h4>
            <p class="white-text no-margin"><?= $value ?></p>
        </div>

        <? $c++ ?>
    <? endforeach ?>
</div>