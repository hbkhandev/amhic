<? $heading_classes = [

    'h1' => TSD_Infinisite\ACF_Helper::get_repeater('is_global_type_heading_one_classes'),
    'h2' => TSD_Infinisite\ACF_Helper::get_repeater('is_global_type_heading_two_classes'),
    'h3' => TSD_Infinisite\ACF_Helper::get_repeater('is_global_type_heading_three_classes'),
    'h4' => TSD_Infinisite\ACF_Helper::get_repeater('is_global_type_heading_four_classes'),
    'h5' => TSD_Infinisite\ACF_Helper::get_repeater('is_global_type_heading_five_classes'),
    'h6' => TSD_Infinisite\ACF_Helper::get_repeater('is_global_type_heading_six_classes'),
    'ul' => TSD_Infinisite\ACF_Helper::get_repeater('is_global_type_unordered_list_classes'),
    'ol' => TSD_Infinisite\ACF_Helper::get_repeater('is_global_type_ordered_list_classes'),
    'p' => TSD_Infinisite\ACF_Helper::get_repeater('is_global_type_paragraph_classes'),
    'a' => TSD_Infinisite\ACF_Helper::get_repeater('is_global_type_link_classes'),
    'a.button' => TSD_Infinisite\ACF_Helper::get_repeater('is_global_type_button_classes'),

];

$lookup =[
        'h1' => "Heading One",
        'h2' => "Heading Two",
        'h3' => "Heading Three",
        'h4' => "Heading Four",
        'h5' => "Heading Five",
        'h6' => "Heading Six",
        'ul' => "Unordered List",
        'ol' => 'Ordered List',
        'p' => 'Paragraph',
        'a' => 'Link',
        'a.button' => 'Button'
]

?>

<div class="grid-x ">
    <div class="cell auto">


        <? foreach ($heading_classes as $label => $styles): ?>
        <<?= $label ?> class='center-text'><?= $lookup[$label] ?></<?= $label ?>>

    <? if ($styles): ?>

    <div class="card">
        <div class="card-section">
            <p class="center-text"><strong><?= $lookup[$label] ?> Styles</strong></p>
            <hr>
            <? foreach ($styles as $style): ?>
                <<?= $label ?> class='<?= $style['title'] ?>'><?= $style['title'] ?></<?= $label ?>>
            <? endforeach ?>
        </div>
    </div>
    <? endif ?>


    <? endforeach ?>


    </div>
</div>