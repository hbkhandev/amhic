<?php
$pt = \TSD_Infinisite\Acme::get_post_types();

foreach ($pt as $type):

    $name = $type['post_type'];
    $taxonomy_objects = get_object_taxonomies($name, 'objects');

    $query = new WP_Query(['posts_per_page' => -1, 'post_type' => $name]);


    print "<h3>$name</h3>";

    print "<table class='object-list'>";

    print "<thead>";
    print "<tr>";
    print "<td>";
    print "<p>terms</p>";
    print "</td>";


    foreach ($taxonomy_objects as $tax):
        $terms = get_terms($tax->name, ['hide_empty' => 0]);

        foreach ($terms as $term):

            print "<td>";
            print "<p>$term->slug</p>";
            print "</td>";
        endforeach;

    endforeach;


    print "</tr>";
    print "</thead>";

    foreach ($query->posts as $post):

        print "<tr>";
        print "<td>";
        print "<p>$post->post_title</p>";
        print "</td>";



        foreach ($taxonomy_objects as $tax):
            $terms = get_terms($tax->name, ['hide_empty' => 0]);

			// Get all the current term of the post
			$active_terms = [];
			if ( $post_terms = get_the_terms( $post->ID, $tax->name ) ) {
				foreach( $post_terms as $post_term ) {
					$active_terms[] = $post_term->term_id;
				}
			}

            foreach ($terms as $term):
                print "<td>";
                print "<input type='checkbox' data-tax='{$tax->name}' data-term='{$term->term_id}' data-post-id='{$post->ID}' " . ( in_array( $term->term_id, $active_terms ) ? 'checked="checked"' : '' ) . " />";
                print "</td>";
            endforeach;

        endforeach;


        print "</tr>";


    endforeach;

    print "</table>";


endforeach;

//print_r($taxonomy_objects);


?>