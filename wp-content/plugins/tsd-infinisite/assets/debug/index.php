<div class="card">
    <div class="card-section">

        <div class="grid-x grid-padding-x align-center align-middle">
            <div class="small-12 medium-shrink cell">
                <h3 class="">InfiniSite Debugging Suite v.Alpha</h3>
                <p>Pick a debug on the right</p>
            </div>
            <div class="small-12 medium-shrink cell">
                <select onchange="if (this.value) window.location.href=this.value">
                    <option disabled>Select A Debug Template</option>
					<? foreach ( $this->files as $label => $filepath ): ?>
                        <? $active = $this->active == $label ? 'selected': '' ?>
                        <option <?= $active ?> value="<?= site_url("?is_debug=$label") ?>"><?= $label ?></option>
					<? endforeach ?>
                </select>
            </div>
        </div>
    </div>
</div>