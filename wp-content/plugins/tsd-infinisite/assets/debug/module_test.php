<?
$test_data = new \TSD_Infinisite\IS_Test_Page_Builder_Modules();
$test_data->add_live_data();
?>

<div class="grid-container" data-editor-style>
    <div class="grid-x grid-padding-x grid-padding-y">
        <div class="cell">
            <h3>PHPUnitTest Visual Display</h3>
            <p>Each of these sections is a test case for the unit testing.</p>
        </div>

    </div>
    <div class="grid-x grid-padding-x grid-padding-y small-up-1 medium-up-2">

        <? foreach ($test_data->page_builder_modules as $c => $module_data): ?>

            <div class="cell">
                <h5 class="no-margin"><?= $c ?></h5>
                <p class="gray-text">ACF Module: <?= $module_data['acf_fc_layout'] ?></p>
                <div class="callout">

                    <?
                    $module_args = [
                        'module' => $module_data
                    ];

                    if (array_key_exists('template', $module_data))
                        $module_args['module_template'] = $_SERVER['DOCUMENT_ROOT'] . $module_data['template'];


                    $module = new \TSD_Infinisite\ACF_Module($module_args);

                    print $module->get_html();
                    ?>
                </div>
            </div>
        <? endforeach; ?>


    </div>
    <div class="cell">
        <div class="spacer large"></div>
    </div>
</div>