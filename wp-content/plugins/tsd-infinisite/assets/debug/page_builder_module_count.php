<?php $module_information = \TSD_Infinisite\Page_Builder::get_module_use_info_for_debug(); ?>

<div class="grid-container">
    <div class="grid-x">
        <div class="cell medium-3">
            <ul class="vertical tabs" data-tabs id="page_builder_module_count_debug_tabs">
				<? $c = 0; ?>
				<? foreach ( $module_information as $mod_label => $mod_info ): ?>
					<? $active = $c == 0 ? 'is-active' : ''; ?>
                    <li class="tabs-title <?= $active ?>"><a href="#panel<?= $c ?>"><?= $mod_label ?></a></li>
					<? $c ++; ?>
				<? endforeach ?>
            </ul>
        </div>
        <div class="cell medium-9">
            <div class="tabs-content" data-tabs-content="page_builder_module_count_debug_tabs" style="border-top: 1px solid #e6e6e6;">

				<? $c = 0; ?>
				<? foreach ( $module_information as $mod_label => $mod_info ): ?>
					<? $active = $c == 0 ? 'is-active' : ''; ?>
                    <div class="tabs-panel <?= $active ?>" id="panel<?= $c ?>">
						<? $count = count( $mod_info ) ?>
                        <h4><?= $mod_label ?> - <?= $count ?> posts</h4>
                        <hr>
                        <div class="grid-x small-up-1 medium-up-2 large-up-3 grid-margin-x grid-margin-y">

							<? foreach ( $mod_info as $post_label => $post_info ): ?>
								<? $post = $post_info['post'] ?>
								<? $count = $post_info['count'] == 1 ? '' : "({$post_info['count']} instances)"; ?>
                                <div class="cell">
                                    <div class="card">
                                        <div class="card-section">
                                            <p class="no-margin"><?= $post->post_title ?> <?= $count ?></p>
                                            <hr class="small">
                                            <div class="grid-x">
                                                <div class="cell small-auto medium-auto">
													<?= $post->get_edit_link() ?>
                                                </div>
                                                <div class="cell small-auto medium-auto">
													<?= $post->get_single_view_link() ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


							<? endforeach ?>
                        </div>
                    </div>
					<? $c ++; ?>
				<? endforeach ?>
            </div>
        </div>
    </div>
</div>