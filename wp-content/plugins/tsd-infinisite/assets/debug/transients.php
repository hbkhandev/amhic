<? \TSD_Infinisite\Transient::get_all() ?>
<script>

    let row = $(".transient-item");

    row.click(function () {
        let obj = $(this),
            name = obj.data('name');
        $.get({
            url: '/wp-json/is_api/v1/delete_transient?name=' + name,
            success: function(){
                obj.remove()
            }
        })
    })

</script>
