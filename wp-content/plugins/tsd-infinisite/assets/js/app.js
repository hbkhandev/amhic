/**
 * Master IS JS function doc
 */


function init_sticky_element_hack() {

    jQuery("[data-js-stick-to-window]").each(function () {
        var obj = jQuery(this);

        var args = {};

        args.offset_top = 45;

        var inline_offset = obj.attr("data-js-sticky-offset");

        if (inline_offset)
            args.offset_top = inline_offset;

        if (jQuery('body').hasClass("admin_bar") && args.offset_top >= 32)
            args.offset_top = 32;

        obj.stick_in_parent(args);


    })

}

function init_div_click_hack() {

    jQuery("[data-link]").click(function () {
        window.location.href = jQuery(this).attr("data-link");
    });

    jQuery("[data-popup]").click(function () {
        window.open(jQuery(this).attr("data-link"), '_blank');
    });

}


function init_website_query_module_slider() {

    var carousel = jQuery('.is-website-slider-query-module');

    carousel.each(function () {
        jQuery(this).owlCarousel({
            'items': 1,
            'autoHeight': 1,
            'nav': 1,
            'autoplay': 1,
            'loop': 1,
            'navContainerClass': 'owl-nav align-spaced flex',
            'navText': ['<h6><i class="fas fa-arrow-circle-left"></i> Prev</h6>', '<h6>Next <i class="fas fa-arrow-circle-right"></i></h6>']
        });
    })

}


function init_accordion_menu_update() {


}

function init_section_link_shortcode_fn() {

    /**
     * this enables the shortcode function that allows the user to insert scroll links to rows
     */

    var links = jQuery("[data-is-section-link]");

    if (!links.length) return;

    links.click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        var section = jQuery(this).data("is-section-link");


        var obj = jQuery("#" + section);

        jQuery(window).scrollTo(obj, 250);

    });

}

function init_topic_slider() {

    var sliders = jQuery(".dca_topic_two_line_slider");

    if (!sliders.length) return;

    sliders.each(function () {

        var obj = jQuery(this),
            toggle = jQuery("<span class='dca_topic_two_line_slider_toggle cursor-pointer'>Show More</span>");

        obj.after(toggle);

        toggle.click(function () {
            obj.toggleClass("show_all_tags");
        })


    });

}

function init_ratio_hack() {

    var hack = jQuery("[data-ratio]");

    hack.each(function () {
        var obj = jQuery(this),
            r = obj.attr('data-ratio'),
            w = obj.width(),
            adj = w * r;

        obj.height(adj + 'px');
    })


}

function init_mobile_menu_hack() {


    var trigger = $(".mobile-menu-trigger-container, .close-menu-container");

    trigger.click(function () {
    })


}

// function init_smooth_scroll() {
//
//     let smooth_link = jQuery(".smooth-link"),
//         moveTo = new MoveTo();
//
//
//     smooth_link.each(function () {
//         moveTo.registerTrigger($(this).get(0));
//     })
//
// }


function init_drop_button_row() {

    // TODO: hook up the button equalizer function

    let row = $(".drop-button-row");

    if (!row.length) return;


    row.each(function () {
        let btns = $(this).find(".button");
        btns.each(function () {
            let from_top = this.getBoundingClientRect().top;
        })

    })


}

function init_full_height_post_grid() {

    // this is a wpbakery grid builder helper function
    // TODO: find a better place to put this

    let grid = $(".full-height-post-grid");

    if (!grid.length) return null;

    let col = grid.find(".is_vc_post > div"),
        height = 0;

    col.each(function () {
        let obj = $(this).height();
        height = height >= obj ? height : obj;
    });

    col.height(height + "px");

}

function hack_awards_excerpt() {

    let awards = jQuery(".exc-awards-card, .exc-contract-card");

    if (!awards.length) return null;

    height = 0;

    awards.each(function () {
        let img = jQuery(this).find(".logo");
        height = img.height() >= height ? img.height() : height;
    });

    awards.find(".logo").height(height + 'px');

}


// jQuery(function () {
//     jQuery(document).foundation();
// });

jQuery(window).resize(function () {
    init_ratio_hack();
});

jQuery(window).load(function () {
    // init_smooth_scroll();
    init_full_height_post_grid();
    hack_awards_excerpt();
});

init_ratio_hack();

init_sticky_element_hack();
init_website_query_module_slider();
init_div_click_hack();
init_accordion_menu_update();
init_section_link_shortcode_fn();
init_topic_slider();
init_mobile_menu_hack();
init_drop_button_row();
