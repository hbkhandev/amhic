jQuery(document).ready( function() {

	function is_build_posts_box( taxonomies, posts ) {
		var html = '';

		if ( posts.length ) {

			html += '<table class="table table-bordered is-posts-list" cellpadding="0" cellspacing="0">';
				html += '<thead>';
					html += '<tr>';
						if ( taxonomies.length ) {
							html += '<th></th>';

							taxonomies.forEach( tax => {
	
								if ( tax.terms.length ) {
									html += '<th colspan="' + tax.terms.length + '">' + tax.label + '</th>';
								} else {
									html += '<th>' + tax.label + '<br><small>No terms found</small></th>';
								}
											
							});
						} else {
							html += '<th>No taxonomies found.</th>';
						}
						
					html += '</tr>';
					html += '<tr>';
						html += '<th>Title</th>';
						if ( taxonomies.length ) {
							taxonomies.forEach( tax => {

								if ( tax.terms.length ) {
									tax.terms.forEach(term => {
										html += '<td>' + term.name + '</td>';
									});
								} else {
									html += '<td>' + tax.label + '</td>';
								}
				
							});
						}
					html += '</tr>';
				html += '</thead>';
				html += '<tbody>';

					posts.forEach( post => {
						html += '<tr>';
							html += '<td>' + post.title + '</td>';
							taxonomies.forEach( tax => {
								if ( tax.terms.length ) {
									tax.terms.forEach(term => {
										var checked = post.active_terms.indexOf( term.slug ) !== -1 ? 'checked' : '';
										html += '<td><input type="checkbox" data-post-id="' + post.id + '" data-taxonomy="' + tax.name + '" data-term="' + term.slug + '" data-term-id="' + term.id + '" value="1" ' + checked + '></td>';
									});
								} else {
									html += '<td>&nbsp;</td>';
								}
							});

						html += '</tr>';
					});

				html += '</tbody>';
			html += '</table>';

		} else {
			html += '<p>No posts found for this post type</p>';
		}

		return html;
	}

	jQuery('[name="tax_editor_pt"').on('change', function() {
		
		// console.log( jQuery(this).val() );

		var elemObj = jQuery(this);
		elemObj.prop('disabled', true);

		var post_types = jQuery(this).val() ? jQuery(this).val() : [];

		jQuery.ajax({
			url : is_bulk_tax_editor.ajax_url,
			type : 'post',
			dataType: 'json',
			data : {
				action : 'is_bulk_tax_editor_get_tax_and_posts',
				nonce: is_bulk_tax_editor.nonce,
				post_types: post_types,
			},
			success : function( response ) {
				// console.log( response );

				var html = '';

				response.data.forEach(post_type => {
					html += '<div class="row clearfix is-taxonomies" data-post-type="' + post_type.type + '">';
						html += '<div class="col">';
							html += '<h4>' + post_type.label + '</h4>';
							html += '<div class="table-container">';
								html += is_build_posts_box( post_type.taxonomies, post_type.posts );
							html += '</div>';
						html += '</div>';
					html += '</div>';
				});

				jQuery('#tax_editor_tax_list').html( html );
			},
			complete: function() {
				elemObj.prop('disabled', false);
			}
		});

	});



	jQuery('body').on('change', '.is-posts-list input[type="checkbox"]', function() {

		var checkbox = jQuery(this);

		if ( checkbox.is(':checked') ) {
			var enabled = 1;
		} else {
			var enabled = 0
		}

		var post_id = checkbox.data('post-id');
		var tax_name = checkbox.data('taxonomy');
		var term_id = checkbox.data('term-id');

		checkbox.prop('disabled', true);

		jQuery.ajax({
			url : is_bulk_tax_editor.ajax_url,
			type : 'post',
			dataType: 'json',
			data : {
				action : 'tsd_update_post_term',
				nonce: is_bulk_tax_editor.nonce,
				pid: post_id,
				taxname: tax_name,
				termid: term_id,
				status: enabled,
			},
			success : function( response ) {
				// console.log( response );
			},
			complete: function() {
				checkbox.prop('disabled', false);
			}
		});


	});
});