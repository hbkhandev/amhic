(function($) {
    if ( typeof wpLink !== 'undefined' ) {

        /**
         * Modify link attributes
         */
        wpLink.getAttrs = function() {
            wpLink.correctURL();
            return {
                // rel:        $( '#wpse-rel-no-follow' ).prop( 'checked' ) ? 'nofollow' : '',
                href:       $('#wpse_link_tracking_tag').val() ? $.trim( $( '#wp-link-url' ).val() + "?lttag=" + $('#wpse_link_tracking_tag').val() ) : $.trim( $( '#wp-link-url' ).val() ) ,
                target:     $( '#wp-link-target' ).prop( 'checked' ) ? '_blank' : ''
            };
        }

        $(document).on( 'wplink-open', function( wrap ) {

            // Custom HTML added to the link dialog
            if( $('#wpse-rel-no-follow').length < 1 )
                $('#link-options')
                    // .append( '<div> <label><span></span> <input type="checkbox" id="wpse-rel-no-follow"/> No Follow Link</label></div>')
                    .append( '<div>' +
                    '            <label><span>Tracking Tag</span>' +
                    '            <input type="text" name="wpse-link-tracking-tag" id="wpse_link_tracking_tag" placeholder="(no spaces, commas)">' +
                    '           </label>' +
                    '       </div>');

            // Get the current link selection:
            var _node = wpse_getLink();

            if( _node ) {
                // Fetch the rel attribute
                var _rel = $( _node ).attr( 'rel' );

                // Update the checkbox
                $('#wpse-rel-no-follow').prop( 'checked', 'nofollow' === _rel );
            }

        });
    }


})(jQuery);