function pam() {

    let c = document.querySelector(".is_padding_and_margin");

    if (!c) return;

    let pam = c.querySelectorAll('.pam_entry'),
        field_to_update = c.querySelector(".wpb_vc_param_value"),
        current_val = field_to_update.value;

    pam.forEach(input => input.addEventListener('input', e => update_value(e)));

    current_val.split(",").forEach((str, i) => {
        console.log(str, i);
        pam.item(i).checked = str === 'true';
    });

    function update_value(e) {
        let vals = [];
        pam.forEach(p => vals.push(p.checked));
        field_to_update.value = vals.join();
    }
}

pam();