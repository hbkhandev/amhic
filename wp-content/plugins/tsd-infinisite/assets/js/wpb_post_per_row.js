function ppr() {

    let c = document.querySelector(".is_wpb_post_per_row");

    if (!c) return;

    let ppr = c.querySelectorAll('.ppr_entry'),
        field_to_update = c.querySelector(".wpb_vc_param_value"),
        current_val = field_to_update.value;

    ppr.forEach(input => input.addEventListener('change', () => update_value()));

    current_val.split(",").forEach((str, i) => {
        ppr.item(i).value = str;
    });

    function update_value() {
        let vals = [];
        ppr.forEach(p => vals.push(p.value));
        field_to_update.value = vals.join();
    }
}

ppr();