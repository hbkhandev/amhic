function ps() {

    let container = document.querySelector(".is_wpb_post_selector_draggable");

    if(!container) return;

    let lists = container.querySelectorAll('ul.sortable'),
        pt_selector = container.querySelector('select#post_type_select'),
        posts_to_save = container.querySelector('ul.posts-sort-to'),
        posts_to_display = container.querySelector('ul.posts-sort-from'),
        value_to_update = container.querySelector('input.wpb_vc_param_value');

    if (value_to_update.value !== '') {
        let parseme = value_to_update.value.slice(0, -1);

        let items = parseme.split("|");
        items.forEach(item => {

            let node = document.createElement("li");

            try {
                let json_item = JSON.parse(item);
                node.setAttribute('data-post-id', json_item.ID);
                node.append(json_item.post_title);
            } catch {
                console.log(item, 'invalid json');
                node.append("invalid json found");
            }

            posts_to_save.appendChild(node);
        });


        // console.log(parseme);
        // let values = JSON.parse(parseme);
        // console.log(values);
    }

    lists.forEach((list) => {
        let atts = {
            animation: 150,
            group: 'shared',
            onEnd: e => {
                let str = '';
                posts_to_save.querySelectorAll('li').forEach(item => {
                    let val = item.innerHTML,
                        id = item.getAttribute('data-post-id');
                    str = str + json_encode({"ID": id, "post_title": val}) + "|";
                });
                value_to_update.value = str;
            }
        };

        let cn = list.className;

        new Sortable(list, atts);
    });

    pt_selector.addEventListener('change', (e) => {
        let type = e.target[e.target.selectedIndex].text,
            url = 'https://baitisprod.wpengine.com/wp-json/is_api/v1/query',
            data = {
                query: {
                    post_type: type
                }
            };


        fetch(url, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(data), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => {


                while (posts_to_display.firstChild)
                    posts_to_display.removeChild(posts_to_display.firstChild);

                try {

                    response.forEach(item => {

                        let node = document.createElement("li");

                        node.setAttribute('data-post-id', item.ID);
                        node.append(item.post_title);

                        posts_to_display.appendChild(node);

                    })
                } catch {
                    console.log('no results found');
                }
            })
            .catch(error => console.error('Error:', error));

    });

}

ps();