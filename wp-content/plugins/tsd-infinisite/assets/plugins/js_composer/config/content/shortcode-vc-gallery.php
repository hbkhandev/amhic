<?php
if ( ! defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$vals = [ 'name'        => __( 'Image Gallery', 'js_composer' ),
          'base'        => 'vc_gallery',
          'icon'        => 'icon-wpb-images-stack',
          'category'    => __( 'Content', 'js_composer' ),
          'description' => __( 'Responsive image gallery', 'js_composer' ),
          'params'      => [ [ 'type'        => 'textfield',
                               'heading'     => __( 'Widget title', 'js_composer' ),
                               'param_name'  => 'title',
                               'description' => __( 'Enter text used as widget title (Note: located above content element).', 'js_composer' ), ],
                             [ 'type'        => 'dropdown',
                               'heading'     => __( 'Gallery type', 'js_composer' ),
                               'param_name'  => 'type',
                               'value'       => [ __( 'Flex slider fade', 'js_composer' )  => 'flexslider_fade',
                                                  __( 'Flex slider slide', 'js_composer' ) => 'flexslider_slide',
                                                  __( 'Nivo slider', 'js_composer' )       => 'nivo',
                                                  __( 'Image grid', 'js_composer' )        => 'image_grid', ],
                               'description' => __( 'Select gallery type.', 'js_composer' ), ],
                             [ 'type'        => 'dropdown',
                               'heading'     => __( 'Auto rotate', 'js_composer' ),
                               'param_name'  => 'interval',
                               'value'       => [ 3,
                                                  5,
                                                  10,
                                                  15,
                                                  __( 'Disable', 'js_composer' ) => 0, ],
                               'description' => __( 'Auto rotate slides each X seconds.', 'js_composer' ),
                               'dependency'  => [ 'element' => 'type',
                                                  'value'   => [ 'flexslider_fade',
                                                                 'flexslider_slide',
                                                                 'nivo', ], ], ],
                             [ 'type'        => 'dropdown',
                               'heading'     => __( 'Image source', 'js_composer' ),
                               'param_name'  => 'source',
                               'value'       => [ __( 'Media library', 'js_composer' )  => 'media_library',
                                                  __( 'External links', 'js_composer' ) => 'external_link', ],
                               'std'         => 'media_library',
                               'description' => __( 'Select image source.', 'js_composer' ), ],
                             [ 'type'        => 'attach_images',
                               'heading'     => __( 'Images', 'js_composer' ),
                               'param_name'  => 'images',
                               'value'       => '',
                               'description' => __( 'Select images from media library.', 'js_composer' ),
                               'dependency'  => [ 'element' => 'source',
                                                  'value'   => 'media_library', ], ],
                             [ 'type'        => 'exploded_textarea_safe',
                               'heading'     => __( 'External links', 'js_composer' ),
                               'param_name'  => 'custom_srcs',
                               'description' => __( 'Enter external link for each gallery image (Note: divide links with linebreaks (Enter)).', 'js_composer' ),
                               'dependency'  => [ 'element' => 'source',
                                                  'value'   => 'external_link', ], ],
                             [ 'type'        => 'textfield',
                               'heading'     => __( 'Image size', 'js_composer' ),
                               'param_name'  => 'img_size',
                               'value'       => 'thumbnail',
                               'description' => __( 'Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'js_composer' ),
                               'dependency'  => [ 'element' => 'source',
                                                  'value'   => 'media_library', ], ],
                             [ 'type'        => 'textfield',
                               'heading'     => __( 'Image size', 'js_composer' ),
                               'param_name'  => 'external_img_size',
                               'value'       => '',
                               'description' => __( 'Enter image size in pixels. Example: 200x100 (Width x Height).', 'js_composer' ),
                               'dependency'  => [ 'element' => 'source',
                                                  'value'   => 'external_link', ], ],
                             [ 'type'        => 'dropdown',
                               'heading'     => __( 'On click action', 'js_composer' ),
                               'param_name'  => 'onclick',
                               'value'       => [ __( 'None', 'js_composer' )                => '',
                                                  __( 'Link to large image', 'js_composer' ) => 'img_link_large',
                                                  __( 'Open prettyPhoto', 'js_composer' )    => 'link_image',
                                                  __( 'Open custom link', 'js_composer' )    => 'custom_link', ],
                               'description' => __( 'Select action for click action.', 'js_composer' ),
                               'std'         => 'link_image', ],
                             [ 'type'        => 'exploded_textarea_safe',
                               'heading'     => __( 'Custom links', 'js_composer' ),
                               'param_name'  => 'custom_links',
                               'description' => __( 'Enter links for each slide (Note: divide links with linebreaks (Enter)).', 'js_composer' ),
                               'dependency'  => [ 'element' => 'onclick',
                                                  'value'   => [ 'custom_link' ], ], ],
                             [ 'type'        => 'dropdown',
                               'heading'     => __( 'Custom link target', 'js_composer' ),
                               'param_name'  => 'custom_links_target',
                               'description' => __( 'Select where to open  custom links.', 'js_composer' ),
                               'dependency'  => [ 'element' => 'onclick',
                                                  'value'   => [ 'custom_link',
                                                                 'img_link_large', ], ],
                               'value'       => vc_target_param_list(), ],
                             vc_map_add_css_animation(),
                             [ 'type'        => 'el_id',
                               'heading'     => __( 'Element ID', 'js_composer' ),
                               'param_name'  => 'el_id',
                               'description' => sprintf( __( 'Enter element ID (Note: make sure it is unique and valid according to <a href="%s" target="_blank">w3c specification</a>).', 'js_composer' ), 'http://www.w3schools.com/tags/att_global_id.asp' ), ],
                             [ 'type'        => 'textfield',
                               'heading'     => __( 'Extra class name', 'js_composer' ),
                               'param_name'  => 'el_class',
                               'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' ), ],
                             [ 'type'       => 'css_editor',
                               'heading'    => __( 'CSS box', 'js_composer' ),
                               'param_name' => 'css',
                               'group'      => __( 'Design Options', 'js_composer' ), ], ], ];

$post_excerpts = TSD_Infinisite\WPB_Core_Helper::get_excerpt_templates( 'twig/image_gallery' );

foreach ( $post_excerpts as $fp => $template ):
    $name = end( explode( "/", $fp ) );

    $vals['params'][1]['value'][ $name ] = "is_template_$name";
endforeach;

return $vals;
