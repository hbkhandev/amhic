<?

namespace TSD_Infinisite;

use Carbon\Carbon;

class ACF {

    static function get_background_display_options($key = false, $fields_from_user = false) {

        /*
         *
         * this function was originally built to have margin and padding in them -
         * i'm moving away from that, but i dont want to pull the code out until i'm
         * sure we don't need them
         *
         *
         */


        if (!$key)
            return false;

        $defaults = [];

        $fields = wp_parse_args($defaults, $fields_from_user);



        $opacity = ['key'     => "{$key}_opacity",
                    'label'   => "Color Opacity",
                    'name'    => 'opacity',
                    'type'    => 'number',
                    'min'     => 0,
                    'max'     => 1,
                    'step'    => .01,
                    'suffix'  => '%',
                    'wrapper' => ['width' => 33]];

        $map_image_to_viewport_edges = ['key'          => "{$key}_map_to_viewport_edges",
                                        'label'        => "Map to Viewport Edges",
                                        'name'         => 'viewport_edge',
                                        'instructions' => 'Use if you need the background image to extend beyond the horizontal content',
                                        'type'         => 'true_false',
                                        'ui'           => 1,
                                        'wrapper'      => ['width' => 33]];

        $image = ['key'          => "{$key}_image",
                  'label'        => "Image",
                  'name'         => 'image',
                  'type'         => 'image',
                  'preview_size' => 'medium',
                  'wrapper'      => ['width' => 33]];

        $background_position = ['key'           => "{$key}_background_position",
                                'label'         => "Background Position",
                                'name'          => 'position',
                                'type'          => 'text',
                                'required'      => 1,
                                'default_value' => 'center',
                                'wrapper'       => ['width' => 22]];

        $background_size = ['key'           => "{$key}_background_size",
                            'label'         => "Background Size",
                            'name'          => 'size',
                            'type'          => 'text',
                            'default_value' => 'cover',
                            'required'      => 1,
                            'wrapper'       => ['width' => 22]];

        $background_repeat = ['key'           => "{$key}_background_repeat",
                              'label'         => "Image Repeat",
                              'name'          => 'repeat',
                              'type'          => 'select',
                              'choices'       => ['no-repeat' => 'None',
                                                  'repeat-x'  => 'Repeat X',
                                                  'repeat-y'  => 'Repeat Y',
                                                  'repeat'    => 'Repeat',],
                              'default_value' => 'no-repeat',
                              'wrapper'       => ['width' => 22]];


        $return = [];

        if (in_array('image', $fields)):
            $return[] = $opacity;
            $return[] = $map_image_to_viewport_edges;
            $return[] = $image;
            $return[] = $background_position;
            $return[] = $background_size;
            $return[] = $background_repeat;
        endif;


        return $return;

    }

    static function get_color_field($key = false, $atts = []) {

        return false;

    }

    static function get_accordion($key = false, $title = false, $args = []) {

        $defaults = ['type'   => 'accordion',
                     'closed' => 1];

        if ($key)
            $defaults['key'] = $key;

        $acf_field = \wp_parse_args($args, $defaults);

        $acf_field['label'] = $title ? $title : 'Accordion';
        if (!$title)
            $acf_field['endpoint'] = 1;

        return $acf_field;

    }

    static function get_tab($key = false, $title = 'Tab Divider', $args = []) {

        // this is used in the foundation options - tread lightly!
        if (!$key)
            return;

        $defaults = ['key'       => $key,
                     'label'     => $title,
                     'type'      => 'tab',
                     'placement' => 'left',];

        $acf_field = \wp_parse_args($args, $defaults);

        return $acf_field;

    }

    static function get_horizontal_tab($title, $key = false, $args = []) {

        $defaults = ['type' => 'tab',];

        if ($title)
            $defaults['label'] = $title;
        if ($title)
            $defaults['key'] = $key;

        $acf_field = \wp_parse_args($args, $defaults);

        return $acf_field;

    }

    static function get_message($message, $key = false, $args = []) {

        $defaults = ['type'    => 'message',
                     'newline' => 'wpautop'];

        if ($message)
            $defaults['message'] = $message;
        if ($message)
            $defaults['key'] = $key;

        $acf_field = \wp_parse_args($args, $defaults);

        return $acf_field;

    }

    static function get_flexbox_alignment_classes_group($key = false, $args = []) {

        if (!$key)
            return;

        $flexbox_horizontal_alignment_class = ['key'          => "{$key}_horizontal",
                                               'type'         => 'select',
                                               'ui'           => 1,
                                               'name'         => 'horizontal_alignment',
                                               'instructions' => 'Horizontal',
                                               'choices'      => ['left'    => 'Left',
                                                                  'center'  => 'Center',
                                                                  'right'   => 'Right',
                                                                  'spaced'  => 'Space Around',
                                                                  'justify' => 'Space Between',],
                                               'wrapper'      => ['width' => 50]];
        $flexbox_vertical_alignment_class = ['key'          => "{$key}_vertical",
                                             'type'         => 'select',
                                             'ui'           => 1,
                                             'name'         => 'vertical_alignment',
                                             'instructions' => 'Vertical',
                                             'choices'      => ['top'     => 'Top',
                                                                'middle'  => 'Middle',
                                                                'bottom'  => 'Bottom',
                                                                'stretch' => 'Stretch',],
                                             'wrapper'      => ['width' => 50]];


        $defaults = ['key'        => $key,
                     'name'       => Acme::strip_field_for_name($key),
                     'label'      => 'Flexbox Alignment Classes',
                     'type'       => 'group',
                     'sub_fields' => [$flexbox_horizontal_alignment_class,
                                      $flexbox_vertical_alignment_class,]];

        $flexbox_classes = \wp_parse_args($args, $defaults);

        return $flexbox_classes;

    }

    static function get_row_width_height($key = false, $args = []) {

        if (!$key)
            return;

        $width = ['key'         => "{$key}_width",
                  'ui'          => 1,
                  'name'        => 'max_width',
                  'placeholder' => 'Max Width',
                  'type'        => 'text',
                  'wrapper'     => ['width' => 50]];

        $height = ['key'         => "{$key}_height",
                   'ui'          => 1,
                   'name'        => 'fixed_height',
                   'placeholder' => 'Fixed Height',
                   'type'        => 'text',
                   'wrapper'     => ['width' => 50]];


        $defaults = ['key'        => $key,
                     'label'      => 'Max Width / Fixed Height',
                     'name'       => Acme::strip_field_for_name($key),
                     'type'       => 'group',
                     'sub_fields' => [$width,
                                      $height,]];

        $dimentions = \wp_parse_args($args, $defaults);

        return $dimentions;

    }

    static function get_row_spacers($key = false, $args = []) {

        if (!$key)
            return;

        $field_key = Acme::strip_field_for_name($key);


        $spacer_choices = ['false'  => 'None',
                           'xsmall' => 'Extra Small',
                           'small'  => 'Small',
                           'medium' => 'Medium',
                           'large'  => 'Large',
                           'xlarge' => 'Extra Large',];


        $top = ['key'           => "{$key}_top",
                'ui'            => 1,
                'name'          => 'top',
                'placeholder'   => 'Spacer Top',
                'type'          => 'select',
                'default_value' => 'medium',
                'choices'       => $spacer_choices,
                'wrapper'       => ['width' => 50]];

        $bottom = ['key'           => "{$key}_bottom",
                   'ui'            => 1,
                   'name'          => 'bottom',
                   'placeholder'   => 'Spacer Bottom',
                   'type'          => 'select',
                   'default_value' => 'medium',
                   'choices'       => $spacer_choices,
                   'wrapper'       => ['width' => 50]];


        $defaults = ['key'        => $key,
                     'name'       => Acme::strip_field_for_name($key),
                     'label'      => 'Top / Bottom Spacers',
                     'type'       => 'group',
                     'sub_fields' => [$top,
                                      $bottom,]];

        $top_bottom = \wp_parse_args($args, $defaults);

        return $top_bottom;

    }

    static function get_mce_format_menu_color_options($selector = false, $suffix = '') {

        return;

        $return = [];

        if (!$selector)
            return $return;

        $colors = Palette::get_colors();

        $suffix == '' ? '' : "-$suffix";

        foreach ($colors as $role => $value)
            $return[] = ['title'    => $role,
                         'selector' => $selector,
                         'classes'  => "{$role}-{$suffix}"];

        return $return;
    }

    static function get_post_excerpt_templates_for_select_menu() {
        return ACF::scan_directories_for_themes_for_select_menu('post_excerpts');
    }

    static function get_archive_template_views_for_select_menu() {
        return ACF::scan_directories_for_themes_for_select_menu('archive_templates');
    }

    static function scan_directories_for_themes_for_select_menu($directory_name = false) {

        $return = [];

        if (!$directory_name)
            return $return;

        $path = INFINISITE_URI . "twig/post_excerpts/*";
        $templates = glob($path);

        $child_theme_path = THEME_URI . "twig/post_excerpts/*";
        $child_theme_templates = glob($child_theme_path);

        foreach ($child_theme_templates as $ctt)
            $templates[] = $ctt;


        // attaching the templates to the archive module selector

        if ($templates)
            foreach ($templates as $template):

                $info = explode("/", $template);
                $filename = array_pop($info);
                $return[$template] = $filename;

            endforeach;


        return $return;
    }

    static function get_device_template_fields($key, $choices) {

        $return = [];

        $devices = ['mobile', 'tablet', 'desktop', 'large_desktop'];

        foreach ($devices as $device)
            $return[] = ['key'     => "{$key}_{$device}",
                         'name'    => "$device",
                         'label'   => $device,
                         'type'    => 'select',
                         'ui'      => 1,
                         'choices' => $choices];

        return $return;


    }

    static function is_preset_field_lookup($type) {

        $lookup = ['name'           => 'text',
                   'title'          => 'text',
                   'image'          => 'image',
                   'embed'          => 'oembed',
                   'file'           => 'file',
                   'link'           => 'link',
                   'identification' => 'text',
                   'subtitle'       => 'text',
                   'start_date'     => 'date_picker',
                   'end_date'       => 'date_picker',
                   'location'       => 'google_map',
                   'address_line_1' => 'text',
                   'address_line_2' => 'text',
                   'author_text'    => 'text',
                   'author_post'    => 'relationship',
                   'excerpt'        => 'wysiwyg',
                   'content'        => 'wysiwyg',
                   'source'         => 'text',
                   'hours'          => 'text',
                   'phone_number'   => 'text',
                   'email_address'  => 'text',
                   'fax_number'     => 'text',
                   'facebook'       => 'url',
                   'linkedin'       => 'url',
                   'youtube'        => 'url',
                   'twitter'        => 'url',
                   'instagram'      => 'url',];

        return $lookup[$type];

    }

    static function get_post_archive_query_template_display_logic() {
        $return = [[['field'    => 'field_ispb_layout_post_archive_query_template',
                     'operator' => ' == ',
                     'value'    => 'none']]];

        return $return;
    }

    static function icon_display_field_options($key = '', $atts = []) {

        $return = [];

        if ($key == '')
            return $return;

        if (substr($key, 0, 6) != 'field_')
            $key = "field_$key";


        $return['icon_select'] = ['key'        => "{$key}_icon_select",
                                  'name'       => 'icon_select',
                                  'type'       => 'select',
                                  'label'      => 'Icon Type',
                                  'ui'         => 1,
                                  'allow_null' => 1,
                                  'choices'    => Icon_Library::get_fontawesome_icons_for_acf_select(),
                                  'wrapper'    => ['width' => 25],];

        $return['icon_color'] = ACF::get_color_field("{$key}_icon_color", ['label'   => 'Icon Color',
                                                                           'name'    => 'icon_color',
                                                                           'wrapper' => ['width' => 25]]);

        $return['icon_background_color'] = ACF::get_color_field("{$key}_icon_background_color", ['label'         => 'Background Color',
                                                                                                 'name'          => 'icon_background_color',
                                                                                                 'wrapper'       => ['width' => 25],
                                                                                                 'allow_null'    => 1,
                                                                                                 'default_value' => '',]);


        $return['position'] = ['key'     => "{$key}_position",
                               'name'    => 'icon_position',
                               'type'    => 'select',
                               'label'   => 'Position',
                               'ui'      => 1,
                               'choices' => ['#' => 'Gimme Positions!'],
                               'wrapper' => ['width' => 25],];


        $return['icon_svg'] = ['key'               => "{$key}_icon_svg",
                               'name'              => 'icon_svg',
                               'type'              => 'file',
                               'label'             => 'Custom SVG',
                               'conditional_logic' => [[['field'    => "{$key}_icon_select",
                                                         'operator' => '==',
                                                         'value'    => '']]]];

        foreach ($atts as $key => $value)
            $return[$key] = $value;

        return $return;


    }

    static function get_value($field, $array_key) {

        if (!$field)
            return false;


        if (array_key_exists($array_key, $field))
            return $field[$array_key];

        return false;


    }

    public static function build_global_text_style_wysiwyg_format_menu() {

        $lookup = ['heading_one'    => 'h1',
                   'heading_two'    => 'h2',
                   'heading_three'  => 'h3',
                   'heading_four'   => 'h4',
                   'heading_five'   => 'h5',
                   'heading_six'    => 'h6',
                   'unordered_list' => 'ul',
                   'ordered_list'   => 'ol',
                   'paragraph'      => 'p',
                   'link'           => 'a',
                   'button'         => 'a.button',];

        $styles = [];


        foreach ($lookup as $label => $element):

            $classes = ACF_Helper::get_repeater("is_global_type_{$label}_classes");


            if(!$classes) continue;

            foreach ($classes as $class)
                $styles[$element]['classes'][]['title'] = $class['title'];

        endforeach;

        $items = [];

        $c = 0;


        foreach ($styles as $elem => $style_group):


            if (!is_array($style_group['classes']))
                continue;

            $sub_styles = [];

            foreach ($style_group['classes'] as $class)
                $sub_styles[] = ['title'    => $class['title'],
                                 'selector' => $elem,
                                 'classes'  => $class['title']];


            $items[$c] = ['title'    => $elem,
                          'selector' => $elem,
                          'items'    => $sub_styles];
            $c++;
        endforeach;

        return $items;

    }
}

