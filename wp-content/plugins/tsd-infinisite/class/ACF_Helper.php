<?

namespace TSD_Infinisite;

use Carbon\Carbon;

class ACF_Helper implements \ArrayAccess {

    /**
     * the purpose of this class is to serve as an api for the ACF functions
     * as they need to display in the templates
     *
     */

    public $fields = [];
    private $ID;
    private $post;
    public $permalink = '';

    public function __construct($post) {

        if (gettype($post) !== 'WP_Post')
            $post = \get_post($post);

        if (!$post)
            return;

        $this->fields = DB::post_meta($post->ID);

        $this->ID = $post->ID;
        $this->post = $post;
        $this->permalink = $this->post->permalink;


        if (!$this->fields && Acme::can_user_see_debug() && false):
            print 'no fields found';
        endif;

    }

    /**
     * these functions allow us to take an array value
     * so we can use the array notation to get our fields
     * $field->get("content") === $field['content']
     */

    public function offsetSet($offset, $value) {
    }

    public function offsetExists($offset) {
    }

    public function offsetUnset($offset) {
    }

    public function offsetGet($offset) {
        return $this->get($offset);
    }

    public function get_ID() {
        return $this->ID;
    }

    public function get($field, $fallback = false) {
        $init = ACF::get_value($this->fields, $field);

        if ($init)
            return $init;

        if (!$fallback)
            return false;

        if (is_string($fallback)):

            $fallback = ACF::get_value($this->fields, $fallback);

            if ($fallback)
                return $fallback;

            return false;

        endif;

        foreach ($fallback as $key):

            $item = ACF::get_value($this->fields, $key);

            if ($item)
                return $item;

        endforeach;

        return false;

    }

    public function permalink() {
        return \get_permalink($this->ID);
    }

    public function get_permalink() {
        return \get_permalink($this->ID);
    }

    public function published_date() {
        return new Carbon($this->post->post_date);
    }


    public function output($field = false, $elem = 'p', $atts = []) {

        if (!$field)
            return '';

        $value = $this->get($field);

        if (!$value)
            return '';

        if (!is_array($atts))
            $atts = ['class' => $atts];

        $defaults = ['class' => '',
                     'style' => ''];

        $config = \wp_parse_args($atts, $defaults);

        $html = "<{$elem} class='{$config['class']}' style='{$config['style']}'>" . $value . "</{$elem}>";

        return $html;


    }

    public function format_date($format = "F jS Y", $meta_key = 'start_date') {

        $time = new \DateTime($this->get($meta_key));

        if (!$time)
            return;

        return $time->format($format);

    }

    public function get_image($size = 'large', $atts = []) {


        $defaults = ['acf_field' => 'image'];

        $config = \wp_parse_args($atts, $defaults);

        $acf_img = $this->get($config['acf_field']);

        if ($acf_img['type'] !== 'image')
            return false;

        $html = "<img src='{$acf_img['sizes'][$size]}' />";

        return $html;


    }

    public function get_default_image($size = 'large', $atts = []) {

        $_pt = $this->post->post_type;

        $_field = \get_field("is_cpt_{$_pt}_default_image", "options");

        if ($size === false)
            return $_field;

        return $_field['sizes'][$size];

    }

    public function get_start_date($acf_field = 'start_date') {

        // simple get function for the start date as a carbon object

        $start_date = $this->get($acf_field);
        if (!$start_date)
            return false;
        return new Carbon($start_date);
    }

    public function get_end_date($acf_field = 'end_date') {

        // simple get function for the end date as a carbon object

        $end_date = $this->get($acf_field);
        if (!$end_date)
            return false;
        return new Carbon($end_date);
    }

    public function print_date_range($elem = 'p', $atts = []) {

        $defaults = [

        ];

        $config = \wp_parse_args($atts, $defaults);

        $html = "<{$elem}>" . $this->formatted_date_range() . "</{$elem}>";

        return $html;

    }

    public function end_date_in_past() {

        $end_date = $this->get_end_date();

        if (!$end_date)
            return false;

        $now = new \DateTime();

        return $end_date->format("c") <= $now->format("c");

    }

    public function formatted_date_range() {

        if (!$this->get_start_date())
            return false;

        $start = $this->get_start_date();
        $end = $this->get_end_date();

        $start_month = $start->format('F');
        $start_day = $start->format('j');
        $start_year = $start->format('Y');

        $same_month = false;
        $same_day = false;

        if ($end):
            $end_month = $end->format('F');
            $end_day = $end->format('j');
            $end_year = $end->format("Y");
            $same_month = $start_month == $end_month;
            $same_day = $start->format("mdy") == $end->format("mdy");
        endif;


        // if we don't have an end date defined, it should
        // be treated as a one day event
        if ($same_day || !$this->get_end_date())
            return "$start_month $start_day, $start_year";

        if (!$same_month)
            return "$start_month $start_day&ndash;$end_month $end_day, $end_year";

        return "$start_month $start_day&ndash;$end_day, $end_year";


    }

    public function output_link($options = [], $acf_field_title = 'link') {

        $acf_field = $this->get($acf_field_title);

        if (!$acf_field)
            return false;

        $defaults = ['title'         => $acf_field['title'],
                     'text'          => $acf_field['title'],
                     'url'           => $acf_field['url'],
                     'target'        => $acf_field['target'],
                     'icon'          => false,
                     'icon_position' => 'right',
                     'class'         => 'no-margin',];

        $config = \wp_parse_args($options, $defaults);

        $icon_html = "<i class='{$config['icon']}'></i>";

        $left_icon = $config['icon_position'] == 'left' ? $icon_html : '';
        $right_icon = $config['icon_position'] == 'right' ? $icon_html : '';


        $html = "<a 
                    href='{$config['url']}' 
                    target='{$config['target']}' 
                    title='{$config['title']}' 
                    class='acf_link {$config['class']}'>
                        {$left_icon}
                        {$config['text']}
                        {$right_icon}
                </a>";

        return $html;

    }

    public function get_link_html($field_name = 'link', $atts = []) {

        if (!$this->get($field_name))
            return ['', ''];

        $defaults = ['class' => ''];

        $config = \wp_parse_args($atts, $defaults);

        $acf_link = $this->get($field_name);

        $link_opener = "<a href='{$acf_link['url']}' title='{$acf_link['title']}' target='{$acf_link['target']}' class='{$config['class']}'>";
        $link_closer = "</a>";

        return [$link_opener, $link_closer];
    }

    static function get_link_html_from_acf_module($acf_module) {


        if ($acf_module == '')
            return ['', ''];
        if ($acf_module == false)
            return ['', ''];

        $link_opener = "<a href='{$acf_module['url']}' title='{$acf_module['title']}' target='{$acf_module['target']}'>";
        $link_closer = "</a>";

        return [$link_opener, $link_closer];

    }

    public function get_basic_map($atts = []) {

        /**
         * super simple function here, really just getting a basic google map
         * and adding the default marker to the given location
         */

        $defaults = ['acf_field_name' => 'location'];

        $config = wp_parse_args($atts, $defaults);

        $map = new Google_Map($atts);

        $acf_location = $this->get($config['acf_field_name']);

        if ($acf_location)
            $map->add_marker($acf_location);

        return $map->get_html();

    }


    public function find_self_in_relationship_fields($post_type = 'post') {

        if (!is_array($post_type))
            $post_type = [$post_type];


        $id_char_count = strlen($this->ID);
        $query_relationship_string = "s:{$id_char_count}:\"{$this->ID}\"";

        $query = new \WP_Query(['post_type'      => $post_type,
                                'posts_per_page' => 3,
                                'meta_query'     => [['key'     => 'author_post',
                                                      'value'   => $query_relationship_string,
                                                      'compare' => 'LIKE']]]);

        return $query->found_posts ? $query->posts : false;

    }

    public function email_link($acf_field = 'email_address', $atts = []) {

        if ($this->get($acf_field) == '')
            return '';

        $defaults = ['url'    => $this->get($acf_field),
                     'title'  => '',
                     'target' => '',
                     'text'   => $this->get($acf_field)];

        $config = \wp_parse_args($atts, $defaults);

        return "<a href='{$config['url']} class='{$config['class']}' target='{$config['target']}'>{$config['text']}</a>";


    }

    public static function get_repeater($acf_name = '', $post_id = 'option') {

        if ($acf_name == '')
            return null;

        if ($post_id == 'option'):
            $rows = get_option('options_' . $acf_name);
        else:
            $rows = get_post_meta($acf_name, $post_id, 1);
        endif;

        if (!$rows)
            return false;

        global $wpdb;

        $wp_ = $wpdb->prefix;
        $return = [];

        for ($i = 0; $i < $rows; $i++):

            $prefix = $post_id == 'option' ? 'options_' : '';
            $table = $post_id == 'option' ? 'options' : 'postmeta';

            // the unserscore is to make sure that 1 doesn't return 10, 11, 12 etc...
            $query = "{$prefix}{$acf_name}_{$i}\_%";
            // the minus is to make up for the underscore
            $query_strlen = strlen($query) - 2;

            $repeater_query = $wpdb->get_results("SELECT * FROM {$wp_}{$table} WHERE option_name like '{$query}'", OBJECT);

            foreach ($repeater_query as $j => $field):

                $name = substr($field->option_name, $query_strlen);
                $value = $field->option_value;

                if(is_serialized($value))
                    $value = unserialize($value);

                $return[$i][$name] = $value;

            endforeach;

        endfor;


        return $return;


    }

}
