<?php

namespace TSD_Infinisite;

class ACF_Module implements \ArrayAccess {


    private $config;
    private $module = [];

    private $output_html = '';
    private $module_template = '';


    public function __construct($atts = []) {

        $this->config($atts);
        $this->build_output();


    }

    /**
     * these functions allow us to take an array value
     * so we can use the array notation to get our fields
     * $field->get("content") === $field['content']
     */

    public function offsetSet($offset, $value) {
    }

    public function offsetExists($offset) {
    }

    public function offsetUnset($offset) {
    }

    public function offsetGet($offset) {
        return $this->get($offset);
    }


    public function get_module() {
        return $this->module;
    }

    private function config($atts) {

        $_init_config = ['module' => false];

        $this->config = \wp_parse_args($atts, $_init_config);

        $this->module = $this->config['module'];

        if (!$this->config['module_template']):
            $this->config['module_template'] = INFINISITE_URI . "module_templates/{$this->module['acf_fc_layout']}.php";
        endif;


    }

    private function check_for_default_template() {

        if ($this->config['module_template'] === $_SERVER['DOCUMENT_ROOT'])
            $this->config['module_template'] = INFINISITE_URI . "module_templates/{$this->module['acf_fc_layout']}.php";


    }

    private function build_output() {


        if (!$this->config['module_template']):
            if (Acme::can_user_see_debug())
                $this->output_html = "template file not found";
            return;
        endif;

        $this->check_for_default_template();


        if (substr($this->config['module_template'], -4, 4) == 'twig')
            $this->build_output_from_twig_template();

        if (substr($this->config['module_template'], -3, 3) == 'php')
            $this->build_output_from_php_template();


    }

    private function build_output_from_php_template() {

        // in order to make our bucket shortcode-based overrides work,
        // we needed to prevent all fields of type wysiwyg from having
        // their content filtered as they were passed from get_fields()
        // in the layout object. so - when we render the template - we

        // apr 7 - turned this off to give us the ability to leave
        // shortcodes uncalculated another way. maybe we can use that in

        // $is_wysiwyg = $this->config['module']['acf_fc_layout'] == 'simple_content';
        $is_wysiwyg = false;

        $filename = $this->config['module_template'];

        // this is used in templates to detect page options
        if (array_key_exists('layout', $this->config))
            $layout = $this->config['layout'];

        if (file_exists($filename)):

            ob_start();
            // we're making an alias here so that the module files can
            // use the variable name module - which makes more sense for them
            $module =& $this;

            include($this->config['module_template']);
            $op = ob_get_clean();

        else:
            $show_error = Acme::can_user_see_error_message();
            $op = $show_error ? "<h2>Page Builder Module Template File Not Found</h2><p>$filename</p>" : '';
        endif;

        $this->output_html = $is_wysiwyg ? apply_filters("the_content", $op) : $op;


    }

    private function build_output_from_twig_template() {


        // if it's a twig file, we gotta set up the
        // variables.

        $loader = new \Twig_Loader_Filesystem('/');
        $twig = new \Twig_Environment($loader);
        $twig->addExtension(new \Twig_Extension_Debug());
        $template = $this->config['module_template'];


        // the module shortcut is for ease of use, and backwards capability

        $template_atts = ['module' => $this->module,
                          'this'   => $this];


        try {
            $this->output_html .= $twig->render($template, $template_atts);
        } catch (\Exception $e) {
            if (!Acme::can_user_see_debug()):
                // silenty fail for the front end users
                $this->output_html .= '';
                return false;
            endif;
            $this->output_html .= "Error at Layout::get_single_view_content() display_custom_template $e";
        }


    }

    public function get($field = false) {

        if (!$field)
            return '';

        if (!array_key_exists($field, $this->module))
            return '';

        return $this->module[$field];

    }


    public function get_html() {
        if (!$this->module)
            return 'no module found';
        return $this->output_html;
    }


}