<?php

namespace TSD_Infinisite;

class API_V0 {

    protected $api_base = 'is_api/v1';

    public function __construct() {
        add_action( 'rest_api_init', function() {
            self::add_endpoints();
        } );

    }

    public function set_api_base( $endpoint ) {
        $this->api_base = $endpoint;
    }

    public function add_endpoints() {


        $this->get_route( 'post-content/(?P<id>\d+)', 'get_post_content_by_id' );
        $this->get_route( 'post/(?P<id>\d+)', 'get_post_by_id' );
        $this->get_route( 'template', 'get_post_templates' );
        $this->post_route( 'template_query', 'get_post_templates_by_query' );
        $this->post_route( 'tax_template_query', 'get_taxonomy_templates_by_query' );
        $this->post_route( 'add_location_to_post', 'add_location_to_post' );

        $this->post_route( '(?P<post_type>[a-zA-Z0-9-]+)/new', 'add_new_post' );

        $this->delete_route( 'post', 'delete_post' );

        $this->post_route( 'post', 'update_post' );

        $this->post_route( 'query', 'get_is_query' );

        $this->post_route( 'instagram_token_renewal', 'instagram_token_renewal' );

        $this->get_route( 'instagram_code_add', 'instagram_code_add' );

        $this->get_route( 'post_types', 'get_post_types' );

        $this->get_route( '(?P<post_type>[a-zA-Z0-9-]+)/posts', 'get_post_type_posts' );

        $this->get_route( '(?P<post_type>[a-zA-Z0-9-]+)/fields', 'get_post_type_fields' );

        $this->get_route( '(?P<post_type>[a-zA-Z0-9-]+)/edit', 'edit_post_type' );

        $this->get_route( 'post_type_info/(?P<post_type>[a-zA-Z0-9-]+)', 'get_post_type_info' );

        $this->get_route( 'page_builder_info', 'get_pb_info' );
        $this->get_route( 'social_media_auth', 'social_media_auth' );

        $this->get_route( 'images', 'get_wp_attachments' );
        $this->get_route( 'delete_transient', 'delete_transient' );


    }

    public static function add_location_to_post( $request ) {

        $data = $request->get_params();

        $id = $data['post_id'];
        unset( $data['post_id'] );

        $loc_data = serialize( $data );

        $map = \update_post_meta( $id, 'location', $loc_data );
        $acf = \update_post_meta( $id, '_location', 'field_practitioner_location' );

        return [ $map, $acf ];

    }

    // endpoint wrapper functions

    // public wrapper - set limitations here
    public static function theme_add_get_route( $endpoint, $callback ) {

        $api = new API_V0();

        return register_rest_route( $api->api_base, $endpoint, [ 'methods'  => 'GET',
                                                                 'permission_callback' => '__return_true',
                                                                 'callback' => $callback, ] );

    }

    // public wrapper - set limitations here
    public static function theme_add_post_route( $endpoint, $callback ) {

        $api = new API_V0();

        return register_rest_route( $api->api_base, $endpoint, [ 'methods'  => 'POST',
                                                                 'permission_callback' => '__return_true',
                                                                 'callback' => $callback, ] );

    }

    protected function get_route( $endpoint, $callback = false ) {
        if ( ! $callback )
            $callback = $endpoint;
        // this is just a wrapper function
        $this->register_api_endpoint( $endpoint, $callback );
    }

    public function set_post_route( $endpoint, $callback ) {
        $this->register_api_endpoint( $endpoint, $callback );
    }

    protected function post_route( $endpoint, $callback = false ) {
        if ( ! $callback )
            $callback = $endpoint;
        $this->register_api_endpoint( $endpoint, $callback, 'POST' );
    }

    private function delete_route( $endpoint, $callback ) {
        $this->register_api_endpoint( $endpoint, $callback, 'DELETE' );
    }


    // controller functions

    private function register_api_endpoint( $endpoint = '', $callback, $method = 'GET' ) {
        if ( $endpoint == '' )
            return;

        register_rest_route( $this->api_base, $endpoint, [ 'methods'             => $method,
                                                           'permission_callback' => '__return_true',
                                                           'callback'            => [ $this, $callback ], ] );
    }

    public function instagram_code_add( $request ) {

        $data = $request->get_params();

        print_r( $data );

        $code = sanitize_text_field( $data['code'] );

        $html = "
        
        <form action='#'>
        </form>
        
        ";


        return $code;

    }

    public function social_media_auth( $request ) {

        //print_r( $_GET );

        $state = json_decode( stripslashes( $_GET['state'] ), true );

        $network  = $state['network'];
        $username = $state['username'];

        if ( 'facebook' == $network ) {

            // Authorization Code
            $code = $_GET['code'];

            // TODO: get client id and client secret for current network
            $client_id     = get_option( "options_{$network}_{$username}_client_id" );
            $client_secret = get_option( "options_{$network}_{$username}_client_secret" );

            // Get access token -----------
            $graph_url = "https://graph.facebook.com/v4.0/oauth/access_token";
            $postData  = http_build_query( [ 'client_id'     => urlencode( $client_id ),
                                             'redirect_uri'  => site_url() . "/wp-json/is_api/v1/social_media_auth",
                                             'client_secret' => $client_secret,
                                             'code'          => $code, ] );

            $ch = curl_init();

            curl_setopt( $ch, CURLOPT_URL, $graph_url );
            curl_setopt( $ch, CURLOPT_HEADER, 0 );
            curl_setopt( $ch, CURLOPT_POST, 1 );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $postData );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );

            $output = json_decode( curl_exec( $ch ), true );

            $accessToken = $output['access_token'];
            $tokenType   = $output['token_type'];
            $expiration  = $output['expires_in'];    // in seconds - 60 days - (https://developers.facebook.com/docs/facebook-login/access-tokens/refreshing/)

            // -----------

            // TODO: update ACF field here...
            if ( $code )
                update_option( "options_{$network}_{$username}_auth_code", $code );

            if ( $accessToken )
                update_option( "options_{$network}_{$username}_access_token", $accessToken );

        }
        elseif ( 'twitter' == $network ) {
            // TODO: do twitter stuff...
        }

        wp_redirect( admin_url( 'admin.php?page=acf-options-social-media' ) );    // redirect back to options page once fields are updated

        die();

    }

    public function get_post_content_by_id( $data ) {

        $post = new \TSD_Infinisite\IS_Post( $data['id'] );

        return apply_filters( "the_content", $post );
    }

    public function get_post_by_id( $data ) {

        $post = new \TSD_Infinisite\IS_Post( $data['id'] );

        if ( $post->wp_obj->post_status !== 'publish' )
            return false;

        return $post;

    }

    public function get_post_templates( $data ) {

        $ids = '';
        if ( isset( $_GET['posts'] ) )
            $ids = explode( ",", $_GET['posts'] );

        $tax   = isset( $_GET['taxonomy'] ) ? $_GET['taxonomy'] : false;
        $terms = '';

        if ( isset( $_GET['terms'] ) )
            $terms = explode( ",", $_GET['terms'] );

        $template = $_GET['template'];

        $atts = [];

        if ( isset( $ids ) && count( $ids ) ):
            $atts['post_type'] = 'any';
            $atts['post__in']  = $ids;
        endif;


        $query = new IS_Query( $atts );

        $html = [];


        foreach ( $query->posts as $post )
            $html[] = $post->render_php_template( $template );


        return $html;


    }

    public function get_post_templates_by_query( $data ) {

        $atts = $_POST;

        $template = $atts['is_template'];
        unset( $atts['is_template'] );

        $query = new IS_Query( $atts );

        $html = [];
        foreach ( $query->posts as $post )
            $html[] = $post->render_php_template( $template );

        return $html;

    }

    public function get_taxonomy_templates_by_query( $data ) {

        $atts = $_POST;

        $template = $atts['is_template'];
        unset( $atts['is_template'] );

        $query = new IS_Term_Query( $atts );


        $op = [];

        foreach ( $query->terms as $term ):
            $op[] = Acme::get_file( $template, [ 'term' => $term ] );
        endforeach;

        return $op;

    }

    public function instagram_token_renewal( WP_REST_Request $request ) {

        $data = $request->get_params();

        print_r( $data['key'] );

        return true;


    }

    public function add_new_post( $data ) {

        // forwarding function

        return $this->update_post( $data );

        //        if (!is_object($data))
        //            return false;
        //
        //
        //        $new_id = \wp_insert_post(['post_type'   => $data['post_type'],
        //                                   'post_title'  => $data['post_title'],
        //                                   'post_status' => 'publish']);
        //
        //        if (count($data['meta'])):
        //            foreach ($data['meta'] as $key => $value)
        //                \update_post_meta($new_id, $key, $value);
        //        endif;
        //
        //        return new \TSD_Infinisite\IS_Post($new_id);

    }

    public function delete_post( WP_REST_Request $request ) {
        $data = $request->get_params();

        return \wp_delete_post( $data['id'] );
    }

    public function get_is_query( $data ) {

        //    print_r($data);

        $query = new \TSD_Infinisite\IS_Query( $data['query'] );

        if ( ! $query->posts )
            return false;

        return rest_ensure_response( $query->posts );

    }

    public function update_post( WP_REST_Request $request ) {

        $data     = $request->get_params();
        $new_data = $data['is_post'];
        $new_id   = $new_data['wp_post_obj']['ID'];
        $is_post  = new \TSD_Infinisite\IS_Post( $new_id );

        return $is_post->save( $new_data );


    }

    public function get_post_types() {
        return \TSD_Infinisite\Acme::get_post_types();
    }

    public function get_post_type_info( WP_REST_Request $request ) {
        $info = \TSD_Infinisite\CPT::get_post_type_info_for_api( $request['post_type'] );

        return $info;

    }


    public function get_post_type_posts( WP_REST_Request $request ) {

        $pt = $request->get_params()['post_type'];


        $query = new \TSD_Infinisite\IS_Query( [ 'post_type'      => $pt,
                                                 'posts_per_page' => - 1 ] );

        return $query->posts;


    }

    public function get_pb_info( WP_REST_Request $request ) {

        $module_information = \TSD_Infinisite\Page_Builder::get_module_use_info_for_debug();

        return $module_information;


    }

    public function get_wp_attachments( WP_REST_Request $request ) {


        $query = new \WP_Query( [ 'post_type'   => 'attachment',
                                  'post_status' => 'any' ] );

        return $query->posts;

    }


    public function get_post_type_fields( WP_REST_Request $request ) {

        // we're just going to return the acf fields directly here.

        $custom_fields = get_field( "is_cpt_{$request['post_type']}_custom_meta_fields", "options" );

        return $custom_fields;

    }

    public function edit_post_type( WP_REST_Request $request ) {

        $return                   = [];
        $return['post_type_meta'] = get_field( "is_cpt_{$request['post_type']}_custom_meta_fields", "options" );

        $attachment_query      = new \WP_Query( [ 'post_type'      => 'attachment',
                                                  'posts_per_page' => - 1,
                                                  'post_status'    => 'any' ] );
        $return['attachments'] = $attachment_query->posts;

        $post_type_query = new \TSD_Infinisite\IS_Query( [ 'post_type'      => $request['post_type'],
                                                           'posts_per_page' => - 1 ] );
        $return['posts'] = $post_type_query->posts;


        return $return;

    }

    public function delete_transient( $data ) {

        $name = $data['name'];

        global $wpdb;

        return $wpdb->query( "
          DELETE FROM $wpdb->options
          WHERE option_name = '_transient_{$name}'
        " );

    }

}
