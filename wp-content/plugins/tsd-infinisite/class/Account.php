<?


namespace TSD_Infinisite;

use CalendR\Exception;

class Account {

    public $name;
    public $config = [];
    public $api_key = false;
    public $network = '';


    function __construct($name) {

        $network = Social::get_account_info_by_username($name);


        var_dump($network);

        $this->config($network);
    }


    private function config($network) {

        $this->name = $network['username'];
        $this->config['url'] = $network['url'];
        $this->config['network'] = $network['network'];

        $fn_name = "setup_{$network['network']}";
        $this->network = $this->config['network'];

        self::$fn_name();


    }

    public function get_link() {
        return $this->config['url'];
    }


    private function setup_instagram() {

        $auth_token_value = get_field("instagram_{$this->name}_access_token", "options");

        $this->api_key = $auth_token_value;

    }


    public function get_posts($count = 3, $posts_per_page = -1) {


        // this is basically a public-facing alias for internal functions

        $fn_name = "get_{$this->network}_posts";

        return self::$fn_name($count, $posts_per_page);


    }

    private function renew_instagram_api_token() {


        print debug_print_backtrace();
        die();


        $fields = ['client_id'     => '24caa629127c47d19b991ff80cc6faea',
                   'client_secret' => 'cb780dd0ed604c04a6dba26080e809f1',
                   'grant_type'    => 'authorization_code',
                   'redirect_uri'  => 'http://dandelionpatch.staging.wpengine.com',
                   'code'          => 'YOUR-CODE'];

        $url = 'https://api.instagram.com/oauth/access_token';


        $token_request = self::curl($url, $fields);

        var_dump($content);

    }

    private function get_instagram_posts($count, $posts_per_page) {

        $vars = ['access_token' => $this->api_key,
                 'count'        => $count];
        $query = http_build_query($vars);
        $url_base = 'https://api.instagram.com/v1/users/self/media/recent?';

        $data = self::curl($url_base . $query);

        if ($data['meta']['code'] == 400):
            $this->renew_instagram_api_token();
            return false;
        endif;

        return $data['data'];


    }

    public static function curl($url = '', $data = false) {

        if ($data)
            if (count($data))
                return self::curl_post($url, $data);

        if ($url === '')
            return 'no url provided';

        // this is just a wrapper function for a curl request

        try {
            $curl_connection = curl_init($url);
            curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);

            //Data are stored in $data
            $data = json_decode(curl_exec($curl_connection), true);
            curl_close($curl_connection);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return $data;


    }


    private static function curl_post($url, $data) {

        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $result = curl_exec($ch);
            curl_close($ch);
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return json_decode($result);

    }


}