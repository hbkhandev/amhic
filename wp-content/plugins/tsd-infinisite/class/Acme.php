<?

namespace TSD_Infinisite;

class Acme {

    /*
     * for backwards compatability
     */

    static function header() {
        Header::get_default_header_file();
    }

    static function footer() {
        Footer::get_default_footer_file();
    }


    /*
     * for real, gosh darn honest helper functions
     */


    static function get_templates( $directory, $user_config = [] ) {


        /**
         * GET TEMPLATES
         *
         * This is the new centralized location for all template loading.
         *
         * $directory: string / array - directory to load templates from.
         *
         * This function will look in the directory of both the plugin and
         * the theme. If files are found, they will be returned in a key/value
         * format.
         *
         * [ theme path => filename ]
         *
         *
         * TODO: update the filename to something more human friendly.
         *
         * TODO: refactor module templates to load from here
         * TODO: refactor header / footer device templates to load from here
         * TODO: refactor social media templates to load from here
         * TODO: ID other loading places to refactor from here
         *
         * TODO: why is this running so much on the dashboard?
         *
         */


        $return = [];

        if ( is_string( $directory ) ) {
            $directory = [ $directory, $directory, $directory ];
        }

        foreach ( $directory as $filepath ) {
            if ( substr( $filepath, 0, 1 ) == '/' ) {
                $filepath = substr( $filepath, 1 );
            }
        }

        $defaults = [ 'twig'                 => true,
                      'load_from_user_theme' => true,
                      'load_from_plugin'     => true, ];

        $config = wp_parse_args( $user_config, $defaults );

        $use_slash = substr( $directory[0], - 1, 1 ) == '/';
        $url_slash = $use_slash ? '' : '/';


        $plugin_php_templates_filepath = INFINISITE_URI . $directory[0] . $url_slash . "*";
        $plugin_php_templates          = glob( $plugin_php_templates_filepath );

        $theme_php_templates_filepath = THEME_URI . $directory[1] . $url_slash . "*";
        $theme_php_templates          = glob( $theme_php_templates_filepath );

        $parent_php_templates_filepath = PARENT_URI . $directory[2] . $url_slash . "*";
        $parent_php_templates          = glob( $parent_php_templates_filepath );

        $templates = array_merge( $plugin_php_templates, $theme_php_templates, $parent_php_templates );


        // attaching the templates to the submenu template module selector

        if ( ! $templates ) {
            return $return;
        }

        $plugin_subdirs = array_filter( glob( $plugin_php_templates_filepath ), 'is_dir' );
        $theme_subdirs  = array_filter( glob( $theme_php_templates_filepath ), 'is_dir' );
        $parent_subdirs = array_filter( glob( $parent_php_templates_filepath ), 'is_dir' );

        $subdir_filepaths = array_merge( $plugin_subdirs, $theme_subdirs, $parent_subdirs );

        foreach ( $subdir_filepaths as $subdir_filepath ):

            $val   = explode( '/', $subdir_filepath );
            $label = array_pop( $val );

            $files        = glob( $subdir_filepath . '/*' );
            $subdir_files = [];

            foreach ( $files as $file ):

                $pos           = strpos( $file, '/wp-content' );
                $template_path = substr( $file, $pos );

                $val                            = explode( '/', $file );
                $file_label                     = array_pop( $val );
                $subdir_files[ $template_path ] = Acme::underscore( $file_label );
            endforeach;

            $return[ $label ] = $subdir_files;

        endforeach;


        foreach ( $templates as $template ):

            $pos           = strpos( $template, '/wp-content' );
            $template_path = substr( $template, $pos );

            $is_php_file  = substr( $template, - 4 ) === '.php';
            $is_twig_file = substr( $template, - 5 ) === '.twig';
            $is_js_file   = substr( $template, - 3 ) === '.js';

            $file_test = $is_php_file || $is_twig_file || $is_js_file;

            if ( ! $file_test )
                continue;

            $info                     = explode( "/", $template );
            $filename                 = array_pop( $info );
            $return[ $template_path ] = Acme::underscore( $filename );

        endforeach;

        return $return;

    }

    static function acf_init_get_options_repeater_hack( $name = false, $fields = false ) {

        $return = [];
        if ( ! $name || ! $fields ) {
            return $return;
        }

        $repeater_count = get_option( "options_" . $name );

        for ( $i = 0; $i < $repeater_count; $i ++ ):

            foreach ( $fields as $field ) {
                $return[ $i ][ $field ] = get_option( "options_{$name}_{$i}_{$field}" );
            }

        endfor;

        return $return;

    }

    static function check_array_for_post( $post, $array ) {

        /**
         * this is a helper function for Page_Builder::get_module_use_info_for_debug()
         */

        if ( count( $array ) == 0 ) {
            return false;
        }
        foreach ( $array as $_p ) {
            if ( $_p->ID == $post->ID ) {
                return true;
            }
        }

        return false;

    }

    static function get_gravity_forms() {

        $return = [];

        if ( ! class_exists( 'GFAPI' ) ) {
            return $return;
        }

        $all_forms = \GFAPI::get_forms();

        foreach ( $all_forms as $form ) {
            $return[ $form['id'] ] = $form['title'];
        }

        return $return;

    }

    static function standard_page() {
        if ( \is_search() ) {
            return false;
        }

        return true;
    }

    static function build_cartsian_set( $array ) {

        if ( ! $array ) {
            return [ [] ];
        }
        $subset          = array_shift( $array );
        $cartesianSubset = self::build_cartsian_set( $array );
        $result          = [];
        foreach ( $subset as $value ) {
            foreach ( $cartesianSubset as $p ) {
                array_unshift( $p, $value );
                $result[] = $p;
            }
        }

        return $result;

    }

    static function explodeTree( $array, $delimiter = '_', $baseval = false ) {
        // taken from http://kvz.io/blog/2007/10/03/convert-anything-to-tree-structures-in-php/
        if ( ! is_array( $array ) ) {
            return false;
        }
        $splitRE   = '/' . preg_quote( $delimiter, '/' ) . '/';
        $returnArr = [];
        foreach ( $array as $key => $val ) {
            // Get parent parts and the current leaf
            $parts    = preg_split( $splitRE, $key, - 1, PREG_SPLIT_NO_EMPTY );
            $leafPart = array_pop( $parts );

            // Build parent structure
            // Might be slow for really deep and large structures
            $parentArr = &$returnArr;
            foreach ( $parts as $part ) {
                if ( ! isset( $parentArr[ $part ] ) ) {
                    $parentArr[ $part ] = [];
                }
                elseif ( ! is_array( $parentArr[ $part ] ) ) {
                    if ( $baseval ) {
                        $parentArr[ $part ] = [ '__base_val' => $parentArr[ $part ] ];
                    }
                    else {
                        $parentArr[ $part ] = [];
                    }
                }
                $parentArr = &$parentArr[ $part ];
            }

            // Add the final part to the structure
            if ( empty( $parentArr[ $leafPart ] ) ) {
                $parentArr[ $leafPart ] = $val;
            }
            elseif ( $baseval && is_array( $parentArr[ $leafPart ] ) ) {
                $parentArr[ $leafPart ]['__base_val'] = $val;
            }
        }

        return $returnArr;
    }

    static function array_depth( $array ) {
        $max_indentation = 1;

        $array_str = print_r( $array, true );
        $lines     = explode( "\n", $array_str );

        foreach ( $lines as $line ) {
            $indentation = ( strlen( $line ) - strlen( ltrim( $line ) ) ) / 4;

            if ( $indentation > $max_indentation ) {
                $max_indentation = $indentation;
            }
        }

        return ceil( ( $max_indentation - 1 ) / 2 ) + 1;
    }

    static function get_alpha( $num ) {

        $_a = range( 'a', 'z' );

        return $_a[ $num ];

    }

    static function file_exists( $filepath ) {
        $file = false;

        if ( file_exists( $filepath ) )
            $file = true;

        if ( file_exists( $_SERVER['DOCUMENT_ROOT'] . '/' . $filepath ) )
            $file = true;

        if ( file_exists( THEME_URI . $filepath ) )
            $file = true;

        if ( file_exists( INFINISITE_URI . $filepath ) )
            $file = true;

        if ( file_exists( THEME_URI . $filepath ) )
            $file = true;

        if ( file_exists( INFINISITE_URI . $filepath ) )
            $file = true;

        return $file;
    }

    static function get_file( $filepath, $vars = [] ) {

        $file = false;

        if ( file_exists( $filepath ) )
            $file = $filepath;

        else if ( file_exists( $_SERVER['DOCUMENT_ROOT'] . '/' . $filepath ) )
            $file = $_SERVER['DOCUMENT_ROOT'] . '/' . $filepath;

        else if ( file_exists( THEME_URI . $filepath ) )
            $file = THEME_URI . $filepath;

        else if ( file_exists( INFINISITE_URI . $filepath ) )
            $file = INFINISITE_URI . $filepath;

        else if ( file_exists( THEME_URI . $filepath ) )
            $file = THEME_URI . '/' . $filepath;

        else if ( file_exists( INFINISITE_URI . $filepath ) )
            $file = INFINISITE_URI . '/' . $filepath;

        if ( ! is_string( $file ) )
            return $file;


        if(!$file) return null;

        ob_start();

        foreach ( $vars as $key => $value )
            $$key = $value;

        if ( self::file_exists( $file ) )
            include( $file );
        else
            print "File $file not found somehow (Acme::get_file)";


        return ob_get_clean();


    }

    static function get_row_info_by_id( $id = 0 ) {

        if ( $id === 0 )
            return false;

        global $wpdb;

        $prefix  = "rows_%_row_";
        $pre_len = strlen( $prefix );

        $keys = [ 'class', 'id', 'sticky', 'title', 'x_padding', 'y_padding' ];

        $sql_string = '';

        foreach ( $keys as $c => $key ):
            $or         = $c == count( $keys ) - 1 ? '' : 'OR ';
            $sql_string .= "meta_key LIKE '{$prefix}{$key}' AND post_id = {$id} {$or}";
        endforeach;

        $sql_query = "
            SELECT *
            FROM {$wpdb->prefix}postmeta
            WHERE {$sql_string}
        ";

        $results = $wpdb->get_results( $sql_query );

        if ( ! count( $results ) )
            return null;

        $return = [];

        foreach ( $results as $c => $row ):
            $cat              = substr( $row->meta_key, $pre_len );
            $return[ $cat ][] = $row->meta_value;
        endforeach;

        return $return;


    }

    static function format_acf_tables( $table = [] ) {

        if ( $table == [] )
            return false;

        $return = [];

        if ( count( $table['header'] ) )
            foreach ( $table['header'] as $content )
                $return['header'][] = $content['c'];

        if ( count( $table['body'] ) )
            foreach ( $table['body'] as $row_index => $row )
                foreach ( $row as $cell_index => $cell )
                    if ( count( $table['header'] ) )
                        $return[ $row_index ][ $table['header'][ $cell_index ]['c'] ] = $cell['c'];
                    else
                        $return[ $row_index ][ $cell_index ] = $cell['c'];

        return $return;
    }

    static function get_yt_poster_images( $iframe_url ) {

        $id_count = 11;
        $pos      = strpos( $iframe_url, 'embed/' );
        $str      = substr( $iframe_url, $pos + 6, $id_count );
        $return   = [];
        for ( $i = 0; $i <= 3; $i ++ )
            $return[] = "https://img.youtube.com/vi/{$str}/{$i}.jpg";

        return $return;


    }

    static function element( $tag = 'p', $value = '', $atts = [] ) {

        if ( ! $value )
            return '';

        if ( ! is_array( $atts ) )
            $atts = [ 'class' => $atts ];

        $defaults = [ 'class' => '',
                      'style' => '' ];

        $config = \wp_parse_args( $atts, $defaults );

        $html = "<{$tag} class='{$config['class']}' style='{$config['style']}'>" . $value . "</{$tag}>";

        return $html;


    }

    public static function get_post_type_count( $tax, $term ) {
        $query = "
            select  
            p.post_type, count(p.post_type) count
            from wp_terms term
            join wp_term_taxonomy tax on term.term_id = tax.term_id
            join wp_term_relationships r on tax.term_taxonomy_id = r.term_taxonomy_id
            join wp_posts p on ID = object_id
            where taxonomy= '$tax'
            and slug = '$term'
            group by p.post_type
            order by post_date desc
            ";
        global $wpdb;

        return $wpdb->get_results( $query );
    }

    /*
     * auth checks
     */


    static function is_admin() {

        return is_user_logged_in();

    }

    static function can_user_see_debug() {

        // TODO: this is going to need some serious jazzing up.

        return is_user_logged_in();


    }

    static function does_post_type_have_single_view_enabled( $post = \WP_Post::class ) {

        $post_type     = \get_post_type( $post );
        $post_type_obj = \get_post_type_object( $post_type );


        if ( $post_type_obj->publicly_queryable === 0 ) {
            return false;
        }

        return true;

    }

    static function can_user_see_error_message() {


        // TODO: this is going to need some serious jazzing up.

        return is_user_logged_in();


    }

    static function nearest_in_array( $search, $arr ) {

        $closest = null;
        foreach ( $arr as $item ) {
            if ( $closest === null || abs( $search - $closest ) > abs( $item - $search ) ) {
                $closest = $item;
            }
        }

        return $closest;
    }


    /*
     * is base styles
     */


    static function update_is_stylesheets() {

        // we don't use this yet - this going to be for the template builder
        // Template::update_is_custom_template_styles();

        // foundation pulls its colors from our palette - so we need to set
        // the values for our palette

        // this builds the default foundation cfg and saves it to the db
        Foundation::init_foundation_config();

        // this builds the foundation stylesheet
        // Foundation::update_scss();

        // this breaks because we don't have the foundation vars
        // Acme::update_IS_utility_classes();
    }

    static function stylesheet_update_check() {

        $update = false;

        if ( $_GET['update_foundation_stylesheets'] ) {
            $update = true;
        }


        if ( $update ) {
            Acme::update_is_stylesheets();
        }

    }

    static function update_IS_utility_classes() {

        return;

        $palette = new Palette();

        $palette->update_utility_classes();

    }

    static function update_IS_app_scss() {

        /**
         * we're updating the site's app.css file here - we
         * need to do this when we update the plugin's color settings
         */

        try {
            $scss = new \Leafo\ScssPhp\Compiler();
            $scss->addImportPath( INFINISITE_URI . 'assets/scss/' );
            $scss->addImportPath( INFINISITE_URI . 'bower_components/foundation-sites/scss/foundation/' );

            $app_scss = file_get_contents( INFINISITE_URI . 'assets/scss/app.scss' );

            $compiled_app_scss = $scss->compile( $app_scss );

            // and here we update the stylesheet.
            $file = INFINISITE_URI . 'assets/css/app.css';

            return file_put_contents( $file, $compiled_app_scss );
        } catch ( exception $e ) {
            print "Failed: $e";
        }


    }

    /*
     * child theme extenders
     */

    static function add_wysiwyg_editor_format( $data ) {


        // TODO: write this function.


    }


    /*
     * html generators
     */


    static function browser_title() {

        if ( \is_front_page() ) {
            return \get_bloginfo( 'name' );
        }

        return \wp_title( '' );

    }

    static function display_templates_per_device_size( $device_templates, $user_config ) {

        /*
         * takes array of key / value pairs
         * key = device size
         * value = template filepath
         *
         * returns html with show/hide classes
         */

        $return = '';


        $defaults = [];

        $config = wp_parse_args( $user_config, $defaults );


        $is_size_breakpoint_conversion_lookup = [ 'mobile'        => [ 'xsmall', 'small' ],
                                                  'tablet'        => [ 'medium' ],
                                                  'desktop'       => [ 'large' ],
                                                  'large_desktop' => [ 'xlarge', 'xxlarge' ] ];

        $breakpoints = [ 'xsmall', 'small', 'medium', 'large', 'xlarge', 'xxlarge' ];

        $processed_templates = [];

        if ( $device_templates ) {
            foreach ( $device_templates as $device => $template ) {
                $processed_templates[ $template ][] = $device;
            }
        }


        if ( $processed_templates ):

            $return = '';
            foreach ( $processed_templates as $template => $sizes ):

                $hide_class    = '';
                $show_for_size = [];

                foreach ( $sizes as $size ):
                    $show_sizes = $is_size_breakpoint_conversion_lookup[ $size ];

                    foreach ( $show_sizes as $show_size ) {
                        $show_for_size[] = $show_size;
                    }

                endforeach;

                foreach ( $breakpoints as $breakpoint ):

                    if ( in_array( $breakpoint, $show_for_size ) ) {
                        continue;
                    }

                    $hide_class .= "hide-for-{$breakpoint}-only ";

                endforeach;

                $container = array_key_exists( 'container_class', $config ) ? $config['container_class'] : '';

                $return .= "<div class='$hide_class $container' style='position: relative;'>";
                ob_start();

                if ( array_key_exists( 'vars', $config ) ) {
                    if ( $config['vars'] ) {
                        foreach ( $config['vars'] as $name => $value ) {
                            $$name = $value;
                        }
                    }
                }

                // i'm doing this to take a few directories off our template. this feels like an awful idea.
                $explode = explode( '/', $template );
                array_shift( $explode );
                array_shift( $explode );
                array_shift( $explode );
                array_shift( $explode );
                $template = implode( "/", $explode );


                $vars = [ 'img',
                          'primary_menu',
                          'secondary_menu',
                          'secondary_menu_branch_toggle',
                          'cta_menu',
                          'cta_menu_template',
                          'primary_content',
                          'secondary_content',
                          'overlay_menu',
                          'overlay_menu_template',
                          'overlay_menu_launcher_icon',
                          'offcanvas_menu',
                          'offcanvas_menu_launcher_icon',
                          'header_search_template',
                          'header_search_launcher_icon',
                          'large_menu',
                          'large_menu_template',
                          'large_menu_branch_toggle',
                          'large_menu_launcher_icon',
                          'top_linkgs',
                          'top_links_template',
                          'sticky',
                          'header',
                          'social_media', ];

                $vars_for_template = [];

                foreach ( $vars as $var ):
                    if ( isset( $$var ) )
                        $vars_for_template[ $var ] = $$var;
                endforeach;

                print Acme::get_file( $template, $vars_for_template );

                $return .= ob_get_clean();
                $return .= "</div>";

            endforeach;

        endif;

        return $return;

    }

    static function dbg( $content = false, $dump_type = false ) {

        if ( ! $content ) {
            return false;
        }

        switch ( $dump_type ):

            case 1:
                print "<pre>";
                var_dump( $content );
                print "</pre>";
                break;

            case 2:
                print "<pre>";
                var_export( $content );
                print "</pre>";
                break;

            case 3:
                // this is for the wp backend, the margin moves the output out from behind the sidebar
                print "<pre style='margin-left: 186px;'>";
                print_r( $content );
                print "</pre>";
                break;

            default:

                print "<pre>";
                print_r( $content );
                print "</pre>";

                break;

        endswitch;


    }

    static function dd( $content = false ) {

        if ( ! $content ) {
            return false;
        }
        Acme::dbg( $content );
        die();
    }

    static function media_gallery( $filepath = '', $gallery = [] ) {

        /**
         * takes an array of IS_Media posts and a template filepath and returns
         * formatted html
         *
         * the only thing that really makes this function worth existing is that
         * we're going to use this structure to display all grouped media files, be
         * they photos, videos, audio, ... pdfs?
         */


        if ( ! count( $gallery ) )
            return apply_filter( "the_content", "Empty Gallery" );

        if ( ! file_exists( $filepath ) )
            return apply_filter( "the_content", "Template file not found." );

        ob_start();
        include( $filepath );

        return ob_get_clean();


    }


    /*
     * page builder
     */


    static function paginate( $query = false ) {

        $vars_for_query_string = $_GET;

        $page = 1;

        if ( $vars_for_query_string ):
            if ( array_key_exists( 'page', $vars_for_query_string ) ):
                $page = $vars_for_query_string['page'];
                unset( $vars_for_query_string['page'] );
            endif;
        endif;

        $query_string = http_build_query( $vars_for_query_string );


        $pagination_html = paginate_links( [ 'base'    => "?page=%_%&{$query_string}",
                                             'format'  => "%#%",
                                             'type'    => 'list',
                                             'current' => max( 1, $page ),
                                             'total'   => $query->max_num_pages ] );

        return $pagination_html;

    }

    static function get_pagination( $query = false, $archive_object = false ) {

        // this has been rerouted to the IS_Query class
        return IS_Query::get_pagination( $query, $archive_object );

    }

    static function get_swp_pagination( $query = false ) {

        // this has been rerouted to the IS_Query class
        return IS_Query::get_swp_pagination( $query );


    }


    static function get_searchwp_engines_for_page_builder_filter_object() {

        // this has been rerouted to the IS_Query class
        return Page_Builder::get_searchwp_engines_for_page_builder_filter_object();

    }


    /*
     * Lookups
     */


    static function device_size_to_visibility_class( $size = false ) {

        if ( ! $size ) {
            return false;
        }

        switch ( $size ):
            case 'mobile':
                $visibility = 'hide-for-medium';
                break;

            case 'tablet':
                $visibility = 'show-for-medium hide-for-xlarge';
                break;

            case 'desktop':
                $visibility = 'show-for-xlarge hide-for-xxlarge';
                break;

            case 'large_desktop':
                $visibility = 'show-for-xxlarge';
                break;

        endswitch;

        return $visibility;


    }

    static function device_size_to_breakpoint_class( $size = false ) {

        if ( ! $size ) {
            return false;
        }

        $lookup = [ 'mobile'        => 'small',
                    'tablet'        => 'medium',
                    'desktop'       => 'large',
                    'large_desktop' => 'xlarge', ];

        return $lookup[ $size ];


    }


    /*
     * text wranglers
     */

    static function generate_columns( $array = false, $block_grid = false ) {

        if ( ! $array ) {
            return false;
        }

        $op = '';

        $block_grid_notation = $block_grid ? 'up-' : '';

        foreach ( $array as $size => $count ) {
            $op .= "$size-{$block_grid_notation}$count ";
        }

        return $op;

    }


    static function underscore( $str, array $noStrip = [] ) {
        // non-alpha and non-numeric characters become spaces
        $str = preg_replace( '/[^a-z0-9' . implode( "", $noStrip ) . ']+/i', ' ', $str );
        $str = trim( $str );
        $str = str_replace( " ", "_", $str );
        $str = strtolower( $str );

        return $str;
    }

    static function slugify( $string ) {
        return str_replace( ' ', '-', strtolower( $string ) );
    }

    static function strip_field_for_name( $key ) {
        return substr( $key, 6 );
    }

    static function process_template_names_for_acf_select_fields( $templates = [], $atts = [] ) {

        /*
         *
         * ugly labels put in place for debug class select mneus
         *
         * returns filepath => select label
         */

        $return = [];

        if ( ! count( $templates ) ) {
            return $return;
        }

        $defaults = [ 'ugly_labels'       => false,
                      'reverse_key_value' => true ];

        $config = \wp_parse_args( $atts, $defaults );


        foreach ( $templates as $choice ):
            $info = explode( "/", $choice );

            $filename_unformatted = array_pop( $info );
            $filename_with_spaces = str_replace( '_', ' ', $filename_unformatted );

            $tmp_array_var             = explode( '.', $filename_with_spaces );
            $filename_without_filetype = array_shift( $tmp_array_var );

            $_ugly_label = array_shift( explode( '.', $filename_unformatted ) );

            $_r = $config['ugly_labels'] ? $_ugly_label : ucwords( $filename_without_filetype );

            if ( $config['reverse_key_value'] ):
                $return[ $_r ] = $choice;
            else:
                $return[ $choice ] = $_r;
            endif;
        endforeach;

        return $return;

    }

    static function process_ispb_layout_markup( $content, $classes = false ) {

        $op = '';

        if ( ! $content ) {
            return $op;
        }
        if ( ! count( $content ) ) {
            return $op;
        }

        foreach ( $content as $key => $class ):

            // if there is no background color defined, we're still getting
            // a blank value - so we need to listen for that and not pass this
            // in because it overrides other styles, and they might not be
            // aware that this default value is being applied to the cell... like
            // the time i was that caused me to add this.

            if ( $key == 'background-color' && $class == 'rgb(0,0,0,0)' ) {
                continue;
            }


            if ( $classes ):
                $op .= "$class ";
                continue;
            endif;

            $op .= "$key: $class; ";

        endforeach;

        return $op;


    }


    static function is_serialized( $value, &$result = null ) {
        // Bit of a give away this one
        if ( ! is_string( $value ) ) {
            return false;
        }
        // Serialized false, return true. unserialize() returns false on an
        // invalid string or it could return false if the string is serialized
        // false, eliminate that possibility.
        if ( $value === 'b:0;' ) {
            $result = false;

            return true;
        }
        $length = strlen( $value );
        $end    = '';
        // this was giving a warning that fires when we have wp_debug on
        // if we have an empty value - so we check for that
        if ( empty( $value[0] ) ):
            $result = false;

            return true;
        endif;
        switch ( $value[0] ) {
            case 's':
                if ( $value[ $length - 2 ] !== '"' ) {
                    return false;
                }
            case 'b':
            case 'i':
            case 'd':
                // This looks odd but it is quicker than isset()ing
                $end .= ';';
            case 'a':
            case 'O':
                $end .= '}';
                if ( $value[1] !== ':' ) {
                    return false;
                }
                switch ( $value[2] ) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                        break;
                    default:
                        return false;
                }
            case 'N':
                $end .= ';';
                if ( $value[ $length - 1 ] !== $end[0] ) {
                    return false;
                }
                break;
            default:
                return false;
        }
        if ( ( $result = @unserialize( $value ) ) === false ) {
            $result = null;

            return false;
        }

        return true;
    }

    static function table_to_tsv_format( $data = [] ) {

        /**
         * this function works with the custom acf field type table
         * to convert the data from row based array to column based csv text string
         */

        $return = '';

        if ( ! is_array( $data ) ) {
            return $return;
        }

        if ( $data['header'] ):
            foreach ( $data['header'] as $c => $header_cell ):
                $return .= $header_cell['c'];
                $return .= $c == count( $data['header'] ) - 1 ? "\n" : "\t";
            endforeach;

        endif;


        if ( array_key_exists( 'body', $data ) ):
            foreach ( $data['body'] as $row ):
                foreach ( $row as $c => $cell ):
                    $return .= $cell['c'];
                    $return .= $c == count( $row ) - 1 ? "\n" : "\t";
                endforeach;
            endforeach;

        endif;

        return $return;

    }

    static function get_text_link_from_acf( $field = false, $options = [] ) {

        if ( ! $field ) {
            return false;
        }

        $defaults = [ 'title'         => $field['title'],
                      'text'          => $field['title'],
                      'url'           => $field['url'],
                      'target'        => $field['target'],
                      'icon'          => 'far fa-arrow-circle-right',
                      'icon_position' => 'right',
                      'class'         => 'no-margin', ];

        $config = \wp_parse_args( $options, $defaults );

        $icon_html = "<i class='{$config['icon']}'></i>";

        $left_icon  = $config['icon_position'] == 'left' ? $icon_html : '';
        $right_icon = $config['icon_position'] == 'right' ? $icon_html : '';

        $html = "<a 
                    href='{$config['url']}' 
                    target='{$config['target']}' 
                    title='{$config['title']}' 
                    class='acf_link {$config['class']}'>
                        {$left_icon}
                        {$config['text']}
                        {$right_icon}
                </a>";

        return $html;

    }

    static function get_link_html_from_acf_module( $acf_module ) {


        if ( $acf_module == '' ) {
            return [ '', '' ];
        }
        if ( $acf_module == false ) {
            return [ '', '' ];
        }

        $link_opener = "<a href='{$acf_module['url']}' title='{$acf_module['title']}' target='{$acf_module['target']}'>";
        $link_closer = "</a>";

        return [ $link_opener, $link_closer ];

    }

    static function get_link_html_from_post_id( $id ) {

        $post = \get_post( $id );

        $permalink = \get_permalink( $id );

        $link_opener = "<a href='{$permalink}' title='{$post->post_title}'>";
        $link_closer = "</a>";

        return [ $link_opener, $link_closer ];

    }

    static function get_formatted_event_date_string( $acf_fields = false ) {

        if ( ! $acf_fields ) {
            return;
        }

        $start_date = ACF::get_value( $acf_fields, 'start_date' );
        $end_date   = ACF::get_value( $acf_fields, 'end_date' );

        if ( ! $start_date ) {
            return;
        }

        $start = new \DateTime( $start_date );
        $end   = new \DateTime( $end_date );

        $start_month = $start->format( 'F' );
        $start_day   = $start->format( 'j' );

        $end_month = $end->format( 'F' );
        $end_day   = $end->format( 'j' );

        $same_day = $start->format( "mdy" ) == $end->format( "mdy" );

        $same_month = $start_month == $end_month;

        $end_year = $end->format( "Y" );

        // if we don't have an end date defined, it should
        // be treated as a one day event
        if ( $same_day || ! $acf_fields['end_date'] ) {
            return "$start_month $start_day, $end_year";
        }

        if ( ! $same_month ) {
            return "$start_month $start_day&ndash;$end_month $end_day, $end_year";
        }

        return "$start_month $start_day&ndash;$end_day, $end_year";


    }


    /*
     * query functions
     */


    static function get_post_types( $suppress_defaults = false ) {

        /*
         * This function just serves to bundle information for other function calls
         */

        $transient = DB::function_cache( 'Acme::get_post_types' );

        if ( ! empty( $transient ) )
            return $transient;

        $acf_post_types = get_field( 'is_global_post_types', 'options' );


        if ( ! $acf_post_types ) {
            return DB::function_cache( 'Acme::get_post_types', false );
        }

        $post_types = [];

        foreach ( $acf_post_types as $post_type ):

            $pt = $post_type['post_type'];

            $post_type['config'] = [ 'single_view_template' => get_option( "options_is_cpt_{$pt}_template" ),
                                     'hierarchical'         => get_option( "options_is_cpt_{$pt}_hierarchical" ),
                                     'page_builder'         => get_option( "options_is_cpt_{$pt}_page_builder" ),
                                     'exclude_from_search'  => get_option( "options_is_cpt_{$pt}_exclude_from_search" ) ];

            $post_types[] = $post_type;

        endforeach;


        if ( ! is_array( $post_types ) ) {
            return DB::function_cache( 'Acme::get_post_types', false );
        }

        $defaults = [ [ 'name'      => 'Page',
                        'post_type' => 'page',
                        'config'    => [ 'single_view'         => 1,
                                         'hierarchical'        => 1,
                                         'page_builder'        => 1,
                                         'exclude_from_search' => 0, ] ],
                      [ 'name'      => 'Post',
                        'post_type' => 'post',
                        'config'    => [ 'single_view'         => 1,
                                         'hierarchical'        => 1,
                                         'page_builder'        => 1,
                                         'exclude_from_search' => 0, ] ], ];


        $wc = new IS_WooCommerce();

        if ( $wc->is_active )
            $defaults[] = $wc->process_post_type();


        if ( ! $post_types ) {
            return DB::function_cache( 'Acme::get_post_types', $defaults );
        }

        if ( ! $suppress_defaults ) {
            foreach ( $defaults as $default ) {
                $post_types[] = $default;
            }
        }

        return DB::function_cache( 'Acme::get_post_types', $post_types );
    }

    static function get_post_type_options( $user_post_type = false ) {
        if ( ! $user_post_type ) {
            return false;
        }

        $post_types = self::get_post_types();

        if ( ! is_array( $post_types ) ) {
            return;
        }

        $post_type = false;

        foreach ( $post_types as $pt ):
            if ( $pt['post_type'] != $user_post_type ) {
                continue;
            }
            $post_type = $pt;
        endforeach;

        $post_type = Option::get_is_post_type_config_options( $post_type['post_type'] );

        return $post_type;

    }

    static function get_all_posts_by_type( $post_type = false ) {
        if ( ! $post_type ) {
            return false;
        }

        $query = new \WP_Query( [ 'post_type'      => $post_type,
                                  'posts_per_page' => - 1 ] );

        return $query->posts;

    }

    static function get_all_searchable_post_types_excluding( $post_types_to_exclude = false ) {

        if ( ! $post_types_to_exclude ) {
            return false;
        }

        if ( ! is_array( $post_types_to_exclude ) ) {
            $post_types_to_exclude = [ $post_types_to_exclude ];
        }

        $post_types = \get_post_types( [ 'exclude_from_search' => 0 ] );

        foreach ( $post_types_to_exclude as $type ) {
            unset( $post_types[ $type ] );
        }

        return $post_types;

    }

    static function get_is_post_types( $suppress_defaults = true ) {

        /*
         * outputs an array of the post types defined in the is post type builer
         */

        $return = [];

        $post_types = self::get_post_types( $suppress_defaults );

        if ( ! $post_types )
            return $return;

        foreach ( $post_types as $pt )
            $return[ $pt['post_type'] ] = $pt['name'];

        // if we have defined post and page manually, we don't want them to be duplicated
        return array_unique( $return );

    }

    static function get_taxonomy_info( $post = false ) {

        if ( ! $post ) {
            return false;
        }

        // this function builds out all the taxonomy info for a post

        $taxonomies = get_object_taxonomies( $post->post_type, 'objects' );


        $return = [];

        foreach ( $taxonomies as $taxonomy ):

            $terms = \wp_get_post_terms( $post->ID, $taxonomy->name );

            $return[] = [ 'taxonomy' => $taxonomy,
                          'terms'    => $terms ];

        endforeach;

        return $return;

    }

    static function get_categories_for_page_builder_filter_object() {

        $return = [];

        $post_types = Acme::get_is_post_types( 0 );

        $taxonomies = Option::get_acf_global_option( 'options_custom_taxonomies' );

        $wc = new IS_WooCommerce();

        if ( $wc->is_active ):

            $return = $wc->add_taxonomies_for_page_builder_filter_object();

        endif;

        if ( ! $taxonomies )
            return;

        foreach ( $post_types as $post_type ):

            $this_term_applies_to_this_post_type = false;
            $taxonomy_for_filter                 = false;

            if ( $taxonomies ) {
                foreach ( $taxonomies as $taxonomy ) {
                    if ( in_array( strtolower( $post_type ), $taxonomy['post_types'] ) ):
                        $this_term_applies_to_this_post_type = true;
                        $taxonomy_for_filter[]               = $taxonomy;
                    endif;
                }
            }

            if ( ! $this_term_applies_to_this_post_type ) {
                continue;
            }

            $return_taxonomies = $taxonomy_for_filter;

            foreach ( $return_taxonomies as $taxonomy ) {
                $return[ $post_type ][ $taxonomy['term'] ] = $taxonomy['name'];
            }


        endforeach;

        return $return;

    }

    static function get_most_recent_post( $post_type = false ) {

        if ( ! $post_type )
            return null;

        $query = new \WP_Query( [ 'post_type'      => $post_type,
                                  'posts_per_page' => 1, ] );

        if ( ! $query->found_posts )
            return false;


        return new IS_Post( $query->posts[0] );

    }






    /*
     * image functions
     */


    /**
     * Get size information for all currently-registered image sizes.
     *
     * @return array $sizes Data for all currently-registered image sizes.
     * @uses   get_intermediate_image_sizes()
     * @global $_wp_additional_image_sizes
     */
    static function get_image_sizes() {
        global $_wp_additional_image_sizes;

        $sizes = [];

        foreach ( \get_intermediate_image_sizes() as $_size ) {
            if ( in_array( $_size, [ 'thumbnail', 'medium', 'medium_large', 'large' ] ) ) {
                $sizes[ $_size ]['width']  = get_option( "{$_size}_size_w" );
                $sizes[ $_size ]['height'] = get_option( "{$_size}_size_h" );
                $sizes[ $_size ]['crop']   = (bool) get_option( "{$_size}_crop" );
            }
            elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
                $sizes[ $_size ] = [ 'width'  => $_wp_additional_image_sizes[ $_size ]['width'],
                                     'height' => $_wp_additional_image_sizes[ $_size ]['height'],
                                     'crop'   => $_wp_additional_image_sizes[ $_size ]['crop'], ];
            }
        }

        return $sizes;
    }

    static function get_image_sizes_for_acf() {

        $return = [];

        foreach ( Acme::get_image_sizes() as $label => $atts )
            $return[ $label ] = $label;

        return $return;

    }

    /**
     * Get size information for a specific image size.
     *
     * @param string $size The image size for which to retrieve data.
     *
     * @return bool|array $size Size data about an image size or false if the size doesn't exist.
     * @uses   get_image_sizes()
     *
     */
    static function get_image_size( $size ) {
        $sizes = self::get_image_sizes();

        if ( isset( $sizes[ $size ] ) ) {
            return $sizes[ $size ];
        }

        return false;
    }

    /**
     * Get the width of a specific image size.
     *
     * @param string $size The image size for which to retrieve data.
     *
     * @return bool|string $size Width of an image size or false if the size doesn't exist.
     * @uses   get_image_size()
     *
     */
    static function get_image_width( $size ) {
        if ( ! $size = self::get_image_size( $size ) ) {
            return false;
        }

        if ( isset( $size['width'] ) ) {
            return $size['width'];
        }

        return false;
    }

    /**
     * Get the height of a specific image size.
     *
     * @param string $size The image size for which to retrieve data.
     *
     * @return bool|string $size Height of an image size or false if the size doesn't exist.
     * @uses   get_image_size()
     *
     */
    static function get_image_height( $size ) {
        if ( ! $size = self::get_image_size( $size ) ) {
            return false;
        }

        if ( isset( $size['height'] ) ) {
            return $size['height'];
        }

        return false;
    }

    static function fudge_image_sizes_for_testing() {
        $image_sizes = self::get_image_sizes();

        $return = [];

        foreach ( $image_sizes as $label => $size ):

            $w       = $size['width'];
            $h       = $size['height'];
            $img_url = "http://placehold.it/{$w}x{$h}";

            $return["{$label}"]        = $img_url;
            $return["{$label}-width"]  = $w;
            $return["{$label}-height"] = $h;
        endforeach;

        return $return;

    }

    static function get_image_info_by_id( $image_id = false ) {

        /**
         * simple wrapper function to emulate ACF's image
         * array structure for when we need to insert an image
         * object to override a template file's image programatically
         */

        if ( ! $image_id ) {
            return false;
        }
        $pre_meta = get_post_meta( $image_id );
        $meta     = unserialize( $pre_meta['_wp_attachment_metadata'][0] );

        $img_date = explode( '/', $meta['file'] );
        $img_url  = "/wp-content/uploads/{$img_date[0]}/{$img_date[1]}/";

        $return = [];

        $all_sizes = Acme::get_image_sizes();

        $return['url'] = "/wp-content/uploads/{$meta['file']}";


        foreach ( $all_sizes as $c => $size ):

            if ( ! $meta['sizes'] ) {
                continue;
            }

            if ( array_key_exists( $c, $meta['sizes'] ) ):
                // if we have the thumbnail generated, we grab the info
                // stick the url prepend on the image source and put
                // it in the return value
                $return['sizes'][ $c ]             = $img_url . $meta['sizes'][ $c ]['file'];
                $return['sizes'][ $c . '-width' ]  = $meta['sizes'][ $c ]['width'];
                $return['sizes'][ $c . '-height' ] = $meta['sizes'][ $c ]['height'];
            else:
                // if we don't have the thumbnail generated, we get the full size
                // version - note that this requires a different url prepend.
                $return['sizes'][ $c ]             = '/wp-content/uploads/' . $meta['file'];
                $return['sizes'][ $c . '-width' ]  = $meta['width'];
                $return['sizes'][ $c . '-height' ] = $meta['height'];

            endif;
        endforeach;

        return $return;

    }


    /*
     * acf optimization functions
     */


    static function admin_screen_show_config_tab( $part = 'page', $atts = [] ) {

        $defaults = [ 'page' => true,
                      'row'  => true,
                      'cell' => true, ];

        $config = \wp_parse_args( $atts, $defaults );

        return $config[ $part ];

    }

    static function admin_screen_show_scss_tab( $part = 'page', $atts = [] ) {

        $defaults = [ 'page' => true,
                      'row'  => true,
                      'cell' => true, ];

        $config = \wp_parse_args( $atts, $defaults );

        return $config[ $part ];

    }


    /*
     * wpb helper functions
     */

    static function wpb_accordion( $content, $atts = [] ) {
        // takes array with array of 'title' and 'content'

        $defaults = [];

        $config = \wp_parse_args( $atts, $defaults );


        $sc = "[vc_tta_accordion 
        c_align='center' c_icon='chevron' c_position='right'
        collapsible_all='true' el_id='elementID' el_class='elementClass']";

        $id = uniqid( 'wpb_' );

        foreach ( $content as $c => $tab ):
            $sc .= "
                [vc_tta_section 
                    title='{$tab['title']}' tab_id='wpb_{$id}_{$c}'
                    el_class='sectionClass'
                ]
                [vc_column_text el_class='divide-y']
                    {$tab['content']}
                [/vc_column_text]
                [/vc_tta_section]
            ";


        endforeach;
        $sc .= "[/vc_tta_accordion]";

        return \do_shortcode( $sc );

    }



}
