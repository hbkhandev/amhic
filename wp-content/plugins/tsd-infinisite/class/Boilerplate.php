<?

namespace TSD_Infinisite;

class Boilerplate {

    public static function define_constants() {


        define('INFINISITE_ASSETS', INFINISITE_DIR . 'assets');
        define('INFINISITE_JS', INFINISITE_ASSETS . '/js');
        define('INFINISITE_CSS', INFINISITE_ASSETS . '/css');
        define('INFINISITE_FONT', INFINISITE_ASSETS . '/font-awesome-5');

    }

    public static function enqueue_assets() {

        if (file_exists(INFINISITE_URI . 'assets/dist/app.css'))
            wp_enqueue_style('app', INFINISITE_ASSETS . '/dist/app.css');

        if (file_exists(THEME_URI . 'assets/dist/tailwind.css'))
            wp_enqueue_style('tailwind', THEME_DIR . 'assets/dist/tailwind.css');

        if (file_exists(THEME_URI . 'assets/css/app.css'))
            wp_enqueue_style('child_theme_app', THEME_DIR . 'assets/css/app.css');

        if (file_exists(THEME_URI . 'assets/dist/app.css'))
            wp_enqueue_style('child_theme_app_webpack', THEME_DIR . 'assets/dist/app.css');

        /******************
         * SCRIPTS / JS
         ******************/

        if (file_exists(THEME_URI . 'assets/dist/app.js'))
            wp_enqueue_script('is_child_webpack_js', THEME_DIR . 'assets/dist/app.js', [], false, 1);

    }

    public static function admin_assets() {

        if (file_exists(THEME_URI . 'assets/dist/tailwind.admin.css'))
            wp_enqueue_style('adminTailwind', THEME_DIR . 'assets/dist/tailwind.admin.css');

        if (file_exists(THEME_URI . 'assets/dist/tailwind.tinymce.css'))
            add_editor_style(THEME_DIR . 'assets/dist/tailwind.tinymce.css');

    }

}
