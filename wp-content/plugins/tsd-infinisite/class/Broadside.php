<?php

namespace TSD_Infinisite;

class Broadside
{


    private $html = '';
    private $tabs = [];
    private $tab_html = '';
    private $content_html = '';

    private static $filepath = '';

    function __construct()
    {

        Broadside::$filepath = INFINISITE_URI . 'assets/broadside_includes/';


        $this->build_typography();
        $this->build_colors();
        $this->build_buttons();
        $this->build_tab_html();
        $this->build_content_html();
        $this->build_output();
        $this->output();

    }

    private function build_typography()
    {

        ob_start();
        include(Broadside::$filepath . 'typography.php');
        $typography_content = ob_get_clean();

        $this->tabs['typography'] = [
            'title' => 'Typography',
            'content' => $typography_content,
            'edit' => 1
        ];

    }

    private function build_colors()
    {




        ob_start();
        include(Broadside::$filepath . 'color_palette.php');
        $color_html = ob_get_clean();


        ob_start();
        $args = array(
            'post_id' => 'options',
            'field_groups' => array('group_is_global_colors_group') // this is the ID of the field group
        );
        acf_form($args);
        $edit_form = ob_get_clean();


        $this->tabs['colors'] = [
            'title' => 'Color Palette',
            'content' => $color_html,
        ];

    }

    private function build_buttons()
    {

        ob_start();
        include(Broadside::$filepath . 'buttons.php');
        $button_html = ob_get_clean();


        $this->tabs['buttons'] = [
            'title' => 'Buttons',
            'content' => $button_html,
        ];

    }

    private function build_tab_html()
    {

        $c = 0;
        foreach ($this->tabs as $tab):
            $active = $c == 0 ? 'is-active' : '';
            $this->tab_html .= "
                <li class='tabs-title $active'>
                    <a href='#broadside_{$c}'>
                        {$tab['title']}
                    </a>
                </li>";
            $c++;
        endforeach;

        $this->tab_html = "
            <ul class='vertical tabs' data-tabs id='is-broadside-tabs'>
                $this->tab_html
            </ul>
        ";

    }

    private function build_content_html()
    {


        $c = 0;
        foreach ($this->tabs as $tab):
            $active = $c == 0 ? 'is-active' : '';

            $edit_html = "
                <a href='#' class='button small no-margin'>Edit</a>
            ";

            $edit_button = array_key_exists('edit', $tab) ? $edit_html : '&nbsp;';


            $this->content_html .= "
                <div class='tabs-panel $active' id='broadside_{$c}'>
                <div class='grid-x align-middle'>
                    <div class='cell auto'>
                        <h2 class='no-margin'>{$tab['title']}</h2>
                    </div>
                    <div class='cell shrink'>
                        $edit_button
                    </div>
                    <div class='cell'>
                        <hr />
                    </div>
                </div>
                    {$tab['content']}
                </div>
            ";
            $c++;
        endforeach;


        $this->content_html = "
            <div class='tabs-content' data-tabs-content='is-broadside-tabs' style='border-top: 1px solid #e6e6e6;'>
                $this->content_html
            </div>
        ";

    }


    private function build_output()
    {

        $this->html = "
        <div class='grid-container' data-editor-style>
            <div class='grid-x'>
                <div class='medium-3 cell'>
                    $this->tab_html
                </div>
                <div class='medium-9 cell'>
                    $this->content_html
                </div>
                <div class='cell'>
                    <div class='spacer large'></div>
                </div>
            </div>
        </div>
        ";

    }

    private function output()
    {

        print $this->html;
    }


}