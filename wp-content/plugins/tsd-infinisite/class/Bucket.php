<?php

namespace TSD_Infinisite;

class Bucket {

    private $html = '';

    private $post_id;
    private $fields = [];


    public function __construct($module) {


        $this->post_id = $module['bucket_post'];
        $this->fields  = $module['bucket_post_group_fields']["bucket_post_fields_$this->post_id"];


        // this builds the interrupts
        $this->build_shortcodes($module);


        // this executes the posts with the interrupts in place
        $layout     = new Layout($this->post_id);
        $this->html = $layout->get_page_content();
        $this->html .= $layout->get_css();


        // this removes the interrupts
        // clean up the shortocdes and the global variable space
        $this->remove_shortcodes();


    }


    public function get_html() {
        return $this->html;
    }


    private function build_shortcodes() {

        /**
         * builds out the functions used to handle the shortcode text substitutions
         */


        // turns on the hack that disables post formatting

        $GLOBALS['is_post_format_disable'] = true;

        if(!$this->fields) return;

        foreach ($this->fields as $key => $field) {


            // this pulls off the "bucket_shortcode_" string at the front of the
            // meta key saved for the bucket module's particular value
            $shortcode_name = substr($key, 17);


            // when the shortcode is executed, the value in the global var place
            // at the time will be returned, so we need to give it a home that
            // can be referred to later when the function actually runs
            $GLOBALS['text_for_shortcode'][$shortcode_name] = $field;


            $GLOBALS['shortcode_tags'][$shortcode_name] = function($atts = [], $encapsulated_content = '', $shortcode_name = '') {


                // three returns:
                //
                // 1 - if we have explicitly defined text
                // 2 - if we have a default value in the shortcode
                // 3 - blank space

                $default_text = '';

                $overwrite_text = $GLOBALS['text_for_shortcode'][$shortcode_name];

                if ($overwrite_text != '')
                    return $overwrite_text;

                if (is_array($atts)):
                    if (array_key_exists('default_text', $atts))
                        $default_text = $atts['default_text'];

                    return $default_text;
                endif;

                return '';

            };

        }

    }

    private function remove_shortcodes() {

        if(!$this->fields) return;

        foreach ($this->fields as $key => $field) {

            // this pulls off the "bucket_shortcode_" string at the front of the
            // meta key saved for the bucket module's particular value
            $shortcode_name = substr($key, 17);
            \remove_shortcode($shortcode_name);

        }

        // removes the array that we use for our shortcode callback functions
        unset($GLOBALS['text_for_shortcode']);

        // turns off the hack to disable post formatting
        unset($GLOBALS['is_post_format_disable']);


    }


    /*
     * this section is for the static functions that set up the bucket fields
     */

    public static function create_bucket_acf_fields() {


        $shortcode_text = ['key'          => 'field_is_bucket_text_shortcode_name',
                           'name'         => 'shortcode_name',
                           'label'        => 'Name',
                           'type'         => 'text',
                           'instructions' => 'This will be the name of the shortcode used in the page builder.'];

        $shortcode_text_layout = ['key'        => 'field_is_bucket_text_shortcode_layout',
                                  'name'       => 'is_bucket_text_shortcode_layout',
                                  'label'      => 'Text Shortcode',
                                  'sub_fields' => [$shortcode_text]];

        $acf_bucket_layouts = ['text_shortcode' => $shortcode_text_layout];


        acf_add_local_field_group(['key'      => 'group_is_bucket_post_fields',
                                   'title'    => 'Bucket Fields',
                                   'location' => [[['param'    => 'post_type',
                                                    'operator' => '==',
                                                    'value'    => 'bucket']]],
                                   'fields'   => [['key'          => 'field_is_bucket_fields',
                                                   'label'        => 'Fields',
                                                   'name'         => 'is_bucket_flexible_content_layouts',
                                                   'type'         => 'flexible_content',
                                                   'layouts'      => $acf_bucket_layouts,
                                                   'button_label' => 'Add Field']]]);


    }


}