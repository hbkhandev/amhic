<?

namespace TSD_Infinisite;

class CPT {
    /**
     * this guy lives a double life - the static functions are to set up acf pages
     * the oop functions are for the api
     */

    private $acf_options = [];


    public function __construct( string $post_type ) {
        $this->set_options( $post_type );

    }


    private function set_options( $_pt ) {

        // TODO: replace with acf global options page info
        $options = $_pt;

        $this->acf_options = $options;


    }

    static function register_post_type_builder() {

        $post_type_builder_field_group = self::build_post_type_builder_field_group();
        acf_add_local_field_group( $post_type_builder_field_group );

    }

    private static function build_post_type_builder_field_group() {


        $cpt_meta_title = [ 'key'          => 'field_is_global_cpt_meta_title',
                            'label'        => 'Name',
                            'name'         => 'name',
                            'type'         => 'text',
                            'required'     => 1,
                            'instructions' => 'The name displayed in the menu',
                            'wrapper'      => [ 'width' => 100 / 2 ] ];

        $cpt_meta_post_type = [ 'key'          => 'field_is_global_cpt_meta_post_type',
                                'label'        => 'Post Type',
                                'name'         => 'post_type',
                                'instructions' => 'No spaces, 22 character max',
                                'type'         => 'text',
                                'required'     => 1,
                                'wrapper'      => [ 'width' => 100 / 2 ] ];

        $fields = [ [ 'key'          => 'field_is_global_cpt_repeater',
                      'label'        => 'Post Types',
                      'name'         => 'is_global_post_types',
                      'type'         => 'repeater',
                      'layout'       => 'table',
                      'instructions' => "Build Your Post Types Here",
                      'button_label' => 'Add Custom Post Type',
                      'sub_fields'   => [ $cpt_meta_title,
                                          $cpt_meta_post_type, ] ] ];

        $field_group = [ 'key'      => 'field_is_global_cpt_field_group',
                         'title'    => 'Post Type Builder',
                         'location' => [ [ [ 'param'    => 'options_page',
                                             'operator' => '==',
                                             'value'    => 'is-cpt-options', ] ], ],
                         'fields'   => $fields ];

        return $field_group;

    }

    static function register_built_post_types() {


        // we store our cpt prefs in the db through acf - so we need
        // to start out with grabbing that information
        $post_types = Acme::get_post_types();

        if ( ! $post_types )
            return;

        // TODO: this get post types function returns a non-array sometimes.

        if ( ! is_array( $post_types ) )
            return;


        foreach ( $post_types as $post_type ):

            $pt = $post_type['post_type'];

            if ( $pt === 'product' )
                continue;

            if ( in_array( $pt, [ 'post', 'page' ] ) )
                continue;

            $config = [ 'public'          => true,
                        'capability_type' => 'page',
                        'label'           => $post_type['name'],
                        'menu_position'   => 5,
                        'show_in_rest'    => true,
                        'supports'        => [ 'title',
                                               'revisions' ] ];


            $single_view_template          = get_option( "options_is_cpt_{$pt}_template" );
            $post_type_is_heirarchical     = get_option( "options_is_cpt_{$pt}_hierarchical" );
            $post_type_exclude_from_search = get_option( "options_is_cpt_{$pt}_exclude_from_search" );
            $post_type_custom_slug         = get_option( "options_is_cpt_{$pt}_custom_slug" );
            $post_type_page_builder        = get_option( "options_is_cpt_{$pt}_page_builder" );
            $post_type_comment_support     = get_option( "options_is_cpt_{$pt}_comment_support" );
            $post_type_featured_image     = get_option( "options_is_cpt_{$pt}_featured_image_support" );

            $public = 0;
            if ( $single_view_template )
                $public = 1;
            $config['publicly_queryable'] = $public;

            if ( $post_type_page_builder && WPBAKERY_INSTALLED )
                $config['supports'][] = 'editor';


            if ( $post_type_is_heirarchical ):
                $config['hierarchical'] = true;
                $config['supports'][]   = 'page-attributes';
            endif;

            if ( $post_type_exclude_from_search )
                $config['exclude_from_search'] = true;

            if ( $post_type_custom_slug ):
                $config['rewrite']['slug']       = $post_type_custom_slug;
                $config['rewrite']['with_front'] = true;
            endif;

            if ( $post_type_comment_support )
                $config['supports'][] = 'comments';

            if($post_type_featured_image)
                $config['supports'][] = 'thumbnail';

            if($post_type_featured_image)
                $config['supports'][] = 'thumbnail';

            register_post_type( $pt, $config );

        endforeach;


        foreach ( $post_types as $post_type ):


            $pt = $post_type['post_type'];

            $fields = [];

            $pt_config_field_group_acf = acf_get_fields( "is_cpt_{$pt}_settings" );
            $pt_config                 = [];
            if ( $pt_config_field_group_acf )
                foreach ( $pt_config_field_group_acf as $field ):
                    $acf_field = get_option( $field['name'] );
                    if ( ! $acf_field )
                        continue;
                    $pt_config[ $field['name'] ] = $acf_field;
                endforeach;

            $custom_fields = get_field( "is_cpt_{$pt}_custom_meta_fields", "options" );


            if ( $custom_fields ):
                // This is returning a number - we should probably fire it later
                if ( count( $custom_fields ) && is_array( $custom_fields ) ):
                    foreach ( $custom_fields as $field ):

                        $is_preset = $field['preset_type'] != 'custom';

                        $field_name = $is_preset ? $field['preset_type'] : $field['meta_key'];
                        $field_type = $is_preset ? ACF::is_preset_field_lookup( $field['preset_type'] ) : $field['type'];

                        $args = [ "key"      => "field_{$post_type['post_type']}_{$field_name}",
                                  "label"    => $field['label'],
                                  "name"     => $field_name,
                                  "type"     => $field_type,
                                  "required" => $field['required'],
                                  'wrapper'  => [ 'width' => $field['field_width'] ] ];

                        $google_map_coords = apply_filters( "is/cpt/set_default_google_map_coords", [ 38.9072,
                                                                                                      - 77.0369 ] );

                        if(array_key_exists("instructions", $field))
                            $args['instructions'] = $field['instructions'];

                        if ( $field_type == 'google_map' )
                            $args['center_lat'] = $google_map_coords[0];
                        if ( $field_type == 'google_map' )
                            $args['center_lng'] = $google_map_coords[1];

                        if ( $field_type == 'google_map' )
                            $args['zoom'] = 8;


                        if ( $field_type == 'image' )
                            $args['preview_size'] = 'medium';
                        if ( $field_type == 'date_picker' )
                            $args['display_format'] = 'F j, Y';
                        if ( $field_type == 'date_picker' )
                            $args['return_format'] = 'c';

                        $fields[] = $args;


                    endforeach;
                endif;
            endif;

            $fields_for_post_type_field_group = apply_filters( "update_custom_post_type_meta", $fields );


            $post_type_field_group = [ 'key'      => "field_{$post_type['post_type']}_fields",
                                       'title'    => "{$post_type['name']} Custom Meta Fields",
                                       'position' => "acf_after_title",
                                       // 'style' => 'seamless',
                                       'location' => [ [ [ 'param'    => 'post_type',
                                                           'operator' => '==',
                                                           'value'    => $post_type['post_type'], ] ], ],
                                       'fields'   => $fields_for_post_type_field_group ];

            $post_type_field_group = apply_filters( "is/update_cpt_meta_fields/" . $post_type['post_type'], $post_type_field_group );

            if ( count( $fields_for_post_type_field_group ) )
                acf_add_local_field_group( $post_type_field_group );


        endforeach;


    }

    static function build_post_type_option_edit_page() {


        $post_types = Acme::get_post_types();

        $user_enabled_post = false;

        if ( $post_types )
            foreach ( $post_types as $pt )
                if ( $pt['post_type'] == 'post' )
                    $user_enabled_post = true;


        if ( ! $user_enabled_post ):

            /**
             * this fires if the user doesn't define a post type
             * of "post" - unfortunately, we can't just unregister
             * it - we need to manually hide the links to the post type
             * functions
             */

            add_action( 'admin_menu', function() {
                remove_menu_page( 'edit.php' );
            } );

            add_action( 'admin_bar_menu', function( $wp_admin_bar ) {
                $wp_admin_bar->remove_node( 'new-post' );
            }, 999 );

            add_action( 'wp_dashboard_setup', function() {
                remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
            }, 999 );

        endif;

        if ( $post_types )
            if ( count( $post_types ) && is_array( $post_types ) )
                foreach ( $post_types as $post_type ):

                    acf_add_options_sub_page( [ 'page_title'             => "{$post_type['name']} Post Type Settings",
                                                'menu_title'             => ucwords( $post_type['name'] ),
                                                'menu_slug'              => "acf-options-post-type-{$post_type['post_type']}",
                                                'parent_slug'            => 'is-cpt-options',
                                                'wp_admin_page_variable' => "{$post_type['post_type']}_settings",
                                                'capability'             => 'activate_plugins', ] );


                    /**
                     * Logic to control display of custom meta fields
                     */

                    $custom_meta_display = [ [ [ 'field'    => "field_is_cpt_{$post_type['post_type']}_custom_meta_preset_type",
                                                 'operator' => '==',
                                                 'value'    => 'custom' ] ] ];

                    $label = [ 'key'     => "field_is_cpt_{$post_type['post_type']}_custom_meta_label",
                               'label'   => 'Label',
                               'name'    => "label",
                               'type'    => 'text',
                               'wrapper' => [ 'width' => 30 ] ];

                    $preset_type_choices = [ 'custom' => 'Custom' ] + CPT::get_is_cpt_meta_field_select_options();

                    $preset_type = [ 'key'           => "field_is_cpt_{$post_type['post_type']}_custom_meta_preset_type",
                                     'label'         => 'Preset Type',
                                     'name'          => "preset_type",
                                     'type'          => 'select',
                                     'ui'            => 1,
                                     'default_value' => 'name',
                                     'choices'       => $preset_type_choices,
                                     'wrapper'       => [ 'width' => 30 ], ];

                    $field_width = [ 'key'           => "field_is_cpt_{$post_type['post_type']}_custom_meta_field_width",
                                     'label'         => 'Field Width',
                                     'name'          => "field_width",
                                     'type'          => 'number',
                                     'default_value' => 100,
                                     'step'          => 1,
                                     'min'           => 0,
                                     'max'           => 100,
                                     'wrapper'       => [ 'width' => 30 ] ];

                    $required = [ 'key'     => "field_is_cpt_{$post_type['post_type']}_custom_meta_required",
                                  'label'   => 'Required',
                                  'name'    => "required",
                                  'type'    => 'true_false',
                                  'wrapper' => [ 'width' => 10 ] ];

                    $name = [ 'key'               => "field_is_cpt_{$post_type['post_type']}_custom_meta_key",
                              'label'             => 'Meta Key',
                              'name'              => "meta_key",
                              'type'              => 'text',
                              'instructions'      => 'Database Name',
                              'conditional_logic' => $custom_meta_display,
                              'wrapper'           => [ 'width' => 33 ] ];


                    $instructions = [ 'key'               => "field_is_cpt_{$post_type['post_type']}_custom_meta_instructions",
                                      'label'             => 'Instructions',
                                      'name'              => "instructions",
                                      'type'              => 'text',
                                      'instructions'      => 'Displays field Instructions',
                                      'conditional_logic' => $custom_meta_display,
                                      'wrapper'           => [ 'width' => 32 ] ];
                    $type         = [ 'key'               => "field_is_cpt_{$post_type['post_type']}_custom_meta_type",
                                      'label'             => 'Type',
                                      'name'              => "type",
                                      'type'              => 'select',
                                      'ui'                => 1,
                                      'conditional_logic' => $custom_meta_display,
                                      'choices'           => [ 'text'         => 'Text',
                                                               'true_false'   => 'True / False',
                                                               'textarea'     => 'Textarea',
                                                               'image'        => 'Image',
                                                               'gallery'      => 'Gallery',
                                                               'relationship' => 'Relationship',
                                                               'wysiwyg'      => 'WYSIWYG Editor',
                                                               'email'        => 'Email Address',
                                                               'url'          => 'URL',
                                                               'file'         => 'File',
                                                               'link'         => 'Link',
                                                               'color_picker' => 'Color' ],
                                      'wrapper'           => [ 'width' => 32 ] ];

                    $custom_meta_fields = [ $label,
                                            $preset_type,
                                            $field_width,
                                            $required,
                                            $name,
                                            $type,
                                            $instructions, ];

                    $cpt_field_group_custom_meta_fields = [ 'key'          => "field_is_cpt_{$post_type['post_type']}_custom_meta",
                                                            'label'        => 'Meta Fields',
                                                            'name'         => "is_cpt_{$post_type['post_type']}_custom_meta_fields",
                                                            'type'         => 'repeater',
                                                            'layout'       => 'block',
                                                            'button_label' => 'Add Custom Meta',
                                                            'sub_fields'   => $custom_meta_fields ];


                    $single_view_choices = CPT::get_single_view_templates();

                    if ( $post_type['post_type'] == 'page' )
                        $single_view_choices[ false ] = 'Default Single View';

                    $cpt_field_group_page_builder = [ 'key'     => "field_is_cpt_{$post_type['post_type']}_page_builder",
                                                      'label'   => 'Page Builder',
                                                      'name'    => "is_cpt_{$post_type['post_type']}_page_builder",
                                                      'type'    => 'true_false',
                                                      'wrapper' => [ 'width' => 20 ] ];

                    $cpt_field_group_template = [ 'key'        => "field_is_cpt_{$post_type['post_type']}_template",
                                                  'label'      => 'Single View Template',
                                                  'name'       => "is_cpt_{$post_type['post_type']}_template",
                                                  'type'       => 'select',
                                                  'ui'         => 1,
                                                  'allow_null' => 1,
                                                  'choices'    => $single_view_choices,
                                                  'wrapper'    => [ 'width' => 80 ] ];

                    $is_cpt_display_single_view_options = [ [ [ 'field'    => "field_is_cpt_{$post_type['post_type']}_template",
                                                                'operator' => '!=',
                                                                'value'    => "0" ] ] ];


                    $cpt_field_group_hierarchical = [ 'key'               => "field_is_cpt_{$post_type['post_type']}_hierarchical",
                                                      'label'             => 'Hierarchical',
                                                      'name'              => "is_cpt_{$post_type['post_type']}_hierarchical",
                                                      'type'              => 'true_false',
                                                      'conditional_logic' => $is_cpt_display_single_view_options,
                                                      'wrapper'           => [ 'width' => 25 ] ];


                    $cpt_field_group_hide_header = [ 'key'               => "field_is_cpt_{$post_type['post_type']}_hide_header",
                                                     'label'             => 'Hide Header',
                                                     'name'              => "is_cpt_{$post_type['post_type']}_hide_header",
                                                     'type'              => 'true_false',
                                                     'conditional_logic' => $is_cpt_display_single_view_options,
                                                     'wrapper'           => [ 'width' => 25 ] ];

                    $cpt_field_group_hide_footer = [ 'key'               => "field_is_cpt_{$post_type['post_type']}_hide_footer",
                                                     'label'             => 'Hide Footer',
                                                     'name'              => "is_cpt_{$post_type['post_type']}_hide_footer",
                                                     'type'              => 'true_false',
                                                     'conditional_logic' => $is_cpt_display_single_view_options,
                                                     'wrapper'           => [ 'width' => 25 ] ];

                    $cpt_field_group_default_image = [ 'key'          => "field_is_cpt_{$post_type['post_type']}_default_image",
                                                       'label'        => 'Default Image',
                                                       'name'         => "is_cpt_{$post_type['post_type']}_default_image",
                                                       'type'         => 'image',
                                                       'preview_size' => 'medium',
                                                       'wrapper'      => [ 'width' => 50 ] ];


                    $cpt_field_group_exclude_from_search = [ 'key'     => "field_is_cpt_{$post_type['post_type']}_exclude_from_search",
                                                             'label'   => 'Exclude from Search',
                                                             'name'    => "is_cpt_{$post_type['post_type']}_exclude_from_search",
                                                             'type'    => 'true_false',
                                                             'wrapper' => [ 'width' => 50 ] ];


                    $cpt_field_group_custom_slug = [ 'key'     => "field_is_cpt_{$post_type['post_type']}_custom_slug",
                                                     'label'   => 'Custom Slug',
                                                     'name'    => "is_cpt_{$post_type['post_type']}_custom_slug",
                                                     'type'    => 'text',
                                                     'wrapper' => [ 'width' => 33 ] ];


                    $cpt_field_group_comment_support = [ 'key'     => "field_is_cpt_{$post_type['post_type']}_comment_support",
                                                         'label'   => 'Comment Support',
                                                         'name'    => "is_cpt_{$post_type['post_type']}_comment_support",
                                                         'type'    => 'true_false',
                                                         'wrapper' => [ 'width' => 33 ] ];


                    $cpt_field_group_featured_image_support = [ 'key'     => "field_is_cpt_{$post_type['post_type']}_featured_image_support",
                                                                'label'   => 'Enable WP Featured Image',
                                                                'name'    => "is_cpt_{$post_type['post_type']}_featured_image_support",
                                                                'type'    => 'true_false',
                                                                'wrapper' => [ 'width' => 33 ] ];

                    $cpt_field_group_fields = [ ACF::get_horizontal_tab( 'Meta Fields', 'field_cpt_fields_tab' ),
                                                $cpt_field_group_custom_meta_fields,
                                                ACF::get_horizontal_tab( 'Single View', 'field_cpt_single_view' ),
                                                $cpt_field_group_page_builder,
                                                $cpt_field_group_template,
                                                $cpt_field_group_hierarchical,
                                                $cpt_field_group_hide_header,
                                                $cpt_field_group_hide_footer,
                                                ACF::get_horizontal_tab( 'Config', 'field_cpt_config' ),
                                                $cpt_field_group_default_image,
                                                $cpt_field_group_exclude_from_search,
                                                $cpt_field_group_custom_slug,
                                                $cpt_field_group_comment_support,
                                                $cpt_field_group_featured_image_support ];

                    $cpt_field_group = [ 'key'      => "is_cpt_{$post_type['post_type']}_settings",
                                         'title'    => "{$post_type['name']} Options",
                                         'fields'   => $cpt_field_group_fields,
                                         'location' => [ [ [ 'param'    => 'options_page',
                                                             'operator' => '==',
                                                             'value'    => "acf-options-post-type-{$post_type['post_type']}", ], ], ], ];


                    acf_add_local_field_group( $cpt_field_group );


                endforeach;


    }


    static function get_single_view_templates() {

        return Acme::get_templates( "single_view_templates" );

    }


    static function get_is_cpt_meta_field_select_options() {
        return [ 'name'           => 'Name',
                 'title'          => 'Title',
                 'image'          => 'Image',
                 'embed'          => 'Embed',
                 'file'           => 'File',
                 'link'           => 'Link',
                 'identification' => 'Identification',
                 'subtitle'       => 'Subtitle',
                 'start_date'     => 'Start Date',
                 'end_date'       => 'End Date',
                 'location'       => 'Location',
                 'address_line_1' => 'Address Line 1',
                 'address_line_2' => 'Address Line 2',
                 'author_text'    => 'Author (Text)',
                 'author_post'    => 'Author (Post)',
                 'excerpt'        => 'Excerpt',
                 'content'        => 'Content',
                 'source'         => 'Source',
                 'hours'          => 'Hours',
                 'phone_number'   => 'Phone Number',
                 'email_address'  => 'Email Address',
                 'fax_number'     => 'Fax Number',
                 'facebook'       => 'Facebook',
                 'linkedin'       => 'Linkedin',
                 'youtube'        => 'Youtube',
                 'twitter'        => 'Twitter',
                 'instagram'      => 'Instagram', ];
    }

    static function get_post_type_info_for_api( $post_type = '' ) {


        if ( $post_type == '' )
            return "enter post type";

        $all_fields = get_fields( "options" );

        $return = [];

        $needle = "is_cpt_{$post_type}";
        // the +1 is for the trailing underscore
        $needle_length = strlen( $needle ) + 1;

        foreach ( $all_fields as $key => $value ):
            if ( strpos( $key, $needle ) === 0 ):
                $new_key            = substr( $key, $needle_length );
                $return[ $new_key ] = $value;
            endif;
        endforeach;

        return $return;


    }


}
