<?

namespace TSD_Infinisite;

use CalendR\Event\AbstractEvent;
use CalendR\Event\Provider\ProviderInterface;


/**
 * Class Calendar
 * @package TSD_Infinisite
 *
 * Not super sure how i want to integrate this with the calendr library
 *
 * this is intended to be an api for that function
 *
 */
class Calendar
{


    private $factory;
    private $config = [];
    private $posts = [];
    private $calendr_events = [];

    private $event_collection = [];
    private $event_lookup = [];

    // this is going to be either one month, or
    // an owl carousel of multiple months
    private $monthly_output;


    function __construct($posts = [], $config = [])
    {

        $this->config($posts, $config);
        $this->create_calendr();
        $this->set_events();
        $this->build_monthly_output();


    }


    private function config($posts, $atts)
    {


        if (gettype($posts) == 'WP_Post') $posts = [$posts];

        $this->posts = $posts;

        $defaults = [
            'acf_field_name_start_date' => 'start_date',
            'acf_field_name_end_date' => 'end_date',
            'templates' => [
                'event' => '/wp-content/plugins/tsd-infinisite/components/calendar/templates/event/montgomery_demo_event.php',
                'month' => '/wp-content/plugins/tsd-infinisite/components/calendar/templates/month/montgomery_demo_month.php',
                'list' => '/wp-content/plugins/tsd-infinisite/components/calendar/templates/list/montgomery_demo_list.php',
            ]
        ];

        $this->config = \wp_parse_args($atts, $defaults);


    }

    public function set_config($config)
    {

        foreach ($config as $key => $value)
            $this->config[$key] = $value;

    }


    private function create_calendr()
    {

        $this->factory = new \CalendR\Calendar();

        $this->event_collection = new \CalendR\Event\Provider\Basic();

        $this->factory->getEventManager()->addProvider('event_collection', $this->event_collection);

    }


    private function set_events()
    {

        if (!count($this->posts)) return;


        foreach ($this->posts as $post):

            $fields = new ACF_Helper($post);

            $start_date = $fields->get_start_date($this->config['acf_field_name_start_date']);
            $end_date = $fields->get_end_date($this->config['acf_field_name_end_date']);

            if (!$end_date) $end_date = $start_date;

            $lookup_entry = $this->event_lookup
            [$start_date->format("Y")]
            [$start_date->format("m")]
            [$start_date->format("j")];

            $lookup_entry_value = $lookup_entry ? 1 : $lookup_entry + 1;

            $this->event_lookup[$start_date->format("Y")][$start_date->format("m")][$start_date->format("j")] = $lookup_entry_value;

            $new_event = new Calendar_Event($post->ID, $start_date, $end_date);

            $this->calendr_events[] = $new_event;
            $this->event_collection->add($new_event);


        endforeach;

    }

    private function get_first_event_date()
    {

        $year_key = min(array_keys($this->event_lookup));
        $month_key = min(array_keys($this->event_lookup[$year_key]));
        $day_key = min(array_keys($this->event_lookup[$year_key][$month_key]));

        return [
            'year' => \intval($year_key),
            'month' => \intval($month_key),
            'day' => \intval($day_key)
        ];


    }


    private function get_last_event_date()
    {

        $year_key = max(array_keys($this->event_lookup));
        $month_key = max(array_keys($this->event_lookup[$year_key]));
        $day_key = max(array_keys($this->event_lookup[$year_key][$month_key]));

        return [
            'year' => \intval($year_key),
            'month' => \intval($month_key),
            'day' => \intval($day_key)
        ];


    }

    private function build_monthly_output()
    {

        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . $this->config['templates']['month']);
        $this->monthly_output .= ob_get_clean();

    }


    public function get_output($type = 'month')
    {

        switch ($type):

            case 'month':
                return $this->monthly_output;
                break;

            default:
                if (!Acme::can_user_see_debug()) return false;
                return "No Output Type Selected";
                break;


        endswitch;

    }


}


class Calendar_Event extends AbstractEvent
{
    protected $begin;
    protected $end;
    protected $uid;

    public $post;
    public $fields;

    // And all your fields

    public function __construct($uid, \DateTime $start, \DateTime $end)
    {

        $this->uid = $uid;

        // If you don't add time, the second to last day of the
        // event doesn't get added. I wish I were making that up.

        $this->begin = clone $start->modify("+1 second");
        $this->end = clone $end->modify("+1 second");


        $this->config();
    }

    private function config()
    {

        $this->post = IS_Post::db_get($this->uid);
        $this->fields = new ACF_Helper($this->post);

    }

    public function getUid()
    {
        return $this->uid;
    }

    public function getBegin()
    {
        return $this->begin;
    }

    public function getEnd()
    {
        return $this->end;
    }
}


class Calendar_Event_Provider implements ProviderInterface
{
    public function getEvents(\DateTime $begin, \DateTime $end, array $options = array())
    {
        /*
         Returns an array of events here. $options is the second argument of
         $factory->getEvents(), so you can filter your event on anything (Calendar id/slug ?)
        */
    }
}