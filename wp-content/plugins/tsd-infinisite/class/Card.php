<?

namespace TSD_Infinisite;

class Card {

    private $html;
    private $post;
    private $excerpt;

    public function __construct( IS_Post $post ) {
        $this->post = $post;
        $this->config();
        $this->build();
    }

    private function config() {
        $this->excerpt = $this->post->excerpt();
    }

    protected function build() {

        $this->html = "
            <div class='card'>
                <div class='title'>
                    <h3>{$this->post->post_title}</h3>
                </div>
                {$this->excerpt}
            </div>
        ";
    }

    public function get() {
        return $this->html;
    }

    public function print() {
        print $this->html;
    }

}
