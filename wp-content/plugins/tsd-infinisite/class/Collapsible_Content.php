<?

namespace TSD_Infinisite;

class Collapsible_Content {


    /**
     * Collapsible_Content constructor.
     *
     * We're going to build this out of an acf module - that way we can access it through the ACF_Module API
     *
     * TODO: update this to add relevant information
     *
     * @param $module
     *
     * @return boolean
     *
     */


    public $tiers = [];
    public $post_types = [];
    public $wp_meta_query = [];
    public $cartesian_array = [];
    public $grouped_category_terms = [];
    public $acf_module = [];
    public $slide_options = [];
    public $explode_lookup = [];
    public $branch_index = [];
    public $branch_index_counter = 0;
    public $custom_posts = [];
    public $html = '';

    public $term_lookup_index = 0;

    public function __construct($acf_module = ACF_Module::class) {

        $this->set_config($acf_module);
        $this->build_cartesian_array();
        $this->build_wp_tax_queries();
        $this->group_category_terms();

        $content = new Loop($this);
        $this->html = $content->get_content();

        return true;
    }

    // this is how we output the html in the browser
    public function get_html() {

        return "
		<div class='is-collapsible-content'>
			{$this->html}		
		</div>
		";
    }

    private function set_config($acf_module) {

        $module = $acf_module->get_module();
        $this->acf_module = $module;
        $this->tiers = $module['tiers'];
        $this->post_types = $module['post_types'];
        $this->default_slide_template = $module['content_template'];


        // the custom posts key is from use in custom modules - not
        // programmed into the frontend - we could, and then
        // we could remove this conditional

        if (array_key_exists('custom_posts', $module)):
            $this->custom_posts = $module['custom_posts'];
        endif;


        $this->slide_options['style'] = $module['display_options_post_style'];
        $this->slide_options['accordion'] = $module['display_options_accordion'];
        $this->slide_options['horizontal_tab'] = $module['display_options_horizontal_tab'];
        $this->slide_options['vertical_tab'] = $module['display_options_vertical_tab'];
        $this->slide_options['list'] = $module['display_options_list'];
    }

    protected function build_cartesian_array() {


        $all_tier_terms = [];

        foreach ($this->tiers as $tier):

            $tier_category = \get_terms($tier['category']);

            $tier_terms = [];

            foreach ($tier_category as $c => $term):
                $tier_terms[] = ['slug'     => $term->slug,
                                 'taxonomy' => $term->taxonomy];
            endforeach;

            $all_tier_terms[] = $tier_terms;

        endforeach;


        $this->cartesian_array = Acme::build_cartsian_set($all_tier_terms);


        // TODO: build out meta queriess


    }


    // this is how we send out acf information to the loop function
    public function get_tier($depth) {

        return $this->tiers[$depth];

    }


    protected function build_wp_tax_queries() {

        $queries_to_be_grouped = [];

        foreach ($this->cartesian_array as $query_series):
            foreach ($query_series as $taxonomy):
                $queries_to_be_grouped[] = ['taxonomy' => $taxonomy['taxonomy'],
                                            'field'    => 'slug',
                                            'terms'    => $taxonomy['slug']];

            endforeach;
        endforeach;


        $this->wp_meta_query = array_chunk($queries_to_be_grouped, count($this->tiers));


    }


    protected function group_category_terms() {

        $to_explode = [];

        foreach ($this->wp_meta_query as $query_group) {

            $explode_string = '';

            foreach ($query_group as $c => $query):

                $explode_string .= "{$query['taxonomy']}.{$query['terms']}";

                if ($c + 1 != count($query_group)) {
                    $explode_string .= '\\';
                }

            endforeach;

            $to_explode[$explode_string] = $query_group;

        }


        // use a great function found online to build out a tree
        // from an array with properly formatted key values

        $this->grouped_category_terms = Acme::explodeTree($to_explode, '\\');
        $this->process_array_loop($this->grouped_category_terms);

    }

    function process_array_loop(&$array, &$group = 0) {


        // this function places the is queries on the
        // leaves of the posts so we can check for empty posts
        // and do other cool stuff


        foreach ($array as &$tier) {

            if (isset($tier[0])) {

                $this->branch_index[] = $this->wp_meta_query[$group];


                $query = new IS_Query(['post_type'      => $this->post_types,
                                        'tax_query'      => $this->wp_meta_query[$group],
                                        'posts_per_page' => -1,
                                        'post__in'       => $this->custom_posts]);

                $group++;
                $tier['query'] = $query;
            } else {
                Collapsible_Content::process_array_loop($tier, $group);
            }

        }

    }

    public static function get_term_from_encoded_string($string = '') {

        $return = '';

        if ($string == '') {
            return $return;
        }


        $info = explode(".", $string);


        $term = \get_term_by('slug', $info[1], $info[0]);

        return $term;

    }


}

class Loop {

    public $depth = 0;
    public $content;
    public $cc;
    public $index = [];
    public $c = 0;
    public $top_level_loosies = [];

    public function __construct($cc) {

        $this->cc = $cc;
        $id = uniqid("cc_loop_");
        $this->content = $this->loop($cc->grouped_category_terms, $id);

    }

    public function get_content() {
        return $this->content;
    }


    private function loop($branch, $branch_id = false, $parent_id = false) {

        // we're going to build out the html in the shape of the
        // grouped category terms array

        // the depth variable is to keep track of what acf tier
        // we're on so we grab the right set of preferences

        // the content for parent var is our master output
        // variable. we cycle through the branches of the array
        // saving our html to it as we go.

        $content_for_parent = '';
        $branch_has_posts = array_key_exists('0', $branch);
        $child_content = [];


        if (!$branch_has_posts):


            // if we don't have posts yet, we need to trigger our
            // recursive function again, saving its value to our
            // output html

            if ($parent_id) {
                $grandparent_id = $parent_id;
            }

            if ($branch_id) {
                $parent_id = $branch_id;
            }

            $branch_id = uniqid("branch_");
            $c = 0;
            foreach ($branch as $key => $limb):


                if (!array_key_exists($this->depth, $this->index)):
                    $this->index[$this->depth] = 0;
                endif;


                $limb_id = "{$branch_id}_{$c}";


                $this->depth++;


                $this->index[$this->depth]++;

                $child_content[] = $this->loop($limb, $limb_id, $parent_id);
                $this->depth--;
                $c++;
            endforeach;


            $this->c++;
            $content_for_parent .= $this->build_branch($child_content, $branch, $branch_id, $grandparent_id);


        endif;


        if ($branch_has_posts) :

            // we dump at the end of the function because once we hit the leaves, we
            // dont want to run the function anymore


            if ($branch_id) {
                $grandparent_id = $branch_id;
            }


            $content_for_parent .= $this->build_leaves($branch, $this->index, $grandparent_id, $this->cc);

            return $content_for_parent;


        endif;


        array_pop($this->index);

        // this return value passes the content that we created "up" to the previous level
        // do not change!

        return $content_for_parent;

    }

    protected function build_branch($child_content, $branch, $branch_id, $parent_id) {

        $branch = new Branch($this, $child_content, $branch, $branch_id, $parent_id);

        return $branch->get_html();

    }

    protected function build_leaves($branch, $index, $grandparent_id, $cc) {

        /*
         * this function builds the excerpt query for the leaves
         */

        $leaves = new Leaves($branch, $this->depth, $grandparent_id, $cc);

        return $leaves->get_content();

    }


}

class Branch {

    public $tier;
    public $branch;
    public $child_content = [];
    public $display_style = '';
    public $title = '';

    public $index = [];
    public $parent_terms = [];
    public $branch_count = 0;
    public $container_html = '';
    public $launcher_html = '';
    public $content_html = '';
    public $html = '';
    public $str_index;
    public $branch_id = '';
    public $show_navigation = false;
    public $depth = '';
    public $parent_id = '';
    public $cc;
    public $post_counts = [];

    public $active_index = 0;

    public $parent_cats = [];

    // a loosie is a post that doesn't have terms for all the requisite tier taxonomies
    public $loosies = [];

    function __construct($loop, $child_content, $branch, $branch_id, $parent_id) {


        $this->tier = $loop->cc->get_tier($loop->depth);
        $this->acf_options = $this->tier["{$this->tier['display_style']}_options"];
        $this->branch = $branch;
        $this->child_content = $child_content;
        $this->display_style = $this->tier['display_style'];
        $this->title = $this->tier['title'];
        $this->branch_id = $branch_id;
        $this->parent_id = $parent_id;
        $this->depth = $loop->depth;
        $this->index = $loop->index;

        $this->cc = $loop->cc;

        $this->check_navigation();

        $this->build_index();
        $this->gather_loosies();

        $this->build_html();


    }

    private function check_navigation() {

        if ($this->depth == 0 || $this->depth == 1) {
            return;
        }

        $parent_tier = $this->cc->get_tier($this->depth - 1);
        $parent_modal = $parent_tier['display_style'] == 'modal';

        if ($parent_modal) {
            $this->show_navigation = true;
        }
    }


    protected function build_index() {

        $this->branch_count = array_shift($this->index);
        array_pop($this->index);

        $cats = $this->cc->grouped_category_terms;

        foreach ($this->index as $index) {

            $keys = array_keys($cats);
            $sought_key = $keys[$index - 1];
            $this->parent_cats[] = $sought_key;
            $cats = $cats[$sought_key];

        }

        // in these two each loops we go through the branch's parent categories
        // and create arrays with wp tax queries on them so we can check how many
        // children each branch has in it.

        // we also need to figure out a way to query posts that are

        $found_posts = false;

        foreach ($this->branch as $title => $branch) {
            $tax_query = [];
            foreach ($this->parent_cats as $cat) {
                $term = Collapsible_Content::get_term_from_encoded_string($cat);

                $tax_query[] = ['taxonomy' => $term->taxonomy,
                                'field'    => 'slug',
                                'terms'    => [$term->slug]];
            }
            $parent_term = Collapsible_Content::get_term_from_encoded_string($title);

            $tax_query[] = ['taxonomy' => $parent_term->taxonomy,
                            'field'    => 'slug',
                            'terms'    => [$parent_term->slug]];


            $query = new \WP_Query(['post_type' => $this->cc->post_types,
                                    'tax_query' => $tax_query,
                                    'post__in'  => $this->cc->custom_posts,]);

            $post_count = intval($query->found_posts);
            $this->post_counts[] = $post_count;
            $this->branch[$title]['disabled'] = $post_count == 0 ? "data-ccm-disabled" : "";
            $this->branch[$title]['post_count'] = $post_count;

            if ($post_count == 0 && !$found_posts):
                $this->active_index++;
            else:
                $found_posts = true;
            endif;

            $tier_keys = array_keys($this->cc->tiers);
            $query_keys = array_keys($tax_query);

            $remaining_keys = array_diff($tier_keys, $query_keys);

            // updating the tax_query with the


            foreach ($remaining_keys as $remaining_key) {
                $category = $this->cc->tiers[$remaining_key]['category'];
            }


        }


    }

    protected function gather_loosies() {


        $loosie_query = [];
        $loosie_taxonomies = [];

        $tier_count = count($this->cc->acf_module['tiers']);
        $depth_count = intval($this->depth) + 1;

        if ($depth_count == 1 && $tier_count == 1) {
            return;
        }

        if ($depth_count == $tier_count) {
            return;
        }


        // go through the parent branches terms and add them to our loosie query
        foreach ($this->parent_cats as $parent_cat):

            $term = \TSD_Infinisite\Collapsible_Content::get_term_from_encoded_string($parent_cat);

            $loosie_query[] = ['taxonomy' => $term->taxonomy,
                               'field'    => 'slug',
                               'terms'    => [$term->slug]];

            $loosie_taxonomies[] = $term->taxonomy;

        endforeach;


        foreach ($this->branch as $encoded_title => $branch):

            // make a new reference to add the branch's particular tax term to
            // the loosie query, in addition to the parent terms
            $loosie_wp_tax_query = $loosie_query;
            $loosie_wp_taxonomies = $loosie_taxonomies;

            $branch_term = \TSD_Infinisite\Collapsible_Content::get_term_from_encoded_string($encoded_title);

            // add the branch's term to the upcoming query
            $loosie_wp_tax_query[] = ['taxonomy' => $branch_term->taxonomy,
                                      'field'    => 'slug',
                                      'terms'    => [$branch_term->slug]];


            // add the branch's taxonomy to the list used for the inversion
            $loosie_wp_taxonomies[] = $branch_term->taxonomy;


            // we need to ensure that all of the other specified categories for the ccm
            // aren't selected in the loosie (which is a rule of being a loosie)
            $all_other_tier_taxonomies = [];

            // build out the array to be inverted
            foreach ($this->cc->tiers as $tier) {
                $all_other_tier_taxonomies[] = $tier['category'];
            }

            // invert all the active taxonomies from the rest of them to create
            // the array of terms that need to be not set
            $all_other_tier_taxonomies = array_diff($all_other_tier_taxonomies, $loosie_wp_taxonomies);

            foreach ($all_other_tier_taxonomies as $taxonomy):

                $loosie_wp_tax_query[] = ['taxonomy' => $taxonomy,
                                          'field'    => 'slug',
                                          'operator' => 'NOT EXISTS'];


            endforeach;

            // finally, with all that done, we grab all the posts types
            // specified by the acf moudule, with our tax query, and
            // at long last, you have your loosie
            $this->loosies[] = new \WP_Query(['post_type'      => $this->cc->acf_module['post_types'],
                                              'posts_per_page' => -1,
                                              'tax_query'      => $loosie_wp_tax_query,
                                              'post__in'       => $this->cc->custom_posts]);


        endforeach;

    }


    public function show_loosies($c) {

        $is_query = $this->loosies[$c];

        if (!$is_query->posts) {
            return '';
        }

        if (!count($is_query->posts)) {
            return '';
        }

        $return = "<p>Loose Posts Found</p>";

        foreach ($is_query->posts as $post) {
            $return .= "<h6>{$post->post_title}</h6>";
        }

        return $return;


    }

    protected function build_html() {

        $this->launcher_html = $this->grab_template($this->acf_options['launcher_template']);
        $this->content_html = $this->grab_template($this->acf_options['content_template']);
        $this->container_html = $this->grab_template($this->acf_options['container_template']);

        $this->html .= $this->container_html;

    }

    public function is_active($index) {

        return $this->active_index == $index;

    }


    public function get_html() {
        return $this->html;
    }

    private function grab_template($filepath) {

        return Acme::get_file($filepath, ['launcher' => $this]);

    }


}

class Leaves {


    public $branch;
    public $parent_id;
    public $query;
    public $index;
    public $depth;

    public $html = '';

    public $query_html = '';

    public $cc;

    public $display_style;
    public $templates;
    public $acf_options;


    public $launcher_html;
    public $content_html;
    public $post_excerpts = [];
    public $container_html;
    public $show_navigation = false;

    public $excerpt_html = [];

    public $branch_id = '';

    public function __construct($branch, $depth, $parent_id, $cc) {

        $this->branch = $branch;
        $this->query = $branch['query'];
        $this->depth = $depth;
        $this->cc = $cc;
        $this->branch_id = uniqid('leaf_cluster_');
        $this->acf_options = $this->cc->acf_module;
        $this->display_style = $this->acf_options['display_options_post_style'];
        $this->templates = $this->acf_options["display_options_{$this->display_style}"];
        $this->parent_id = $parent_id;

        $this->check_navigation();

        $this->build_html();

    }


    private function check_navigation() {

        if ($this->depth == 0 || $this->depth == 1) {
            return;
        }

        $parent_tier = $this->cc->get_tier($this->depth - 1);
        $parent_modal = $parent_tier['display_style'] == 'modal';

        if ($parent_modal) {
            $this->show_navigation = true;
        }
    }


    public function get_content() {

        return $this->html;

    }

    protected function build_query() {

        $template = $this->cc->acf_module['display_options_query']['query_template'];

        $args = [// we use the is_custom_posts type because that's how
                 // the is query object knows that we're looking for manual posts
                 'post_type'                   => 'is_custom_posts',
                 'post_archive_query_template' => $template];

        return $this->query->posts ? $this->query->convert_to_IS_Post_Archive_Module($args) : 'no posts found';


    }

    protected function build_html() {


        // if the leaf display type is a query or excerpts - we're
        // going to handle the display of the entire container
        // using the IS Post Archive class.

        if ($this->display_style == 'query'):
            $this->html = $this->build_query();

            return;
        endif;

        if ($this->display_style == 'excerpts') :
            $this->html = $this->build_post_excerpts();

            return;
        endif;

        // otherwise, we build out our content here.

        $this->launcher_html = $this->grab_template($this->templates['launcher_template']);
        $this->content_html = $this->grab_template($this->templates['content_template']);
        $this->container_html = $this->grab_template($this->templates['container_template']);

        $this->html = $this->container_html;


    }

    protected function build_post_excerpts() {

        $cell_html = '';

        foreach ($this->query->posts as $post) {
            $post = new IS_Post($post);
            $this->post_excerpts[] = $post->post_title;
            $cell_html .= $post->render_php_template($this->templates['template']);

        };

        $block_grid_class = $this->build_excerpt_display_post_per_row_class();

        $padding_classes = $this->build_excerpt_display_cell_padding_class();

        $row_html = "
		
		<div class='grid-x $block_grid_class $padding_classes'>
			{$cell_html}
		</div>
		
		";

        return $row_html;


    }

    private function build_excerpt_display_post_per_row_class() {

        $return = '';


        foreach ($this->templates['posts_per_row'] as $size => $value):
            if (!$value) {
                continue;
            }
            $return .= "{$size}-up-{$value} ";

        endforeach;


        return $return;

    }

    private function build_excerpt_display_cell_padding_class() {


        $return = '';


        if ($this->templates['cell_padding']['x']) :
            $return .= 'grid-padding-x ';
        endif;

        if ($this->templates['cell_padding']['y']) :
            $return .= 'grid-padding-y ';
        endif;

        return $return;

    }


    private function grab_template($filepath) {

        return Acme::get_file($filepath, ['launcher' => $this]);

    }


}

