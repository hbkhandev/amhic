<?php


namespace TSD_Infinisite;

class DB {



    public $active = 0;

    public function __construct() {

        $this->active = false;
//        $this->active = get_option("options_enable_transients",1);

    }

    // this is a caching class

    public $is_global_post_type = false;

    public static function function_cache($label, $value_to_save = false, $expiration = 7 * DAY_IN_SECONDS) {


        $label_for_db = "isfn_" . $label;

        // if we have a value to save, we do it first and dump

        if ($value_to_save):
            \set_transient($label_for_db, $value_to_save, $expiration);
            return $value_to_save;
        endif;

        // if we aren't doing caching, we dump

        $cache = new DB;
        if (!$cache->active)
            return $value_to_save;



        // checking if the name is too long

        $name_too_long = strlen($label_for_db) >= 100;

        if ($name_too_long):
            Flash::error($label_for_db . ' rejected : Function Name too long');
            return null;
        endif;


        // checking to see if we have a value in the database

        $transient = \get_transient($label_for_db);


        // first we dump if it isn't empty
        if (!empty($transient)):
            return $transient;
        endif;

        if($transient === false):
            return $transient;
        endif;

        Flash::error("eof $label");

        return null;

    }


    public static function acf_cache($acf_name, $id = 'options', $expiration = 7 * DAY_IN_SECONDS) {

        $test = new DB;
        if (!$test->active)
            return \get_field($acf_name, $id);

        $transient_label = "isacf_" . $acf_name . '_' . $id;

        $name_too_long = strlen($transient_label) >= 100;

        if ($name_too_long):
            Flash::error($transient_label . ' rejected : ACF_Name too long');
            return null;
        endif;


        $transient = \get_transient($transient_label);

        if (!empty($transient)):
            return $transient;
        endif;

        $acf = \get_field($acf_name, $id);

        \set_transient($transient_label, $acf, $expiration);

        return $acf;

    }


    public static function post_meta($post_id = false, $expiration = 7 * DAY_IN_SECONDS) {

        if (!$post_id)
            return null;

        $is_cache = new DB;
        if (!$is_cache->active)
            return \get_fields($post_id);


        $transient_label = "is_acf_post_" . $post_id;

        $name_too_long = strlen($transient_label) >= 100;

        if ($name_too_long):
            Flash::error($transient_label . ' rejected : ACF_Name too long');
            return null;
        endif;

        $transient = \get_transient($transient_label);

        if (!empty($transient)):
            return $transient;
        endif;

        $all_fields = \get_fields($post_id);

        \set_transient($transient_label, $all_fields, $expiration);

        return $all_fields;

    }


    public static function clear_palette_cache(){

        \delete_transient('isfn_palette::get_is_palette');

    }


    public static function clear_editor_cache(){

        \delete_transient('isfn_is_editor::update_styles');

    }


}
