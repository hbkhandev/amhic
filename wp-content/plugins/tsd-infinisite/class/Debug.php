<?php

namespace TSD_Infinisite;

class Debug {

	public $active = false;
	public $files = [];

	protected $layout;
	protected $page_content = '';
	protected $page_title = '';
	protected $debug_filepath = '';

	private $debug_page_id;
	private $html = '';

	protected $debug_header_html = '';
	protected $select_form_html = '';


    public function __construct()
    {

        if (!isset($_GET['is_debug'])) return;

        if (!Acme::can_user_see_debug()) return;

		$_pof = get_option( "page_on_front" );



		if ( $_pof == 0 ) {
			print "IS debug requires a static home page";
			$this->active = true;

			return;
		}

		// we don't want to run this in the backend - it's messin with our scripts!
		if (\is_admin()) {
			return;
		}

		if (!array_key_exists('is_debug', $_GET)) {
			return;
		}


		$this->debug_page_id = $_pof;

		$this->layout = new Layout( $this->debug_page_id );

		$this->check_for_user_requested_page();

		$this->build_file_list();

		$this->build_debug_page_title();
		$this->build_debug_page_select_form();
		$this->build_debug_header();


		$this->build_page_content();
		$this->build_html_output();

	}

	public function is_active() {

		if ( $this->active === false ) {
			return false;
		}

		return true;
	}

	private function check_for_user_requested_page() {

		if ( isset( $_GET['is_debug'] ) ) {
			$this->active = $_GET['is_debug'];
		}

		if ( $this->active === '' ) {
			$this->active = 'index';
		}

	}

	private function build_file_list() {


		$plugin_filepath      = INFINISITE_URI . "assets/debug/*";
		$plugin_dbg_templates = glob( $plugin_filepath );
		$theme_filepath       = THEME_URI . "assets/debug/*";
		$theme_dbg_templates  = glob( $theme_filepath );

		$_filepaths = array_merge( $plugin_dbg_templates, $theme_dbg_templates );

		// this is an old function that creates
		$_files_to_sort = Acme::process_template_names_for_acf_select_fields( $_filepaths, [
			'ugly_labels'       => true,
			'reverse_key_value' => true
		] );

		// a little magical mixup to put the index key/value in the first slot
		$this->files = array_splice( $_files_to_sort, array_search( 'index', array_keys( $_files_to_sort ) ), 1 ) + $_files_to_sort;


	}

	protected function build_debug_page_select_form() {

		$options = '';

		foreach ( $this->files as $label => $filepath ):
			$active  = $this->active == $label ? 'selected' : '';
			$dir     = site_url( "?is_debug=$label" );
			$options .= "<option value='$dir' $active>$label</option>";
		endforeach;

		$content = "
            <select onchange='if (this.value) window.location.href=this.value'>
                <option disabled>Select A Debug Template</option>
				$options
            </select>
		
		";

		$this->select_form_html = $content;

	}

	protected function build_debug_page_title() {

		$this->page_title = $this->active;

	}

	protected function build_debug_header() {


		$_content = "
			<div class='grid-x align-middle'>
				<div class='cell small-auto medium-auto'>
					<h4>
						{$this->page_title}
					</h4>
				</div>
				<div class='cell small-shrink medium-shrink'>
					{$this->select_form_html}
				</div>
			</div>
		";

		$this->debug_header_html = $_content;

	}

	private function build_page_content() {


		$this->debug_filepath = $this->files[ $this->active ];

		ob_start();
		include( $this->debug_filepath );
		$this->page_content = ob_get_clean();


	}


	protected function build_html_output() {


        $head = new Header();
		$_doc_header    = $head->get_header();
		$_header        = $this->layout->header->get_outside();
		$_debug_header  = $this->debug_header_html;
		$_debug_content = $this->page_content;
		$_footer        = $this->layout->footer->get_footer();

		ob_start();
		wp_footer();
		$_doc_footer = ob_get_clean();

		if ( $this->active == 'index' ) {
			$_debug_header = '';
		}


		$this->html .= "
			$_doc_header
			$_header
			
			<div class='grid-container' data-editor-style>
			
				$_debug_header
			
				<div class='grid-x'>
					<div class='cell small-12'>
						$_debug_content
					</div>
				</div>
			</div>
			
			$_footer
			$_doc_footer
			";

	}

	public function get_output() {
		return $this->html;
	}

}
