<?php

namespace TSD_Infinisite;

class Documentation {

    public $base = "/wp-admin/admin.php?page=is_help&section=";

    public function url($page) {

        // this is just a text-shortening function to build the menu links

        return "{$this->base}$page";

    }

    public static function register_help_menu($admin_bar) {


        // this is where the top level help menu is registered - it's called
        // from the main plugin file during the admin_bar_menu hook
        // we need to create the menu structure by scanning the file system
        // and then assemble that into the correct data format


        $directories[INFINISITE_URI] = INFINISITE_URI . 'components/documentation';
        $directories[PARENT_URI] = PARENT_URI . 'components/documentation';
        $directories[THEME_URI] = THEME_URI . 'components/documentation';

        $doc = new Documentation();


        foreach ($directories as $filepath => $directory):


            $admin_bar->add_node(['id'    => 'infinisite_help',
                                  'title' => 'InfiniSite Help',
                                  'href'  => $doc->url("home")]);

            // this is the file that scans the directory structure to assemble the
            // wordpress menu
            $tree = Documentation::build_documentation_tree($directory, $filepath);

            // if the directory doesn't exist, there's no menus to make, so we
            // move on to the next directory

            if (!$tree)
                continue;


            // if we do find directories that need to be turned into a menu,
            // its time to get to work

            foreach ($tree as $key => $branch)
                Documentation::create_help_menu_branch($key, $branch, $admin_bar);

        endforeach;


    }

    public static function register_help_pages() {

        // the way this works is that we register a single help page, and then
        // use query strings to pull in the template for whatever page has been selected from the menu


         \add_options_page('InfiniSite Help', 'IS Help', 'manage_options', 'is_help', function() {


            // error handling

            $op = 'no function assigned';

            if (!array_key_exists('section', $_GET)):
                $op = 'no page to load';
            endif;

            // the home page doesn't get a index file in our directory building
            // function, so we just cheat it here. this is a good way for
            // us to drop in future cheats as well

            switch ($_GET['section']):
                case 'home':
                    $template_filepath = "components/documentation/index.php";
                    break;

                default:
                    $template_filepath = $_GET['section'];
                    break;

            endswitch;


            $directories = [THEME_URI, PARENT_URI, INFINISITE_URI];
            $image_dirs = [THEME_DIR, PARENT_DIR, INFINISITE_DIR];

            // scan the 3 potential areas for templates

            foreach ($directories as $c => $directory):

                // using the server uri, we check to see if each area holds
                // our file

                if (!file_exists($directory . $template_filepath))
                    continue;

                // once we have the file we're going to use, we place
                // the directory path, not the server uri

                $current_file = $image_dirs[$c] . $template_filepath;

                // we need to trim off the text for the file that's loaded

                $explode = explode("/", $current_file);
                array_pop($explode);
                $title = end($explode);

                // if we're in a subpage now, we need to load the assets from the right place
                $current_file_is_subpage = $title === 'subpages';

                if ($current_file_is_subpage):
                    array_pop($explode);
                    $title = end($explode);
                endif;


                $asset_directory = implode('/', $explode) . '/assets/';


            endforeach;

            // get the html for the selected template

            $template_html = Acme::get_file($template_filepath);

            // build out the html for the page using the selected template

            $op = "
                <div data-is-style style='padding-right: 15px;'>
                    <div class='spacer xsmall'></div>
                    <div class='grid-x align-bottom'>
                        <div class='cell auto'>
                            <h5 class='primary-text no-margin'>IS Help System</h5>
                        </div>
                        <div class='cell shrink'>
                            <h6 class='secondary-text no-margin'>{$title}</h6>
                        </div>
                    </div>
                    <div class='spacer small'></div>
                    $template_html
                </div>
                ";

            // drop in the relative image url

            $op = str_replace('<img src="', "<img src=\"{$asset_directory}", $op);
            $op = str_replace('<img src=\'', "<img src='{$asset_directory}", $op);
            $op = str_replace('<script src="', "<script src=\"{$asset_directory}", $op);
            $op = str_replace('<script src=\'', "<script src='{$asset_directory}", $op);

            print $op;

        });

    }

    public static function build_documentation_tree($directory, $filepath) {

        // this is the file that actually scans the file system and builds
        // our directories

        // a directory can have multiple components
        // the index.php file tells the system that this has a page, and
        // isn't just a pass thru container

        // the assets directory is where you place your img / js - no css
        // support yet.

        // the subpages directory is a simple way of linking multiple pages
        // in a single section - they all use the same asset path


        if (!file_exists($directory))
            return;

        if (strpos($directory, '.'))
            return;


        $ffs = scandir($directory);

        unset($ffs[array_search('.', $ffs, true)]);
        unset($ffs[array_search('..', $ffs, true)]);

        // prevent empty ordered elements
        if (count($ffs) < 1)
            return;

        $return = [];

        foreach ($ffs as $ff) :

            $key = strtok($ff, ".");

            $has_index = file_exists("$directory/$ff/index.php");
            $has_subpages = file_exists("$directory/$ff/subpages");
            $subtree = Documentation::build_documentation_tree("$directory/$ff", $filepath);

            $len = strlen($filepath);

            $rel_path = substr($directory, $len);

            if ($has_index)
                $return[$key]['index'] = "$rel_path/$ff/index.php";

            if ($has_subpages)
                $return[$key]['subpages'] = Documentation::get_directory_subpages("$directory/$ff/subpages", $filepath);

            if (is_array($subtree) && count($subtree))
                $return[$key]['subfolders'] = $subtree;

        endforeach;

        return $return;
    }


    public static function get_directory_subpages($directory, $filepath) {


        if (!file_exists($directory))
            return;
        $ffs = scandir($directory);


        $len = strlen($filepath);

        $rel_path = substr($directory, $len);

        unset($ffs[array_search('.', $ffs, true)]);
        unset($ffs[array_search('..', $ffs, true)]);

        // prevent empty ordered elements
        if (count($ffs) < 1)
            return;

        $return = [];

        foreach ($ffs as $ff) :
            $key = strtok($ff, ".");
            $return[$key] = "$rel_path/$ff";
        endforeach;

        return $return;

    }

    public static function add_subfolders($branch, $parent_id, $admin_bar) {
        $doc = new Documentation();

        if (array_key_exists('subfolders', $branch)):
            foreach ($branch['subfolders'] as $key => $subfolder):

                $id = strtolower(str_replace(' ', '_', $key));


                $attr = ['id'     => $id,
                         'title'  => $key,
                         'parent' => $parent_id];

                if (array_key_exists('index', $subfolder)):
                    $attr['href'] = $doc->url($subfolder['index']);
                else:
                    $attr['href'] = '#';
                    $attr['meta']['class'] = 'no_children';
                endif;

                $admin_bar->add_node($attr);

                Documentation::add_subfolders($subfolder, $id, $admin_bar);
                Documentation::add_subpages($subfolder, $id, $admin_bar);

            endforeach;
        endif;
    }

    public static function add_subpages($branch, $parent_id, $admin_bar) {
        if (array_key_exists('subpages', $branch)):
            $doc = new Documentation();
            foreach ($branch['subpages'] as $key => $subpage):

                $id = strtolower(str_replace(' ', '_', $key));

                $attr = ['id'     => $id,
                         'title'  => $key,
                         'parent' => $parent_id,
                         'href'   => $doc->url($subpage)];

                $admin_bar->add_node($attr);


            endforeach;
        endif;

    }

    public static function create_help_menu_branch($key, $branch, $admin_bar) {

        // this is the function used to create the inital layer of our menu
        // the document

        $doc = new Documentation();

        $id = strtolower(str_replace(' ', '_', $key));
        $parent_id = $id;

        $attr = ['id'     => $id,
                 'title'  => $key,
                 'parent' => 'infinisite_help'];


        if (array_key_exists('index', $branch)):
            $attr['href'] = $doc->url($branch['index']);
        else:
            $attr['href'] = '#';
            $attr['meta']['class'] = 'no_children';
        endif;

        $admin_bar->add_node($attr);


        Documentation::add_subfolders($branch, $parent_id, $admin_bar);
        Documentation::add_subpages($branch, $parent_id, $admin_bar);


    }


}