<?

namespace TSD_Infinisite;

use CalendR\Exception;

class Error_Handler {

    private $key_url;


    public function __construct() {

        if (!function_exists('acf_add_local_field_group')) {
            return false;
        }

//          we should be running this weather or not we have keys entered
//        $key = get_option('options_sentry_project_id');
//
//        if (!$key) {
//            return false;
//        }
//
//        $this->key_url = $key;
//
        $fields = DB::function_cache('error_handler_fields');

        if (empty($fields))
            $fields = Error_Handler::create_acf_option_fields();

        Error_Handler::register_acf_global_option_page($fields);


        $this->init();


    }


    private static function create_acf_option_fields() {

        // TODO: finish, lol.

        $fields = [['key'          => 'field_is_trackduck_project_id',
                    'label'        => 'TrackDuck Project ID',
                    'name'         => 'trackduck_project_id',
                    'type'         => 'text',
                    'instructions' => 'ID from project config',
                    'wrapper'      => ['width' => 50]],
                   ['key'          => 'field_sentry_project_id',
                    'label'        => 'Sentry Project ID',
                    'name'         => 'sentry_project_id',
                    'type'         => 'text',
                    'instructions' => 'Fed into Raven_Client() init',
                    'wrapper'      => ['width' => 50]],];

        return $fields;

    }

    private static function register_acf_global_option_page($fields) {


        $global_fields = ['key'      => 'group_is_global_integration_ids',
                          'title'    => 'Options',
                          'fields'   => $fields,
                          'location' => [[['param'    => 'options_page',
                                           'operator' => '==',
                                           'value'    => 'is-display-settings',],],],];

        acf_add_local_field_group($global_fields);

    }

    private function init() {

        $client = new \Raven_Client($this->key_url);

        $error_handler = new \Raven_ErrorHandler($client);
        $error_handler->registerExceptionHandler();
        $error_handler->registerErrorHandler();
        $error_handler->registerShutdownFunction();

    }

}