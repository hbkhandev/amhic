<?php

namespace TSD_Infinisite;

class Flash {

    private static function create_message($text = '', $atts = []) {

        $defaults = ['level'       => 'warning',
                     'dismissible' => 1,
                     'raw_html'    => 0];

        $config = \wp_parse_args($atts, $defaults);

        add_action('admin_notices', function() use ($text, $config) {

            $dismiss_class = $config['dismissible'] ? 'is-dismissible' : '';

            $notice_markup = $config['raw_html'] ? $text : "<p>{$text}</p>";

            $notice = "
                <div class='notice {$dismiss_class} notice-{$config['level']}'>
                    $notice_markup
                </div>
            ";

            print $notice;
        });
    }

    static function message($text) {
        Flash::create_message($text, ['level' => 'warning']);
    }

    static function error($text) {
        Flash::create_message($text, ['level' => 'error']);
    }


    static function success($text) {
        Flash::create_message($text, ['level' => 'success']);
    }


    static function warning($text) {
        Flash::create_message($text, ['level' => 'warning']);
    }

    static function debug($op) {

        ob_start();
        Acme::dbg($op, 1);
        $text = ob_get_clean();

        Flash::create_message( $text , ['level'    => 'warning',
                                      'raw_html' => 1]);
    }


}