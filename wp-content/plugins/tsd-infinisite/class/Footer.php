<?

namespace TSD_Infinisite;

class Footer {

    public $img;
    public $primary_menu;
    public $secondary_menu;
    public $cta_menu;
    public $primary_content;
    public $secondary_content;
    public $background_options;
    public $social_networks;

    public $inline_styles;
    public $inline_classes;
    public $responsive_template_html;
    public $menus = [];

    public $search_icon = false;
    public $search_template = false;
    public $search_template_output = '';

    private $display_inline = false;
    private $layout_id;

    public $device_templates;


    public function __construct( $atts = [] ) {

        // TODO: can we optimize this into one big call?

        $footer_image = \wp_get_attachment_image_src( get_option( "options_footer_background_options_image" ), 'full' );

        $this->img            = get_field( "footer_logo", "option" );
        $this->primary_menu   = get_field( "primary_footer_menu", "option" );
        $this->secondary_menu = get_field( "secondary_footer_menu", "option" );
        $this->cta_menu       = get_field( "cta_footer_menu", "option" );

        $this->menus['primary']   = \wp_get_nav_menu_object( $this->primary_menu );
        $this->menus['secondary'] = \wp_get_nav_menu_object( $this->secondary_menu );
        $this->menus['cta']       = \wp_get_nav_menu_object( $this->cta_menu );


        $this->primary_content    = get_field( "footer_primary_content_area", "option" );
        $this->secondary_content  = get_field( "footer_secondary_content_area", "option" );
        $this->social_networks    = Social::prep_social_media_fields_for_templating( get_field( 'footer_social_media_links', 'option' ) );
        $this->background_options = get_field( 'footer_background_options', 'option' );
        $this->device_templates   = get_field( 'footer_device_templates', 'option' );
        $this->search_template    = get_field( "footer_search_icon_template", "options" );
        $this->search_icon        = get_field( "footer_search_icon_display_settings", "options" );


        if ( $this->search_template )
            $this->search_template_output = $this->build_launcher_icon();

        // todo: custom style support? page / post type / global scss box?
        $this->inline_styles = 'feature coming soon?';

        if ( is_array( $atts ) )
            if ( $atts['id'] )
                $this->id = $atts['id'];

        if ( is_numeric( $atts ) )
            $this->id = $atts;

        // determine if id uses inline or outside footer
        // set up IS backend row styless
        // render twig templates for social media icons
        // render twig template for

        $this->inline_styles  = $this->prep_inline_styles();
        $this->inline_classes = $this->prep_inline_classes();


        if ( property_exists( $this, 'id' ) )
            $this->display_inline = Layout::check_for_inline_header_footer( 'footer', $this->id );

        $this->responsive_template_html = $this->build_responsive_template_html();


    }


    private function build_launcher_icon() {

        $template = $_SERVER['DOCUMENT_ROOT'] . $this->search_template;

        ob_start();
        include( $template );

        return ob_get_clean();

    }

    private function prep_inline_styles() {

        $bg = $this->background_options;

        $styles = [];

        if ( $bg['image'] ):
            $styles['background-image']    = "url({$bg['image']['url']})";
            $styles['background-position'] = "center";
            $styles['background-size']     = 'cover';
        endif;

        if ( $bg['opacity'] && $bg['color'] ):
            $hex  = Palette::get_hex_color_from_role( $bg['color'] );
            $rgba = Palette::hex2rgba( $hex, $bg['opacity'] );

            $styles['background-color']      = $rgba;
            $blend_mode                      = $bg['blending'] ? $bg['blending'] : "overlay";
            $styles['background-blend-mode'] = $blend_mode;
        endif;


        $return = '';

        if ( ! count( $styles ) )
            return $return;

        foreach ( $styles as $property => $value )
            $return .= "$property: $value;";


        return $return;

    }

    private function prep_inline_classes() {

        $bg = $this->background_options;

        $classes = [];

        if ( $bg['color'] && ! $bg['opacity'] )
            $classes[] = $bg['color'] . '-background';

        $return = '';

        foreach ( $classes as $class )
            $return .= "$class ";

        return $return;


    }

    private function build_responsive_template_html() {
        //
        //        $transient = DB::function_cache('footer_template_html');
        //
        //        if(!empty($transient)):
        //            return $transient;
        //        endif;


        $footer_menu = Acme::display_templates_per_device_size( $this->device_templates, [ 'vars' => [ 'primary_menu'           => $this->primary_menu,
                                                                                                       'secondary_menu'         => $this->secondary_menu,
                                                                                                       'menus'                  => $this->menus,
                                                                                                       'cta_menu'               => $this->cta_menu,
                                                                                                       'img'                    => $this->img,
                                                                                                       'primary_content'        => $this->primary_content,
                                                                                                       'background_options'     => $this->background_options,
                                                                                                       'secondary_content'      => $this->secondary_content,
                                                                                                       'search_template'        => $this->search_template,
                                                                                                       'search_template_output' => $this->search_template_output,
                                                                                                       'search_icon'            => $this->search_icon,
                                                                                                       'social_media'           => $this->build_responsive_social_media_html(), ] ] );

        return DB::function_cache( 'footer_template_html', $footer_menu );

    }

    public function build_responsive_social_media_html() {

        // dont' run if we don't have networks
        if ( ! $this->social_networks )
            return;

        $loader = new \Twig_Loader_Filesystem( '/' );
        $twig   = new \Twig_Environment( $loader, [ 'debug' => true ] );
        $twig->addExtension( new \Twig_Extension_Debug() );

        $template_filepath = $_SERVER['DOCUMENT_ROOT'] . $this->social_networks['template'];

        try {
            return $twig->render( $template_filepath, [ 'social' => $this->social_networks['select'], ] );
        } catch ( \Exception $e ) {
            return 'twig error';

        }
    }

    public function get_footer() {
        return "
            <div style='margin-top: auto; $this->inline_styles' class='is-footer-container full-width $this->inline_classes'>
                $this->responsive_template_html
            </div>
        ";
    }

    public function hide() {

        /**
         * checking if we should hide the footer on this page
         */

        $hide_globally = false;

        $hide_on_post_type = false;

        $hide_on_taxononomy = false;

        $hide_on_single_post = false;

        if ( property_exists( $this, 'id' ) ):
            $hide_on_single_post = get_field( 'hide_footer', $this->id );

            $_pt = \get_post_type( $this->id );

            $hide_on_post_type = get_field( "is_cpt_{$_pt}_hide_footer", "option" );

        endif;

        if ( $hide_globally || $hide_on_post_type || $hide_on_taxononomy || $hide_on_single_post )
            return true;

        return false;


    }

    public function get_inside_footer() {
        if ( $this->display_inline && ! $this->hide() )
            return $this->get_footer();
    }

    public function get_outside_footer() {
        if ( ! $this->display_inline && ! $this->hide() )
            return $this->get_footer();
    }


    public function get_footer_file() {
        ob_start();
        Footer::get_default_footer_file();

        return ob_get_clean();
    }

    static function get_default_footer_file( $template = 'default' ) {
        include INFINISITE_URI . 'components/layout/footer/default.php';
    }

}
