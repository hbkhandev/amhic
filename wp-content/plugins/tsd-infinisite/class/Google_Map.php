<?php


namespace TSD_Infinisite;

class Google_Map {

    protected $markers = [];
    protected $config = [];
    protected $map_init_js = '';
    protected $map_marker_js = '';
    protected $map_html = '';
    protected $marker_index = 0;
    protected $manual_zoom;
    protected $manual_center;


    public function __construct($config = []) {

        $this->config($config);

    }

    private function config($config) {


        $defaults = ['height'   => '400px',
                     'width'    => '100%',
                     'map_id'   => uniqid('is_google_map_'),
                     'zoom'     => 12,
                     'template' => ['infobox' => '/wp-content/plugins/tsd-infinisite/'],
                     'position' => ['lat' => '38.889931',
                                    'lng' => '-77.009003']];

        $this->config = \wp_parse_args($config, $defaults);


        $this->map_html = "
            <div 
                id='{$this->config['map_id']}' 
                style='
                    height: {$this->config['height']}; 
                    width:  {$this->config['width']};
                    '>
            </div>
        ";

        $this->set_map_js();


    }

    private function set_map_js() {

        $this->map_init_js = "
            <script>
                var map_{$this->config['map_id']} = new google.maps.Map(document.getElementById('{$this->config['map_id']}'), {
                  center: {lat: {$this->config['position']['lat']}, lng: {$this->config['position']['lng']}},
                  zoom: 12
                });
                
                var map_{$this->config['map_id']}_markers = new Array();
            </script>
        ";
    }

    public function update_config($config) {
        $this->config($config);
    }


    public function add_marker($acf_location = [], $title = '') {

        $marker_failure = false;

        foreach (['lat', 'lng'] as $var):

            if (!array_key_exists($var, $acf_location))
                $marker_failure = true;

            if ($acf_location[$var] === "" || $acf_location[$var] === null)
                $marker_failure = true;

        endforeach;

        if($marker_failure)
            return false;

        $this->map_marker_js .= "
        <script>
            var marker_args_{$this->marker_index} = {
              position: {
                  lat: {$acf_location['lat']},
                  lng: {$acf_location['lng']}
              },
              map: map_{$this->config['map_id']}
            };
            
            marker_args_{$this->marker_index}.animation = google.maps.Animation.DROP; 
            
            var map_marker_{$this->marker_index} = new google.maps.Marker(marker_args_{$this->marker_index})
            
            map_{$this->config['map_id']}_markers.push(
                map_marker_{$this->marker_index}
            );
            
            
            var map_marker_{$this->marker_index}_infowindow = new google.maps.InfoWindow({
                content: '{$title}'
            });
            
            map_marker_{$this->marker_index}.addListener('click', function(){
                close_all_windows();
               map_marker_{$this->marker_index}_infowindow.open(
                   map_{$this->config['map_id']},
                   map_marker_{$this->marker_index}
               );
                
                
            });
        </script>
        ";

        $this->marker_index++;
    }

    public function set_center($coords = false) {
        if(!$coords) return;

        $this->manual_center = "map_{$this->config['map_id']}
            .setCenter({lat: {$coords[0]}, lng: {$coords[1]}});";

    }

    public function set_zoom($zoom = 5){
        $this->manual_zoom = $zoom;
    }

    public function get_html() {

        $bound_block = $this->manual_zoom ? '//' : '';
        $zoom_string = $this->manual_zoom ? "map_{$this->config['map_id']}.setZoom({$this->manual_zoom});" : "";
        $center_string = $this->manual_center ? $this->manual_center : '';

        return "
            $this->map_html 
            $this->map_init_js
            $this->map_marker_js
            
            
            <script>
            
            
            function close_all_windows(){
                for(var i = 1; i <= {$this->marker_index}; i++){
                    j = i - 1;
                    window['map_marker_' + j + '_infowindow'].close();
                }
            }
            
            var map_{$this->config['map_id']}_bounds = new google.maps.LatLngBounds();
            
            map_{$this->config['map_id']}_markers.forEach(function(d, i){
                map_{$this->config['map_id']}_bounds.extend(d.getPosition());
            });
            
            
            if(map_{$this->config['map_id']}_markers.length === 1) {
                map_{$this->config['map_id']}
                    .setZoom(16);
                map_{$this->config['map_id']}
                    .setCenter(
                            map_{$this->config['map_id']}_markers[0].getPosition()
                            );
            } else {
             {$bound_block} map_{$this->config['map_id']}.fitBounds(map_{$this->config['map_id']}_bounds);
            }
            
            
            $zoom_string
            $center_string
            
            
            
            </script>
            
            ";
    }

}