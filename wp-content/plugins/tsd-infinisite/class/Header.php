<?

namespace TSD_Infinisite;

class Header {

    public $primary_menu;
    public $secondary_menu;
    public $secondary_menu_branch_toggle;
    public $top_links;
    public $cta_menu;
    public $cta_menu_template;
    public $large_menu;
    public $large_menu_template;
    public $large_menu_branch_toggle;
    public $large_menu_launcher_icon;
    public $overlay_menu;
    public $overlay_menu_template;
    public $overlay_menu_launcher_icon;
    public $header_search_template;
    public $header_search_launcher_icon;
    public $display_inline = false;
    public $offcanvas_menu;
    public $offcanvas_menu_launcher_icon;
    public $background_options;
    public $img;
    public $device_templates;
    public $social_networks;
    public $max_width;
    public $inline_styles;
    public $inline_classes;
    public $responsive_template_html;
    public $launcher_html;
    public $sticky = false;
    public $hidden_from_layout = false;

    public $search_position;


    public function __construct($atts = []) {

        /**
         * Init new header object
         *
         * @param $id       sets the page ID, used to determine placement
         */

        // the header used to just be initalized with an id.
        // TODO: refactor and destroy!
        if (is_numeric($atts)) {
            $this->id = $atts;
        }

        if (is_array($atts)):
            if (array_key_exists('id', $atts)) {
                $this->id = $atts['id'];
            }
        endif;


        $this->primary_menu = get_field("primary_header_menu", "option");
        $this->secondary_menu = get_field("secondary_header_menu", "option");
        $this->secondary_menu_branch_toggle = get_field("secondary_header_menu_branch_toggle", "option");
        $this->top_links = get_field("top_links_header_menu", "option");
        $this->top_links_template = get_field("top_links_header_menu_template", "option");
        $this->cta_menu = get_field("cta_header_menu", "option");
        $this->cta_menu_template = get_field("cta_header_menu_template", "option");
        $this->large_menu = get_field("large_header_menu", "option");
        $this->large_menu_template = get_field("large_header_menu_template", "option");
        $this->large_menu_branch_toggle = get_field("large_header_menu_branch_toggle", "option");

        $this->overlay_menu = get_field("overlay_header_menu", "option");
        $this->overlay_menu_template = get_field("overlay_header_menu_template", "option");

        // building the launcher icon for the overlay menu based on the acf icon group

        $overlay_menu_launcher_icon_acf_group = get_field("header_overlay_icon_launcher_display_settings", "options");
        $overlay_menu_launcher_icon = new Launcher_Icon($overlay_menu_launcher_icon_acf_group, 'overlay');
        $this->overlay_menu_launcher_icon = $overlay_menu_launcher_icon->get_html();


        $this->offcanvas_menu = get_field("offcanvas_header_menu", "option");
        $this->offcanvas_menu_template = get_field("offcanvas_header_menu_template", "option");

        $offcanvas_menu_launcher_icon_acf_group = get_field("header_offcanvas_icon_launcher_display_settings", "options");
        $offcanvas_menu_launcher_icon = new Launcher_Icon($offcanvas_menu_launcher_icon_acf_group, 'offcanvas');
        $this->offcanvas_menu_launcher_icon = $offcanvas_menu_launcher_icon->get_html();


        $this->background_options = get_field("header_background_options", "option");
        $this->device_templates = get_field("header_device_templates", "option");
        $this->social_networks = Social::prep_social_media_fields_for_templating(get_field('header_social_media_links', 'option'));

        $this->header_search_position = get_field("is_header_search_position", "option");
        $this->header_search_template = get_field("is_header_search_template", "option");
        $this->header_search_launcher_icon = $this->build_header_search_launcher_icon();

        // setting up max-width
        $this->max_width = get_field("header_max_width", "option");


        $post_header_img = false;

        if (isset($this->id)):
            // checking the page for an overridden header value
            // if we don't find - we set the header logo to be the global default
            $post_header_img = get_field("header_logo", $this->id);
            $this->display_inline = Layout::check_for_inline_header_footer('header', $this->id);

            // if we are running the header with an id, we can check to see if the header is sticky
            $this->sticky = Header::is_header_sticky($this->id);

        endif;

        $this->img = $post_header_img ? $post_header_img : get_field("header_logo", "option");

        $this->inline_styles = $this->prep_inline_styles();
        $this->inline_classes = $this->prep_inline_classes();

        $this->responsive_template_html = $this->build_responsive_template_html();

        $this->hidden_from_layout = $this->hide();


    }

    public function header_in_first_row() {

        $header_in_first_row = false;

        $header_in_first_row = true;

        return $header_in_first_row;


    }

    public function hide() {

        /**
         * checking if we should hide the footer on this page
         */

        $hide_globally = false;

        $hide_on_post_type = false;

        $hide_on_taxononomy = false;

        $hide_on_single_post = false;

        if (property_exists($this, 'id')) {
            $hide_on_single_post = get_field('hide_header', $this->id);
        }


        $post_type = get_post_type();

        if (is_string($post_type)):
            $hide_on_post_type = get_field("is_cpt_{$post_type}_hide_header", 'options');
        endif;


        if ($hide_globally || $hide_on_post_type || $hide_on_taxononomy || $hide_on_single_post) {
            return true;
        }

        return false;


    }


    private function build_header_search_launcher_icon() {

        $template = $_SERVER['DOCUMENT_ROOT'] . $this->header_search_template;

        $no_template = substr($template, -4, 4) !== '.php' || !file_exists($template);

        if($no_template) return '';

        ob_start();
        include($template);

        return ob_get_clean();

    }


    private function prep_inline_styles() {

        $bg = $this->background_options;
        $styles = [];

        if ($bg['image']):
            $styles['background-image'] = "url({$bg['image']['url']})";
            $styles['background-position'] = "center";
            $styles['background-size'] = 'cover';
        endif;

        if ($bg['opacity'] && $bg['color']):
            $hex = Palette::get_hex_color_from_role($bg['color']);
            $rgba = Palette::hex2rgba($hex, $bg['opacity']);

            $styles['background-color'] = $rgba;
            $blend_mode = $bg['blending'] ? $bg['blending'] : "overlay";
            $styles['background-blend-mode'] = $blend_mode;
        endif;


        $return = '';

        if (!count($styles)) {
            return $return;
        }

        foreach ($styles as $property => $value) {
            $return .= "$property: $value;";
        }


        return $return;

    }

    private function prep_inline_classes() {

        $bg = $this->background_options;

        $classes = [];

        if ($bg['color'] && !$bg['opacity']) {
            $classes[] = $bg['color'] . '-background';
        }

        $return = '';

        foreach ($classes as $class) {
            $return .= "$class ";
        }

        return $return;


    }

    public function change_logo($id = false) {

        /**
         * if we need to override the logo image - we do that here
         */

        if (!$id) {
            return;
        }
        if (!get_post($id) instanceof \WP_Post) {
            return;
        }

        $image = Acme::get_image_info_by_id($id);

        $this->img = $image;
    }


    public function build_responsive_template_html() {

        $transient = DB::function_cache('header_template_html');

        if (!empty($transient))
            return $transient;

        $header_menu = Acme::display_templates_per_device_size($this->device_templates, ['vars' => ['img'                          => $this->img,
                                                                                                    'primary_menu'                 => $this->primary_menu,
                                                                                                    'secondary_menu'               => $this->secondary_menu,
                                                                                                    'secondary_menu_branch_toggle' => $this->secondary_menu_branch_toggle,
                                                                                                    'cta_menu'                     => $this->cta_menu,
                                                                                                    'cta_menu_template'            => $this->cta_menu_template,
                                                                                                    'overlay_menu'                 => $this->overlay_menu,
                                                                                                    'overlay_menu_template'        => $this->overlay_menu_template,
                                                                                                    'overlay_menu_launcher_icon'   => $this->overlay_menu_launcher_icon,
                                                                                                    'offcanvas_menu'               => $this->offcanvas_menu,
                                                                                                    'offcanvas_menu_launcher_icon' => $this->offcanvas_menu_launcher_icon,
                                                                                                    'header_search_template'       => $this->header_search_template,
                                                                                                    'header_search_launcher_icon'  => $this->header_search_launcher_icon,
                                                                                                    'large_menu'                   => $this->large_menu,
                                                                                                    'large_menu_template'          => $this->large_menu_template,
                                                                                                    'large_menu_branch_toggle'     => $this->large_menu_branch_toggle,
                                                                                                    'large_menu_launcher_icon'     => $this->large_menu_launcher_icon,
                                                                                                    'top_links'                    => $this->top_links,
                                                                                                    'top_links_template'           => $this->top_links_template,
                                                                                                    'sticky'                       => $this->sticky,
                                                                                                    'header'                       => $this,
                                                                                                    'social_media'                 => $this->build_responsive_social_media_html(),]]);

        return DB::function_cache('header_template_html', $header_menu);


    }


    static function default_offcanvas() {
        $device_templates = get_field("offcanvas_device_templates", "options");
        return Acme::display_templates_per_device_size($device_templates, ['container_class' => 'full-height']);
    }


    private function build_responsive_social_media_html() {

        // no, no. we aren't doing this anymore.
        return '';

    }

    public function get_responsive_social_media_html() {

        return $this->build_responsive_social_media_html();

    }


    public function get_outside() {

        if ($this->hide()) {
            return false;
        }
        if ($this->display_inline) {
            return false;
        }

        return $this->get_output_html();
    }

    public function display_outisde() {
        print $this->get_outside();
    }

    public function get_inline() {


        if ($this->hide()) {
            return false;
        }
        if (!$this->display_inline) {
            return false;
        }

        return $this->get_output_html();

    }

    public function display_inline() {

        print $this->get_inline();
    }


    public function get_output_html() {
        return "
            <div style='$this->inline_styles' class='ispb_header_container cell $this->inline_classes'>
                $this->responsive_template_html
            </div>
        ";
    }


    public static function display_header_in_module($layout, $index) {

        // TODO: depreciate me! don't use me! use $header->display_in_hero()
        // the only element that will be allowed to contain a header is hero.

        $display_header_elements = false;
        $is_first_element = false;

        // im turning this off for now, how do we set this in externally called modules?
        if (!$layout) {
            return false;
        }

        if ($index['row_index'] == 0 && $index['cell_index'] == 0 && $index['module_index'] == 0) {
            $is_first_element = true;
        }

        return ($is_first_element && $layout->header->hidden_from_layout);


    }

    public function display_in_hero() {


        return true;

    }


    public static function get_responsive_logo_img($thumbnail = false) {

        $desktop = get_field("header_logo", "option");
        $mobile = get_field("header_mobile_logo", "option");

        $desktop_visiblity = $mobile ? 'show-for-medium' : '';

        $desktop_img_src = $thumbnail ? $desktop['sizes'][$thumbnail] : $desktop['url'];
        $mobile_img_src = $thumbnail ? $mobile['sizes'][$thumbnail] : $mobile['url'];

        $desktop_html = "<div class='$desktop_visiblity'>
            <img src='{$desktop_img_src}' />
        </div>";

        $mobile_html = $mobile ? "<div class='hide-for-medium'>
            <img src='{$mobile_img_src}' />
        </div>" : '';


        $return = "<div class='is_header_responsive_logo'>
            $desktop_html $mobile_html
        </div>";

        return $return;


    }

    public function get_header() {
        ob_start();
        Header::get_default_header_file();
        return ob_get_clean();
    }

    public static function get_default_header_file($template = 'default') {
        include INFINISITE_URI . 'components/layout/header/default.php';
    }

    static function is_header_sticky($id = false) {


        $global_sticky = get_field("header_sticky_global", "options");

        return $global_sticky;

    }

}
