<?

namespace TSD_Infinisite;

class Editor {

    public static $formats = 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img';
    public static $header_formats = 'h1,h2,h3,h4,h5,h6';




    public static function toolbar_row_3($buttons) {

        array_unshift($buttons, 'styleselect');
        array_unshift($buttons, 'fontselect');
        array_unshift($buttons, 'fontsizeselect');
        return $buttons;

    }

    public static function admin_init_fn() {
        self::enqueue_editor_styles();
        add_editor_style(INFINISITE_CSS . "/editor.php", 1);
        add_editor_style(INFINISITE_FONT . '/web-fonts-with-css/css/fontawesome-all.min.css');

        // adding in the child theme's css file so we can see text styles in the editor
        if (file_exists(THEME_URI . 'assets/css/app.css'))
            add_editor_style(THEME_DIR . 'assets/css/app.css');


    }

    public static function remove_editor_from_page_and_post_type() {

        if(WPBAKERY_INSTALLED) return;

        remove_post_type_support('page', 'editor');
        remove_post_type_support('post', 'editor');

    }

    public static function enqueue_editor_styles() {
        // todo:: this is firing in non-admin
        //        wp_enqueue_style('custom_editor_style_admin', INFINISITE_CSS . "/editor.php");
        //        wp_enqueue_style('custom-foundation-styles', INFINISITE_CSS . '/is_foundation_styles.css');

        //        wp_enqueue_style('custom-text-styles', INFINISITE_CSS . '/is_text_styles.css');
    }


    public static function add_style($style_group = [], $group_name = 'Custom Template Styles') {


        add_filter("is/editor/update_formats", function($styles) use ($style_group, $group_name) {


            if (array_key_exists('title', $style_group))
                $style_group = [$style_group];

            foreach ($style_group as $styles_to_add):

                $field_to_update = null;

                if(!array_key_exists('selector', $styles_to_add))
                    $styles_to_add['selector'] = self::$formats;

                if($styles_to_add['selector'] == 'headers' || $styles_to_add['selector'] == 'header')
                    $styles_to_add['selector'] = self::$header_formats;

                foreach ($styles as $c => $group)
                    if ($group['title'] === $group_name)
                        $field_to_update = $c;


                if (is_null($field_to_update)):
                    $new_group = ['title' => $group_name,
                                  'items' => [$styles_to_add]];
                    array_unshift($styles, $new_group);

                else:
                    $styles[$field_to_update]['items'][] = $styles_to_add;
                endif;


            endforeach;

            return $styles;

        });

    }


}


class Filter {


    public static function add_specific_menu_location_atts($atts, $item, $args) {
        // check if the item is in the primary menu

        $menus = ['patient_quick_links_menu',
                  'caregiver_quick_links_menu',
                  'medical_professionals_quick_links_menu'];

        if (in_array($args->theme_location, $menus)) {
            // add the desired attributes:
            $atts['class'] = 'button';
        }
        return $atts;
    }

}
