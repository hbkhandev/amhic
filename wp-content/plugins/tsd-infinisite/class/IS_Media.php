<?php

namespace TSD_Infinisite;

class IS_Media {

    public $sizes = [];
    private $size_index = [];
    private $post;
    public $id;
    public $title = '';
    public $description = '';

    public function __construct($id) {

        $this->id = $id;

        $this->size_index = acf_get_image_sizes();

        foreach ($this->size_index as $key => $label):

            $img_info = \wp_get_attachment_image_src($this->id, $key);

            foreach ($img_info as $k => $v):

                $field_key = $k == 0 ? '' : '-' . $this->img_src_lookup($k);
                $this->sizes["{$key}{$field_key}"] = $v;

            endforeach;


        endforeach;


    }

    private function img_src_lookup($val) {


        switch ($val):

            case 0:
                return 'url';
                break;

            case 1:
                return 'width';
                break;

            case 2:
                return 'height';
                break;

            case 3:
                return 'intermediate';
                break;

        endswitch;

    }

}