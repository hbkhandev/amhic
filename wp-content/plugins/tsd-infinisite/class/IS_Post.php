<?php

namespace TSD_Infinisite;

use PhpOffice\PhpSpreadsheet\Calculation\DateTime;

class IS_Post {

    public $wp_obj;
    private $fields;
    public $meta_data;
    public $post_title;
    public $post_type;
    public $permalink;
    public $taxonomies;
    public $terms;
    public $id;
    public $ID;
    public $edit_url;
    public $layout;
    public $comments;
    public $card;

    public function __construct( $post = '' ) {


        if ( ! $post instanceof \WP_Post ) {
            $post = \get_post( $post );
        }


        $this->id         = $post->ID;
        $this->ID         = $post->ID;
        $this->wp_obj     = $post;
        $this->post_type  = $post->post_type;
        $this->post_title = $post->post_title;


        $is_template = \get_option( "options_is_cpt_{$this->post_type}_template" );

        $this->permalink = $is_template === '' ? false : \get_permalink( $post->ID );
        $this->fields    = new ACF_Helper( $this->wp_obj );
        // this is mostly for the api, we're going to use a better name than just "fields"
        $this->meta_data = $this->fields;
        $this->edit_url  = \site_url( "wp-admin/post.php?post={$this->ID}&action=edit" );
        //        if(is_user_logged_in())
        //            $this->prepare_categories();
        //		$this->card = new Card( $this );

    }


    // content renderers

    public function render_php_template( $filepath = '', $vars = [] ) {

        $vars['post'] = $this;

        return Acme::get_file( $filepath, $vars );

    }

    public function get_template( $alt = false ) {

        if ( ! $alt ) {
            $alt = 'excerpt';
        }

        $root_file = Acme::file_exists( "/twig/post_excerpts/{$alt}.php" );
        if ( $root_file ) {
            $template = $alt;
        }
        $post_type_file = Acme::file_exists( "/twig/post_excerpts/{$this->post_type}/{$alt}.php" );
        if ( $post_type_file ) {
            $template = "{$this->post_type}/{$alt}";
        }

        if ( ! $template ) {
            return "no template found - $this->post_type ";
        }

        return $this->render_php_template( "/twig/post_excerpts/{$template}.php" );

    }

    public function print_template( $alt = false ) {
        print $this->get_template( $alt );
    }

    public function gallery_template( $atts = [] ) {

        if ( gettype( $atts ) === 'string' ) {
            $t    = $atts;
            $atts = [ 'template_filename' => $t ];
        }

        $defaults = [ 'template_filename' => 'default',
                      'acf_key'           => 'gallery', ];

        $config = \wp_parse_args( $atts, $defaults );

        $template_filename = $config['template_filename'];

        $root_file = Acme::file_exists( "/components/acf_gallery_template/{$template_filename}.php" );
        if ( $root_file ) {
            $template = $template_filename;
        }

        if ( ! $template ) {
            return "no template found - $this->post_type ";
        }

        $vars['gallery'] = $this->get( $config['acf_key'] );
        $filepath        = "/components/acf_gallery_template/{$template}.php";

        return Acme::get_file( $filepath, $vars );


    }

    public function get_content( $words = 0, $acf_tag = false ) {
        $content = $this->wp_obj->post_content;

        if ( is_string( $words ) ):
            $acf_tag = $words;
            $words   = 0;
        endif;

        if ( ! $content && $acf_tag ) {
            $content = $this->get( $acf_tag );
        }

        if ( $words !== 0 ) {
            $content = \wp_trim_words( $content, $words );
        }

        return \apply_filters( "the_content", $content );
    }

    public function post_excerpt() {
        if ( $this->wp_obj->post_excerpt !== '' )
            return $this->wp_obj->post_excerpt;

        if ( $this->wp_obj->post_content !== '' )
            return wp_trim_words( $this->wp_obj->post_content, 30 );

        return $this->excerpt();
    }

    public function excerpt( $words = 30, $meta = 'excerpt', $more = '' ) {
        return \wpautop( \wp_trim_words( $this->get( $meta ), $words, $more ) );
    }

    public function post_date( $format = 'F jS, Y' ) {
        // returns the formatted value of the WP object's post date

        $d = new \DateTime( $this->wp_obj->post_date );

        return $d->format( $format );
    }

    public function make_simple_breadcrumbs( $menu = false, $separator_icon = 'far fa-angle-right' ) {

        if ( ! $menu ) {
            return null;
        }

        $items = wp_get_nav_menu_items( $menu );
        if ( ! $items ) {
            return false;
        }

        _wp_menu_item_classes_by_context( $items ); // Set up the class variables, including current-classes
        $crumbs = [];

        foreach ( $items as $c => $item ) {
            if ( $item->current_item_ancestor || $item->current ) {
                $crumbs[] = "<a href='{$item->url}' class='is_breadcrumb_{$c}' title='{$item->title}'>{$item->title}</a>";
            }
        }

        if ( count( $crumbs ) == 1 ) {
            return false;
        }

        $separator = $separator_icon != 'none' ? "<i class='$separator_icon space-left space-right'></i>" : '';

        $breadcrumbs = implode( $separator, $crumbs );

        return "<div class='is_post_breadcrumbs simple_breadcrumbs'>$breadcrumbs</div>";


    }

    public function display_comments() {

        ob_start();

        \comments_template();
        $content = ob_get_clean();

        return "
            <div class='is-comments is-comment-{$this->post_type}'>
                $content
            </div>
        ";

    }

    public function display_comment_form() {

        ob_start();

        \comment_form();

        $content = ob_get_clean();

        return "
            <div class='is-comment-form is-comment-form-{$this->post_type}'>
                $content
            </div>
        ";

    }

    public function get_edit_link( $atts = [] ) {

        $defaults = [ 'text'   => 'Edit Post',
                      'class'  => '',
                      'target' => '_blank', ];

        $config = \wp_parse_args( $atts, $defaults );

        return "<a 
				href='{$this->edit_url}' 
				class='{$config['class']}' 
				target='{$config['target']}'>
					{$config['text']}
				</a>";


    }

    public function get_single_view_link( $atts = [] ) {

        $defaults = [ 'text'   => 'View Post',
                      'class'  => '',
                      'target' => '_blank', ];

        $config = \wp_parse_args( $atts, $defaults );

        return "<a 
				href='{$this->permalink}' 
				class='{$config['class']}' 
				target='{$config['target']}'>
					{$config['text']}
				</a>";


    }


    public function publication_date_obj() {
        return new \DateTime( $this->wp_obj->post_date );
    }


    public function publication_date( $format = 'F jS, Y' ) {
        $t = new \DateTime( $this->wp_obj->post_date );

        return $t->format( $format );
    }


    // meta formatters

    public function get( $key, $fallback = false ) {

        return $this->fields->get( $key, $fallback );


    }

    public function get_fields() {

        // this is a backwards compatability thing. don't use this.

        return $this->fields;
    }

    public function get_start_date( $field = 'start_date' ) {
        return $this->fields->get_start_date( $field );
    }


    public function get_end_date( $field = 'end_date' ) {
        return $this->fields->get_end_date( $field );
    }

    public function date_range( $elem = 'p', $atts = [] ) {
        return $this->fields->print_date_range( $elem, $atts );
    }

    public function display_link( $atts = [] ) {

        // TODO: write in proper override configs for url, title and target

        $defaults = [ 'acf_field' => 'link', ];
        $config   = \wp_parse_args( $atts, $defaults );
        $field    = $this->get( $config['acf_field'] );

        return "<a href='{$field['url']}' 
                    class='button' 
                    target='{$field['target']}' 
                    title='{$field['title']}'
                >
                    {$field['title']}
                </a>";


    }

    public function output( $field = false, $elem = 'p', $atts = [] ) {
        return $this->fields->output( $field, $elem, $atts );
    }

    public function get_image( $acf_field = 'image', $atts = [] ) {

        // this is the same as a standard image call, but it returns the
        // post type default image if the requested acf image isn't found

        $defaults = [ 'feat_img_check' => true,
                      'tn_size'        => 'large' ];

        $config = wp_parse_args( $atts, $defaults );

        if ( $this->get( $acf_field ) ) {
            return $this->get( $acf_field );
        }

        $_pt = $this->wp_obj->post_type;

        // this doesn't work because it returns the url in the wrong format
        if ( $config['feat_img_check'] && $this->has_post_thumbnail() )
            return $this->get_featured_image_url( $config['tn_size'] );

        return \get_field( "is_cpt_{$_pt}_default_image", "options" );

    }

    public function image_ratio( $acf_field = 'image', $size = 'medium' ) {
        $img = $this->get( $acf_field );
        $_pt = $this->wp_obj->post_type;
        if ( ! $_pt ) {
            $img = \get_field( "is_cpt_{$_pt}_default_image", "options" );
        }

        return $img['sizes']["$size-width"] / $img['sizes']["$size-height"];

    }


    public function has_post_thumbnail() {
        return has_post_thumbnail( $this->wp_obj );
    }

    public function get_featured_image( $size = 'medium' ) {
        return get_the_post_thumbnail( $this->wp_obj, $size );
    }

    public function get_featured_image_url( $size = 'medium' ) {
        return get_the_post_thumbnail_url( $this, $size );
    }

    public function get_default_image( $size = 'large', $atts = [] ) {

        $_pt = $this->wp_obj->post_type;

        $_field = \get_field( "is_cpt_{$_pt}_default_image", "options" );

        if ( $size === false ) {
            return $_field;
        }

        return $_field['sizes'][ $size ];

    }

    public function download_link( $meta_key = 'file', $text = "Download Here", $classes = "button" ) {

        $file    = $this->get( $meta_key );
        $file_id = $file['id'];

        //        \Kint::dump($file);

        return "<a href='/?ismdc={$file['url']}' class='$classes'>$text</a>";
    }

    public function build_svg( $acf_field = 'icon' ) {


        /**
         * This is just a wrapper class for the SVG parser
         * library. We may expand on it later.
         */

        // todo: allow to be built from filepath
        // todo: allow to be built from external url

        $icon = $this->get( $acf_field );

        if ( ! $icon ) {
            return false;
        }

        $icon = new SVG( $icon );

        return $icon;

    }

    public function wp_excerpt( $word_count = 35, $read_more = '' ) {

        $str = $this->wp_obj->post_excerpt !== '' ? $this->wp_obj->post_excerpt : $this->wp_obj->post_content;


        if ( $str === '' ) {
            return '<p>No Content</p>';
        }

        return wpautop( \wp_trim_words( $str, $word_count, $read_more ) );


    }


    // taxonomoy formatters

    public function has_terms( $tax ) {

        if ( ! is_array( $this->terms ) ) {
            return false;
        }

        if ( ! array_key_exists( $tax, $this->terms ) ) {
            return false;
        }

        if ( ! count( $this->terms[ $tax ] ) ) {
            return false;
        }

        return true;

    }

    public function prepare_categories( Array $cat = [] ) {
        $_taxes = get_taxonomies();

        foreach ( $_taxes as $_tax ):

            // we only look up the terms we need
            if ( ! in_array( $_tax, $cat ) )
                continue;

            // we skip terms that we already have
            // and terms that don't exist
            if ( is_array( $this->terms[ $_tax ] ) && count( $this->terms[ $_tax ] ) )
                continue;

            $_t     = \wp_get_post_terms( $this->id, $_tax, [ 'orderby' => 'count',
                                                              'order'   => 'desc', ] );
            $_terms = [];
            foreach ( $_t as $_term ) {
                $_terms[] = new IS_Term( $_term );
            }
            if ( $_terms == [] ) {
                continue;
            }
            $this->terms[ $_tax ]      = $_terms;
            $this->taxonomies[ $_tax ] = \get_taxonomy( $_tax );
        endforeach;
    }

    public function category_list_html() {

        $op = '';

        if ( ! property_exists( $this, 'term' ) ) {
            return false;
        }

        foreach ( $this->terms as $cat => $terms ):

            $termlist = '';
            foreach ( $terms as $term ):
                $url      = \get_term_link( $term );
                $termlist .= "<li><a href='$url'>$term->name</a></li>";
            endforeach;

            $op .= "<h6>{$this->taxonomies[$cat]->label}</h6>" . $termlist;

        endforeach;

        return $op;

    }

    public function list_taxonomy( $tax, $atts = [] ) {

        $defaults = [ 'limit'         => - 1,
                      'labelText'     => false,
                      'labelClass'    => 'gray_xdark-text',
                      'termClass'     => '',
                      'groupHoverRed' => false,
                      'noLabel'       => false ];

        $config = \wp_parse_args( $atts, $defaults );

        if ( ! array_key_exists( $tax, $this->taxonomies ) )
            return '';

        $cat = $this->taxonomies[ $tax ];

        if ( ! $cat )
            return '';


        $termNames = [];

        $op = '';

        foreach ( $this->terms[ $tax ] as $term ):
            $url         = get_term_link( $term );
            $hover       = $config['groupHoverRed'] ? 'hover:text-red-500 text-gray-200' : '';
            $link        = "<a href='$url' class='$hover'>{$term->name}</a>";
            $termNames[] = $term->count !== 1 ? $link : $term->name;
        endforeach;

        if ( $config['limit'] !== - 1 )
            $termNames = array_splice( $termNames, 0, $config['limit'] );

        $termNames = implode( ', ', $termNames );

        if ( $cat->label === 'Contributor' && ! $config['labelText'] )
            $config['labelText'] = 'Author';
        $termLabel = $config['labelText'] ? $config['labelText'] : $cat->label;


        $op .= $config['noLabel'] ? "<h6 class='{$config['termClass']}'>$termNames</h6>" : "<h6 class='{$config['labelClass']}'><strong>{$termLabel}: </strong><span class='{$config['termClass']}'>{$termNames}</span></h6>";

        $op = "<div class='tsd-term-container'>$op</div>";

        return $op;
    }

    // term formatters


    // image helpers

    public function get_ratio_of_image( $img = [] ) {

        return $img['width'] / $img['height'];

    }

    public function get_square_image( $atts = [] ) {

        $function_defaults = [ 'ratio' => 1 ];

        $config = \wp_parse_args( $atts, $function_defaults );

        return self::gallery_image_by_ratio( $config );

    }

    public function get_image_by_ratio( $ratio = 1 ) {
        return $this->gallery_image_by_ratio( [ 'ratio' => $ratio ] );
    }

    public function gallery_image_by_ratio( $atts = [] ) {

        // goes through an acf gallery type and pulls out
        // a specified square image, or failing that - we
        // can get the closest size

        $defaults = [ 'index'       => 0,
                      'get_closest' => true,
                      'ratio'       => 1,
                      'acf_key'     => 'gallery', ];

        $config = \wp_parse_args( $atts, $defaults );

        $ratios = [];
        foreach ( $this->get( "gallery" ) as $img ) {
            $ratios[] = $img['width'] / $img['height'];
        }

        $matches = [];

        foreach ( $this->get( "gallery" ) as $key => $img ) {
            if ( $img['width'] / $img['height'] === $config['ratio'] ) {
                $matches[] = $key;
            }
        }

        if ( count( $matches ) ) {
            return $this->get( "gallery" )[ $matches[ $config['index'] ] ];
        }

        if ( ! $config['get_closest'] ) {
            return false;
        }

        // todo: if we're down to the nearest, we only return the first image - regardless of config

        $nearest_index = \TSD_Infinisite\Acme::nearest_in_array( $config['ratio'], $ratios );
        $nearest_key   = array_search( $nearest_index, $ratios );

        return $this->get( 'gallery' )[ $nearest_key ];

    }

    // post updaters

    public function save( $wp_post, $fields = [] ) {

        $fields = false;

        if ( count( $wp_post['wp_post_obj'] ) ):


            if ( $wp_post['fields'] ) {
                $fields = $wp_post['fields'];
            }

            $wp_post = $wp_post['wp_post_obj'];
        endif;

        if ( ! $this->post_type ) {
            $this->post_type = $wp_post['post_type'];
        }
        $this->id = $this->save_wp_post( $wp_post );

        if ( $fields ) {
            $this->save_acf_fields( $fields );
        }


        return new IS_Post( $this->id );

    }

    public function save_wp_post( $wp_post ) {

        $id = \wp_insert_post( $wp_post );

        return new IS_Post( $id );

    }

    public function save_acf_fields( $fields ) {

        $pt = $this->post_type;

        foreach ( $fields as $key => $value ) {
            \update_field( "field_{$pt}_{$key}", $value, $this->id );
        }

    }


    // quality of life functions

    public function edit_link( $atts = [] ) {
        if ( ! is_user_logged_in() ) {
            return '';
        }
        $defaults = [ 'text'   => 'Edit ' . ucwords( $this->post_type ),
                      'target' => '_blank', ];
        $config   = \wp_parse_args( $atts, $defaults );
        $url      = get_edit_post_link( $this->ID );

        return "<a href='$url' target='{$config['target']}'>{$config['text']}</a>";
    }


    // here be depreciated functions
    public function get_pagebuilder_content() {

        // TODO: make this check to make sure it's actually wpb content

        if ( $this->wp_obj->post_content != '' ) {
            return do_shortcode( $this->wp_obj->post_content );
        }

        return false;
    }

    public function build_layout() {

        $this->layout = new Layout( $this->ID );


    }

    public static function db_get( $id ) {

        // this is an early version of a cache-friendly getter

        // TODO: make it.

        return new IS_Post( \get_post( $id ) );


    }


    // dear future gregg: don't put code down here.

}
