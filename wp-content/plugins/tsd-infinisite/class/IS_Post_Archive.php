<?

namespace TSD_Infinisite;

class IS_Post_Archive {

    /**
     * IS_Post_Archive constructor.
     *
     * Post Archive update project
     *
     * Make archives independently searchable / filterable
     *
     * Make archives independently paginatable
     *
     * Allow for multiple post types
     *
     */

    public static $counter = 0;

    private $output_html;
    private $template_html;
    private $filter_html;
    private $filter_placement;
    public $filter_acf = [];
    private $pagination_html;
    public $id_tag;
    private $module;
    private $post_types;
    private $current_page;
    public $query_args;
    private $config;
    private $filter_template;
    private $row_padding = '';
    public $posts = [];

    private $padding_x = false;
    private $padding_y = false;

    private $hash_key = false;

    public $query;

    public function __construct($module) {
        $this->module = $module;

        $this->init_setup();

        $this->get_user_options();
        $this->check_for_manual_pages();
        $this->set_query();
        $this->add_filters();
        $this->add_special_filters();
        $this->hide_future_check();
        $this->hide_past_check();
        $this->old_code_date_check();
        $this->set_up_search();
        $this->add_manual_arguments();

        $this->execute_query();
        $this->columns = Acme::generate_columns($module['posts_per_row'], 1);

        $this->build_twig_query_template();
        $this->build_twig_excerpt_templates();
        $this->build_no_results();
        $this->build_filter();

        $this->build_pagination();


        if ($this->filter_placement == 'none') {
            $this->load_no_filter_layout();
        }

        if ($this->filter_placement == 'left') {
            $this->load_left_filter_layout();
        }

        if ($this->filter_placement == 'top') {
            $this->load_top_filter_layout();
        }

        // this is a default - in case no filter placement is set
        if (!$this->filter_placement || $this->filter_placement == '') {
            $this->load_no_filter_layout();
        }


    }

    private function init_setup() {

        self::$counter++;

        $this->id_tag = "archive_" . self::$counter;

        // if the user has submitted pagination or filter requests, we pass the args
        // of the archive to the url as an array with the key equal to the id of the
        // post archive module

        if (isset($_GET[$this->id_tag])) {
            $this->config = $_GET[$this->id_tag];
        }

        $this->post_types[] = $this->module['post_type'] == 'is_custom_posts' ? 'any' : $this->module['post_type'];

        // TODO: update
        $this->current_page = 1;

        $this->padding_x = $this->module['padding']['x'];
        $this->padding_y = $this->module['padding']['y'];

        $this->filter_acf['filter_title'] = $this->module['filter_display_config']['post_archive_filter_display_config_filter_title'];
        $this->filter_acf['searchwp_engine'] = $this->module['filter_display_config']['post_archive_filter_display_config_searchwp_engine'];
        $this->filter_acf['keyword_search'] = $this->module['filter_display_config']['post_archive_filter_display_config_keyword_search'];


    }

    private function execute_query() {

        $hash = md5(serialize($this->query_args));
        $transient_label = "query_$hash";
        $transient_post_label = "query_{$hash}_posts";
        $transient = DB::function_cache($transient_label);


        if (!empty($transient)):

            $transient = DB::function_cache($transient_label);
            $transient_posts = DB::function_cache($transient_post_label);
            $this->query = $transient;

            foreach ($transient_posts as $transient_post)
                $this->posts[] = $transient_post;

            return;

        else:


            /**
             * with all that done, it's time to set what page we're on
             * and do our query!
             */

            if (array_key_exists('s', $this->query_args)):
                $this->query = new \SWP_Query($this->query_args);
            else:
                $this->query = new \WP_Query($this->query_args);
            endif;


            foreach ($this->query->posts as $post)
                $this->posts[] = new IS_Post($post);

            DB::function_cache($transient_label, $this->query);
            DB::function_cache($transient_post_label, $this->posts);

        endif;

    }


    private function build_pagination() {

        /**
         * pagination section
         */

        // setting our variable
        $pagination_html = '';

        // our default state is to turn it off
        $show_pagination = false;

        // if the user has requested pagination, we will display it
        if ($this->module['pagination']) {
            $show_pagination = true;
        }

        // in this case, the module has found all the posts that it's looking
        // for and no pagination will ever be needed, even if the toggle is on
        if ($this->query->found_posts = $this->module['posts_per_page']) {
            $show_pagination = false;
        }

        if ($this->query->max_num_pages > 1 && $this->module['pagination']) {
            $show_pagination = true;
        }

        if (!$show_pagination) {
            return;
        }


        $pagination = Acme::get_pagination($this->query, $this);

        $this->pagination_html = "
            <div class='cell archive-pagination'>
               $pagination
            </div>
        ";


    }

    private function build_filter() {

        /*
        * this is the filter system, we boot up a new twig for it,
        * get the vars from the db and go for it
        */

        $filter_repeater = $this->module['filter_repeater'];


        if (!$filter_repeater) {
            return;
        }

        $filter_display_config = $this->module['filter_display_config'];
        $this->filter_placement = $filter_display_config['post_archive_filter_display_config_filter_placement'];

        $taxonomy_info_for_is_post_filter = [];
        foreach ($filter_repeater as $c => $taxonomy):

            $taxonomy_info_for_is_post_filter[$c]['taxonomy'] = get_taxonomy($taxonomy['post_archive_filter_repeater_taxonomy']);
            $taxonomy_info_for_is_post_filter[$c]['terms'] = get_terms($taxonomy['post_archive_filter_repeater_taxonomy'], ['hide_empty' => 1]);
            $taxonomy_info_for_is_post_filter[$c]['config'] = $taxonomy;
        endforeach;

        $loader = new \Twig_Loader_Filesystem('/');

        $twig = new \Twig_Environment($loader, ['debug' => true]);

        $twig->addExtension(new \Twig_Extension_Debug());

        $this->filter_template = $_SERVER['DOCUMENT_ROOT'] . $filter_display_config['filter_template'];

        // if we're using a php template, we can just grab the file and use the contents.

        if (substr($this->filter_template, -3, 3) === 'php'):

            // once again, if we're using a php template, we can just grab the
            // file and use the contents.

            ob_start();
            include($this->filter_template);
            $this->filter_html = ob_get_clean();
            return;

        endif;

        try {

            $this->filter_html = $twig->render($this->filter_template, ['filter_info'     => $taxonomy_info_for_is_post_filter,
                                                                        'display'         => $filter_display_config,
                                                                        'current_keyword' => $this->config['keyword'],
                                                                        'post'            => $_POST,
                                                                        'get'             => $_GET,
                                                                        'id'              => $this->id_tag,
                                                                        // TODO: get this running
                                                                        // 'query_string_form_fields' => $query_string_form_fields,
                                                                        'query_string'    => http_build_query($_GET)]);
        } catch (\Exception $e) {
            if (is_user_logged_in()) {
                $this->filter_html = '<p>Error getting filter template</p>';
            }
        }

    }

    private function build_twig_query_template() {

        if ($this->module['template_type'] != 'query') {
            return;
        }

        $template = $this->module['post_archive_query_template'];

        if (substr($template, -3, 3) == 'php'):

            /**
             * this feels dirty. we shouldn't be processing this, but
             * it makes the system less infinite, so we will include
             * code to catch and process php files.
             */

            $this->columns = '';
            ob_start();
            include($_SERVER['DOCUMENT_ROOT'] . $template);
            $this->template_html = ob_get_clean();

            return;
        endif;


        $loader = new \Twig_Loader_Filesystem('/');

        $twig = new \Twig_Environment($loader, ['debug' => true]);
        $twig->addExtension(new \Twig_Extension_Debug());


        foreach ($this->query->posts as $c => $post):
            $post->post_type_config = Option::get_is_post_type_config_options($post->post_type);
            $this->query->posts[$c] = new IS_Post($post);
        endforeach;

        $twig_filepath = $_SERVER['DOCUMENT_ROOT'] . $this->module['post_archive_query_template'];

        try {
            $this->template_html .= $twig->render($twig_filepath, ['query'       => $this->query->query_vars,
                                                                   'posts'       => $this->query->posts,
                                                                   'found_posts' => $this->query->found_posts,]);
        } catch (\Exception $e) {

            $dbg = @\Kint::dump($e);

            $this->template_html = "
                <div class='cell'>
                    <h2>Twig Error - post archive - query style</h2>
                    <p>Seeking {$this->module['post_archive_query_template']}</p>
                    $dbg
                </div>
                ";
        }

    }

    private function build_twig_excerpt_templates() {

        if ($this->module['template_type'] != 'excerpt') {
            return;
        }
        if (!$this->query->posts) {
            return;
        }


        /*
        *
        * TODO: refactor to ... new class? template class?
        *
        * there are three places we need to check for templates -
        * IS twig/post/excerpts
        * Child theme twig/post/excerpts
        * Global User Template Area
        *
        * i don't think we have the user templates hooked up yet -
        * so, we're loading twig files from two places
        *
        *
        */


        if ($this->padding_x) {
            $this->row_padding .= "grid-padding-x ";
        }

        if ($this->padding_y) {
            $this->row_padding .= "grid-padding-y";
        }


        foreach ($this->posts as $post):
            $template = array_key_exists('template', $this->module) ? $this->module['template'] : false;
            $this->template_html .= IS_Post_Archive::build_twig_template($post, $template, $this->module['post_archive_excerpt_template']);
        endforeach;


    }


    public static function build_twig_template($post, $template, $default_template = '') {

        /**
         * this function has been refactored out of the main object
         * so that we can use it in a debug page
         *
         * it takes an single WP Post, and returns the html that is
         * sent to the browser
         */


        $return = '';
        $fields = DB::post_meta($post->ID);


        $public = Acme::does_post_type_have_single_view_enabled($post);
        $post->permalink = $public ? \get_permalink($post->ID) : false;

        $template_to_render = $template ? $template : $default_template;
        $template_to_render = $_SERVER['DOCUMENT_ROOT'] . $template_to_render;

        $taxonomies = get_object_taxonomies($post->post_type, 'objects');

        $fields = new ACF_Helper($post);


        $taxonomy_info = [];

        foreach ($taxonomies as $taxonomy):

            $terms = \wp_get_post_terms($post->ID, $taxonomy->name);

            $taxonomy_info[] = ['terms'    => $terms,
                                'taxonomy' => $taxonomy,];

        endforeach;


        $default_image = DB::acf_cache("options_is_cpt_{$post->post_type}_default_image");


        $loader = new \Twig_Loader_Filesystem('/');


        if (substr($template_to_render, -3, 3) == 'php'):

            /**
             * this feels dirty. we shouldn't be processing this, but
             * it makes the system less infinite, so we will include
             * code to catch and process php files.
             */

            ob_start();
            include($template_to_render);
            $html = ob_get_clean();

            return $html;
        endif;


        $twig = new \Twig_Environment($loader, ['debug' => true]);
        $twig->addExtension(new \Twig_Extension_Debug());

        try {
            $return .= $twig->render($template_to_render, ['post'             => $post,
                                                           'fields'           => $post->get_fields(),
                                                           'default_image'    => $default_image,
                                                           'permalink'        => $post->permalink,
                                                           'categories'       => $taxonomy_info,
                                                           'post_type_config' => Option::get_is_post_type_config_options($post->post_type)]);
        } catch (\Exception $e) {

            $heading = is_user_logged_in() ? 'Twig Render Error IS_Post_Archive::build_twig_excerpt_templates()' : 'Template error, please contact your web admin';
            $message = is_user_logged_in() ? @\Kint::dump($e) : '';


            $return = "
                <div class='cell'>
                    <h2>$heading</h2>
                    <p>$message</p>
                </div>
                ";
        }

        return $return;

    }


    private function build_no_results() {


        if ($this->query->found_posts)
            return;

        $this->template_html .= "";

        if (!is_user_logged_in())
            return;

        $this->template_html .= "<h2>No Posts Found</h2>";

        // TODO: set this to check if the user is looking to view debug messages

        $this->template_html .= \Kint::dump($this->query);
    }

    private function set_up_search() {


        if (!isset($this->config['keyword']) || $this->config['keyword'] == '') {
            return;
        }

        /**
         * Search WP Query
         *
         * If we're running a keyword search, we need to declare a
         * separate object for the query to use Search WP.
         *
         * We also use this time to add our search query and filter vars.
         *
         */

        $this->query_args['s'] = sanitize_text_field($this->config['keyword']);
        $this->query_args['engine'] = sanitize_text_field($this->config['engine']);

        // swp needs to be able to search the attachment type in order to search pdfs
        $this->query_args['post_type'][] = 'attachment';


    }

    private function add_manual_arguments() {

        /**
         *  Adding Manual Arguments
         *
         *  This looks through the "Manual Arguments" tab in the archive and adds the JSON elements to the PHP Query object
         */


        $manual_query_args = $this->module['custom_query'];

        if (!$manual_query_args) {
            return;
        }

        $new_args = json_decode($manual_query_args, 1);

        if ($new_args === null) {
            $new_args = json_decode("{" . $manual_query_args . "}", 1);
        }


        if (!count($new_args)) {
            return;
        }

        // This will overwrite existing key / value pairs generated by other functions.

        foreach ($new_args as $key => $value):
            if ($key == 'offset') {
                $value = intval($value);
            }
            $this->query_args[$key] = $value;
        endforeach;


    }

    private function add_filters() {

        if (!isset($this->config['filters'])) {
            return;
        }
        $this->filters = $this->config['filters'];

        foreach ($this->filters as $terms):
            $tax_query = ['field' => 'slug',];
            foreach ($terms as $label => $types):
                $tax_query['taxonomy'] = $label;
                if (is_array($types)):
                    foreach ($types as $type):
                        if ($type != '') {
                            $tax_query['terms'][] = $type;
                        }
                    endforeach;
                    continue;
                endif;
                if ($types != '') {
                    $tax_query['terms'] = $types;
                }
            endforeach;

            $this->query_args['tax_query'][] = $tax_query;

        endforeach;


    }

    private function hide_future_check() {


        $_hide_future = $this->module['hide_future_posts'];

        if (!$_hide_future) {
            return;
        }

        $this->query_args['meta_query'][] = ['key'     => 'start_date',
                                             'value'   => date('Ymd'),
                                             'compare' => '<',
                                             'type'    => 'date'];
    }

    private function hide_past_check() {


        $_hide_past = $this->module['hide_past_posts'];

        if (!$_hide_past) {
            return;
        }

        $this->query_args['meta_query'][] = ['key'     => 'start_date',
                                             'value'   => date('Ymd'),
                                             'compare' => '>',
                                             'type'    => 'date'];


    }

    private function old_code_date_check() {


        $_df = $this->module['date_filter'];

        if ($_df === null) {
            return;
        }

        if (is_numeric($_df)):
            // this is kept here for old code
            // 1 = future; 0 = past;
            $this->query_args['order'] = $_df ? 'ASC' : 'DESC';
            $this->query_args['meta_query'][] = ['key'     => 'start_date',
                                                 'value'   => date('Ymd'),
                                                 'compare' => $_df ? '>' : '<',
                                                 'type'    => 'date'];

            return;
        endif;
    }


    private function add_special_filters() {

        /**
         * This is where we set up the options for how the archive handles date sorting.
         * Right now, we only sort by start date. We may expand that, but we probably don't
         * need to.
         */

        // if the field is left blank, leave the function

        $special_filter = $this->module['date_filter'];

        if ($special_filter == null) {
            return;
        }

        if ($special_filter == '') {
            return;
        }


        if (substr($special_filter, 0, 11) === 'start_date_'):

            $meta_field_to_use = "start_date";
            $this->query_args['meta_key'] = $meta_field_to_use;
            $this->query_args['orderby'] = 'meta_value_num';
            $order = substr($special_filter, 11);
            $this->query_args['order'] = $order === 'ascending' ? 'ASC' : 'DESC';

        endif;


        if (substr($special_filter, 0, 10) === 'last_name_') :
            $order = substr($special_filter, 10);

            $this->query_args['orderby'] = 'wpse_last_word';

            $this->query_args['order'] = $order === 'a_to_z' ? 'ASC' : 'DESC';

        endif;


        if (substr($special_filter, 0, 11) === 'post_title_') :
            $this->query_args['orderby'] = 'title';
            $this->query_args['order'] = substr($special_filter, 11) == 'a_to_z' ? 'ASC' : 'DESC';
        endif;


    }

    private function check_for_manual_pages() {

        // if you haven't set the post type to custom, then we don't
        // need to do anything in this function


        if ($this->module['post_type'] != 'is_custom_posts') {
            return;
        }

        if ($this->module['custom_posts'] == '') {
            return;
        }

        // if you have - we just need to get the posts that you specified.
        foreach ($this->module['custom_posts'] as $custom_post) {
            $this->query_args['post__in'][] = $custom_post->ID;
        }

        $this->query_args['orderby'] = 'post__in';

    }

    private function get_user_options() {
        // TODO: parse JSON from the module and set as options
        $this->user_options = [];
    }

    private function set_query() {

        $defaults = ['posts_per_page' => $this->module['posts_per_page'],
                     'post_type'      => $this->post_types,
                     'page'           => $this->current_page];


        $config = wp_parse_args($this->user_options, $defaults);

        foreach ($config as $key => $value) {
            $this->query_args[$key] = $value;
        }


        $this->query_args['page'] = $this->config['paged'] ? $this->config['paged'] : 1;
        $this->query_args['paged'] = $this->config['paged'] ? $this->config['paged'] : 1;

        if ($this->module['post_type'] == 'is_custom_posts') {
            $this->query_args['post_type'] = 'any';
        }

    }

    public function get_content() {
        return $this->output_html;
    }

    private function load_no_filter_layout() {

        $this->output_html = "
            <div class='is-post-excerpt'>
                <div class='grid-x'>
                    <div class='cell'>
                        <div class='grid-x $this->columns $this->row_padding'>
                            $this->template_html
                        </div>
                        <div class='grid-x'>
                            $this->pagination_html
                        </div>
                    </div>
                </div>
            </div>
        ";

    }

    private function load_left_filter_layout() {
        $this->output_html = "<div class='is-post-excerpt'>
                <div class='grid-x'>
                    <div class='cell small-12 medium-3 large-2'>
                        $this->filter_html
                    </div>
                    <div class='cell small-12 medium-9 large-10'>

                        <div class='grid-x $this->columns grid-padding-x grid-padding-y $this->row_padding'>
                            $this->template_html
                        </div>
                        <div class='grid-x grid-margin-x grid-padding-y center-text'>
                            $this->pagination_html
                        </div>
                    </div>
                </div>
            </div>";
    }

    private function load_top_filter_layout() {
        $this->output_html = "
        <div class='is-post-excerpt grid-container full'>
                $this->filter_html
                <div class='grid-x $this->columns grid-padding-x grid-padding-y $this->row_padding'>
                    $this->template_html
                </div>
                <div class='grid-x grid-margin-x grid-padding-y center-text'>
                    $this->pagination_html
                </div>
            </div>
            ";
    }

}
