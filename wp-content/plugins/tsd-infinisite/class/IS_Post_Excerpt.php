<?php


namespace TSD_Infinisite;

class IS_Post_Excerpt {

    public static function debug_test(){


        $post_types = Acme::get_post_types();
        $excerpt_templates = Acme::get_templates('twig/post_excerpts');

        $posts = [];

        foreach($post_types as $post_type):

            $query = new \WP_Query([
                'post_type' => $post_type['post_type'],
                'posts_per_page' => 1,
            ]);

            $posts[$post_type['post_type']] = $query->posts;
        endforeach;


        include(INFINISITE_URI . 'assets/acme/is_post_excerpt_debugger_template.php');


    }





}