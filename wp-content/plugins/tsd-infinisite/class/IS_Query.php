<?php

namespace TSD_Infinisite;

use CalendR\Calendar;

class IS_Query extends \WP_Query {


    /**
     * not sure exactly what i want to do with this just yet
     */

    protected $query_args = [];
    protected $wp_query = [];


    public $posts = [];
    public $has_posts;
    public $is_search = false;
    public $pages;
    public $query_id = 'is_query';
    public $ignore_search = false;
    public $calendar = false;

    function __construct( $atts = [] ) {

        if ( array_key_exists( 'is_query_id', $atts ) )
            $this->query_id = $atts['is_query_id'];

        if ( array_key_exists( 'is_ignore_search', $atts ) )
            $this->ignore_search = $atts['is_ignore_search'];

        $this->set_query_args( $atts );
        $this->set_search_args();
        $this->execute();


    }

    public static function get_from_relationship( $relationship = [] ) {
        if ( $relationship === [] )
            return false;

        $temp_id = [];
        foreach ( $relationship as $post )
            $temp_id[] = $post->ID;

        return self::get_by_ids( $temp_id );
    }

    public static function get_by_ids( $ids = [], $count = - 1 ) {
        // this function allows you to pass in an
        // array of post ids and get a query back

        if ( ! $ids )
            return false;

        return new IS_Query( [ 'post_type'      => 'any',
                               'post__in'       => $ids,
                               'posts_per_page' => $count ] );
    }

    public static function get_query() {
        return new IS_Query();
    }

    public function pagination() {

        $id = $this->query_id;

        $this->query_vars;

        $page = 1;

        // our pagination is done on the query string
        // todo: move pagination off the query string
        $get = $_GET;

        if ( array_key_exists( $id, $get ) ):
            if ( array_key_exists( 'paged', $get[ $id ] ) ):
                $page = $get[ $id ]['paged'];
                unset( $get[ $id ]['paged'] );
            endif;
        endif;

        $query_string = http_build_query( $this->query_vars );

        $pg_args = [ 'base'    => "?{$id}[paged]=%_%&{$query_string}",
                     'format'  => "%#%",
                     'type'    => 'list',
                     'current' => max( 1, $page ),
                     'total'   => $this->wp_query->max_num_pages ];

        return paginate_links( $pg_args );


    }

    public function get_wp_query() {
        return $this->wp_query;
    }

    public function get_args() {
        return $this->query_args;
    }

    private function set_query_args( $atts ) {

        $defaults = [ 'posts_per_page' => 8,
                      'post_type'      => 'page' ];


        $vars_for_query_string = $_GET;

        if ( $vars_for_query_string ):
            if ( array_key_exists( $this->query_id, $vars_for_query_string ) ):
                if ( array_key_exists( 'paged', $vars_for_query_string[ $this->query_id ] ) ):
                    $defaults['paged'] = $vars_for_query_string[ $this->query_id ]['paged'];
                endif;
            endif;
        endif;


        // TODO: we need to detect existing complex orderby params and update
        // TODO: why does this break tax query?
        // $atts['meta_key'] = 'is_sticky';
        // $atts['orderby'] = [ 'meta_value' => 'DESC', 'date' => 'DESC' ];

        $this->query_args = \wp_parse_args( $atts, $defaults );


    }

    private function set_search_args() {

        if ( $this->ignore_search )
            return;


        $search_id = "{$this->query_id}_search_form";
        if ( ! array_key_exists( $search_id, $_POST ) )
            return;


        $this->is_search = true;

        $query = [];


        foreach ( $_POST as $key => $value )
            if ( substr( $key, 0, strlen( $this->query_id ) ) === $this->query_id )
                $query[ substr( $key, strlen( $this->query_id ) + 1 ) ] = $value;

        foreach ( $query as $k => $v )
            if ( $v === '' )
                unset( $query[ $k ] );

        $emptyQuery = count( $query ) === 1 && array_keys( $query )[0] === 'search_form';

        if ( $emptyQuery )
            unset( $query['search_form'] );

        $args = [];

        if ( array_key_exists( "query", $query ) && $query['query'] !== '' )
            $args['s'] = $query['query'];

        if ( array_key_exists( 'publication_date', $query ) ):
            $dq_obj = explode( "-", $query['publication_date'] );

            $args['date_query'][] = [ 'year'  => $dq_obj[0],
                                      'month' => $dq_obj[1] ];

        endif;


        // this is a checker to see if we should make the date query inclusive
        $range = [ false, false ];
        if ( array_key_exists( 'publish_before', $query ) && $query['publish_before'] !== 'null' ):
            $dq_obj                       = explode( "-", $query['publish_before'] );
            $args['date_query']['before'] = [ 'year'  => $dq_obj[0],
                                              'month' => $dq_obj[1] ];
            $range[0]                     = true;
        endif;

        if ( array_key_exists( 'publish_after', $query ) && $query['publish_after'] !== 'null' ):
            $dq_obj                      = explode( "-", $query['publish_after'] );
            $args['date_query']['after'] = [ 'year'  => $dq_obj[0],
                                             'month' => $dq_obj[1] ];
            $range[1]                    = true;
        endif;

        if ( $range[0] && $range[1] )
            $args['date_query']['inclusive'] = true;


        // this is for processing taxonomies
        foreach ( $query as $k => $v ):

            if ( substr( $k, 0, 4 ) !== 'tax_' )
                continue;

            if ( $v === "{$this->query_id}_null" )
                continue;

            $taxonomy  = substr( $k, 4 );
            $trimlen   = strlen( $this->query_id ) + 6 + strlen( $taxonomy );
            $tax_terms = substr( $v, $trimlen );

            $args['tax_query'][] = [ 'taxonomy' => $taxonomy,
                                     'field'    => 'slug',
                                     'terms'    => $tax_terms ];


        endforeach;

        if ( array_key_exists( 'post_type', $query ) && $query['post_type'] !== 'null' )
            $args['post_type'] = $query['post_type'];


        $this->query_args = \wp_parse_args( $args, $this->query_args );

    }

    public function clear_tax_args() {
        $this->query_args['tax_query'] = [];
    }

    public function add_tax_arg( $tax_query ) {

        $this->query_args['tax_query'][] = $tax_query;

    }

    private function execute() {


        if ( class_exists( 'SWP_Query' ) && isset( $this->query_args['s'] ) ):
            $this->wp_query = new \SWP_Query( $this->query_args );
        else:
            $this->wp_query = new \WP_Query( $this->query_args );
        endif;

        $this->pages = (int) $this->wp_query->max_num_pages;

        $this->posts = [];

        foreach ( $this->wp_query->posts as $post )
            $this->posts[] = new IS_Post( $post );


        $this->has_posts = $this->wp_query->has_posts;

        $query_elements_to_bring_over = [ 'found_posts', 'post_count' ];

        foreach ( $query_elements_to_bring_over as $element )
            $this->$element = $this->wp_query->$element;


    }

    // you can't use the word get
    public function grab( $post_type = 'post', $posts_per_page = false ) {

        $this->query_args['post_type'] = $post_type;

        if ( $posts_per_page )
            $this->query_args['posts_per_page'] = $posts_per_page;

        $this->execute();

        return $this->posts;

    }

    static function paginate( $query = false ) {


        if ( get_class( $query ) === 'SWP_Query' ):

            $posts_in = [];

            foreach ( $query->posts as $post )
                $posts_in[] = $post->ID;

            $query = new \WP_Query( [ 'posts_in' => $posts_in ] );

        endif;

        return self::get_pagination( $query );


    }

    static function get_pagination( $query = false, $archive_object = false, $id = 'archive_1' ) {

        // todo: move me into acme.

        $wp_query = null;
        $wp_query = $query;

        $id = $archive_object ? $archive_object->id_tag : $id;

        $vars_for_query_string = $_GET;

        $page = 1;

        if ( $vars_for_query_string ):
            if ( array_key_exists( $id, $vars_for_query_string ) ):
                if ( array_key_exists( 'paged', $vars_for_query_string[ $id ] ) ):
                    $page = $vars_for_query_string[ $id ]['paged'];
                    unset( $vars_for_query_string[ $id ]['paged'] );
                endif;
            endif;
        endif;

        $query_string = http_build_query( $vars_for_query_string );


        $pagination_html = paginate_links( [ 'base'    => "?{$id}[paged]=%_%&{$query_string}",
                                             'format'  => "%#%",
                                             'type'    => 'list',
                                             'current' => max( 1, $page ),
                                             'total'   => $query->max_num_pages ] );

        return $pagination_html;
    }


    static function get_swp_pagination( $query = false ) {


        $pagination = paginate_links( [ 'format'  => '?page=%#%',
                                        'base'    => '%_test_%',
                                        'current' => get_query_var( "page" ),
                                        'total'   => $query->max_num_pages, ] );

        print wp_kses_post( $pagination );

        return;


    }

    public function convert_to_IS_Post_Archive_Module( $user_config = [] ) {

        // this will convert a manually created IS Query to a IS_Post_Archive to allow for template display

        $defaults = [ 'acf_fc_layout'                 => 'post_archive',
                      'post_type'                     => 'any',
                      'template_type'                 => 'query',
                      'custom_posts'                  => $this->posts,
                      'post_archive_excerpt_template' => '',
                      'post_archive_query_template'   => '/wp-content/plugins/tsd-infinisite/twig/query_excerpts/default.twig',
                      'posts_per_page'                => '3',
                      'pagination'                    => 'true',
                      'posts_per_row'                 => [ 'small'  => '1',
                                                           'medium' => '1',
                                                           'large'  => '1' ],
                      'date_filter'                   => '',
                      'hide_past_posts'               => '',
                      'hide_future_posts'             => '',
                      'padding'                       => [ 'x' => true,
                                                           'y' => true ],
                      'filter_display_config'         => [ 'post_archive_filter_display_config_filter_placement' => 'none',
                                                           'post_archive_filter_display_config_filter_title'     => '',
                                                           'post_archive_filter_display_config_keyword_search'   => false,
                                                           'post_archive_filter_display_config_searchwp_engine'  => '',
                                                           'filter_template'                                     => '', ],
                      'filter_repeater'               => '',
                      'custom_query'                  => '' ];

        $module_config = \wp_parse_args( $user_config, $defaults );


        $is_page_builder_acf_module_config = new IS_Post_Archive( $module_config );

        return $is_page_builder_acf_module_config->get_content();

    }

    public function search_form( $atts = [] ) {

        $taxonomies = [];

        if ( array_key_exists( 'dropdown_filters', $atts ) ):
            foreach ( $atts['dropdown_filters'] as $t )
                $taxonomies[] = get_taxonomy( $t );

            unset( $atts['dropdown_filters'] );
        endif;

        $form_data = [];

        foreach ( $_GET as $key => $value )
            if ( substr( $key, 0, strlen( $this->query_id ) ) === $this->query_id )
                $form_data[ $key ] = $value;

        if ( array_key_exists( 'result_page_id', $atts ) && $atts['result_page_id'] ):
            $atts['url'] = get_permalink( $atts['result_page_id'] );
            unset( $atts['result_page_id'] );
        endif;

        $defaults = [ 'taxes'               => $taxonomies,
                      'id'                  => $this->query_id,
                      'publication_date'    => false,
                      'keyword_search'      => true,
                      'keyword_label'       => 'Keyword',
                      'keyword_placeholder' => 'Enter Your Query',
                      'search_label'        => 'Search',
                      'template'            => 'search',
                      'post_type'           => false,
                      'form_data'           => $form_data,
                      'query'               => $this,
                      'url'                 => strtok( $_SERVER["REQUEST_URI"], '?' ) ];
        $vars     = wp_parse_args( $atts, $defaults );

        $search_template = "/components/search/IS_Query/{$vars['template']}.php";

        return Acme::get_file( $search_template, $vars );

    }

    public function build_calendar( $atts = [] ) {

        $defaults = [ 'template' => 'default',
                      'id'       => $this->query_id,
                      'posts'    => 50 ];
        $config   = \wp_parse_args( $atts, $defaults );

        $factory = new \CalendR\Calendar;
        $factory->setFirstWeekday( 7 );

        $args                   = $this->query_args;
        $args['posts_per_page'] = $config['posts'];

        $_q    = new IS_Query( $args );
        $posts = $_q->posts;

        $event_collection = new \CalendR\Event\Provider\Basic();
        $factory->getEventManager()->addProvider( 'event_collection', $event_collection );


        $event_lookup = [];


        foreach ( $posts as $c => $post ):

            $start_date = $post->wp_obj->post_date;

            if ( ! $start_date )
                continue;

            $date = new \DateTime( $start_date );

            $event_lookup[ $date->format( "Y" ) ][ $date->format( "n" ) ][] = $post;

            $new_event         = new Calendar_Event( $c, $date, $date );
            $new_event->post   = $post;
            $new_event->fields = $post->get_fields()->fields;
            $event_collection->add( $new_event );

        endforeach;

        // sorting the event lookup
        foreach ( $event_lookup as $year => $events )
            krsort( $event_lookup[ $year ] );

        krsort( $event_lookup );

        $filepath = "/components/calendar/IS_Query/{$config['template']}.php";

        return Acme::get_file( $filepath, [ 'event_lookup' => $event_lookup,
                                            'id'           => $this->query_id,
                                            'factory'      => $factory ] );

    }

    public function get_template( $alt = false ) {

        if ( ! $alt )
            $alt = 'excerpt';

        $root_file = Acme::file_exists( "/twig/query_excerpts/{$alt}.php" );
        if ( $root_file )
            $template = $alt;

        if ( ! $template )
            return "no template found - $this->post_type ";

        $vars['query'] = $this;
        $filepath      = "/twig/query_excerpts/{$template}.php";

        return Acme::get_file( $filepath, $vars );

    }

    public function print_template( $alt = false ) {
        print $this->get_template( $alt );
    }


    public function found_post_titles() {

        if(!$this->wp_query->found_posts)
            return "<p>No posts found.</p>";

        $return = '';

        if ( ! $this->posts )
            return "<p>No posts found for this query.</p>";

        foreach ( $this->posts as $p )
            $return .= "<p><a href='$p->permalink'>$p->post_title</a></p>";

        return $return;

    }

    public function posts_by_type() {

        $posts = [];

        foreach ( $this->posts as $p )
            $posts[ get_post_type_object( get_post_type( $p->ID ) )->labels->name ][] = $p;

        return $posts;

    }


}
