<?

namespace TSD_Infinisite;

class IS_Term {

    public $WP_Term;
    public $count;
    public $description;
    public $name;
    public $slug;
    public $taxonomy;
    public $parent;
    public $term_id;

    public $link;
    public $edit_link;

    public function __construct( $term ) {

        if ( is_int( $term ) )
            $term = \get_term( $term );

        $this->config( $term );

    }

    private function config( $term ) {
        $this->WP_Term     = $term;
        $this->count       = $term->count;
        $this->description = $term->description;
        $this->name        = $term->name;
        $this->slug        = $term->slug;
        $this->taxonomy    = $term->taxonomy;
        $this->parent      = $term->parent;
        $this->term_id     = $term->term_id;

        $this->link      = \get_term_link( $term );
        $this->edit_link = \site_url( "/wp-admin/term.php?taxonomy={$this->taxonomy}&tag_ID={$this->term_id}" );
    }

    public function get( $key ) {
        return get_field( $key, "{$this->taxonomy}_$this->term_id" );
    }

    public function get_template( $template = 'default' ) {

        $filepath = '';

        $root_filepath = "/twig/term_excerpts/{$template}.php";
        $root_file     = Acme::file_exists( $root_filepath );
        if ( $root_file )
            $filepath = $root_filepath;

        $post_type_filepath = "/twig/term_excerpts/{$this->taxonomy}/{$template}.php";
        $post_type_file     = Acme::file_exists( $post_type_filepath );
        if ( $post_type_file )
            $filepath = $post_type_filepath;

        return Acme::get_file( $filepath, [ 'term' => $this ] );


    }

    public function get_post_query( $config = [] ) {
        $defaults = [ 'post_type' => 'any',
                      'tax_query' => [ [ 'taxonomy' => $this->taxonomy,
                                         'terms'    => $this->term_id ] ] ];
        $args     = wp_parse_args( $config, $defaults );

        return new IS_Query( $args );
    }

    public function get_posts( $config = [] ) {
        return self::get_post_query( $config )->posts;
    }


}



