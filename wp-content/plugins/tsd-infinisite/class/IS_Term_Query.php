<?

namespace TSD_Infinisite;

class IS_Term_Query {


    public $query = [];
    public $terms = [];
    public $config = [];


    public function __construct( $atts = [] ) {

        $this->config( $atts );
        $this->setup_query();
        $this->execute();


    }

    private function config( $atts ) {

        $defaults = [];

        $config = \wp_parse_args( $atts, $defaults );

        $this->config = $config;

    }

    private function setup_query() {

        if ( array_key_exists( 'include', $this->config ) )
            $this->query['include'] = $this->config['include'];

        foreach ( $this->config as $k => $v )
            $this->query[ $k ] = $v;

    }

    private function execute() {

        $this->query = new \WP_Term_Query( $this->query );

        foreach ( $this->query->terms as $term )
            $this->terms[] = new IS_Term( $term );

    }

    public function get_IDS() {
        $return = [];

        foreach ( $this->query->terms as $term )
            $return[] = $term->term_id;

        return $return;


    }

}
