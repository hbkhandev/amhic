<?php

namespace TSD_Infinisite;

class IS_Test_Page_Builder_Modules
{

    public $page_builder_modules = [];

    public function __construct()
    {

        $this->build_page_builder_modules();

    }


    public function add_live_data()
    {


        // in order for this to work, there must be some basic files in the media library
        // TODO: check for the files and display error messages if they aren't there

        $image_query = new \WP_Query([
            'post_type' => 'attachment',
            'post_status' => 'inherit',
            'posts_per_page' => -1
        ]);

        $image_type_file_attachment = false;

        foreach ($image_query->posts as $post):
            $fields = get_post_meta($post->ID);
            if (!\array_key_exists("_wp_attachment_metadata", $fields)) continue;
            $id = $post->ID;
            $image_type_file_attachment = $post;
        endforeach;

        $image = Acme::get_image_info_by_id($id);

        $image['caption'] = "Test Caption Here";

        $image['link'] = [
            'title' => "Link title here",
            'url' => 'http://topshelfdesign.net',
            'target' => ''
        ];

        $acf_file_attachment = [
            'title' => $image_type_file_attachment->post_title,
            'url' => $image['url']
        ];

        $this->page_builder_modules['Single Image Full Width']['image'] = $image;
        $this->page_builder_modules['Single Image with Custom Template']['image'] = $image;

        $this->page_builder_modules['hero']['image'] = $image;


        $this->page_builder_modules['multiple_images']['images'] = [$image, $image];

        //
        $this->page_builder_modules['multiple_file_container']['file_container'][]['file'] = $acf_file_attachment;


    }


    private function build_page_builder_modules()
    {


        $this->page_builder_modules['Basic Simple Content'] = [
            'acf_fc_layout' => 'simple_content',
            'simple_content' => '<h3>Lorem Ipsum</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas id placerat ante. Donec vitae neque porta, facilisis mauris eget, luctus risus. Vivamus sagittis, elit eu bibendum pulvinar, metus risus vehicula massa, vitae imperdiet metus nisi ut erat. Curabitur pulvinar arcu justo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc id eros lacus.</p>',
        ];


        $this->page_builder_modules['SVG Icon'] = [
            'acf_fc_layout' => 'icon',
            'icon' => false,
            'svg' => '',
            'size' => 'fa-8x',
            'animation_effects' => 'fa-pulse',
            'rotate_flip' => 'fa-flip-horizontal',
            'link' => [
                'title' => 'Title Text Here',
                'url' => 'http://google.com',
                'target' => '_blank',
            ],

            'color_select' => 'secondary',

            'caption' => 'Caption Here',
        ];

        // TODO: get this working - need to make test data menu
        $this->page_builder_modules['submenu'] = [
            'acf_fc_layout' => 'submenu',
            'template' => '/wp-content/plugins/tsd-infinisite/components/submenu_templates/accordion.php',
        ];


        // two icons - this one with an svg
        // and this one with an icon
        $this->page_builder_modules['Standard Icon'] = [
            'acf_fc_layout' => 'icon',
            'icon' => 'bomb',
            'svg' => false,
            'size' => 'fa-8x',
            'animation_effects' => 'fa-pulse',
            'rotate_flip' => 'fa-flip-horizontal',
            'link' => [
                'title' => 'Title Text Here',
                'url' => 'http://google.com',
                'target' => '_blank',
            ],

            'color_select' => 'secondary',

            'caption' => 'Caption Here',
        ];


        // default tempalte
        $this->page_builder_modules['Single Image Full Width'] = [
            'acf_fc_layout' => 'single_image',
            'full_width' => '1',
        ];

        // custom tempalte
        $this->page_builder_modules['Single Image with Custom Template'] = [
            'acf_fc_layout' => 'single_image',
            'full_width' => '1',
            'template' => '/wp-content/plugins/tsd-infinisite/twig/layouts/single_image/gallery.twig'
        ];


        $this->page_builder_modules['multiple_images'] = [
            'acf_fc_layout' => 'multiple_images',
            'images' => []
        ];

        $this->page_builder_modules['accordion'] = [
            'acf_fc_layout' => 'accordion',
            'blade' => [
                '0' => [
                    'title' => 'Accordion Blade',
                    'content' => '<p>Accordion Content</p>',

                ],

            ],

        ];

        $this->page_builder_modules['multiple_file_container'] = [
            'acf_fc_layout' => 'multiple_file_container',
            'file_container' => []
        ];

        $this->page_builder_modules['button'] = [

            'acf_fc_layout' => 'button',
            'buttons' => [
                [
                    'link' => [
                        'title' => 'Link',
                        'url' => 'http://google.com',
                        'target' => '_blank',
                    ],
                ],
                [
                    'link' => [
                        'title' => 'Link 2',
                        'url' => 'http://yahoo.com',
                        'target' => '_blank',
                    ],
                ],
            ],
            'config' => [
                'alignment' => 'left',
                'size' => 'small',
                'orientation' => 'horizontal',
                'color_select' => 'primary',
                'hollow' => false,
            ],
        ];

        $this->page_builder_modules['spacer'] = [
            'acf_fc_layout' => 'spacer',
            'size' => 'medium',
            'show_divider' => '1',
        ];
        $this->page_builder_modules['header'] = [
            'acf_fc_layout' => 'header',
            'text' => 'Test Header',
            'element' => 'h3',
            'color_select' => 'primary',
            'alignment' => 'center',
            'link' => [
                'title' => '',
                'url' => 'http://google.com/',
                'target' => '',
            ],

        ];
        $this->page_builder_modules['embed'] = [
            'acf_fc_layout' => 'embed',
            'aspect_ratio' => 'widescreen',
            'embed' => '<iframe width="520" height="390" src="https://www.youtube.com/embed/4BXpi7056RM?feature=oembed" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>',
        ];
        $this->page_builder_modules['hero'] = [
            'acf_fc_layout' => 'hero',
            'title' => 'Test Hero',
            'template' => '/wp-content/plugins/tsd-infinisite/module_templates/hero/montgomery_standard_hero.php',
            'image' => [],
            'content_blocks' => [
                [
                    'wysiwyg' => '<p>test content here</p>'

                ]
            ],
        ];
        $this->page_builder_modules['text_row'] = [
            'acf_fc_layout' => 'text_row',
            'cells_per_row' => '2',
            'columns' => [
                [
                    'content' => 'test'
                ],
                [
                    'content' => 'test 2'
                ]
            ]
        ];
        $this->page_builder_modules['cta'] = [
            'acf_fc_layout' => 'cta'
        ];
        $this->page_builder_modules['wireframe'] = [
            'acf_fc_layout' => 'wireframe',
            'title' => 'Wireframe Title',
            'height' => '150px',
            'form' => '1',
            'note_in_modal' => true,
            'note' => '<p>Wireframe content here.</p>',
            'links' => [
                [
                    'link' => [
                        'title' => 'Link A',
                        'url' => 'http://google.com',
                        'target' => '_blank',
                    ],
                ],
                [
                    'link' => [
                        'title' => 'Link A',
                        'url' => 'http://google.com',
                        'target' => '_blank',
                    ],
                ]
            ]
        ];
        $this->page_builder_modules['gravity_form'] = [
            'acf_fc_layout' => 'gravity_form',
            'form' => '1'
        ];


        $this->page_builder_modules['Post Archive'] = [
            'acf_fc_layout' => 'post_archive',
            'post_type' => 'page',
            'template_type' => 'excerpt',
            'post_archive_excerpt_template' => '/wp-content/plugins/tsd-infinisite/twig/post_excerpts/blog_excerpt.twig',
            'posts_per_page' => '3',
            'pagination' => '1',
            'posts_per_row' => [
                'small' => '1',
                'medium' => '2',
                'large' => '2',
            ],
            'date_filter' => null,
            'filter_repeater' => false,
            'padding' => [
                'x' => 0,
                'y' => 0
            ],
            'custom_query' => '',
        ];

        $this->page_builder_modules['post_archive_2'] = [
            'acf_fc_layout' => 'post_archive',
            'post_type' => 'page',
            'template_type' => 'query',
            'post_archive_query_template' => '/wp-content/plugins/tsd-infinisite/twig/query_excerpts/default.twig',
            'posts_per_page' => '3',
            'pagination' => '1',
            'posts_per_row' => [
                'small' => '1',
                'medium' => '1',
                'large' => '1',
            ],
            'date_filter' => null,
            'filter_repeater' => false,
            'padding' => [
                'x' => 0,
                'y' => 0
            ],
            'custom_query' => '',
        ];
    }

}