<?

namespace TSD_Infinisite;

class IS_WooCommerce {

    public $is_active;

    public function __construct() {

        $this->config();


//        remove_post_type_support('product', ['editor']);

        $active_plugins = apply_filters('active_plugins', get_option('active_plugins'));
        $active_wc = in_array('woocommerce/woocommerce.php', $active_plugins);
        if ($active_wc):


            add_action('woocommerce_loaded', function() {


                /*
                 * massive block of WC only functions here
                 */


                // code for WC add to cart function

                function cs_wc_loop_add_to_cart_scripts() {
                    if (is_shop() || is_product_category() || is_product_tag() || is_product()) : ?>

                        <script>
                            jQuery(document).ready(function ($) {
                                $(document).on('change', '.quantity .qty', function () {
                                    $(this).parent('.quantity').next('.add_to_cart_button').attr('data-quantity', $(this).val());
                                });
                            });
                        </script>

                    <?php endif;
                }

                add_action('wp_footer', 'cs_wc_loop_add_to_cart_scripts');


            });

        endif;


        new \TSD_Infinisite\Shortcode();


    }


    private function config() {

        $this->is_active = class_exists("WooCommerce");

    }


    public function process_post_type() {


        $pt = 'product';
        $config = ['single_view_template' => get_option("options_is_cpt_{$pt}_template"),
                   'hierarchical'         => get_option("options_is_cpt_{$pt}_hierarchical"),
                   'page_builder'         => get_option("options_is_cpt_{$pt}_page_builder"),
                   'exclude_from_search'  => get_option("options_is_cpt_{$pt}_exclude_from_search")];

        return ['name'      => 'Products',
                'post_type' => 'product',
                'config'    => $config];

    }

    public function add_taxonomies_for_page_builder_filter_object() {

        $return['Products'] = ['product_cat'        => 'Category',
                               'product_tag'        => 'Tag',
                               'product_attributes' => 'Attributes'];

        return $return;


    }

}

$wc = new IS_WooCommerce();

if (!$wc->is_active)
    return false;

class IS_Product extends \WC_Product {

    public $image = false;
    public $gallery = [];
    public $post;
    private $gallery_loaded = false;

    public function __construct($product = 0) {


        parent::__construct($product);

        if ($this->get_image_id())
            $this->image = new IS_Media($this->get_image_id());

        $this->post = new IS_Post($this->id);

    }

    public function load_gallery($include_main = true) {

        $this->gallery_loaded = true;

        if ($include_main)
            $this->gallery[] = $this->image;

        foreach ($this->get_gallery_image_ids() as $id)
            $this->gallery[] = new IS_Media($id);

    }

    public function add_to_cart_form($atts = []) {

        $defaults = ['class' => 'no-margin-children'];

        $config = \wp_parse_args($atts, $defaults);

        $op = '';

        $link = ['url'   => '',
                 'label' => '',
                 'class' => ''];
        switch ($this->product_type) {
            case "variable" :
                $link['url'] = apply_filters('variable_add_to_cart_url', get_permalink($this->id));
                $link['label'] = apply_filters('variable_add_to_cart_text', __('Select options', 'woocommerce'));
                break;
            case "grouped" :
                $link['url'] = apply_filters('grouped_add_to_cart_url', get_permalink($this->id));
                $link['label'] = apply_filters('grouped_add_to_cart_text', __('View options', 'woocommerce'));
                break;
            case "external" :
                $link['url'] = apply_filters('external_add_to_cart_url', get_permalink($this->id));
                $link['label'] = apply_filters('external_add_to_cart_text', __('Read More', 'woocommerce'));
                break;
            default :
                if ($this->is_purchasable()) {
                    $link['url'] = apply_filters('add_to_cart_url', esc_url($this->add_to_cart_url()));
                    $link['label'] = apply_filters('add_to_cart_text', __('Add to cart', 'woocommerce'));
                    $link['class'] = apply_filters('add_to_cart_class', 'add_to_cart_button');
                } else {
                    $link['url'] = apply_filters('not_purchasable_url', get_permalink($this->id));
                    $link['label'] = apply_filters('not_purchasable_text', __('Read More', 'woocommerce'));
                }
                break;
        }
        // If there is a simple product.
        if ($this->product_type == 'simple') {


            $url = esc_url(apply_filters('woocommerce_add_to_cart_form_action', $this->get_permalink()));
            $input = woocommerce_quantity_input([], $this, 0);

            $op = "<form action='#' class='cart grid-x {$config['class']}' method='post' enctype='multipart/form-data'>
            <div class='cell shrink'>
                  $input
            </div>
            <div class='cell shrink'>
                <button type='submit' name='add-to-cart' value='{$this->id}' data-product_sku='{$this->get_sku()}' data-quantity='1' class='button'>Add to Cart</button>
            </div>
                 
            </form>";

        } else {
            $op = apply_filters('woocommerce_loop_add_to_cart_link', sprintf('<a href=" % s" rel="nofollow" data-product_id=" % s" data-product_sku=" % s" class=" % s button product_type_ % s">%s</a>', esc_url($link['url']), esc_attr($this->id), esc_attr($this->get_sku()), esc_attr($link['class']), esc_attr($this->product_type), esc_html($link['label'])), $this, $link);
        }

        return $op;

    }

    public function apply_gallery_template($template) {

        if (!$this->gallery_loaded)
            $this->load_gallery();

        if (!count($this->gallery))
            return apply_filters("the_content", "No gallery found");

        return Acme::media_gallery($template, $this->gallery);


    }

    public function review_form() {

        $GLOBALS['product'] = $this;
        global $post;
        $post = get_post($this->id);
        setup_postdata($post);

        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/wp-content/plugins/woocommerce/templates/single-product-reviews.php');

        // lets have a tidy global scope now, y'all.
        unset($GLOBALS['product']);
        wp_reset_postdata();
        return ob_get_clean();
    }

}