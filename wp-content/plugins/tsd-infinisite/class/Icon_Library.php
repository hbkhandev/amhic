<?php

namespace TSD_Infinisite;

use SVG\SVGImage;

class Icon_Library
{

    private $path_to_icons = 'components/svg_icons';
    public $library = [];

    public function __construct()
    {

        $this->build_library();



    }

    private function build_library(){


        $plugin_php_templates_filepath = INFINISITE_URI . $this->path_to_icons . "/*";
        $plugin_php_icons = glob($plugin_php_templates_filepath);

        $theme_php_templates_filepath = THEME_URI . $this->path_to_icons . "/*";
        $theme_php_icons = glob($theme_php_templates_filepath);

        $templates = array_merge($plugin_php_icons, $theme_php_icons);


        foreach($templates as $filepath)
            $this->library[] = SVGImage::fromFile($filepath);

    }



	static function get_fontawesome_icons_for_acf_select() {
		return Font_Awesome::get_select_menu_options();
	}

	static function get_fontawesome_sizes_for_acf_select() {

		$sizes = [
			'fa-xs' => '.75',
			'fa-sm' => '.875',
			''      => '1',
			'fa-lg' => '1.33',
		];

		for ( $i = 2; $i <= 20; $i ++ ) {
			$sizes["fa-{$i}x"] = "{$i}x";
		}

		return $sizes;
	}


}