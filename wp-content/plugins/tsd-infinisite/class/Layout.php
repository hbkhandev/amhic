<?

namespace TSD_Infinisite;

class Layout {

    public $header;
    public $footer;
    protected $rows = [];
    protected $css;
    protected $html;
    protected $page_content;
    protected $search_query;
    private $uniqid;

    public $config = [ 'is_custom_single_view' => false,
                       'post_options'          => [ 'inline_footer' => false,
                                                    'inline_header' => false,
                                                    'hide_footer'   => false,
                                                    'hide_header'   => false ] ];

    public function __construct( $id = false ) {

        $this->build_config( $id );

        $this->build_html();

        return;

    }

    private function build_config( $id ) {

        $this->config['id'] = $id;

        $this->config['post_type'] = get_post_type( $id );

        $this->config['post_type_options'] = [ 'template' => get_field( "field_is_cpt_{$this->config['post_type']}_template", "options" ) ];

        if ( $this->is_archive() )
            $this->config['archive_term'] = get_queried_object();

        $this->uniqid = \uniqid( 'is_page_builder_' );

        $this->header = 'todo: get header';
        $this->footer = 'todo: get footer';

    }



    public function get_row_acf() {
        return $this->config['acf_rows'];
    }

    public function get_css() {
        /**
         * makes the css of the layout available. best used with sidebars and buckets
         */
        return $this->css;
    }

    public function get_page_content() {

        $op = "<div class='is-page-builder-wrapper cell auto' data-uniqid='{$this->uniqid}'>";
        $op .= $this->page_content;
        $op .= "</div>";

        return $op;
    }


    public function get_toc( $args = [], $rows = [] ) {

        if ( $rows === [] )
            $rows = $this->config['acf_rows'];

        $defaults = [ 'classes' => '' ];

        $config = \wp_parse_args( $args, $defaults );

        return $this::build_toc( $config, $rows );


    }

    public static function build_toc( $config, $rows, $link_base = false ) {

        $output = '';

        $output .= "<ul class='is_toc {$config['classes']}'>";

        foreach ( $rows as $acf_row ):

            if ( $acf_row['row_id'] == '' ) {
                continue;
            }
            if ( $acf_row['row_title'] == '' ) {
                continue;
            }
            if ( substr( 'is_row_toc', $acf_row['row_classes'] ) == - 1 ) {
                continue;
            }

            $link = $link_base ? "data-link='{$link_base}#{$acf_row['row_id']}'" : "data-is-section-link='{$acf_row['row_id']}'";

            $output .= "<li $link>
                {$acf_row['row_title']}
            </li>";

        endforeach;


        $output .= "</ul>";

        return $output;
    }


    public function get_uniquid() {
        return $this->uniqid;
    }

    public function build_html() {

        $this->html .= Acme::get_file('header.php');
        $this->html .= "<div data-uniqid='{$this->uniqid}'>";

        $this->page_content = $this->get_single_view_content();

        if ( ! $this->config['is_custom_single_view'] ) :
                $pb = '';

                if ( have_posts() ):
                    while ( have_posts() ):
                        the_post();
                        $pb .= apply_filters( "the_content", get_the_content() );
                    endwhile;
                endif;

                $this->page_content = "<div class='is_wpb_container'>$pb</div>";

        endif;

        $this->html .= \apply_filters( "is_page_content_filter", $this->page_content );


        $this->html .= "</div>";
        $this->html .= Acme::get_file('footer.php');
    }

    private function is_custom_search() {


        if ( isset( $_GET['swpquery'] ) || isset( $_GET['s'] ) ) {
            return true;
        }

        return false;

    }

    private function is_search() {

        if ( is_search() || $this->is_custom_search() ) {
            return true;
        }

        return false;

    }

    private function prepare_custom_search() {

        $_query = isset( $_GET['swpquery'] ) ? $_GET['swpquery'] : $_GET['s'];

        $_engine = isset( $_GET['engine'] ) ? $_GET['engine'] : 'default';
        $_page   = \get_query_var( "page" );

        $args = apply_filters( "is/search/query_args", [ 's'      => $_query,
                                                         'engine' => $_engine ] );

        if ( $_page !== '' )
            $args['page'] = $_page;


        if ( isset( $_GET['swpquery'] ) ):
            $this->search_query = new \SWP_Query( $args );
        else:
            $this->search_query = new \WP_Query( $args );
        endif;

    }

    private function is_archive() {

        if ( \is_archive() ) {
            return true;
        }

        return false;

    }

    protected function get_single_view_content() {


        if ( is_404() ):

            $this->config['is_custom_single_view'] = true;
            $this->html                            .= Acme::get_file( "components/404.php" );

            return;

        endif;


        if ( $this->is_search() ):

            $this->config['is_custom_single_view'] = true;

            if ( $this->is_custom_search() ) {
                $this->prepare_custom_search();
            }

            $swp_engine = array_key_exists( 'engine', $_GET ) ? $_GET['engine'] : false;

            $plugin_swp_engine_template = INFINISITE_URI . "components/search/{$swp_engine}.php";
            $theme_swp_engine_template  = THEME_URI . "components/search/{$swp_engine}.php";


            if ( ! $this->search_query->found_posts ):

                $plugin_swp_engine_template = INFINISITE_URI . "components/search/no_posts.php";
                $theme_swp_engine_template  = THEME_URI . "components/search/no_posts.php";

                if ( file_exists( $theme_swp_engine_template ) ):
                    $search_template = $theme_swp_engine_template;
                elseif ( file_exists( $plugin_swp_engine_template ) ):
                    $search_template = $plugin_swp_engine_template;
                endif;

            elseif ( file_exists( $plugin_swp_engine_template ) ):
                $search_template = $plugin_swp_engine_template;
            elseif ( file_exists( $theme_swp_engine_template ) ):
                $search_template = $theme_swp_engine_template;
            else:
                $child_theme_has_custom_search_page = file_exists( THEME_URI . 'components/search/default.php' );
                $template_base                      = $child_theme_has_custom_search_page ? THEME_URI : INFINISITE_URI;
                $search_template                    = $template_base . 'components/search/default.php';
            endif;
            ob_start();
            include( $search_template );
            $this->html .= ob_get_clean();

            return;

        endif;

        if ( $this->is_archive() ):

            $this->config['is_custom_single_view'] = true;

            $obj = \get_queried_object();

            $acf_template = get_option( "options_{$obj->taxonomy}_archive_template" );

            if ( ! $acf_template && Acme::can_user_see_error_message() ) {
                print 'no template set for given taxonomy, consult IS tax options';
            }

            if ( ! $acf_template ) {
                return false;
            }

            $this->html .= Acme::get_file( $acf_template, [ 'layout' => $this ] );

            return;

        endif;


        $custom_template = $this->get_custom_template_for_post();

        if ( ! $custom_template ) {
            return false;
        }

        $this->config['is_custom_single_view'] = true;

        if ( ! file_exists( $custom_template ) ) {
            if ( Acme::can_user_see_debug() ) {
                $this->html .= "<div class='callout alert'><p class='no-padding'>File \"$custom_template\" not found, please contact the website admin.</p></div>";
            }

            return;
        }


        $temp     = explode( '.', $custom_template );
        $filetype = array_pop( $temp );

        switch ( $filetype ):

            case 'php':

                // if it's a php file, we're good to go.
                // the single view will know it's ID from the url
                // and can get access to its own data

                ob_start();
                $fields = $this->acf_fields;
                include( $custom_template );
                $this->html .= ob_get_clean();
                break;

            default:
                $this->html .= "No Template Type Found";
                break;

        endswitch;

        return '';
    }

    public function get_html() {

        return $this->html;

    }

    private function get_custom_template_for_post() {

        // TODO: write in global, post type, taxononmy, single post checks

        $post_type_template = $this->config['post_type_options']['template'];

        if ( ! $post_type_template ) {
            return false;
        }

        return $_SERVER['DOCUMENT_ROOT'] . $post_type_template;

    }

    public static function get_page_builder( $id ) {

        return "<p>$id</p>";

    }


    static function check_for_inline_header_footer( $element = false, $post_id = false ) {

        if ( ! $element || ! $post_id ) {
            return false;
        }


        // TODO: do i need a way to be turned off based on taxonomy?

        $global_inline = get_field( "{$element}_inside_first_row", "option" );


        // TODO: i need a way to be turned off via post types
        $post_type_inline = false;

        $single_post_inline = get_field( "inline_{$element}", $post_id );

        if ( $global_inline || $post_type_inline || $single_post_inline ) {
            return true;
        }

        return false;


    }


    static function get_menus_for_acf_global_header_footer() {

        $cache = DB::function_cache( 'Layout::acf_global_hf_menus' );

        if ( ! empty( $cache ) )
            return (array) $cache;

        $menus = get_terms( [ 'taxonomy' => 'nav_menu', ] );

        $return = [];

        if ( ! $menus ) {
            return $return;
        }

        foreach ( $menus as $menu ) {
            $return[ $menu->slug ] = $menu->name;
        }

        return DB::function_cache( 'Layout::acf_global_hf_menus', $return );

    }

    static function get_toc_by_id( $id = 0 ) {

        if ( $id == 0 )
            return '';

        $rows = get_field( "rows", $id );

        return self::build_toc( [], $rows, \get_permalink( $id ) );


    }


}
