<?php
/**
 * Created by PhpStorm.
 * User: jcylasta
 * Date: 10/15/2018
 * Time: 5:48 PM
 */

namespace TSD_Infinisite;


class Link_Tracking_Admin
{

    private $db_table = 'link_tracker';
    private $db_meta_table = 'link_tracker_meta';

    /**
     * Static Singleton Holder
     * @var self
     */
    protected static $instance = NULL;

    /**
     * Get (and instantiate, if necessary) the instance of the class
     *
     * @return self
     */
    public static function instance() {
        if ( ! self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Admin constructor.
     */
    public function __construct()
    {
        global $wpdb;
        $this->db_table = $wpdb->prefix . $this->db_table;
        $this->db_meta_table = $wpdb->prefix . $this->db_meta_table;

        add_action('admin_init', array($this, 'admin_init'), 5, 0);
        add_action('admin_head', array($this, 'add_mce_link_tracker'));

    }

    public function admin_init() {
    }


    // Creates a Tag field in the WYSIWYG link editor that when filled will make the link a tracked link
    // TODO: allow multiple tags, separated by spaces in the field, and translated into semi-colon separated tags in the query string
    function add_mce_link_tracker() {
        // check user permissions
        if ( !current_user_can( 'edit_posts' ) &&  !current_user_can( 'edit_pages' ) ) {
            return;
        }
        // check if WYSIWYG is enabled
        if ( 'true' == get_user_option( 'rich_editing' ) ) {
            wp_enqueue_style( 'islt_admin', plugins_url( '../assets/css/admin/is_link_tracker.css', __FILE__ ), array()  );
            add_filter( 'mce_external_plugins', array( &$this, 'add_tinymce_plugin' ) );
        }
    }

    /**
     * Adds a TinyMCE plugin compatible JS file to the TinyMCE / Visual Editor instance
     *
     * @param array $plugin_array Array of registered TinyMCE Plugins
     * @return array Modified array of registered TinyMCE Plugins
     */
    function add_tinymce_plugin( $plugin_array ) {
        $plugin_array['link_tracker'] = plugin_dir_url( __FILE__ ) . '../assets/js/link-tracker.js';
        return $plugin_array;
    }


    // TODO: Create a Link Tracker settings page
    // TODO: Create a Link Tracker Page to house the statistics

    // we need to add a new database table! call it (db prefix)_link_tracker
    // this table needs to have the following records
    // id - index of table, id of particular download
    // datetime - timestamp
    // post_id - id of the post
    public function createTable() {
        global $wpdb;

        $table_name = $this->db_table;

        $charset_collate = $wpdb->get_charset_collate();

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $sql = "CREATE TABLE $table_name (
              id mediumint(9) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              datetime datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
              post_id bigint(20) unsigned NOT NULL
              ) $charset_collate";

//        die('creating table<br/>' . $sql );
        dbDelta( $sql );

        $sql = "CREATE TABLE {$this->db_meta_table} (
              meta_id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              link_id bigint(20) unsigned NOT NULL,
              meta_key mediumtext NOT NULL,
              meta_value longtext NOT NULL
              ) $charset_collate";
        dbDelta( $sql );
    }
}

Link_Tracking_Admin::instance();

class Link_Tracking_Core {

    private $accepted_filetypes = [];
    private $db_table = 'link_tracker';
    private $db_meta_table = 'link_tracker_meta';

    /**
     * Static Singleton Holder
     * @var self
     */
    protected static $instance = NULL;

    /**
     * Get (and instantiate, if necessary) the instance of the class
     *
     * @return self
     */
    public static function instance() {
        if ( ! self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Counter constructor.
     */
    public function __construct()
    {
        $current_user = wp_get_current_user();
//        if (user_can( $current_user, 'administrator' ))
//            return;

        global $wpdb;
        $this->db_table = $wpdb->prefix . $this->db_table;
        $this->db_meta_table = $wpdb->prefix . $this->db_meta_table;

        // Execute hooks
        $this->addHooks();

    }

    // We need to find all links with the url query "link tracking tag"
    public function process_link_tracker() {
        if ( isset($_GET['lttag']) ) {
            $tag = $_GET['lttag'];


            $redirect = get_the_permalink();
            // Store post meta to store download
            $this->saveTracking();

//            die("<pre>".print_r(['server'=>$_SERVER, 'current_post_id'=>get_the_ID(), 'redirect' => $redirect], true)."</pre>");

            header('Location: ' . $redirect);
            exit();
        }

        if ( isset($_GET['link_tracking_test']) ) {

            $trackings = $this->get_link_tracking();
            echo "<pre>".print_r([$trackings], true)."</pre>";

            die();
        }
    }

    // TODO: This should create a link tracker that saves details
    public function create_link($post, $tags){

    }

    private function saveTracking(){

        $this->insertToDB();

    }

    private function insertToDB(){
        global $wpdb;

        try {
            $wpdb->hide_errors();

            $table = $this->db_table;

            // clean up referral url
            $referral_url = str_replace("{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}", '', $_SERVER['HTTP_REFERER']);

            $data = array(
                'post_id'       => get_the_ID()
            );

            $format = array('%s','%s','%s');

            $wpdb->insert($table,$data,$format);

            $wpdb->show_errors();
            $e = $wpdb->last_error;

            if(preg_match("/Table .* doesn't exist/", $e)) {
                Link_Tracking_Admin::instance()->createTable();
                $this->insertToDB();
                return;
            }

            $tracking_id = $wpdb->insert_id;

            $this->add_link_tracker_meta($tracking_id, 'user_ip', $_SERVER['REMOTE_ADDR']);
            $this->add_link_tracker_meta($tracking_id, 'referral_url', $referral_url);
            $this->add_link_tracker_meta($tracking_id, 'user_agent', $_SERVER['HTTP_USER_AGENT']);

            // TODO: loop for multiple tags separated by semi-colon
            $this->add_link_tracker_meta($tracking_id, 'tag', $_GET['lttag']);


        } catch (Exception $e) {
            error_log($e);
        }
    }

    private function add_link_tracker_meta($link_id, $meta_key, $meta_value){
        global $wpdb;

        $table = $this->db_meta_table;

        $result = $wpdb->insert( $table, array(
            'link_id' => $link_id,
            'meta_key' => $meta_key,
            'meta_value' => $meta_value
        ) );
    }

    // TODO: implement get link tracker meta.
    private function get_link_tracking(){
        global $wpdb;

//        $meta_list = $wpdb->get_results( "SELECT $column, meta_key, meta_value FROM $table WHERE $column IN ($id_list) ORDER BY $id_column ASC", ARRAY_A );

        $link_trackings = $wpdb->get_results( "SELECT lt.id, lt.datetime, lt.post_id,
            pm1.meta_value AS tag,
            pm2.meta_value AS referral_url,
            pm3.meta_value AS user_ip,
            pm4.meta_value AS user_agent
        FROM    {$this->db_table} lt LEFT JOIN {$this->db_meta_table} pm1 ON (
                pm1.link_id = lt.id  AND
                pm1.meta_key    = 'tag'
            ) LEFT JOIN {$this->db_meta_table} pm2 ON (
                pm2.link_id = lt.id  AND
                pm2.meta_key    = 'referral_url'
            ) LEFT JOIN {$this->db_meta_table} pm3 ON (
                pm3.link_id = lt.id  AND
                pm3.meta_key    = 'user_ip'
            ) LEFT JOIN {$this->db_meta_table} pm4 ON (
                pm3.link_id = lt.id  AND
                pm3.meta_key    = 'user_agent'
            )", ARRAY_A );


        return $link_trackings;
    }

    private function addHooks(){

        add_action( 'wp', array($this, 'process_link_tracker') );

    }


    private function getMediaPostByAttachment($attachment){
        global $wpdb;
        $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_value LIKE '%$attachment%' AND meta_key = '_wp_attached_file'", OBJECT );

        if(!empty($results))
            return get_post($results[0]->post_id);
        else
            return false;
    }

    /**
     * @return array
     */
    public function getAcceptedFiletypes()
    {
        return $this->accepted_filetypes;
    }

    /**
     * @param array $accepted_filetypes
     */
    public function setAcceptedFiletypes($accepted_filetypes)
    {
        $this->accepted_filetypes = $accepted_filetypes;
    }
}

Link_Tracking_Core::instance();