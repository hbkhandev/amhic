<?php
/**
 * Created by PhpStorm.
 * User: jcylasta
 * Date: 10/15/2018
 * Time: 5:48 PM
 */

namespace TSD_Infinisite;


class Media_Download_Counter_Admin
{

    private $db_table = 'is_file_downloads';

    /**
     * Static Singleton Holder
     * @var self
     */
    protected static $instance = NULL;

    /**
     * Get (and instantiate, if necessary) the instance of the class
     *
     * @return self
     */
    public static function instance() {
        if ( ! self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Admin constructor.
     */
    public function __construct()
    {
        global $wpdb;
        $this->db_table = $wpdb->prefix . $this->db_table;

        add_action('admin_init', array($this, 'admin_init'), 5, 0);
    }

    public function admin_init() {

        add_filter( 'attachment_fields_to_edit', array($this, 'attachment_fields'), 10, 2 );
        wp_enqueue_style( 'tsdmdc_admin', plugins_url( '../assets/css/admin/ismdc.css', __FILE__ ), array()  );

        add_filter( 'manage_media_columns', array($this, 'media_columns_downloads') );
        add_filter( 'manage_upload_sortable_columns', array($this, 'media_sortable_column_downloads') );
        add_action( 'pre_get_posts', array($this, 'media_downloads_orderby') );
        add_action( 'manage_media_custom_column', array($this, 'media_custom_column_downloads'), 10, 2 );
    }

    /**
     * Add 'Downloads' field in the Attachment Details
     *
     * @param $form_fields
     * @param $post
     * @return mixed
     */
    public function attachment_fields($form_fields, $post) {
        if (!strstr($post->post_mime_type,'image')) {
//            echo '<h1>attachment fields</h1>';
            $downloads = get_post_meta($post->ID, 'is_file_downloads', true);
            if (!is_numeric($downloads)) {
                $downloads = 'None';
            }
            $form_fields['ismdc-downloads'] = array(
                'value' => $downloads,
                'input' => 'value',
                'label' => __('Downloads'));
        }
        return $form_fields;
    }

    /**
     * Filter the Media list table columns to add a File Size column.
     *
     * @param array $posts_columns Existing array of columns displayed in the Media list table.
     * @return array Amended array of columns to be displayed in the Media list table.
     */
    function media_columns_downloads( $posts_columns ) {
        $posts_columns['downloads'] = __( 'Downloads', 'is-media-download-counter' );

        return $posts_columns;
    }

    function media_sortable_column_downloads( $columns ) {
        $columns['downloads'] = 'downloads';

        //To make a column 'un-sortable' remove it from the array
        //unset($columns['date']);

        return $columns;
    }

    function media_downloads_orderby( $query ) {
        if( ! is_admin() )
            return;

        $orderby = $query->get( 'orderby');

        if( 'downloads' == $orderby ) {
            $query->set('meta_key','is_file_downloads');
            $query->set('orderby','meta_value_num');
        }
    }

    /**
     * Display File Size custom column in the Media list table.
     *
     * @param string $column_name Name of the custom column.
     * @param int    $post_id Current Attachment ID.
     */
    function media_custom_column_downloads( $column_name, $post_id ) {
        if ( 'downloads' !== $column_name ) {
            return;
        }

        $downloads = get_post_meta($post_id, 'is_file_downloads', true);
        if (!is_numeric($downloads)) {
            $downloads = 0;
        }

        echo $downloads;
    }

    // we also need to add a new database table! call it (db prefix)_is_file_downloads
    // this table needs to have the following records
    // id - index of table, id of particular download
    // datetime - timestamp
    // file_id - id of the meta key that's attached to the file
    // user_ip
    // referral_url - relative url path
    function createTable() {
        global $wpdb;

        $table_name = $this->db_table;

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
              id mediumint(9) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              datetime datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
              file_id bigint(20) unsigned NOT NULL,
              user_ip tinytext NOT NULL,
              referral_url tinytext NOT NULL,
              user_agent tinytext NOT NULL
              ) $charset_collate;";

//        die('creating table<br/>' . $sql );
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }
}

Media_Download_Counter_Admin::instance();

class Media_Download_Counter_Core {

    private $accepted_filetypes = [];
    private $db_table = 'is_file_downloads';

    /**
     * Static Singleton Holder
     * @var self
     */
    protected static $instance = NULL;

    /**
     * Get (and instantiate, if necessary) the instance of the class
     *
     * @return self
     */
    public static function instance() {
        if ( ! self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Counter constructor.
     */
    public function __construct()
    {
        $current_user = wp_get_current_user();
        if (user_can( $current_user, 'administrator' ))
            return;

        global $wpdb;
        $this->db_table = $wpdb->prefix . $this->db_table;

        // Execute hooks
        $this->addHooks();

        //@TODO: make checkbox fields for all file types
        $this->setAcceptedFiletypes(['.pdf', '.docx', '.doc', '.xlsx', '.xls']);

    }

    public function load_scripts() {

        $fileTypes = $this->getAcceptedFiletypes();

        wp_enqueue_script( 'is_mdc', plugins_url( '../js/is-mdc.js', __FILE__ ), array('jquery'));
        wp_localize_script('is_mdc','is_mdc_vars', ['site_url' => site_url(), 'file_types' => join('|', $fileTypes)]);

    }

    public function process_download_count() {
        if ( isset($_GET['ismdc']) ) {
            $redirect = $_GET['ismdc'];
            // Find media post id
            $urlParts = explode('/', $redirect);
            $filename = end($urlParts);
            $media = $this->getMediaPostByAttachment($filename);

//            die("<pre>".print_r([$_SERVER], true)."</pre>");

            // Store post meta to store download
            $this->saveDownload($media->ID);


            header('Location: ' . $redirect);
            exit();
        }
    }

    private function saveDownload($post_id){

//        die('saving download');
        $this->insertToDB($post_id);

        $downloads = $this->getDownloadsCount($post_id);

        update_post_meta($post_id, 'is_file_downloads', $downloads);

    }

    private function getDownloadsCount($post_id){
        global $wpdb;

        $rowcount = $wpdb->get_var("SELECT COUNT(*) as dowload_count FROM `{$this->db_table}` WHERE file_id = $post_id");

        return $rowcount;
    }

    private function insertToDB($post_id){
        global $wpdb;

        try {
            $wpdb->hide_errors();

            $table = $this->db_table;

            // clean up referral url
            $referral_url = str_replace("{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}", '', $_SERVER['HTTP_REFERER']);

            $data = array(
                'file_id'       => $post_id,
                'user_ip'       => $_SERVER['REMOTE_ADDR'],
                'referral_url'  => $referral_url,
                'user_agent'    => $_SERVER['HTTP_USER_AGENT'],
            );

            $format = array('%s','%s','%s');

            $wpdb->insert($table,$data,$format);

            $wpdb->show_errors();
            $e = $wpdb->last_error;

            if(preg_match("/Table .* doesn't exist/", $e)) {
                Media_Download_Counter_Admin::instance()->createTable();
                $this->insertToDB($post_id);
            }
        } catch (Exception $e) {
            error_log($e);
        }
    }

    private function addHooks(){

        add_action( 'wp', array($this, 'process_download_count') );

//        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));
    }


    private function getMediaPostByAttachment($attachment){
        global $wpdb;
        $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_value LIKE '%$attachment%' AND meta_key = '_wp_attached_file'", OBJECT );

        if(!empty($results))
            return get_post($results[0]->post_id);
        else
            return false;
    }

    /**
     * @return array
     */
    public function getAcceptedFiletypes()
    {
        return $this->accepted_filetypes;
    }

    /**
     * @param array $accepted_filetypes
     */
    public function setAcceptedFiletypes($accepted_filetypes)
    {
        $this->accepted_filetypes = $accepted_filetypes;
    }
}

Media_Download_Counter_Core::instance();