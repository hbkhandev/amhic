<?

namespace TSD_Infinisite;

class Module {

    public $cells = [];
    public $config = [];

    public $acf_modules = [];

    private $layout;
    private $html;
    private $acf_module;


    function __construct($layout = false, $row_index = 0, $cell_index = 0, $module_index = 0) {

        /**
         * module constructor :
         *
         * when a module is constructred, we are assuming it is going to
         * be used to print out content to a template. typically, this
         * is going to be coming from a cell object, but if it isn't its fine.
         */

        $this->build_config($layout, $row_index, $cell_index, $module_index);
        $this->build_html();


    }


    private function build_config($layout, $row_index, $cell_index, $module_index) {
        $this->layout = $layout;
        $this->config = ['row_index'    => $row_index,
                         'cell_index'   => $cell_index,
                         'module_index' => $module_index,];


        $this->acf_module = $this->layout->config['acf_rows'][$row_index]['cells'][$cell_index]['is_acf_fc_container'][$module_index];


    }

    public function get_acf_module_type() {

        return $this->acf_module['acf_fc_layout'];

    }

    private function build_html() {


        /**
         * this is going to be the primary function of this module, to take the given acf field
         * and return some html.
         */

        // foreach modules as module, create new module with layout, row index, cell index and module

        // we have shifted template loading into its own class, we used to check for
        // custom templates here and would render them elsewhere. we will find those
        // methods and eliminate them in our march toward 1.0

        // the way that post archives load their templates is different than the rest of the modules
        // so we check if we're dealing with a post archive. if not, we do our standard template check.
        // if we are, we just pass along the default template - because it's templates are set via its
        // own template selection system (query vs per-post)

        if ($this->acf_module['acf_fc_layout'] !== 'post_archive'):

            if (!array_key_exists('template', $this->acf_module)) {
                $this->acf_module['template'] = 'default';
            }

            $default_template = INFINISITE_URI . "module_templates/{$this->acf_module['acf_fc_layout']}.php";

            $custom_template = $_SERVER['DOCUMENT_ROOT'] . $this->acf_module['template'];

            $acf_module_template = $this->acf_module['template'] == 'default' ? $default_template : $custom_template;
        else:

            // post archive functionality is its own class, which is launched from the default template file
            $acf_module_template = INFINISITE_URI . "module_templates/{$this->acf_module['acf_fc_layout']}.php";;
        endif;


        // the ACF_Module is the IS API for Module Calls
        $module = new ACF_Module(['module'          => $this->acf_module,
                                  'module_template' => $acf_module_template,
                                  'layout'          => $this->layout]);

        $this->html .= $module->get_html();


    }

    public function get_html() {

        /**
         * this is the public function that retrieves the output from the module object
         */

        return $this->html;
    }

    public static function get_acf_module_information_for_registration() {

        $transient = DB::function_cache('Module::acf_module_info');

        if (!empty($transient))
            return $transient;


        /**
         * we don't register the modules every time we new up a module
         */

        $return = [];

        $return = array_merge($return, Module::build_simple_acf_modules());
        $return = array_merge($return, Module::build_gravity_form_module());
        $return = array_merge($return, Module::build_bucket_module());
        $return = array_merge($return, Module::build_post_archive_module());
        $return = array_merge($return, Module::build_collapsible_content_module());

        $modules_to_hide = Module::get_acf_modules_to_hide();

        foreach ($modules_to_hide as $module) {
            unset($return[$module]);
        }

        return DB::function_cache('Module::acf_module_info', $return);

    }

    private static function get_acf_modules_to_hide() {


        $module_fields = Option::build_page_builer_config_settings();
        $modules_to_hide = [];

        foreach ($module_fields['fields'] as $field):
            $hide_this_module = \get_option("options_" . $field['name'] . "_hide");
            if (!$hide_this_module) {
                continue;
            }
            $length_of_db_string = strlen('is_page_builder_module_options_');
            $modules_to_hide[] = substr($field['name'], $length_of_db_string);
        endforeach;

        return $modules_to_hide;


    }

    static function build_simple_acf_modules() {

        $acf_modules['simple_content'] = ['label'      => 'Simple Content',
                                          'name'       => 'simple_content',
                                          'key'        => 'field_ispb_layout_simple_content',
                                          'sub_fields' => [ACF::get_horizontal_tab('Content', 'field_ispb_layout_content_wysiwyg_tab'),
                                                           'content'     => ['key'   => 'field_ispb_layout_simple_content_wysiwyg',
                                                                             'label' => 'Simple Content',
                                                                             'name'  => 'simple_content',
                                                                             'type'  => 'wysiwyg',],
                                                           ACF::get_horizontal_tab('Config', 'field_ispb_layout_content_config_tab'),
                                                           'config'      => ['key'     => 'field_isbp_layout_simple_content_template',
                                                                             'label'   => 'Template',
                                                                             'name'    => 'template',
                                                                             'type'    => 'select',
                                                                             'ui'      => 1,
                                                                             'choices' => Page_Builder::ispb_module_templates('simple_content'),
                                                                             'wrapper' => ['width' => 50]],
                                                           'class_names' => ['key'     => 'field_ispb_layout_simple_content_classes',
                                                                             'label'   => 'Class Name',
                                                                             'name'    => 'class_names',
                                                                             'type'    => 'text',
                                                                             'wrapper' => ['width' => 50]]]];

        $acf_modules['submenu'] = ['label'      => 'Submenu',
                                   'name'       => 'submenu',
                                   'key'        => 'field_ispb_layout_submenu',
                                   'sub_fields' => ['menu'     => ['key'     => 'field_ispb_layout_submenu_menu',
                                                                   'name'    => 'menu',
                                                                   'type'    => 'select',
                                                                   'choices' => Page_Builder::get_menus_for_acf_module(),
                                                                   'value'   => 'medium',
                                                                   'ui'      => 1,
                                                                   'wrapper' => ['width' => 50]],
                                                    'template' => ['key'     => 'field_ispb_layout_submenu_template',
                                                                   'label'   => 'Template',
                                                                   'name'    => 'template',
                                                                   'type'    => 'select',
                                                                   'choices' => Acme::get_templates('components/submenu_templates/'),
                                                                   'wrapper' => ['width' => 50]]]];

        $acf_modules['icon'] = ['label'      => 'Icon',
                                'name'       => 'icon',
                                'key'        => 'field_ispb_layout_icon',
                                'sub_fields' => [ACF::get_horizontal_tab('Icon Selection', 'field_5a29673e16383'),
                                                 'icon'      => ['key'           => 'field_ispb_layout_icon_selector',
                                                                 'label'         => 'Font Awesome Icon',
                                                                 'name'          => 'icon',
                                                                 'type'          => 'select',
                                                                 'ui'            => 1,
                                                                 'default_value' => 0,
                                                                 'class'         => 'font-awesome',
                                                                 'choices'       => Icon_Library::get_fontawesome_icons_for_acf_select(),
                                                                 'wrapper'       => ['width' => 50]],
                                                 'svg'       => ['key'     => 'field_ispb_layout_icon_svg',
                                                                 'label'   => 'SVG',
                                                                 'name'    => 'svg',
                                                                 'type'    => 'file',
                                                                 'wrapper' => ['width' => 50]],
                                                 ACF::get_horizontal_tab('Icon Module Config', 'field_5a29673e16382'),
                                                 'size'      => ['key'           => 'field_ispb_layout_icon_size',
                                                                 'label'         => 'Size',
                                                                 'name'          => 'size',
                                                                 'type'          => 'select',
                                                                 'default_value' => '',
                                                                 'choices'       => Icon_Library::get_fontawesome_sizes_for_acf_select(),
                                                                 'wrapper'       => ['width' => 100 / 4]],
                                                 'animation' => ['key'        => 'field_ispb_layout_icon_animation',
                                                                 'label'      => 'Animation Effects',
                                                                 'name'       => 'animation_effects',
                                                                 'type'       => 'select',
                                                                 'ui'         => 1,
                                                                 'allow_null' => 1,
                                                                 'choices'    => ['fa-spin'  => 'Spin',
                                                                                  'fa-pulse' => 'Pulse'],
                                                                 'wrapper'    => ['width' => 100 / 4]],
                                                 'rotation'  => ['key'        => 'field_ispb_layout_icon_rotation',
                                                                 'label'      => 'Rotation / Flip',
                                                                 'name'       => 'rotate_flip',
                                                                 'type'       => 'select',
                                                                 'ui'         => 1,
                                                                 'allow_null' => 1,
                                                                 'choices'    => ['fa-rotate-90'       => '90 Clockwise',
                                                                                  'fa-rotate-180'      => 'Rotate Upside Down',
                                                                                  'fa-rotate-270'      => '90 Counterclockwise',
                                                                                  'fa-flip-horizontal' => 'Flip Horizontal',
                                                                                  'fa-flip-vertical'   => 'Flip Vertical',],
                                                                 'wrapper'    => ['width' => 100 / 4]],
                                                 'link'      => ['key'     => 'field_ispb_layout_icon_link',
                                                                 'label'   => 'Link',
                                                                 'name'    => 'link',
                                                                 'type'    => 'link',
                                                                 'wrapper' => ['width' => 100 / 4]],
                                                 ACF::get_color_field('field_ispb_layout_icon_color_field', ['wrapper' => ['width' => 100 / 2,]]),
                                                 'caption'   => ['key'     => 'field_ispb_layout_icon_caption',
                                                                 'label'   => 'Caption',
                                                                 'name'    => 'caption',
                                                                 'type'    => 'text',
                                                                 'wrapper' => ['width' => 100 / 2,]],]];

        $acf_modules['image'] = ['label'      => 'Single Image',
                                 'name'       => 'single_image',
                                 'key'        => 'field_ispb_layout_single_image',
                                 'sub_fields' => [ACF::get_horizontal_tab('Image', 'field_ispb_layout_image_file_tab'),
                                                  'image'             => ['key'          => 'field_ispb_layout_single_image_file',
                                                                          'label'        => 'Image',
                                                                          'name'         => 'image',
                                                                          'type'         => 'image',
                                                                          'preview_size' => 'medium'],
                                                  ACF::get_horizontal_tab('Config', 'field_ispb_layout_image_config_tab'),
                                                  'full_width_toggle' => ['key'          => 'field_ispb_layout_single_image_full_width',
                                                                          'name'         => 'full_width',
                                                                          'type'         => 'true_false',
                                                                          'instructions' => 'Stretch Image to Cell Width?',
                                                                          'message'      => 'Full Width',
                                                                          'wrapper'      => ['width' => 50]],
                                                  'thumbnail_size'    => ['key'          => 'field_ispb_layout_single_image_thumbnail_size',
                                                                          'name'         => 'thumbnail_size',
                                                                          'type'         => 'select',
                                                                          'instructions' => 'WordPress Thumbnail Size',
                                                                          'choices'      => Acme::get_image_sizes_for_acf(),
                                                                          'default'      => 'medium',
                                                                          'wrapper'      => ['width' => 50]],
                                                  'link'              => ['key'          => 'field_ispb_layout_single_image_link',
                                                                          'name'         => 'link',
                                                                          'type'         => 'link',
                                                                          'instructions' => 'Link (Optional)',
                                                                          'wrapper'      => ['width' => 50]],
                                                  'template'          => ['key'           => 'field_ispb_layout_single_image_template',
                                                                          'name'          => 'template',
                                                                          'type'          => 'select',
                                                                          'ui'            => 1,
                                                                          'default_value' => 'default',
                                                                          'instructions'  => 'Select a Template',
                                                                          'wrapper'       => ['width' => 50],
                                                                          'choices'       => Page_Builder::ispb_module_templates('single_image')],]];

        $acf_modules['multiple_images'] = ['label'      => 'Multiple Images',
                                           'name'       => 'multiple_images',
                                           'key'        => 'field_ispb_layout_multiple_image',
                                           'sub_fields' => [ACF::get_horizontal_tab('Content', 'field_5a0dd7b76102'),
                                                            'images'   => ['key'   => 'field_ispb_layout_multiple_image_files',
                                                                           'label' => 'Images',
                                                                           'name'  => 'images',
                                                                           'type'  => 'gallery',
                                                                           'min'   => 1],
                                                            ACF::get_horizontal_tab('Config', 'field_5a0dd7b765103'),
                                                            'template' => ['key'     => 'field_ispb_layout_multiple_image_template',
                                                                           'label'   => 'Template',
                                                                           'name'    => 'template',
                                                                           'type'    => "select",
                                                                           'ui'      => 1,
                                                                           'choices' => Page_Builder::ispb_module_templates('multiple_images'),],]];

        $acf_modules['accordion'] = ['label'      => 'Accordion',
                                     'name'       => 'accordion',
                                     'key'        => 'field_ispb_layout_accordion',
                                     'sub_fields' => [ACF::get_horizontal_tab('Content', 'field_ispb_layout_accordion_content_tab'),
                                                      ['key'          => 'field_ispb_layout_accordion_blade',
                                                       'label'        => 'Blade',
                                                       'name'         => 'blade',
                                                       'type'         => 'repeater',
                                                       'button_label' => 'Add Blade',
                                                       'min'          => 1,
                                                       'layout'       => 'block',
                                                       'sub_fields'   => [['key'   => 'field_ispb_layout_accordion_title',
                                                                           'label' => 'Title',
                                                                           'name'  => 'title',
                                                                           'type'  => 'text'],
                                                                          ['key'   => 'field_ispb_layout_accordion_content',
                                                                           'label' => 'Content',
                                                                           'name'  => 'content',
                                                                           'type'  => 'wysiwyg']]],
                                                      ACF::get_horizontal_tab('Config', 'field_ispb_layout_accordion_config_tab'),
                                                      ['key'           => 'field_ispb_layout_accordion_start_with_first_open',
                                                       'label'         => 'Start With First Open',
                                                       'name'          => 'start_with_first_open',
                                                       'type'          => 'true_false',
                                                       'ui'            => 1,
                                                       'default_value' => 1,
                                                       'wrapper'       => ['width' => '33']],
                                                      ['key'     => 'field_ispb_layout_accordion_allow_multiple_open',
                                                       'label'   => 'Allow Multiple Open',
                                                       'name'    => 'allow_multiple_open',
                                                       'type'    => 'true_false',
                                                       'ui'      => 1,
                                                       'wrapper' => ['width' => '33']],
                                                      ['key'     => 'field_ispb_layout_accordion_allow_all_closed',
                                                       'label'   => 'Allow All Closed',
                                                       'name'    => 'allow_all_closed',
                                                       'type'    => 'true_false',
                                                       'ui'      => 1,
                                                       'wrapper' => ['width' => '33']],
                                                      ['key'     => 'field_ispb_layout_accordion_template',
                                                       'label'   => 'Template',
                                                       'name'    => 'template',
                                                       'type'    => 'select',
                                                       'choices' => Page_Builder::ispb_module_templates('accordion'),]]];

        $acf_modules['document_viewer'] = ['label'      => 'Document Viewer',
                                           'name'       => 'document_viewer',
                                           'key'        => 'field_ispb_layout_document',
                                           'sub_fields' => [ACF::get_horizontal_tab('Document', 'field_ispb_layout_document_file_tab'),
                                                            ['key'   => 'field_ispb_layout_document_file',
                                                             'label' => 'Document',
                                                             'name'  => 'document',
                                                             'type'  => 'file',],
                                                            ACF::get_horizontal_tab('Config', 'field_ispb_layout_document_config_tab'),
                                                            ['key'     => 'field_ispb_layout_document_template',
                                                             'label'   => 'Template',
                                                             'type'    => 'select',
                                                             'choices' => Page_Builder::ispb_module_templates('document_viewer'),]]];

        $acf_modules['multi_document_viewer'] = ['label'      => 'Multiple File Container',
                                                 'name'       => 'multiple_file_container',
                                                 'key'        => 'field_ispb_layout_multiple_file',
                                                 'sub_fields' => [ACF::get_horizontal_tab('Documents', 'field_ispb_layout_multiple_document_files_tab'),
                                                                  ['key'          => 'field_ispb_layout_multiple_file_container',
                                                                   'label'        => 'File Container',
                                                                   'name'         => 'file_container',
                                                                   'type'         => 'repeater',
                                                                   'button_label' => 'Add File',
                                                                   'min'          => 1,
                                                                   'sub_fields'   => [['key'   => 'field_ispb_layout_multiple_file_file',
                                                                                       'label' => 'File',
                                                                                       'name'  => 'file',
                                                                                       'type'  => 'file']]],
                                                                  ACF::get_horizontal_tab('Config', 'field_ispb_layout_multiple_document_query_tab'),
                                                                  ['key'     => 'field_ispb_layout_multiple_file_container_template',
                                                                   'name'    => 'template',
                                                                   'label'   => 'Template',
                                                                   'type'    => 'select',
                                                                   'ui'      => 1,
                                                                   'choices' => Page_Builder::ispb_module_templates('multiple_file_container'),]]];

        $acf_modules['table_content'] = ['label'      => 'Table Content',
                                         'name'       => 'table_content',
                                         'key'        => 'field_ispb_layout_table_content',
                                         'sub_fields' => ['config'  => ['key'     => 'field_isbp_layout_table_content_template',
                                                                        'label'   => 'Template',
                                                                        'name'    => 'template',
                                                                        'type'    => 'select',
                                                                        'ui'      => 1,
                                                                        'choices' => Page_Builder::ispb_module_templates('table_content')],
                                                          'content' => ['key'        => 'field_ispb_layout_table_content_wysiwyg',
                                                                        'label'      => 'Table Content',
                                                                        'name'       => 'table_content',
                                                                        'type'       => 'table',
                                                                        'use_header' => 0]]];


        $acf_modules['button'] = ['label'      => 'Button',
                                  'name'       => 'button',
                                  'key'        => 'field_ispb_layout_button',
                                  'sub_fields' => [ACF::get_horizontal_tab('Button Options', 'field_ispb_layout_button_links_tab'),
                                                   ['key'          => 'field_ispb_layout_button_group',
                                                    'label'        => 'Links',
                                                    'name'         => 'buttons',
                                                    'type'         => 'repeater',
                                                    'button_label' => 'Add Link',
                                                    'layout'       => 'block',
                                                    'min'          => 1,
                                                    'sub_fields'   => [['key'   => 'field_ispb_layout_button_link',
                                                                        'label' => 'Link',
                                                                        'name'  => 'link',
                                                                        'type'  => 'link']]],
                                                   ACF::get_horizontal_tab('Config', 'field_ispb_layout_button_config_tab'),
                                                   ['key'        => 'field_ispb_layout_button',
                                                    'name'       => 'config',
                                                    'type'       => 'group',
                                                    'layout'     => 'block',
                                                    'sub_fields' => [['key'     => 'field_ispb_layout_button_group_alignment',
                                                                      'label'   => 'Alignment',
                                                                      'name'    => 'alignment',
                                                                      'type'    => 'button_group',
                                                                      'choices' => ['left'   => 'Left',
                                                                                    'center' => 'Center',
                                                                                    'right'  => 'Right'],
                                                                      'wrapper' => ['width' => 33]],
                                                                     ['key'     => 'field_ispb_layout_button_group_size',
                                                                      'label'   => 'Size',
                                                                      'name'    => 'size',
                                                                      'type'    => 'button_group',
                                                                      'choices' => ['small'  => 'Small',
                                                                                    'medium' => 'Medium',
                                                                                    'large'  => 'Large',],
                                                                      'wrapper' => ['width' => 33]],
                                                                     ['key'     => 'field_ispb_layout_button_group_orientation',
                                                                      'label'   => 'Orientation',
                                                                      'name'    => 'orientation',
                                                                      'type'    => 'button_group',
                                                                      'choices' => ['horizontal' => 'Horizontal',
                                                                                    'vertical'   => 'Vertical',],
                                                                      'wrapper' => ['width' => 33]],
                                                                     ['key'     => 'field_ispb_layout_button_classes',
                                                                      'label'   => 'Classes',
                                                                      'name'    => 'classes',
                                                                      'type'    => 'text',
                                                                      'wrapper' => ['width' => 33]],
                                                                     ACF::get_color_field('field_ispb_layout_button_group_color_options', ['wrapper'    => ['width' => 33],
                                                                                                                                           'allow_null' => 1]),
                                                                     ['key'     => 'field_ispb_layout_button_group_hollow',
                                                                      'label'   => 'Hollow',
                                                                      'message' => 'Apply Hollow Button Styling',
                                                                      'name'    => 'hollow',
                                                                      'type'    => 'true_false',
                                                                      'wrapper' => ['width' => 33]],]],]];

        $acf_modules['spacer'] = ['key'        => 'field_ispb_layout_spacer',
                                  'name'       => 'spacer',
                                  'label'      => 'Spacer',
                                  'display'    => 'block',
                                  'sub_fields' => [ACF::get_horizontal_tab('Spacer Size', 'field_ispb_layout_spacer_size_tab'),
                                                   ['key'           => 'field_ispb_layout_spacer_size',
                                                    'label'         => 'Size',
                                                    'name'          => 'size',
                                                    'type'          => 'button_group',
                                                    'default_value' => 'medium',
                                                    'choices'       => ['xsmall' => 'X-Small',
                                                                        'small'  => 'Small',
                                                                        'medium' => 'Medium',
                                                                        'large'  => 'Large',
                                                                        'xlarge' => 'X-Large',]],
                                                   ACF::get_horizontal_tab('Divider Options', 'field_ispb_layout_divider_options_tab'),
                                                   ['key'          => 'field_ispb_layout_spacer_divider_color',
                                                    'label'        => 'Divider Color',
                                                    'name'         => 'divider_color',
                                                    'instructions' => 'Leave blank for no divider, choose colors as needed for theme',
                                                    'type'         => 'select',
                                                    'multiple'     => 1,
                                                    'ui'           => 1,
                                                    'choices'      => Palette::build_get_colors_into_background_options_select_choices(),],
                                                   ['key'               => 'field_ispb_layout_spacer_divider_position',
                                                    'label'             => 'Divider Position',
                                                    'name'              => 'divider_position',
                                                    'type'              => 'button_group',
                                                    'default_value'     => 'middle',
                                                    'choices'           => ['top'    => 'Top',
                                                                            'middle' => 'Middle',
                                                                            'bottom' => 'Bottom',
                                                                            'left'   => 'Left',
                                                                            'center' => 'Center',
                                                                            'right'  => 'Right',],
                                                    'conditional_logic' => [[['field'    => 'field_ispb_layout_spacer_divider_color',
                                                                              'operator' => '!=',
                                                                              'value'    => '']]]],]];

        $acf_modules['header'] = ['label'      => 'Header',
                                  'name'       => 'header',
                                  'key'        => 'field_ispb_layout_header',
                                  'sub_fields' => [ACF::get_horizontal_tab('Header Text', 'field_ispb_layout_header_text_tab'),
                                                   ['key'     => 'field_ispb_layout_header_text',
                                                    'label'   => 'Text',
                                                    'name'    => 'text',
                                                    'type'    => 'text',
                                                    'wrapper' => ['width' => 50]],
                                                   ['key'     => 'field_ispb_layout_header_element',
                                                    'label'   => 'Element',
                                                    'name'    => 'element',
                                                    'type'    => 'select',
                                                    'choices' => ['h1' => 'H1',
                                                                  'h2' => 'H2',
                                                                  'h3' => 'H3',
                                                                  'h4' => 'H4',
                                                                  'h5' => 'H5',
                                                                  'h6' => 'H6'],
                                                    'wrapper' => ['width' => 20]],
                                                   ['key'     => 'field_ispb_layout_header_class',
                                                    'label'   => 'Class',
                                                    'name'    => 'class',
                                                    'type'    => 'text',
                                                    'wrapper' => ['width' => 30]],
                                                   ACF::get_horizontal_tab('Header Config', 'field_ispb_layout_header_config_tab'),
                                                   ACF::get_color_field('field_ispb_layout_header_color', ['allow_null'    => 1,
                                                                                                           'default_value' => '',
                                                                                                           'wrapper'       => ['width' => 100 / 3]]),
                                                   ['key'        => 'field_ispb_layout_header_alignment',
                                                    'label'      => 'Alignment',
                                                    'name'       => 'alignment',
                                                    'type'       => 'select',
                                                    'ui'         => 1,
                                                    'allow_null' => 1,
                                                    'choices'    => ['center' => 'Center',
                                                                     'right'  => 'Right'],
                                                    'wrapper'    => ['width' => 100 / 3]],
                                                   ['key'     => 'field_ispb_layout_header_link',
                                                    'label'   => 'Link',
                                                    'name'    => 'link',
                                                    'type'    => 'link',
                                                    'wrapper' => ['width' => 100 / 3]],]];

        $acf_modules['oEmbed'] = ['key'        => 'field_ispb_layout_embed',
                                  'name'       => 'embed',
                                  'label'      => 'Embed',
                                  'sub_fields' => [['key'           => 'field_ispb_layout_embed_aspect_ratio',
                                                    'name'          => 'aspect_ratio',
                                                    'type'          => 'button_group',
                                                    'default_value' => 'widescreen',
                                                    'choices'       => [''           => 'Letterbox',
                                                                        'widescreen' => 'Widescreen']],
                                                   ['key'     => 'field_ispb_layout_embed_src',
                                                    'name'    => 'embed',
                                                    'label'   => 'Video Embed',
                                                    'type'    => 'oembed',
                                                    'wrapper' => ['width' => 50]],
                                                   ['key'          => 'field_ispb_layout_embed_poster',
                                                    'name'         => 'poster',
                                                    'label'        => 'Poster Image',
                                                    'type'         => 'image',
                                                    'preview_size' => 'medium',
                                                    'wrapper'      => ['width' => 50]]]];

        $acf_modules['query'] = ['key'        => 'field_ispb_layout_query',
                                 'name'       => 'query',
                                 'label'      => 'Query',
                                 'sub_fields' => [ACF::get_horizontal_tab('Query', 'field_ispb_layout_query_Query_tab'),
                                                  ['key'   => 'field_ispb_layout_query_args',
                                                   'label' => 'Query Arguments',
                                                   'name'  => 'arguments',
                                                   'type'  => 'textarea',],
                                                  ACF::get_horizontal_tab('Config', 'field_ispb_layout_query_Config_tab'),
                                                  ['key'     => 'field_ispb_layout_query_template',
                                                   'name'    => 'template',
                                                   'type'    => 'select',
                                                   'label'   => 'Template',
                                                   'choices' => Page_Builder::ispb_module_templates('query'),],]];


        $acf_modules['hero'] = ['key'        => 'field_ispb_layout_hero',
                                'name'       => 'hero',
                                'label'      => 'Hero',
                                'sub_fields' => [ACF::get_horizontal_tab('Config', 'field_ispb_layout_hero_Hero_tab'),
                                                 ['key'     => 'field_ispb_layout_hero_template',
                                                  'name'    => 'template',
                                                  'type'    => 'select',
                                                  'label'   => 'Template',
                                                  'choices' => Page_Builder::ispb_module_templates('hero'),
                                                  'wrapper' => ['width' => 33,]],
                                                 ['key'     => 'field_ispb_layout_hero_primary_text',
                                                  'label'   => 'Title',
                                                  'name'    => 'title',
                                                  'type'    => 'text',
                                                  'wrapper' => ['width' => 33,]],
                                                 ['key'          => 'field_ispb_layout_hero_primary_image',
                                                  'label'        => 'Image',
                                                  'name'         => 'image',
                                                  'type'         => 'image',
                                                  'preview_size' => 'medium',
                                                  'wrapper'      => ['width' => 33,]],
                                                 ACF::get_horizontal_tab('Content Blocks', 'field_ispb_layout_hero_wysiwyg_tab'),
                                                 ['key'          => 'field_ispb_layout_hero_content_blocks',
                                                  'name'         => 'content_blocks',
                                                  'type'         => 'repeater',
                                                  'button_label' => 'Add Content Block',
                                                  'sub_fields'   => [['key'   => 'field_ispb_layout_hero_wysiwyg',
                                                                      'label' => 'Content Blocks',
                                                                      'name'  => 'wysiwyg',
                                                                      'type'  => 'wysiwyg',]]],
                                                 ACF::get_horizontal_tab('Featured Posts', 'field_ispb_layout_hero_featured_posts_tab'),
                                                 ['key'   => 'field_ispb_layout_hero_featured_posts',
                                                  'label' => '',
                                                  'name'  => 'featured_posts',
                                                  'type'  => 'relationship',],
                                                 ACF::get_horizontal_tab('Menu Options', 'field_ispb_layout_hero_menu_options_tab'),
                                                 ['key'          => 'field_ispb_layout_hero_menu_repeater',
                                                  'label'        => 'Menu',
                                                  'name'         => 'menus',
                                                  'type'         => 'repeater',
                                                  'button_label' => 'Add Menu',
                                                  'sub_fields'   => [['key'        => 'field_ispb_layout_hero_menu_select',
                                                                      'label'      => 'Menu',
                                                                      'name'       => 'name',
                                                                      'type'       => 'select',
                                                                      'ui'         => 1,
                                                                      'allow_null' => 1,
                                                                      'choices'    => Page_Builder::get_menus_for_acf_module(),
                                                                      'wrapper'    => ['width' => 75]],
                                                                     ['key'        => 'field_ispb_layout_hero_menu_submenu_walker',
                                                                      'label'      => 'Walker',
                                                                      'name'       => 'walker',
                                                                      'type'       => 'select',
                                                                      'ui'         => 1,
                                                                      'allow_null' => 1,
                                                                      'choices'    => IS_Walker::get_walkers(),
                                                                      'wrapper'    => ['width' => 25]]]],]];

        $acf_modules['text_row'] = ['key'        => 'field_ispb_layout_text_row',
                                    'label'      => 'Text Row',
                                    'name'       => 'text_row',
                                    'sub_fields' => [ACF::get_horizontal_tab('Text Columns', 'field_ispb_layout_text_row_columns_tab'),
                                                     ['key'          => 'field_ispb_layout_text_row_columns',
                                                      'label'        => 'Columns',
                                                      'name'         => 'columns',
                                                      'type'         => 'repeater',
                                                      'button_label' => 'Add Text Row Cell',
                                                      'sub_fields'   => [['key'   => 'field_ispb_layout_text_row_column_content',
                                                                          'label' => 'Content',
                                                                          'name'  => 'content',
                                                                          'type'  => 'wysiwyg']],],
                                                     ACF::get_horizontal_tab('Config', 'field_ispb_layout_text_row_config_tab'),
                                                     ['key'     => 'field_ispb_layout_text_row_blocks_per_row_count',
                                                      'label'   => 'Cells per row',
                                                      'name'    => 'cells_per_row',
                                                      'type'    => 'number',
                                                      'min'     => 0,
                                                      'step'    => 1,
                                                      'wrapper' => ['width' => 25],],
                                                     ['key'     => 'field_ispb_layout_text_row_template',
                                                      'name'    => 'template',
                                                      'type'    => 'select',
                                                      'label'   => 'Module Template',
                                                      'ui'      => 1,
                                                      'wrapper' => ['width' => 75],
                                                      'choices' => Page_Builder::ispb_module_templates('text_row'),],]];

        $acf_modules['cta'] = ['key'        => 'field_ispb_layout_cta',
                               'label'      => 'CTA',
                               'name'       => 'cta',
                               'sub_fields' => [['key'     => 'field_ispb_layout_cta_template',
                                                 'name'    => 'template',
                                                 'type'    => 'select',
                                                 'label'   => 'Template',
                                                 'ui'      => 1,
                                                 'choices' => Page_Builder::ispb_module_templates('cta'),],]];

        $acf_modules['wireframe'] = ['key'        => 'field_ispb_layout_wireframe',
                                     'label'      => 'Wireframe',
                                     'name'       => 'wireframe',
                                     'sub_fields' => [['key'     => 'field_ispb_layout_wireframe_title',
                                                       'name'    => 'title',
                                                       'type'    => 'text',
                                                       'label'   => 'Title',
                                                       'wrapper' => ['width' => '20'],],
                                                      ['key'           => 'field_ispb_layout_wireframe_height',
                                                       'name'          => 'height',
                                                       'type'          => 'text',
                                                       'label'         => 'Height',
                                                       'default_value' => '150px',
                                                       'required'      => 1,
                                                       'wrapper'       => ['width' => '20'],],
                                                      ['key'        => 'field_ispb_layout_wireframe_form',
                                                       'name'       => 'form',
                                                       'type'       => 'select',
                                                       'ui'         => 1,
                                                       'allow_null' => 1,
                                                       'label'      => 'Form',
                                                       'choices'    => Acme::get_gravity_forms(),
                                                       'wrapper'    => ['width' => '20'],],
                                                      ['key'     => 'field_ispb_layout_wireframe_popup_note',
                                                       'name'    => 'note_in_modal',
                                                       'type'    => 'true_false',
                                                       'label'   => 'Note In Modal',
                                                       'ui'      => 1,
                                                       'wrapper' => ['width' => '15'],],
                                                      ['key'     => 'field_ispb_layout_wireframe_template',
                                                       'name'    => 'template',
                                                       'type'    => 'select',
                                                       'label'   => 'Template',
                                                       'ui'      => 1,
                                                       'choices' => Page_Builder::ispb_module_templates('wireframe'),
                                                       'wrapper' => ['width' => '25'],],
                                                      ['key'     => 'field_ispb_layout_wireframe_note',
                                                       'name'    => 'note',
                                                       'type'    => 'wysiwyg',
                                                       'label'   => 'Note',
                                                       'wrapper' => ['width' => '70'],],
                                                      ['key'          => 'field_ispb_layout_wireframe_links',
                                                       'name'         => 'links',
                                                       'type'         => 'repeater',
                                                       'button_label' => 'Add Link',
                                                       'wrapper'      => ['width' => '30'],
                                                       'sub_fields'   => [['key'      => 'field_ispb_layout_wireframe_links_link',
                                                                           'name'     => 'link',
                                                                           'type'     => 'link',
                                                                           'label'    => 'Links',
                                                                           'required' => 1,],]],

                                     ]];


        $acf_modules['d3'] = ['key'        => 'field_ispb_layout_d3',
                              'label'      => 'Data Visualizer',
                              'name'       => 'data_visualizer',
                              'sub_fields' => [ACF::get_horizontal_tab('Config', 'field_ispb_layout_config'),
                                               ['key'     => 'field_ispb_layout_data_viz_logic',
                                                'name'    => 'visualization_logic',
                                                'type'    => 'select',
                                                'label'   => 'Visualization Logic',
                                                'ui'      => 1,
                                                'choices' => Acme::get_templates('components/data_viz_logic'),],
                                               ['key'     => 'field_ispb_layout_data_viz_template',
                                                'name'    => 'template',
                                                'type'    => 'select',
                                                'label'   => 'Visualization Template',
                                                'ui'      => 1,
                                                'choices' => Page_Builder::ispb_module_templates('data_visualizer')],
                                               ACF::get_horizontal_tab('Data', 'field_ispb_layout_data'),
                                               ['key'        => 'field_ispb_layout_data_viz_graph_data',
                                                'name'       => 'data',
                                                'type'       => 'table',
                                                'use_header' => 0,],
                                               ['key'          => 'field_ispb_layout_data_viz_csv_data',
                                                'name'         => 'csv_data',
                                                'instructions' => 'Got more data? Enter your CSV below',
                                                'type'         => 'file',
                                                'label'        => 'CSV Data',
                                                'wrapper'      => ['width' => 33]],
                                               ['key'          => 'field_ispb_layout_data_viz_json_config',
                                                'name'         => 'json_config',
                                                'instructions' => 'Config options? Send \'em here.',
                                                'type'         => 'file',
                                                'label'        => 'JSON Config',
                                                'mime_types'   => 'json',
                                                'wrapper'      => ['width' => 33]],
                                               ['key'          => 'field_ispb_layout_data_viz_map_svg',
                                                'name'         => 'map_svg',
                                                'instructions' => 'If your JS requires a map, upload it here.',
                                                'type'         => 'file',
                                                'label'        => 'Map SVG',
                                                'wrapper'      => ['width' => 33]]]];

        return $acf_modules;

    }

    static function build_gravity_form_module() {

        if (!class_exists('GFAPI')) {
            return [];
        }

        $acf_modules['gravity_form'] = ['label'      => 'Gravity Form',
                                        'name'       => 'gravity_form',
                                        'key'        => 'field_5a29673e15e9f',
                                        'sub_fields' => [['key'     => 'field_5a29673e15ed7',
                                                          'label'   => 'Form',
                                                          'name'    => 'form',
                                                          'type'    => 'select',
                                                          'ui'      => 1,
                                                          'choices' => Acme::get_gravity_forms(),]]];

        return $acf_modules;

    }

    static function build_bucket_module() {

        /*
         * the bucket is only enabled if a post type with the slug 'bucket' is created
         */

        $custom_post_types = Acme::get_post_types();

        $is_has_buckets = false;

        if (!$custom_post_types)
            return [];

        foreach ($custom_post_types as $cpt)
            if ($cpt['post_type'] === 'bucket')
                $is_has_buckets = true;


        if (!$is_has_buckets)
            return [];


        $bucket_query = new \WP_Query(['post_type'      => 'bucket',
                                       'posts_per_page' => -1]);

        if (!$bucket_query->found_posts)
            return [];


        if (is_array($custom_post_types)) {
            foreach ($custom_post_types as $post_archetype) {
                $post_archive_post_type['choices'][$post_archetype['post_type']] = $post_archetype['name'];
            }
        }

        $post_archive_post_type['choices']['is_custom_posts'] = "Manual Post Selection";

        $bucket_choices = [];

        $bucket_post_groups = [];

        $bucket_post_fields = [];


        foreach ($bucket_query->posts as $post):
            $bucket_choices[$post->ID] = $post->post_title;

            $bucket_fields = get_field("is_bucket_flexible_content_layouts", $post->ID);

            $new_post_fields = ['key'               => "field_bucket_post_" . $post->ID,
                                'label'             => $post->post_title . " Fields",
                                'type'              => 'group',
                                'layout'            => 'row',
                                'name'              => 'bucket_post_fields_' . $post->ID,
                                'conditional_logic' => [[['field'    => 'field_ispb_bucket_post_select',
                                                          'operator' => '==',
                                                          'value'    => strval($post->ID)]]]];

            if (!$bucket_fields)
                continue;


            // this gets each instance of a layout
            foreach ($bucket_fields as $field):

                // this gets each field in a layout
                foreach ($field as $key => $value):

                    // acf puts this key/value pair on every module
                    if ($key === 'acf_fc_layout')
                        continue;

                    $new_post_fields['sub_fields'][] = ['key'   => 'field_bucket_shortcode_' . $field[$key],
                                                        'label' => $field[$key],
                                                        'name'  => 'bucket_shortcode_' . $field[$key],
                                                        'type'  => 'text'];

                endforeach;


            endforeach;

            $bucket_post_fields[] = $new_post_fields;
        endforeach;

        $bucket_post_groups[] = ['key'        => 'field_ispb_bucket_post_group_fields',
                                 'name'       => 'bucket_post_group_fields',
                                 'type'       => 'group',
                                 'layout'     => 'block',
                                 'sub_fields' => $bucket_post_fields];


        $post_select = ['key'     => 'field_ispb_bucket_post_select',
                        'name'    => 'bucket_post',
                        'label'   => "Bucket",
                        'type'    => 'select',
                        'ui'      => 1,
                        'choices' => $bucket_choices];


        $bucket_fields[] = $post_select;

        foreach ($bucket_post_groups as $group)
            $bucket_fields[] = $group;


        $acf_modules['bucket'] = ['label'      => 'Bucket',
                                  'name'       => 'bucket',
                                  'key'        => 'field_ispb_bucket_field',
                                  'sub_fields' => $bucket_fields];


        return $acf_modules;


    }

    static function build_post_archive_module() {

        // we are loading all the post types for this, not just the IS custom ones
        $custom_post_types = Acme::get_post_types(false);

        $post_archive_custom_post_options = ['key'               => 'field_ispb_layout_post_archive_manual_selector_repeater',
                                             'label'             => 'Custom Posts',
                                             'name'              => 'custom_posts',
                                             'type'              => 'relationship',
                                             'conditional_logic' => [[['field'    => 'field_ispb_layout_post_archive_cpt_selector',
                                                                       'operator' => '==',
                                                                       'value'    => 'is_custom_posts']]]];

        $post_archive_post_type = ['key'      => 'field_ispb_layout_post_archive_cpt_selector',
                                   'label'    => 'Post Type',
                                   'name'     => 'post_type',
                                   'type'     => 'select',
                                   'wrapper'  => ['width' => 50],
                                   'required' => 0,];


        // attaching the post types to the archive module selector


        // TODO: is this being called in acf/init? the cpt var is wrong sometimes


        if (is_array($custom_post_types)) {
            foreach ($custom_post_types as $post_archetype) {
                $post_archive_post_type['choices'][$post_archetype['post_type']] = $post_archetype['name'];
            }
        }

        $post_archive_post_type['choices']['is_custom_posts'] = "Manual Post Selection";


        $post_archive_display_toggle = ['key'           => 'field_ispb_layout_post_archive_display_type_radio',
                                        'label'         => 'Template Type',
                                        'name'          => 'template_type',
                                        'type'          => 'radio',
                                        'ui'            => 1,
                                        'instructions'  => 'Query templates control the entire cell, excerpt templates will display a standard archive type',
                                        'default_value' => 'excerpt',
                                        'choices'       => ['excerpt' => 'Excerpt',
                                                            'query'   => 'Query',],
                                        'wrapper'       => ['width' => 50],];

        $post_archive_template = ['key'               => 'field_ispb_layout_post_archive_template_selector',
                                  'label'             => 'Post Archive Excerpt Template',
                                  'name'              => 'post_archive_excerpt_template',
                                  'type'              => 'select',
                                  'ui'                => 1,
                                  'choices'           => Acme::get_templates('twig/post_excerpts/'),
                                  'conditional_logic' => [[['field'    => 'field_ispb_layout_post_archive_display_type_radio',
                                                            'operator' => '==',
                                                            'value'    => 'excerpt']]]];


        $query_template = ['key'               => 'field_ispb_layout_post_archive_query_template',
                           'label'             => 'Post Archive Query Template',
                           'name'              => 'post_archive_query_template',
                           'type'              => 'select',
                           'ui'                => 1,
                           'choices'           => Acme::get_templates('twig/query_excerpts/'),
                           'conditional_logic' => [[['field'    => 'field_ispb_layout_post_archive_display_type_radio',
                                                     'operator' => '==',
                                                     'value'    => 'query']]]];

        $post_archive_posts_per_page = ['key'           => 'field_ispb_layout_post_archive_posts_per_page',
                                        'label'         => 'Posts Per Page',
                                        'name'          => 'posts_per_page',
                                        'type'          => 'text',
                                        'default_value' => 3,
                                        'instructions'  => '-1 to display all posts',
                                        'wrapper'       => ['width' => 20],];

        $post_archive_pagination = ['key'           => 'field_ispb_layout_post_archive_pagination_boolean',
                                    'label'         => 'Pagination',
                                    'name'          => 'pagination',
                                    'type'          => 'true_false',
                                    'default_value' => 1,
                                    'message'       => 'Display Pagination',
                                    'wrapper'       => ['width' => 20],];

        $post_archive_posts_per_row = ['key'          => 'field_ispb_layout_post_archive_responsive_row_count',
                                       'label'        => 'Posts Per Row',
                                       'name'         => 'posts_per_row',
                                       'instructions' => '-1 for all posts in one row',
                                       'type'         => 'group',
                                       'wrapper'      => ['width' => 35],
                                       'sub_fields'   => [['key'           => 'field_ispb_layout_post_archive_responsive_row_count_small',
                                                           'label'         => 'Small',
                                                           'name'          => 'small',
                                                           'type'          => 'number',
                                                           'default_value' => 1,
                                                           'wrapper'       => ['width' => 33],],
                                                          ['key'           => 'field_ispb_layout_post_archive_responsive_row_count_medium',
                                                           'label'         => 'Medium',
                                                           'name'          => 'medium',
                                                           'type'          => 'number',
                                                           'default_value' => 2,
                                                           'wrapper'       => ['width' => 33],],
                                                          ['key'           => 'field_ispb_layout_post_archive_responsive_row_count_large',
                                                           'label'         => 'Large',
                                                           'name'          => 'large',
                                                           'type'          => 'number',
                                                           'default_value' => 3,
                                                           'wrapper'       => ['width' => 33],],]];

        $post_archive_hide_past_posts = ['key'     => 'field_ispb_post_archive_hide_past_posts',
                                         'label'   => 'Hide Past Posts',
                                         'name'    => 'hide_past_posts',
                                         'type'    => 'true_false',
                                         'ui'      => 1,
                                         'wrapper' => ['width' => 10]];

        $post_archive_hide_future_posts = ['key'     => 'field_ispb_post_archive_hide_future_posts',
                                           'label'   => 'Hide Future Posts',
                                           'name'    => 'hide_future_posts',
                                           'type'    => 'true_false',
                                           'ui'      => 1,
                                           'wrapper' => ['width' => 10]];

        $post_archive_date_filter = ['key'          => 'field_ispb_post_archive_date_filter',
                                     'label'        => 'Special Filter',
                                     'name'         => 'date_filter',
                                     'type'         => 'select',
                                     'instructions' => 'Last Name filters use the Post Title - if you need to display prefixes or suffixes, put a full version of the name in a CPT field and use that in your templates',
                                     'ui'           => 1,
                                     'allow_null'   => 1,
                                     'choices'      => ['Past'                  => "Past",
                                                        'Future'                => "Future",
                                                        'start_date_descending' => "Descending by Start Date",
                                                        'start_date_ascending'  => "Ascending by Start Date",
                                                        'last_name_a_to_z'      => 'Last Name A to Z',
                                                        'last_name_z_to_a'      => 'Last Name Z to A',
                                                        'post_title_a_to_z'     => 'Post Title A to Z',
                                                        'post_title_z_to_a'     => 'Post Title Z to A'],
                                     'wrapper'      => ['width' => 25],];

        $post_archive_padding = ['key'        => 'field_ispb_layout_post_archive_padding_group',
                                 'label'      => 'Padding',
                                 'name'       => 'padding',
                                 'type'       => 'group',
                                 'wrapper'    => ['width' => 30],
                                 'sub_fields' => [['key'           => 'field_ispb_layout_post_archive_padding_x',
                                                   'label'         => 'X',
                                                   'name'          => 'x',
                                                   'type'          => 'true_false',
                                                   'default_value' => 1,
                                                   'wrapper'       => ['width' => 50],],
                                                  ['key'           => 'field_ispb_layout_post_archive_padding_y',
                                                   'label'         => 'Y',
                                                   'name'          => 'y',
                                                   'type'          => 'true_false',
                                                   'default_value' => 1,
                                                   'wrapper'       => ['width' => 50],],]];

        $post_archive_custom_query = ['key'          => 'field_ispb_layout_post_archive_custom_query',
                                      'name'         => 'custom_query',
                                      'type'         => 'textarea',
                                      'label'        => 'Custom Query Args',
                                      'instructions' => 'Type in your WP Query Object Arguments in JSON notation. Double quotations only, no trailing comma',];

        $post_archive_filter_repeater_taxonomy = ['key'     => 'field_ispb_layout_post_archive_repeater_taxonomy',
                                                  'name'    => 'post_archive_filter_repeater_taxonomy',
                                                  'label'   => 'Taxonomy',
                                                  'type'    => 'select',
                                                  'choices' => Acme::get_categories_for_page_builder_filter_object(),
                                                  'wrapper' => ['width' => 60]];
        $post_archive_filter_repeater_display_type = ['key'     => 'field_ispb_layout_post_archive_repeater_display_type',
                                                      'name'    => 'post_archive_filter_repeater_display_type',
                                                      'label'   => 'Display Type',
                                                      'type'    => 'button_group',
                                                      'ui'      => 1,
                                                      'choices' => ['select' => 'Dropdown',
                                                                    'list'   => 'List',],
                                                      'wrapper' => ['width' => 25]];

        $post_archive_filter_repeater_allow_multiple = ['key'     => 'field_ispb_layout_post_archive_repeater_allow_multiple',
                                                        'name'    => 'post_archive_filter_repeater_allow_multiple',
                                                        'label'   => 'Multi - Select Inputs',
                                                        'type'    => 'true_false',
                                                        'ui'      => 1,
                                                        'wrapper' => ['width' => 15]];

        $post_archive_filter_repeater_fields = [$post_archive_filter_repeater_taxonomy,
                                                $post_archive_filter_repeater_display_type,
                                                $post_archive_filter_repeater_allow_multiple,];

        $post_archive_filter_repeater = ['key'          => 'field_ispb_layout_post_archive_filter_repeater',
                                         'name'         => 'filter_repeater',
                                         'type'         => 'repeater',
                                         'label'        => 'Filters',
                                         'instructions' => 'Add and configure your filters, post types are shown in gray',
                                         'layout'       => 'block',
                                         'button_label' => 'Add Filter Category',
                                         'sub_fields'   => $post_archive_filter_repeater_fields];


        $conditional_logic_display_filter_templates = [[['field'    => 'field_ispb_layout_post_archive_display_config_filter_placement',
                                                         'operator' => '!=',
                                                         'value'    => 'none']]];

        $post_archive_filter_display_config_filter_placement = ['key'           => 'field_ispb_layout_post_archive_display_config_filter_placement',
                                                                'name'          => 'post_archive_filter_display_config_filter_placement',
                                                                'label'         => 'Filter Placement',
                                                                'type'          => 'button_group',
                                                                'default_value' => 'none',
                                                                'choices'       => ['none' => 'None',
                                                                                    'top'  => 'Top',
                                                                                    'left' => 'Left'],
                                                                'wrapper'       => ['width' => 25]];

        $post_archive_filter_display_config_filter_title = ['key'               => 'field_ispb_layout_post_archive_display_config_filter_title',
                                                            'name'              => 'post_archive_filter_display_config_filter_title',
                                                            'label'             => 'Filter Title',
                                                            'placeholder'       => 'Filter & Search',
                                                            'type'              => 'text',
                                                            'ui'                => 1,
                                                            'wrapper'           => ['width' => 25],
                                                            'conditional_logic' => $conditional_logic_display_filter_templates];

        $post_archive_filter_display_config_keyword_search = ['key'               => 'field_ispb_layout_post_archive_display_config_keyword_search',
                                                              'name'              => 'post_archive_filter_display_config_keyword_search',
                                                              'label'             => 'Keyword Search',
                                                              'type'              => 'true_false',
                                                              'ui'                => 1,
                                                              'wrapper'           => ['width' => 25],
                                                              'conditional_logic' => $conditional_logic_display_filter_templates];


        $post_archive_filter_display_config_filter_searchwp_engine = ['key'               => 'field_ispb_layout_post_archive_display_config_searchwp_engine',
                                                                      'name'              => 'post_archive_filter_display_config_searchwp_engine',
                                                                      'label'             => 'SearchWP Engine',
                                                                      'type'              => 'select',
                                                                      'choices'           => Acme::get_searchwp_engines_for_page_builder_filter_object(),
                                                                      'wrapper'           => ['width' => 25],
                                                                      'conditional_logic' => $conditional_logic_display_filter_templates,];

        $post_archive_filter_display_config_filter_template = ['key'               => 'field_ispb_layout_post_archive_display_config_filter_template',
                                                               'name'              => 'filter_template',
                                                               'label'             => 'Filter Template',
                                                               'type'              => 'select',
                                                               'ui'                => 1,
                                                               'choices'           => Acme::get_templates('components/post_archive_filter_templates'),
                                                               'wrapper'           => ['width' => 30],
                                                               'conditional_logic' => $conditional_logic_display_filter_templates];


        $post_archive_filter_display_config_fields = [$post_archive_filter_display_config_filter_placement,
                                                      $post_archive_filter_display_config_filter_title,
                                                      $post_archive_filter_display_config_keyword_search,
                                                      $post_archive_filter_display_config_filter_searchwp_engine,
                                                      $post_archive_filter_display_config_filter_template,];

        $post_archive_filter_display_config = ['key'        => 'field_ispb_layout_post_archive_filter_display_config',
                                               'name'       => 'filter_display_config',
                                               'type'       => 'group',
                                               'layout'     => 'block',
                                               'sub_fields' => $post_archive_filter_display_config_fields];

        $post_archive_fields = [ACF::get_horizontal_tab('Quick Options', 'field_ispb_layout_post_archive_query'),
                                $post_archive_post_type,
                                $post_archive_display_toggle,
                                $post_archive_custom_post_options,
                                $post_archive_template,
                                $query_template,
                                ACF::get_horizontal_tab('Advanced Config', 'field_ispb_layout_post_archive_manual_config'),
                                $post_archive_posts_per_page,
                                $post_archive_pagination,
                                $post_archive_posts_per_row,
                                $post_archive_date_filter,
                                $post_archive_hide_past_posts,
                                $post_archive_hide_future_posts,
                                $post_archive_padding,
                                ACF::get_horizontal_tab('Taxonomy Filters', 'field_ispb_layout_post_archive_filter_options'),
                                $post_archive_filter_display_config,
                                $post_archive_filter_repeater,
                                ACF::get_horizontal_tab('Manual Arguments', 'field_ispb_layout_post_archive_config'),
                                $post_archive_custom_query,];

        $acf_modules['post_archive'] = ['label'      => 'Post Archive',
                                        'name'       => 'post_archive',
                                        'key'        => 'field_5a1c4fd6b8756',
                                        'sub_fields' => $post_archive_fields];

        return $acf_modules;


    }

    static function build_collapsible_content_module() {


        $tier_title = ['key'          => 'field_ccm_tier_title',
                       'name'         => 'title',
                       'type'         => 'text',
                       'label'        => 'Title',
                       'instructions' => 'Optional, overrides name of category as Title',
                       'wrapper'      => ['width' => '33']];

        $tier_category_select = ['key'      => 'field_ccm_tier_category',
                                 'name'     => 'category',
                                 'required' => 1,
                                 'type'     => 'select',
                                 'label'    => 'Category',
                                 'choices'  => Page_Builder::get_taxonomies_for_page_builder_select_menu(),
                                 'wrapper'  => ['width' => '33']];

        $tier_display_select = ['key'      => 'field_ccm_tier_display_style',
                                'name'     => 'display_style',
                                'required' => 1,
                                'type'     => 'select',
                                'label'    => 'Display Style',
                                'choices'  => ['accordion'      => 'Accordion',
                                               'horizontal_tab' => 'Horizontal Tab',
                                               'vertical_tab'   => 'Vertical Tab',
                                               'modal'          => 'Modal (Popup)',
                                               'list'           => 'List',],
                                'wrapper'  => ['width' => '33']];


        $tier_accordion_container_template_select = ['key'     => 'field_ccm_tier_accordion_launcher_container_select',
                                                     'name'    => 'container_template',
                                                     'label'   => 'Container Template',
                                                     'type'    => 'select',
                                                     'choices' => Acme::get_templates('components/collapsible_content/branches/accordion/container'),
                                                     'wrapper' => ['width' => 33]];

        $tier_accordion_blade_template_select = ['key'     => 'field_ccm_tier_accordion_launcher_template_select',
                                                 'name'    => 'launcher_template',
                                                 'label'   => 'Launcher Template',
                                                 'type'    => 'select',
                                                 'choices' => Acme::get_templates('components/collapsible_content/branches/accordion/launcher'),
                                                 'wrapper' => ['width' => 33]];

        $tier_accordion_content_template_select = ['key'     => 'field_ccm_tier_accordion_content_template_select',
                                                   'label'   => 'Content Template',
                                                   'name'    => 'content_template',
                                                   'type'    => 'select',
                                                   'choices' => Acme::get_templates('components/collapsible_content/branches/accordion/content'),
                                                   'wrapper' => ['width' => 33]];

        $tier_accordion_first_open = ['key'     => 'field_ccm_tier_accordion_first_open',
                                      'label'   => 'First Slide Open',
                                      'name'    => 'first_open',
                                      'type'    => 'true_false',
                                      'wrapper' => ['width' => 33]];


        $tier_accordion_multi_expand = ['key'     => 'field_ccm_tier_accordion_multi_expand',
                                        'label'   => 'Allow Multi Expand',
                                        'name'    => 'multi_expand',
                                        'type'    => 'true_false',
                                        'wrapper' => ['width' => 33]];

        $tier_accordion_allow_all_closed = ['key'     => 'field_ccm_tier_accordion_allow_all_closed',
                                            'label'   => 'Allow All Closed',
                                            'name'    => 'allow_all_closed',
                                            'type'    => 'true_false',
                                            'wrapper' => ['width' => 33]];


        $tier_accordion_option_fields = [$tier_accordion_container_template_select,
                                         $tier_accordion_blade_template_select,
                                         $tier_accordion_content_template_select,
                                         $tier_accordion_first_open,
                                         $tier_accordion_multi_expand,
                                         $tier_accordion_allow_all_closed,];

        $tier_accordion_options = ['key'               => 'field_ccm_tier_accordion_options',
                                   'name'              => 'accordion_options',
                                   'label'             => 'Accordion Options',
                                   'type'              => 'group',
                                   'sub_fields'        => $tier_accordion_option_fields,
                                   'conditional_logic' => [[['field'    => 'field_ccm_tier_display_style',
                                                             'operator' => '==',
                                                             'value'    => 'accordion']]]];


        $horizontal_tab_container_template_select = ['key'     => 'field_ccm_tier_horizontal_tab_launcher_container_select',
                                                     'name'    => 'container_template',
                                                     'label'   => 'Container Template',
                                                     'type'    => 'select',
                                                     'choices' => Acme::get_templates('components/collapsible_content/branches/horizontal_tab/container'),
                                                     'wrapper' => ['width' => 33]];


        $horizontal_tab_launcher_template_select = ['key'     => 'field_ccm_horizontal_tab_launcher_select',
                                                    'label'   => 'Launcher Template',
                                                    'name'    => 'launcher_template',
                                                    'type'    => 'select',
                                                    'choices' => Acme::get_templates('components/collapsible_content/branches/horizontal_tab/launcher'),
                                                    'wrapper' => ['width' => 33]];

        $horizontal_tab_content_template_select = ['key'     => 'field_ccm_horizontal_tab_content_select',
                                                   'label'   => 'Content Template',
                                                   'name'    => 'content_template',
                                                   'type'    => 'select',
                                                   'choices' => Acme::get_templates('components/collapsible_content/branches/horizontal_tab/content'),
                                                   'wrapper' => ['width' => 33]];


        $tier_horizontal_tab_option_fields = [$horizontal_tab_container_template_select,
                                              $horizontal_tab_launcher_template_select,
                                              $horizontal_tab_content_template_select];

        $tier_horizontal_tab_options = ['key'               => 'field_ccm_tier_horizontal_tab_options',
                                        'name'              => 'horizontal_tab_options',
                                        'label'             => 'Horizontal Tab Options',
                                        'type'              => 'group',
                                        'sub_fields'        => $tier_horizontal_tab_option_fields,
                                        'conditional_logic' => [[['field'    => 'field_ccm_tier_display_style',
                                                                  'operator' => '==',
                                                                  'value'    => 'horizontal_tab']]]];


        $vertical_tab_container_template_select = ['key'     => 'field_ccm_tier_vertical_tab_launcher_container_select',
                                                   'name'    => 'container_template',
                                                   'label'   => 'Container Template',
                                                   'type'    => 'select',
                                                   'choices' => Acme::get_templates('components/collapsible_content/branches/vertical_tab/container'),
                                                   'wrapper' => ['width' => 33]];

        $vertical_tab_launcher_template_select = ['key'     => 'field_ccm_vertical_tab_launcher_select',
                                                  'label'   => 'Launcher Template',
                                                  'name'    => 'launcher_template',
                                                  'type'    => 'select',
                                                  'choices' => Acme::get_templates('components/collapsible_content/branches/vertical_tab/launcher'),
                                                  'wrapper' => ['width' => 33]];

        $vertical_tab_content_template_select = ['key'     => 'field_ccm_vertical_tab_content_select',
                                                 'label'   => 'Content Template',
                                                 'name'    => 'content_template',
                                                 'type'    => 'select',
                                                 'choices' => Acme::get_templates('components/collapsible_content/branches/vertical_tab/content'),
                                                 'wrapper' => ['width' => 33]];


        $tier_vertical_tab_option_fields = [$vertical_tab_container_template_select,
                                            $vertical_tab_launcher_template_select,
                                            $vertical_tab_content_template_select];

        $tier_vertical_tab_options = ['key'               => 'field_ccm_tier_vertical_tab_options',
                                      'name'              => 'vertical_tab_options',
                                      'label'             => 'Vertical Tab Options',
                                      'type'              => 'group',
                                      'sub_fields'        => $tier_vertical_tab_option_fields,
                                      'conditional_logic' => [[['field'    => 'field_ccm_tier_display_style',
                                                                'operator' => '==',
                                                                'value'    => 'vertical_tab']]]];

        $list_container_template_select = ['key'     => 'field_ccm_tier_list_launcher_container_select',
                                           'name'    => 'container_template',
                                           'label'   => 'Container Template',
                                           'type'    => 'select',
                                           'choices' => Acme::get_templates('components/collapsible_content/branches/list/container'),
                                           'wrapper' => ['width' => 33]];

        $list_launcher_template_select = ['key'     => 'field_ccm_list_launcher_select',
                                          'label'   => 'Launcher Template',
                                          'name'    => 'launcher_template',
                                          'type'    => 'select',
                                          'choices' => Acme::get_templates('components/collapsible_content/branches/list/launcher'),
                                          'wrapper' => ['width' => 33]];

        $list_content_template_select = ['key'     => 'field_ccm_list_content_select',
                                         'label'   => 'Content Template',
                                         'name'    => 'content_template',
                                         'type'    => 'select',
                                         'choices' => Acme::get_templates('components/collapsible_content/branches/list/content'),
                                         'wrapper' => ['width' => 33]];


        $tier_list_option_fields = [$list_container_template_select,
                                    $list_launcher_template_select,
                                    $list_content_template_select];

        $tier_list_options = ['key'               => 'field_ccm_tier_list_options',
                              'name'              => 'list_options',
                              'label'             => 'List Options',
                              'type'              => 'group',
                              'sub_fields'        => $tier_list_option_fields,
                              'conditional_logic' => [[['field'    => 'field_ccm_tier_display_style',
                                                        'operator' => '==',
                                                        'value'    => 'list']]]];


        $modal_container_template_select = ['key'     => 'field_ccm_tier_modal_launcher_container_select',
                                            'name'    => 'container_template',
                                            'label'   => 'Container Template',
                                            'type'    => 'select',
                                            'choices' => Acme::get_templates('components/collapsible_content/branches/modal/container'),
                                            'wrapper' => ['width' => 33]];

        $modal_launcher_template_select = ['key'     => 'field_ccm_modal_launcher_select',
                                           'label'   => 'Launcher Template',
                                           'name'    => 'launcher_template',
                                           'type'    => 'select',
                                           'choices' => Acme::get_templates('components/collapsible_content/branches/modal/launcher'),
                                           'wrapper' => ['width' => 33]];

        $modal_content_template_select = ['key'     => 'field_ccm_modal_content_select',
                                          'label'   => 'Content Template',
                                          'name'    => 'content_template',
                                          'type'    => 'select',
                                          'choices' => Acme::get_templates('components/collapsible_content/branches/modal/content'),
                                          'wrapper' => ['width' => 33]];


        $tier_modal_option_fields = [$modal_container_template_select,
                                     $modal_launcher_template_select,
                                     $modal_content_template_select];

        $tier_modal_options = ['key'               => 'field_ccm_tier_modal_options',
                               'name'              => 'modal_options',
                               'label'             => 'Modal Options',
                               'type'              => 'group',
                               'sub_fields'        => $tier_modal_option_fields,
                               'conditional_logic' => [[['field'    => 'field_ccm_tier_display_style',
                                                         'operator' => '==',
                                                         'value'    => 'modal']]]];


        $tier_repeater = ['key'        => 'field_ccm_tier_repeater',
                          'name'       => 'tiers',
                          'label'      => 'Tiers',
                          'type'       => 'repeater',
                          'layout'     => 'block',
                          'min'        => 1,
                          'sub_fields' => [$tier_title,
                                           $tier_category_select,
                                           $tier_display_select,
                                           $tier_accordion_options,
                                           $tier_horizontal_tab_options,
                                           $tier_vertical_tab_options,
                                           $tier_list_options,
                                           $tier_modal_options,]];

        $post_template = ['key'     => 'field_ccm_content_template',
                          'label'   => 'Content Template',
                          'name'    => 'content_template',
                          'type'    => 'select',
                          'choices' => Acme::get_templates('components/collapsible_content/branches/slide'),
                          'wrapper' => ['width' => 33]];

        $post_display_style = ['key'     => 'field_ccm_post_display_style',
                               'name'    => 'display_options_post_style',
                               'label'   => 'Display Style',
                               'type'    => 'select',
                               'choices' => ['accordion'      => 'Accordion',
                                             'horizontal_tab' => 'Horizontal Tab',
                                             'vertical_tab'   => 'Vertical Tab',
                                             'modal'          => 'Modal (Popup)',
                                             'list'           => 'List',
                                             'query'          => 'Query',
                                             'excerpts'       => 'Excerpts'],
                               'wrapper' => ['width' => 33]];


        $post_type_select = ['key'      => 'field_ccm_post_type_relationship',
                             'label'    => 'Post Types',
                             'name'     => 'post_types',
                             'type'     => 'select',
                             'multiple' => 1,
                             'choices'  => Page_Builder::get_post_types_for_page_builder_field(0),
                             'wrapper'  => ['width' => 33]];


        $post_display_accordion_launcher_template = ['key'     => 'field_post_display_accordion_launcher_template',
                                                     'name'    => 'launcher_template',
                                                     'label'   => 'Launcher Template',
                                                     'type'    => 'select',
                                                     'choices' => Acme::get_templates('components/collapsible_content/leaves/accordion/launcher'),
                                                     'wrapper' => ['width' => 25]];

        $post_display_accordion_content_template = ['key'     => 'field_post_display_accordion_content_template',
                                                    'name'    => 'content_template',
                                                    'label'   => 'Content Template',
                                                    'type'    => 'select',
                                                    'choices' => Acme::get_templates('components/collapsible_content/leaves/accordion/content'),
                                                    'wrapper' => ['width' => 25]];

        $post_display_accordion_container_template = ['key'     => 'field_post_display_accordion_container_template',
                                                      'name'    => 'container_template',
                                                      'label'   => 'Container Template',
                                                      'type'    => 'select',
                                                      'choices' => Acme::get_templates('components/collapsible_content/leaves/accordion/container'),
                                                      'wrapper' => ['width' => 25]];

        $post_display_accordion_excerpt_template = ['key'     => 'field_post_display_accordion_excerpt_template',
                                                    'name'    => 'excerpt_template',
                                                    'label'   => 'Excerpt Template',
                                                    'type'    => 'select',
                                                    'choices' => Acme::get_templates('twig/post_excerpts'),
                                                    'wrapper' => ['width' => 25]];

        $post_display_accordion_option_fields = [$post_display_accordion_launcher_template,
                                                 $post_display_accordion_content_template,
                                                 $post_display_accordion_container_template,
                                                 $post_display_accordion_excerpt_template];

        $post_display_options_accordion_options = ['key'               => 'field_ccm_post_display_accordion_options',
                                                   'name'              => 'display_options_accordion',
                                                   'label'             => 'Accordion Display Options',
                                                   'type'              => 'group',
                                                   'sub_fields'        => $post_display_accordion_option_fields,
                                                   'conditional_logic' => [[['field'    => 'field_ccm_post_display_style',
                                                                             'operator' => '==',
                                                                             'value'    => 'accordion']]],];

        $post_display_horizontal_tab_launcher_template = ['key'     => 'field_post_display_horizontal_tab_launcher_template',
                                                          'name'    => 'launcher_template',
                                                          'label'   => 'Launcher Template',
                                                          'type'    => 'select',
                                                          'choices' => Acme::get_templates('components/collapsible_content/leaves/horizontal_tab/launcher'),
                                                          'wrapper' => ['width' => 25],];

        $post_display_horizontal_tab_content_template = ['key'     => 'field_post_display_horizontal_tab_content_template',
                                                         'name'    => 'content_template',
                                                         'label'   => 'Content Template',
                                                         'type'    => 'select',
                                                         'choices' => Acme::get_templates('components/collapsible_content/leaves/horizontal_tab/content'),
                                                         'wrapper' => ['width' => 25],];


        $post_display_horizontal_tab_container_template = ['key'     => 'field_post_display_horizontal_tab_container_template',
                                                           'name'    => 'container_template',
                                                           'label'   => 'Container Template',
                                                           'type'    => 'select',
                                                           'choices' => Acme::get_templates('components/collapsible_content/leaves/horizontal_tab/container'),
                                                           'wrapper' => ['width' => 25],];


        $post_display_horizontal_tab_post_excerpt_template = ['key'     => 'field_post_display_horizontal_tab_post_excerpt_template',
                                                              'name'    => 'excerpt_template',
                                                              'label'   => 'Excerpt Template',
                                                              'type'    => 'select',
                                                              'choices' => Acme::get_templates('twig/post_excerpts'),
                                                              'wrapper' => ['width' => 25],];

        $post_display_horizontal_tab_option_fields = [$post_display_horizontal_tab_launcher_template,
                                                      $post_display_horizontal_tab_content_template,
                                                      $post_display_horizontal_tab_container_template,
                                                      $post_display_horizontal_tab_post_excerpt_template,];

        $post_display_options_horizontal_tab_options = ['key'               => 'field_ccm_post_display_horizontal_tab_options',
                                                        'name'              => 'display_options_horizontal_tab',
                                                        'label'             => 'Horizontal Tab Display Options',
                                                        'type'              => 'group',
                                                        'sub_fields'        => $post_display_horizontal_tab_option_fields,
                                                        'conditional_logic' => [[['field'    => 'field_ccm_post_display_style',
                                                                                  'operator' => '==',
                                                                                  'value'    => 'horizontal_tab']]],];

        $post_display_vertical_tab_launcher_template = ['key'     => 'field_post_display_vertical_tab_launcher_template',
                                                        'name'    => 'launcher_template',
                                                        'label'   => 'Launcher Template',
                                                        'type'    => 'select',
                                                        'choices' => Acme::get_templates('components/collapsible_content/leaves/vertical_tab/launcher'),
                                                        'wrapper' => ['width' => 25],];

        $post_display_vertical_tab_content_template = ['key'     => 'field_post_display_vertical_tab_content_template',
                                                       'name'    => 'content_template',
                                                       'label'   => 'Content Template',
                                                       'type'    => 'select',
                                                       'choices' => Acme::get_templates('components/collapsible_content/leaves/vertical_tab/content'),
                                                       'wrapper' => ['width' => 25],];

        $post_display_vertical_tab_container_template = ['key'     => 'field_post_display_vertical_tab_container_template',
                                                         'name'    => 'container_template',
                                                         'label'   => 'Container Template',
                                                         'type'    => 'select',
                                                         'choices' => Acme::get_templates('components/collapsible_content/leaves/vertical_tab/container'),
                                                         'wrapper' => ['width' => 25],];

        $post_display_vertical_tab_post_excerpt_template = ['key'     => 'field_post_display_vertical_tab_post_excerpt_template',
                                                            'name'    => 'excerpt_template',
                                                            'label'   => 'Post Excerpt Template',
                                                            'type'    => 'select',
                                                            'choices' => Acme::get_templates('twig/post_excerpts'),
                                                            'wrapper' => ['width' => 25],];

        $post_display_vertical_tab_option_fields = [$post_display_vertical_tab_launcher_template,
                                                    $post_display_vertical_tab_content_template,
                                                    $post_display_vertical_tab_container_template,
                                                    $post_display_vertical_tab_post_excerpt_template,];

        $post_display_options_vertical_tab_options = ['key'               => 'field_ccm_post_display_vertical_tab_options',
                                                      'name'              => 'display_options_vertical_tab',
                                                      'label'             => 'Vertical Tab Display Options',
                                                      'type'              => 'group',
                                                      'sub_fields'        => $post_display_vertical_tab_option_fields,
                                                      'conditional_logic' => [[['field'    => 'field_ccm_post_display_style',
                                                                                'operator' => '==',
                                                                                'value'    => 'vertical_tab']]],];


        $post_display_list_launcher_template = ['key'     => 'field_post_display_list_launcher_template',
                                                'name'    => 'launcher_template',
                                                'label'   => 'Launcher Template',
                                                'type'    => 'select',
                                                'choices' => Acme::get_templates('components/collapsible_content/leaves/list/launcher'),
                                                'wrapper' => ['width' => 25],];

        $post_display_list_content_template = ['key'     => 'field_post_display_list_content_template',
                                               'name'    => 'content_template',
                                               'label'   => 'Content Template',
                                               'type'    => 'select',
                                               'choices' => Acme::get_templates('components/collapsible_content/leaves/list/content'),
                                               'wrapper' => ['width' => 25],];

        $post_display_list_container_template = ['key'     => 'field_post_display_list_container_template',
                                                 'name'    => 'container_template',
                                                 'label'   => 'Container Template',
                                                 'type'    => 'select',
                                                 'choices' => Acme::get_templates('components/collapsible_content/leaves/list/container'),
                                                 'wrapper' => ['width' => 25],];

        $post_display_list_excerpt_template = ['key'     => 'field_post_display_list_excerpt_template',
                                               'name'    => 'excerpt_template',
                                               'label'   => 'Excerpt Template',
                                               'type'    => 'select',
                                               'choices' => Acme::get_templates('twig/post_excerpts'),
                                               'wrapper' => ['width' => 25],];

        $post_display_list_option_fields = [$post_display_list_launcher_template,
                                            $post_display_list_content_template,
                                            $post_display_list_container_template,
                                            $post_display_list_excerpt_template,];

        $post_display_options_list_options = ['key'               => 'field_ccm_post_display_list_options',
                                              'name'              => 'display_options_list',
                                              'label'             => 'List Display Options',
                                              'type'              => 'group',
                                              'sub_fields'        => $post_display_list_option_fields,
                                              'conditional_logic' => [[['field'    => 'field_ccm_post_display_style',
                                                                        'operator' => '==',
                                                                        'value'    => 'list']]],];


        $post_display_modal_launcher_template = ['key'     => 'field_post_display_modal_launcher_template',
                                                 'name'    => 'launcher_template',
                                                 'label'   => 'Launcher Template',
                                                 'type'    => 'select',
                                                 'choices' => Acme::get_templates('components/collapsible_content/leaves/modal/launcher'),
                                                 'wrapper' => ['width' => 25]];

        $post_display_modal_content_template = ['key'     => 'field_post_display_modal_content_template',
                                                'name'    => 'content_template',
                                                'label'   => 'Content Template',
                                                'type'    => 'select',
                                                'choices' => Acme::get_templates('components/collapsible_content/leaves/modal/content'),
                                                'wrapper' => ['width' => 25]];

        $post_display_modal_container_template = ['key'     => 'field_post_display_modal_container_template',
                                                  'name'    => 'container_template',
                                                  'label'   => 'Container Template',
                                                  'type'    => 'select',
                                                  'choices' => Acme::get_templates('components/collapsible_content/leaves/modal/container'),
                                                  'wrapper' => ['width' => 25]];

        $post_display_modal_post_excerpt_template = ['key'     => 'field_post_display_modal_excerpt_template',
                                                     'name'    => 'excerpt_template',
                                                     'label'   => 'Excerpt Template',
                                                     'type'    => 'select',
                                                     'choices' => Acme::get_templates('twig/post_excerpts'),
                                                     'wrapper' => ['width' => 25]];

        $post_display_modal_option_fields = [$post_display_modal_launcher_template,
                                             $post_display_modal_container_template,
                                             $post_display_modal_content_template,
                                             $post_display_modal_post_excerpt_template,];

        $post_display_options_modal_options = ['key'               => 'field_ccm_post_display_modal_options',
                                               'name'              => 'display_options_modal',
                                               'label'             => 'Modal Display Options',
                                               'type'              => 'group',
                                               'sub_fields'        => $post_display_modal_option_fields,
                                               'conditional_logic' => [[['field'    => 'field_ccm_post_display_style',
                                                                         'operator' => '==',
                                                                         'value'    => 'modal']]],];


        $post_display_query_launcher_template = ['key'          => 'field_post_display_query_launcher_template',
                                                 'name'         => 'query_template',
                                                 'label'        => 'Query Template',
                                                 'type'         => 'select',
                                                 'instructions' => 'IS Post Archive Query Templates',
                                                 'choices'      => Acme::get_templates('twig/query_excerpts')];

        $post_display_query_option_fields = [$post_display_query_launcher_template];

        $post_display_options_query_options = ['key'               => 'field_ccm_post_display_query_options',
                                               'name'              => 'display_options_query',
                                               'label'             => 'Query Display Options',
                                               'type'              => 'group',
                                               'sub_fields'        => $post_display_query_option_fields,
                                               'conditional_logic' => [[['field'    => 'field_ccm_post_display_style',
                                                                         'operator' => '==',
                                                                         'value'    => 'query']]],];

        $excerpt_display_template_select = ['key'     => 'field_excerpt_display_template_select',
                                            'name'    => 'template',
                                            'label'   => 'Template',
                                            'type'    => 'select',
                                            'choices' => Acme::get_templates('twig/post_excerpts'),
                                            'wrapper' => ['width' => 30],];


        $excerpt_display_posts_per_page = ['key'           => 'field_excerpt_display_posts_per_page',
                                           'name'          => 'posts_per_page',
                                           'label'         => 'Posts Per Page',
                                           'type'          => 'number',
                                           'min'           => -1,
                                           'max'           => 12,
                                           'default_value' => -1,
                                           'required'      => 1,
                                           'wrapper'       => ['width' => 15]];

        $excerpt_display_per_row_mobile = ['key'           => 'field_excerpt_display_per_row_mobile',
                                           'name'          => 'small',
                                           'label'         => 'Mobile',
                                           'type'          => 'number',
                                           'min'           => 1,
                                           'max'           => 12,
                                           'default_value' => 2,
                                           'wrapper'       => ['width' => 25],];

        $excerpt_display_per_row_tablet = ['key'     => 'field_excerpt_display_per_row_tablet',
                                           'name'    => 'medium',
                                           'label'   => 'Tablet',
                                           'type'    => 'number',
                                           'min'     => 1,
                                           'max'     => 12,
                                           'wrapper' => ['width' => 25],];

        $excerpt_display_per_row_desktop = ['key'     => 'field_excerpt_display_per_row_desktop',
                                            'name'    => 'large',
                                            'label'   => 'Desktop',
                                            'type'    => 'number',
                                            'min'     => 1,
                                            'max'     => 12,
                                            'wrapper' => ['width' => 25],];

        $excerpt_display_per_row_large_desktop = ['key'     => 'field_excerpt_display_per_row_large_desktop',
                                                  'name'    => 'xlarge',
                                                  'label'   => 'Large Desktop',
                                                  'type'    => 'number',
                                                  'min'     => 1,
                                                  'max'     => 12,
                                                  'wrapper' => ['width' => 25],];

        $excerpt_display_posts_per_row = ['key'        => 'field_excerpt_display_posts_per_row',
                                          'name'       => 'posts_per_row',
                                          'label'      => 'Posts Per Row',
                                          'type'       => 'group',
                                          'sub_fields' => [$excerpt_display_per_row_mobile,
                                                           $excerpt_display_per_row_tablet,
                                                           $excerpt_display_per_row_desktop,
                                                           $excerpt_display_per_row_large_desktop,],
                                          'wrapper'    => ['width' => 40]];

        $excerpt_display_cell_padding_x = ['key'     => 'field_excerpt_display_cell_padding_x',
                                           'name'    => 'x',
                                           'label'   => 'X',
                                           'type'    => 'true_false',
                                           'wrapper' => ['width' => 50]];

        $excerpt_display_cell_padding_y = ['key'     => 'field_excerpt_display_cell_padding_y',
                                           'name'    => 'y',
                                           'label'   => 'Y',
                                           'type'    => 'true_false',
                                           'wrapper' => ['width' => 50]];


        $excerpt_display_cell_padding = ['key'        => 'field_excerpt_display_cell_padding',
                                         'name'       => 'cell_padding',
                                         'label'      => 'Cell Padding',
                                         'type'       => 'group',
                                         'sub_fields' => [$excerpt_display_cell_padding_x,
                                                          $excerpt_display_cell_padding_y,],
                                         'wrapper'    => ['width' => 15]];


        $excerpt_display_option_fields = [$excerpt_display_template_select,
                                          $excerpt_display_posts_per_page,
                                          $excerpt_display_posts_per_row,
                                          $excerpt_display_cell_padding,];

        $excerpt_display_options_excerpt_options = ['key'               => 'field_excerpt_display_options_excerpt_display_options',
                                                    'name'              => 'display_options_excerpts',
                                                    'label'             => 'Excerpt Display Options',
                                                    'type'              => 'group',
                                                    'sub_fields'        => $excerpt_display_option_fields,
                                                    'conditional_logic' => [[['field'    => 'field_ccm_post_display_style',
                                                                              'operator' => '==',
                                                                              'value'    => 'excerpts']]],];


        $collapsible_content_fields = [ACF::get_horizontal_tab('Posts', 'field_ispb_dummy_key_name_for_tab_posts'),
                                       $post_template,
                                       $post_display_style,
                                       $post_type_select,
                                       $post_display_options_accordion_options,
                                       $post_display_options_horizontal_tab_options,
                                       $post_display_options_vertical_tab_options,
                                       $post_display_options_list_options,
                                       $post_display_options_modal_options,
                                       $post_display_options_query_options,
                                       $excerpt_display_options_excerpt_options,
                                       ACF::get_horizontal_tab('Tiers', 'field_ispb_dummy_key_name_for_tab_tiers'),
                                       $tier_repeater,];


        $acf_modules['collapsible_content'] = ['label'      => 'Collapsible Content',
                                               'name'       => 'collapsible_content',
                                               'key'        => 'field_ispb_module_collapsible_content',
                                               'sub_fields' => $collapsible_content_fields];

        return $acf_modules;

    }

    public static function display_inline_template($module) {

        if (!isset($module['config']['inline_template'])) {
            return false;
        }

        $acf_inline_template = $module['config']['inline_template'];

        $loader = new \Twig_Loader_Array(['inline_template' => $acf_inline_template]);
        $twig = new \Twig_Environment($loader);
        $twig->addExtension(new \Twig_Extension_Debug());

        try {
            return $twig->render('inline_template', ['module' => $module]);
        } catch (\Exception $e) {
            return "Error at display_inline_template $e";
        }


    }

    public static function display_custom_template($obj) {

        $module = $obj->acf_module;
        $layout = $obj->layout;
        $index = $obj->config;

        if (!isset($module['template'])) {
            return false;
        }

        $template_filepath = $_SERVER['DOCUMENT_ROOT'] . $module['template'];

        if (!file_exists($template_filepath)) {
            return false;
        }


        $post_type_uses_php_template = substr($template_filepath, -3, 3) == 'php';

        if ($post_type_uses_php_template):


            $module = new ACF_Module(['module'   => $module,
                                      'template' => $template_filepath]);

            return $module->get_html();
        endif;

        $template = file_get_contents($template_filepath);

        $loader = new \Twig_Loader_Array(['custom_template' => $template]);
        $twig = new \Twig_Environment($loader);
        $twig->addExtension(new \Twig_Extension_Debug());


        try {
            return $twig->render('custom_template', ['module' => $module, 'layout' => $layout, 'index' => $index]);
        } catch (\Exception $e) {
            return "Error at display_custom_template $e";
        }


    }

    public static function acf_flex_to_html($module, $layout = false, $config = false) {

        $inline_template = Module::display_inline_template($module);

        if ($inline_template) {
            return $inline_template;
        }

        $obj = (object)['acf_module' => $module,
                        'layout'     => $layout,
                        'config'     => $config];


        $custom_template = Module::display_custom_template($obj);

        if ($custom_template) {
            return $custom_template;
        }


        $default_template_filepath = INFINISITE_URI . "module_templates/{$module['acf_fc_layout']}.php";

        if (!file_exists($default_template_filepath)) {
            if (is_user_logged_in()) {
                return "<div class='callout alert'><p class='no-padding'>File \"$default_template_filepath\" not found, please contact the website admin.</p></div>";
            }
        } else {
            ob_start();
            include($default_template_filepath);

            return ob_get_clean();
        }

    }


}
