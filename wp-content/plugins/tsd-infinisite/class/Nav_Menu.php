<?

namespace TSD_Infinisite;

class Nav_Menu
{


    static function add_IS_menu_meta_boxes()
    {

        $custom_param = array(0 => 'This param will be passed to my_render_menu_metabox');
        add_meta_box('is-custom-items-metabox', 'Custom Items', 'TSD_Infinisite\Nav_Menu::render_separator_menu_metabox', 'nav-menus', 'side', 'default', $custom_param);


    }

    static function render_separator_menu_metabox($object, $args)
    {

        global $nav_menu_selected_id;
        // Create an array of objects that imitate Post objects

        $is_menu_separator = (object)[
            'ID' => 1,
            'db_id' => 0,
            'menu_item_parent' => 0,
            'object_id' => 1,
            'post_parent' => 0,
            'type' => 'is-separator',
            'object' => 'is-custom-menu-objects',
            'type_label' => 'IS Custom Menu Items',
            'title' => 'Separator',
            'url' => '#',
            'target' => '',
            'attr_title' => '',
            'description' => '',
            'classes' => [],
            'xfn' => '',
        ];

        $menu_objects = [
            $is_menu_separator
        ];

        foreach (Acme::get_post_types() as $post_type):

            $name = $post_type['name'];
            $slug = $post_type['post_type'];


            $post_type_menu_object = (object)[
                'ID' => 1,
                'db_id' => 0,
                'menu_item_parent' => 0,
                'object_id' => 1,
                'post_parent' => 0,
                'type' => 'is-latest-menu-item',
                'object' => "is_latest_menu_item_type_{$slug}",
                'type_label' => 'IS Custom Menu Items',
                'title' => "Latest {$name} Post",
                'url' => '#',
                'target' => '',
                'attr_title' => $slug,
                'description' => '',
                'classes' => ["latest-menu-item-{$slug}"],
                'xfn' => '',
            ];


            $menu_objects[] = $post_type_menu_object;

        endforeach;


        $db_fields = false;
        // If your links will be hieararchical, adjust the $db_fields array bellow
        if (false) {
            $db_fields = array('parent' => 'parent', 'id' => 'post_parent');
        }
        $walker = new \Walker_Nav_Menu_Checklist($db_fields);
        $removed_args = array(
            'action',
            'customlink-tab',
            'edit-menu-item',
            'menu-item',
            'page-tab',
            '_wpnonce',
        ); ?>
        <div id="my-plugin-div">
        <div id="tabs-panel-my-plugin-all" class="tabs-panel tabs-panel-active">
            <ul id="my-plugin-checklist-pop" class="categorychecklist form-no-clear">
                <?php echo \walk_nav_menu_tree(\array_map('wp_setup_nav_menu_item', $menu_objects), 0, (object)array('walker' => $walker)); ?>
            </ul>

            <p class="button-controls">
			<span class="list-controls">
				<a href="<?php
                echo esc_url(add_query_arg(
                    array(
                        'my-plugin-all' => 'all',
                        'selectall' => 1,
                    ),
                    remove_query_arg($removed_args)
                ));
                ?>#my-menu-test-metabox" class="select-all"><?php _e('Select All'); ?></a>
			</span>

                <span class="add-to-menu">
				<input type="submit"<?php \wp_nav_menu_disabled_check($nav_menu_selected_id); ?>
                       class="button-secondary submit-add-to-menu right" value="<?php esc_attr_e('Add to Menu'); ?>"
                       name="add-my-plugin-menu-item" id="submit-my-plugin-div"/>
				<span class="spinner"></span>
			</span>
            </p>
        </div>
        <?
    }


    static function register_menus()
    {

        register_nav_menu('header', __('Header Menu', 'theme-slug'));
    }


    static function is_check_nav_items_for_custom_functionality($items, $args)
    {

        // loop
        foreach ($items as $menu_post) :

            if ($menu_post->type != 'is-latest-menu-item')
                continue;


            // the post type is prefixed with the string
            // is_latest_menu_item_type_


            $query = new \WP_Query([
                'post_type' => substr($menu_post->object, 25),
                'posts_per_page' => 1
            ]);

            // just in case we don't find anything - we exit the function
            if (!$query->found_posts) continue;

            $post = $query->posts[0];

            // we only want to update the title if the string matches our instructions
            if ($menu_post->title == '[post_title]')
                $menu_post->title = $post->post_title;

            $menu_post->url = get_permalink($post->ID);
        endforeach;


// return
        return $items;

    }

    static function register_nav_menu_item_meta_fields()
    {

        if (!function_exists('acf_add_local_field_group'))
            return;

        $menu_object_icon = [
            'key' => 'field_is_menu_item_icon',
            'label' => 'Icon',
            'name' => 'icon',
            'type' => 'image',
            'preview_size' => 'medium',
        ];

        $menu_object_fontawesome_icon = [
            'key' => 'field_is_menu_item_fontawesome_icon',
            'label' => 'FontAwesome Icon',
            'name' => 'fontawesome_icon',
            'type' => 'select',
            'ui' => 1,
            'allow_null' => 1,
            'choices' => Font_Awesome::get_select_menu_options(),
            'wrapper' => [
                'width' => 60
            ]
        ];

        $menu_object_fontawesome_icon_weight = [
            'key' => 'field_is_menu_item_fontawesome_icon_weight',
            'label' => 'Weight',
            'name' => 'fontawesome_icon_weight',
            'type' => 'select',
            'ui' => 1,
            'allow_null' => 1,
            'choices' => [
                'fas' => 'Solid',
                'far' => 'Regular',
                'fal' => 'Light',
                'fab' => 'Brands'
            ],
            'wrapper' => [
                'width' => 40
            ]
        ];

        $menu_object_link_class = [
            'key' => 'field_is_menu_item_link_class',
            'label' => 'Link Class',
            'name' => 'link_class',
            'type' => 'text',
            'instructions' => 'Override the classes in the menu item\'s anchor tag',
            'preview_size' => 'medium',
        ];

        $menu_object_container_class = [
            'key' => 'field_is_menu_item_container_class',
            'label' => 'Container Class',
            'name' => 'container_class',
            'type' => 'text',
            'instructions' => 'Override the classes in the menu item\'s div container tag',
            'preview_size' => 'medium',
        ];

        $standard_menu_object_fields = [
            $menu_object_icon,
            $menu_object_fontawesome_icon,
            $menu_object_fontawesome_icon_weight,
            $menu_object_link_class,
            $menu_object_container_class
        ];

        $standard_menu_object_field_group = [
            'key' => 'group_is_menu_item_field_group',
            'title' => 'menu item field group',
            'fields' => $standard_menu_object_fields,
            'location' => [
                [
                    [
                        'param' => 'nav_menu_item',
                        'operator' => '==',
                        'value' => 'all',
                    ],
                ],
            ],
        ];

        acf_add_local_field_group($standard_menu_object_field_group);


    }


}
