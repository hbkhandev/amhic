<?

namespace TSD_Infinisite;

class Option {

    public static function create_global_options() {

        $global_options = Option::create_global_scss_styles();
        acf_add_local_field_group( $global_options );
    }

    static function create_global_scss_styles() {

        $global_scss_editor = [ 'key'          => 'field_is_global_scss',
                                'label'        => 'Global SCSS Editor',
                                'name'         => 'is_global_scss',
                                'type'         => 'textarea',
                                'instructions' => 'Veriables, mixins and fonts set here will be avialble to use sitewide. Use sparingly.',
                                'rows'         => 18,
                                'show_in_rest' => 1,
                                'edit_in_rest' => 1,
                                'wrapper'      => [ 'class' => 'is_ace_editor theme_scss' ] ];

        $global_scss_field_group_fields = [ $global_scss_editor ];


        $global_scss_field_group = [ 'key'        => 'group_is_global_scss',
                                     'title'      => 'A Global SCSS Styles',
                                     'style'      => 'seamless',
                                     'location'   => [ [ [ 'param'    => 'options_page',
                                                           'operator' => '==',
                                                           'value'    => 'is-display-settings', ] ] ],
                                     'menu_order' => 0,
                                     'fields'     => $global_scss_field_group_fields ];

        return $global_scss_field_group;


    }

    static function register_acf_global_options_pages() {

        if ( ! function_exists( 'acf_add_options_page' ) ) {
            return;
        }


        acf_add_options_page( [ 'page_title' => "
                                    <i class='fas fa-times'></i> Infinisite Foundation Display Settings
                                    ",
                                'menu_title' => 'InfiniSite Options',
                                'menu_slug'  => 'is-display-settings',
                                'capability' => 'activate_plugins',
                                'icon_url'   => INFINISITE_DIR . 'assets/img/is-icon.png',
                                'redirect'   => false ] );

        // we refactor this to another function so we can get the data for the wp toolbar links
        $acf_options_pages = Option::get_acf_options_page_data();

        foreach ( $acf_options_pages as $acf_options_page ) {
            acf_add_options_sub_page( $acf_options_page );
        }


        acf_add_options_page( [ 'page_title' => 'Builder',
                                'menu_title' => 'Post Types',
                                'menu_slug'  => 'is-cpt-options',
                                'icon_url'   => INFINISITE_DIR . 'assets/img/is-icon.png',
                                'capability' => 'activate_plugins',
                                'redirect'   => false ] );

        CPT::build_post_type_option_edit_page();

        $taxonomies = Option::get_acf_global_option( 'options_custom_taxonomies' );

        acf_add_options_page( [ 'page_title' => 'Taxonomy Type Options',
                                'menu_title' => 'Taxonomies',
                                'menu_slug'  => 'is-tax-options',
                                'icon_url'   => INFINISITE_DIR . 'assets/img/is-icon.png',
                                'capability' => 'activate_plugins',
                                'redirect'   => false ] );

        if ( $taxonomies ) {
            foreach ( $taxonomies as $taxonomy ):

                acf_add_options_sub_page( [ 'page_title'             => "{$taxonomy['name']} Taxonomy Settings",
                                            'menu_title'             => ucwords( $taxonomy['name'] ),
                                            'menu_slug'              => "acf-options-taxonomy-{$taxonomy['term']}",
                                            'parent_slug'            => 'is-tax-options',
                                            'capability'             => 'activate_plugins',
                                            'wp_admin_page_variable' => "{$taxonomy['term']}_settings", ] );


                $taxonomy_options_field_hierarchical = [ 'key'     => "field_is_taxonomies_{$taxonomy['term']}_hierarchical",
                                                         'label'   => 'Hierarchical',
                                                         'type'    => 'true_false',
                                                         'ui'      => 1,
                                                         'name'    => "{$taxonomy['term']}_hierarchical",
                                                         'wrapper' => [ 'width' => 20 ] ];
                $taxonomy_options_field_admin_column = [ 'key'     => "field_is_taxonomies_{$taxonomy['term']}_admin_column",
                                                         'label'   => 'Admin Column',
                                                         'type'    => 'true_false',
                                                         'ui'      => 1,
                                                         'name'    => "{$taxonomy['term']}_admin_column",
                                                         'wrapper' => [ 'width' => 20 ] ];

                $taxonomy_options_field_quick_edit = [ 'key'     => "field_is_taxonomies_{$taxonomy['term']}_quick_edit",
                                                       'label'   => 'Quick Edit',
                                                       'type'    => 'true_false',
                                                       'ui'      => 1,
                                                       'name'    => "{$taxonomy['term']}_quick_edit",
                                                       'wrapper' => [ 'width' => 20 ] ];


                $taxonomy_options_field_rest_api = [ 'key'     => "field_is_taxonomies_{$taxonomy['term']}_rest_api",
                                                     'label'   => 'Rest API',
                                                     'type'    => 'true_false',
                                                     'ui'      => 1,
                                                     'name'    => "{$taxonomy['term']}_rest_api",
                                                     'wrapper' => [ 'width' => 20 ] ];


                $taxonomy_options_field_archive_view = [ 'key'     => "field_is_taxonomies_{$taxonomy['term']}_archive_view",
                                                         'label'   => 'Archive View',
                                                         'type'    => 'true_false',
                                                         'ui'      => 1,
                                                         'name'    => "{$taxonomy['term']}_archive_view",
                                                         'wrapper' => [ 'width' => 20 ] ];


                $taxonomy_options_field_archive_template = [ 'key'               => "field_is_taxonomies_{$taxonomy['term']}_archive_template",
                                                             'label'             => 'Taxonomy Archive Template',
                                                             'type'              => 'select',
                                                             'ui'                => 1,
                                                             'name'              => "{$taxonomy['term']}_archive_template",
                                                             'choices'           => Acme::get_templates( 'single_view_templates/archive_templates' ),
                                                             'conditional_logic' => [ [ [ 'field'    => "field_is_taxonomies_{$taxonomy['term']}_archive_view",
                                                                                          'operator' => '==',
                                                                                          'value'    => '1' ] ] ] ];


                $tax_field_group_fields = [ $taxonomy_options_field_hierarchical,
                                            $taxonomy_options_field_admin_column,
                                            $taxonomy_options_field_quick_edit,
                                            $taxonomy_options_field_archive_view,
                                            $taxonomy_options_field_rest_api,
                                            $taxonomy_options_field_archive_template ];

                $cpt_field_group = [ 'key'      => "is_taxonomy_{$taxonomy['term']}_settings",
                                     'title'    => "{$taxonomy['name']} Options",
                                     'fields'   => $tax_field_group_fields,
                                     'location' => [ [ [ 'param'    => 'options_page',
                                                         'operator' => '==',
                                                         'value'    => "acf-options-taxonomy-{$taxonomy['term']}", ], ], ], ];


                acf_add_local_field_group( $cpt_field_group );
            endforeach;
        }


    }

    static function get_acf_options_page_data() {

        $theme_header_settings = [ 'page_title'             => 'Theme Header Settings',
                                   'menu_title'             => 'Header',
                                   'parent_slug'            => 'is-display-settings',
                                   'capability'             => 'activate_plugins',
                                   'wp_admin_page_variable' => 'header' ];

        $theme_footer_settings = [ 'page_title'             => 'Theme Footer Settings',
                                   'menu_title'             => 'Footer',
                                   'parent_slug'            => 'is-display-settings',
                                   'capability'             => 'activate_plugins',
                                   'wp_admin_page_variable' => 'footer' ];

        $acf_options_pages = [ $theme_header_settings,
                               $theme_footer_settings,
        ];

        return $acf_options_pages;
    }

    static function add_meta_fields_to_media_post_type() {

        $link_field = [ 'key'   => 'field_is_meta_global_attachment_link',
                        'label' => 'Link',
                        'name'  => 'link',
                        'type'  => 'link', ];

        $image_field_group = [ 'key'      => 'group_is_global_attachment_meta_fields',
                               'title'    => 'attachment_meta_fields',
                               'fields'   => [ $link_field, ],
                               'location' => [ [ [ 'param'    => 'attachment',
                                                   'operator' => '==',
                                                   'value'    => 'all', ], ], ], ];

        acf_add_local_field_group( $image_field_group );
    }

    private static function get_module_config( $key ) {

        $field_name = "field_ispb_module_config";

        return [ [ 'key'     => "{$field_name}_{$key}_hide",
                   'name'    => "hide",
                   'label'   => 'Hide',
                   'type'    => 'true_false',
                   'wrapper' => [ 'width' => 10 ] ], ];

    }

    public static function register_image_thumbnail_sizes() {
        // 2 queries (w/o sizes)

        $thumbnail_sizes = get_field( "is_thumbnail_sizes", "options" );

        if ( $thumbnail_sizes ) {
            foreach ( $thumbnail_sizes as $size ) {
                \add_image_size( $size['name'], $size['width'], $size['height'], $size['crop'] );
            }
        }

    }


    public static function build_acf_global_text_style_scss() {
        $lookup = [ 'heading_one'    => 'h1',
                    'heading_two'    => 'h2',
                    'heading_three'  => 'h3',
                    'heading_four'   => 'h4',
                    'heading_five'   => 'h5',
                    'heading_six'    => 'h6',
                    'unordered_list' => 'ul',
                    'ordered_list'   => 'ol',
                    'paragraph'      => 'p',
                    'link'           => 'a',
                    'button'         => 'a.button', ];

        $styles = [];

        foreach ( $lookup as $label => $element ):
            $styles[ $element ]['classes'] = get_field( "is_global_type_{$label}_classes", 'option' );
            $styles[ $element ]['global']  = get_field( "is_global_type_{$label}_scss", "option" );
        endforeach;

        $output_scss = '';

        foreach ( $styles as $element => $style ):

            $sub_classes = '';

            if ( $style['classes'] )
                foreach ( $style['classes'] as $class )
                    $sub_classes .= "
                &.{$class['title']} {
                    {$class['scss']}
                }
            ";

            $output_scss .= "
            {$element} {
                {$style['global']}
                $sub_classes
            }
    ";


        endforeach;

        return $output_scss;
    }

    public static function update_typography_options_scss( $testing = false ) {

        // here we build the sass

        $new_styles = Option::build_acf_global_text_style_scss();

        $styles_to_compile = "
            @import 'is_variables.scss';
            .is_page_builder_row , #tinymce , [data-editor-style] {
                $new_styles
            }
        ";

        // we're going to boot up the compiler here
        $scss = new \Leafo\ScssPhp\Compiler();
        $scss->addImportPath( INFINISITE_URI . '/assets/acme' );


        try {

            $compiled_text_styles = $scss->compile( $styles_to_compile );

            // and here we update the stylesheet.
            $file        = INFINISITE_URI . 'assets/css/is_text_styles.css';
            $file_update = file_put_contents( $file, $compiled_text_styles );

            if ( ! $file_update ) {
                return $file_update;
            }

            return $compiled_text_styles;


        } catch ( \Exception $e ) {

            error_log( $e );

            $notice = AdminNotice::getInstance();
            $notice->displayError( "
                <h2>Warning!</h2>
                Your SCSS has an error somewhere.<br />
                Its probably a variable error. Your CSS file was not updated.<br />
                <pre>$e</pre>
                <h3>Your Attempted Styles are below:</h3>
                <pre>$styles_to_compile</pre>
            " );

            return false;

        }


    }


    static function get_is_post_type_config_options( $post_type ) {

        $return = [];

        /**
         *
         * POST TYPE CONFIG OPTIONS
         *
         *   single_view_template
         *   single_view
         *   sidebar_set
         *   page_builder
         *   hierarchical
         *   custom_meta_fields
         *   meta_fields
         *   default_image
         *
         */

        $return['single_view_template'] = get_option( "options_is_cpt_{$post_type}_template" );
        $return['single_view']          = get_option( "options_is_cpt_{$post_type}_single_view" );
        $return['sidebar_set']          = get_option( "options_is_cpt_{$post_type}_sidebar_set" );
        $return['page_builder']         = get_option( "options_is_cpt_{$post_type}_page_builder" );
        $return['hierarchical']         = get_option( "options_is_cpt_{$post_type}_hierarchical" );
        $return['custom_meta_fields']   = get_option( "options_is_cpt_{$post_type}_custom_meta_fields" );
        $return['meta_fields']          = get_option( "options_is_cpt_{$post_type}_meta_fields" );
        $return['default_image']        = get_option( "options_is_cpt_{$post_type}_default_image" );

        return $return;
    }

    static function get_custom_offcanvas_menu_templates( $directory, $config = [] ) {

        // This is a wrapper function for the ACF header offcanvas template option

        return Acme::get_templates( $directory, $config );

    }


    static function get_acf_global_option( $name ) {

        $cache = DB::function_cache( 'acfo_' . $name );

        if ( $cache !== false )
            return $cache;

        global $wpdb;
        $rows = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}options WHERE option_name = '$name'", OBJECT );

        if ( ! isset( $rows[0] ) ) {
            return false;
        }

        $field_count = $rows[0]->option_value;

        $return = [];

        for ( $i = 0; $i < $field_count; $i ++ ):
            $prefix = "{$name}_{$i}_";


            $fields = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}options WHERE option_name LIKE '{$prefix}%'", OBJECT );

            foreach ( $fields as $field ):
                $field_name = substr( $field->option_name, strlen( $prefix ) );

                if ( Acme::is_serialized( $field->option_value ) ):
                    $return[ $i ][ $field_name ] = unserialize( $field->option_value );
                    continue;
                endif;

                $return[ $i ][ $field_name ] = $field->option_value;

            endforeach;


        endfor;

        return DB::function_cache( 'acfo_' . $name, $return );

    }

}
