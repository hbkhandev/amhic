<?


namespace TSD_Infinisite;

class Page_Builder {

    private $index = [];
    private $modules = [];
    private $acf_rows = [];

    private $empty = true;


    public function __construct($id) {


        $this->acf_rows = get_field("rows", $id);

        if ($this->acf_rows) {
            $this->empty = false;
        }


        $this->build_module_index();

    }

    public function get_module_list() {

        return $this->modules;

    }

    public function get_rows() {
        return $this->acf_rows;
    }


    private function build_module_index() {

        if ($this->empty):

            return;

        endif;

        foreach ($this->acf_rows as $row):
            foreach ($row['cells'] as $cell):
                if (!$cell['is_acf_fc_container']) {
                    continue;
                }
                foreach ($cell['is_acf_fc_container'] as $module):


                    $this->modules[] = $module['acf_fc_layout'];


                endforeach;
            endforeach;
        endforeach;

    }

    public static function create_pagebuilder_layout() {

        $use_trans = false;


        if ($use_trans):

            $page_builder_field_group = DB::function_cache('Page_Builder::create_pagebuilder_layout');

            if ($page_builder_field_group === false) {
                $page_builder_field_group = self::build_page_builder_field_group();
                $page_builder_field_group = apply_filters('update_is_pagebuilder_fields', $page_builder_field_group);
                $page_builder_field_group = DB::function_cache('Page_Builder::create_pagebuilder_layout', $page_builder_field_group);
            };
        else:
            $page_builder_field_group = self::build_page_builder_field_group();
            $page_builder_field_group = apply_filters('update_is_pagebuilder_fields', $page_builder_field_group);

        endif;
        acf_add_local_field_group($page_builder_field_group);

    }

    public static function get_pagebuilder_layouts($config = false) {

        if (!$config) {
            return false;
        }

        $field_group = self::build_page_builder_field_group($config);

        $layouts = $field_group['fields'][1];

        if (array_key_exists('label', $config)) {
            $layouts['label'] = $config['label'];
        }

        return $layouts;

    }

    private static function build_page_builder_field_group($user_config = false) {


        $defaults = ['suffix' => ''];

        $config = wp_parse_args($user_config, $defaults);

        $ispb_row_config_background_options = ["key"        => "field_ispb_row_config_background_options{$config['suffix']}",
                                               'label'      => 'Background Options',
                                               'name'       => 'ispb_row_background_options',
                                               'type'       => 'group',
                                               'sub_fields' => ACF::get_background_display_options('field_ispb_row_config_background_options', ['color',
                                                                                                                                                'image'])];

        $ispb_row_config_max_width_group = ACF::get_row_width_height('field_ispb_row_dimensions', ['wrapper' => ['width' => 33]]);

        $ispb_row_config_flex_align_classes = ACF::get_flexbox_alignment_classes_group('field_ispb_row_flex_align', ['wrapper' => ['width' => 33]]);

        $ispb_row_config_top_bottom_spacing = ACF::get_row_spacers('field_ispb_row_spacers', ['wrapper' => ['width' => 33]]);


        $ispb_row_config_row_x_padding = ["key"     => "field_ispb_row_config_row_x_padding{$config['suffix']}",
                                          'label'   => 'Cell Horizontal Padding',
                                          'name'    => 'row_x_padding',
                                          'type'    => 'true_false',
                                          'wrapper' => ['width' => 10],];


        $ispb_row_config_row_y_padding = ["key"     => "field_ispb_row_config_row_y_padding{$config['suffix']}",
                                          'label'   => 'Cell Vertical Padding',
                                          'name'    => 'row_y_padding',
                                          'type'    => 'true_false',
                                          'wrapper' => ['width' => 10],];

        $ispb_row_config_row_sticky = ["key"     => "field_ispb_row_config_row_sticky{$config['suffix']}",
                                       'label'   => 'Sticky Row',
                                       'name'    => 'row_sticky',
                                       'type'    => 'true_false',
                                       'wrapper' => ['width' => 10],];


        $ispb_row_config_row_id = ["key"     => "field_ispb_row_config_row_id_tag{$config['suffix']}",
                                   'label'   => 'Row ID Tag',
                                   'name'    => 'row_id',
                                   'type'    => 'text',
                                   'wrapper' => ['width' => 20],];


        $ispb_row_config_row_class = ["key"     => "field_ispb_row_config_row_class_tag{$config['suffix']}",
                                      'label'   => 'Row Classes',
                                      'name'    => 'row_class',
                                      'type'    => 'text',
                                      'wrapper' => ['width' => 20],];


        $ispb_row_config_row_title = ["key"     => "field_ispb_row_config_row_title_tag{$config['suffix']}",
                                      'label'   => 'Row Title',
                                      'name'    => 'row_title',
                                      'type'    => 'text',
                                      'wrapper' => ['width' => 20],];


        $ispb_cell_config_background_options = ["key"        => "field_ispb_cell_config_background_options{$config['suffix']}",
                                                'label'      => 'Background Options',
                                                'name'       => 'cell_background_options',
                                                'type'       => 'group',
                                                'sub_fields' => ACF::get_background_display_options('field_ispb_cell_config_background_options', ['color',
                                                                                                                                                  'image'])];


        // this is the primary content container.
        // the $layouts var has all the page builder layouts

        $ispb_cell_config_col_width = ["key"        => "field_ispb_cell_config_col_width{$config['suffix']}",
                                       'label'      => 'Cell Width',
                                       'name'       => 'cell_column_width',
                                       'type'       => 'group',
                                       'wrapper'    => ['width' => 55],
                                       'sub_fields' => [["key"           => "field_ispb_cell_config_col_width_mobile{$config['suffix']}",
                                                         'label'         => 'Mobile',
                                                         'name'          => 'mobile',
                                                         'type'          => 'text',
                                                         'wrapper'       => ['width' => 25],
                                                         'default_value' => 12],
                                                        ["key"     => "field_ispb_cell_config_col_width_tablet{$config['suffix']}",
                                                         'label'   => 'Tablet',
                                                         'name'    => 'tablet',
                                                         'type'    => 'text',
                                                         'wrapper' => ['width' => 25],],
                                                        ["key"     => "field_ispb_cell_config_col_width_desktop{$config['suffix']}",
                                                         'label'   => 'Desktop',
                                                         'name'    => 'desktop',
                                                         'type'    => 'text',
                                                         'wrapper' => ['width' => 25],],
                                                        ["key"     => "field_ispb_cell_config_col_width_large_desktop{$config['suffix']}",
                                                         'label'   => 'Large Desktop',
                                                         'name'    => 'large_desktop',
                                                         'type'    => 'text',
                                                         'wrapper' => ['width' => 25],],]];

        $ispb_cell_config_vertical_self_alignment = ["key"        => "field_ispb_cell_config_vertical_self_alignment{$config['suffix']}",
                                                     'label'      => 'Vertical Self Alignment',
                                                     'name'       => 'cell_vertical_self_alignment',
                                                     'type'       => 'select',
                                                     'ui'         => '1',
                                                     'allow_null' => 1,
                                                     'wrapper'    => ['width' => 15],
                                                     'choices'    => ['top'    => 'Top',
                                                                      'middle' => 'Middle',
                                                                      'bottom' => 'Bottom']];

        $ispb_cell_config_cell_id = ["key"     => "field_ispb_cell_config_cell_id_tag{$config['suffix']}",
                                     'label'   => 'Cell ID Tag',
                                     'name'    => 'cell_id',
                                     'type'    => 'text',
                                     'wrapper' => ['width' => 15],];

        $ispb_cell_config_cell_classes = ["key"     => "field_ispb_cell_config_cell_classes_tag{$config['suffix']}",
                                          'label'   => 'Cell Classes',
                                          'name'    => 'cell_class',
                                          'type'    => 'text',
                                          'wrapper' => ['width' => 15],];


        /*
         *
         * here be the page builder creation
         *
         *
         */


        $infinisite_page_builder_layouts = self::get_page_builder_layouts();

        if (class_exists('GFAPI') && isset($gravity_form)) {
            $infinisite_page_builder_layouts[] = $gravity_form;
        }

        $infinisite_page_builder_layouts = apply_filters("ispb.module_layouts", $infinisite_page_builder_layouts);

        $ispb_cell_layout_container = ["key"          => "field_ispb_cell_acf_fc_container{$config['suffix']}",
                                       'name'         => 'is_acf_fc_container',
                                       'type'         => 'flexible_content',
                                       'button_label' => 'Add Module',
                                       'layouts'      => $infinisite_page_builder_layouts];

        $ispb_cell_scss_code = ["key"     => "field_ispb_cell_scss_code{$config['suffix']}",
                                'label'   => 'SCSS Code',
                                'name'    => 'scss_code',
                                'type'    => 'textarea',
                                'wrapper' => ['class' => 'font-family-monospace']];


        $ispb_cell_repeater_fields = [ACF::get_horizontal_tab('Cell Modules', 'field_ispb_cell_layout_fields'),
                                      $ispb_cell_layout_container,];


        if (Acme::admin_screen_show_config_tab('cell') || true):
            $ispb_cell_repeater_fields[] = ACF::get_horizontal_tab('Cell Configuration', 'field_ispb_cell_configuration');
            $ispb_cell_repeater_fields[] = $ispb_cell_config_background_options;
            $ispb_cell_repeater_fields[] = $ispb_cell_config_col_width;
            $ispb_cell_repeater_fields[] = $ispb_cell_config_vertical_self_alignment;
            $ispb_cell_repeater_fields[] = $ispb_cell_config_cell_id;
            $ispb_cell_repeater_fields[] = $ispb_cell_config_cell_classes;
        endif;

        if (Acme::admin_screen_show_scss_tab('cell')):
            $ispb_cell_repeater_fields[] = ACF::get_horizontal_tab('Cell SCSS', 'field_ispb_cell_scss_tab');
            $ispb_cell_repeater_fields[] = $ispb_cell_scss_code;
        endif;


        $ispb_cell_repeater = ["key"          => "field_ispb_cell_repeater{$config['suffix']}",
                               'name'         => 'cells',
                               'type'         => 'repeater',
                               'min'          => 1,
                               'button_label' => 'Add Cell',
                               'layout'       => 'block',
                               'sub_fields'   => $ispb_cell_repeater_fields];

        $infinisite_row_config_options = [];

        $ispb_row_scss_code = ["key"  => "field_ispb_row_scss_code{$config['suffix']}",
                               'name' => 'scss_code',
                               'type' => 'textarea',];


        // this is the actual field group - we attach the builder
        // repeater to this

        $ispb_row_fields = [ACF::get_horizontal_tab('Row Cells', 'field_ispb_row_cells_tab'),
                            $ispb_cell_repeater,];

        if (Acme::admin_screen_show_config_tab('row')):
            $ispb_row_fields[] = ACF::get_horizontal_tab('Row Configuration', 'field_ispb_row_configuration_tab');
            $ispb_row_fields[] = $ispb_row_config_max_width_group;
            $ispb_row_fields[] = $ispb_row_config_flex_align_classes;
            $ispb_row_fields[] = $ispb_row_config_top_bottom_spacing;
            $ispb_row_fields[] = $ispb_row_config_row_x_padding;
            $ispb_row_fields[] = $ispb_row_config_row_y_padding;
            $ispb_row_fields[] = $ispb_row_config_row_sticky;
            $ispb_row_fields[] = $ispb_row_config_row_id;
            $ispb_row_fields[] = $ispb_row_config_row_class;
            $ispb_row_fields[] = $ispb_row_config_row_title;
            $ispb_row_fields[] = $ispb_row_config_background_options;
        endif;

        if (Acme::admin_screen_show_scss_tab('row')):
            $ispb_row_fields[] = ACF::get_horizontal_tab('Row SCSS', 'field_ispb_row_scss_tab');
            $ispb_row_fields[] = $ispb_row_scss_code;
        endif;


        $pb_repeater_key = array_key_exists('key', $config) ? $config['key'] : 'field_ispb_row_repeater';


        $pb_repeater_name = array_key_exists('name', $config) ? $config['name'] : 'rows';

        $layout_key = 'group_5a0f028698500';


        $is_page_builder_repeater = ['name'         => $pb_repeater_name,
                                     'type'         => 'repeater',
                                     'layout'       => 'block',
                                     'button_label' => 'Add Row',
                                     'key'          => $pb_repeater_key,
                                     'min'          => 1,
                                     'sub_fields'   => $ispb_row_fields];


        $ispb_page_config_inline_header = ["key"     => "field_ispb_page_config_inline_header{$config['suffix']}",
                                           'name'    => 'inline_header',
                                           'label'   => 'Inline Header',
                                           'type'    => 'true_false',
                                           'message' => 'Header in First Row',
                                           'wrapper' => ['width' => 25]];


        $ispb_page_config_inline_footer = ["key"     => "field_ispb_page_config_inline_footer{$config['suffix']}",
                                           'name'    => 'inline_footer',
                                           'label'   => 'Inline Footer',
                                           'type'    => 'true_false',
                                           'message' => 'Footer in Last Row',
                                           'wrapper' => ['width' => 25]];

        $ispb_page_config_hide_header = ["key"     => "field_ispb_page_config_hide_header{$config['suffix']}",
                                         'name'    => 'hide_header',
                                         'label'   => 'Hide Header',
                                         'type'    => 'true_false',
                                         'wrapper' => ['width' => 25]];

        $ispb_page_config_hide_footer = ["key"     => "field_ispb_page_config_hide_footer{$config['suffix']}",
                                         'name'    => 'hide_footer',
                                         'label'   => 'Hide Footer',
                                         'type'    => 'true_false',
                                         'wrapper' => ['width' => 25]];


        $ispb_page_config_soverride_header_logo = ["key"          => "field_ispb_page_header_logo{$config['suffix']}",
                                                   'name'         => 'header_logo',
                                                   'label'        => 'Header Logo',
                                                   'type'         => 'image',
                                                   'preview_size' => 'medium',
                                                   'wrapper'      => ['width' => 50]];

        $ispb_page_scss_code = ["key"   => "field_ispb_page_scss_code{$config['suffix']}",
                                'name'  => 'scss_code',
                                'label' => 'SCSS',
                                'type'  => 'textarea',];

        $page_builder_field_group = ['key'      => $layout_key,
                                     'title'    => 'Infinisite Page Builder',
                                     'location' => [[['param'    => 'post_type',
                                                      'operator' => '==',
                                                      'value'    => 'post',],],
                                                    [['param'    => 'post_type',
                                                      'operator' => '==',
                                                      'value'    => 'page',],],],
                                     'fields'   => [ACF::get_horizontal_tab('Layout', 'field_ispb_content_editor'),
                                                    $is_page_builder_repeater]];

        if (Acme::admin_screen_show_config_tab('page')):
            $page_builder_field_group['fields'][] = ACF::get_horizontal_tab('Page Config', 'field_ispb_page_config');
            $page_builder_field_group['fields'][] = $ispb_page_config_inline_header;
            $page_builder_field_group['fields'][] = $ispb_page_config_inline_footer;
            $page_builder_field_group['fields'][] = $ispb_page_config_hide_header;
            $page_builder_field_group['fields'][] = $ispb_page_config_hide_footer;
            $page_builder_field_group['fields'][] = $ispb_page_config_soverride_header_logo;
        endif;

        if (Acme::admin_screen_show_scss_tab('page')):
            $page_builder_field_group['fields'][] = ACF::get_horizontal_tab('Page SCSS', 'field_ispb_page_scss');
            $page_builder_field_group['fields'][] = $ispb_page_scss_code;
        endif;

        // we are attaching the page builder to all the post types
        // that ask for it

        // TODO: is this being called in init? we have to check for the post type array

        $custom_post_types = Acme::get_post_types();

        if (is_array($custom_post_types)) {
            if ($custom_post_types) {
                foreach ($custom_post_types as $post_type) {
                    if (isset($post_type['config']['page_builder'])) {
                        if ($post_type['config']['page_builder'] == 1) {
                            $page_builder_field_group['location'][][] = ['param'    => 'post_type',
                                                                         'operator' => '==',
                                                                         'value'    => $post_type['post_type']];
                        }
                    }
                }
            }
        }

        return $page_builder_field_group;
    }

    public static function get_page_builder_layouts($prefix = false) {

        // Given that we're building page builder Modules here, we have moved
        // this information into the module file.

        return Module::get_acf_module_information_for_registration();

    }

    public static function get_module_use_info_for_debug() {

        $post_types = Acme::get_is_post_types(false);

        $page_builder_types = [];


        foreach ($post_types as $_pt) :

            $uses_page_builder = get_field("is_cpt_{$_pt}_page_builder", "options");

            if ($uses_page_builder || $_pt == 'page' || $_pt == 'post') {
                $page_builder_types[] = $_pt;
            }

        endforeach;

        $_q = new \WP_Query(['post_type'      => $page_builder_types,
                             'posts_per_page' => -1]);

        $module_index = [];

        if ($_q->found_posts == 0) {
            return $module_index;
        }

        foreach ($_q->posts as $_p):
            $pb = new Page_Builder($_p->ID);
            $mod = $pb->get_module_list();

            if (!count($mod)) {
                continue;
            }

            $post = new IS_Post($_p);

            foreach ($mod as $m):
                $module_index[$m][] = $post;
            endforeach;

        endforeach;


        $module_information = [];

        foreach ($module_index as $label => $posts):


            $posts_to_check = [];

            foreach ($posts as $post):
                $processed = Acme::check_array_for_post($post, $posts_to_check);
                $posts_to_check[] = $post;

                $post_key = "$post->post_title - $post->ID";

                if (!$processed) {
                    $module_information[$label][$post_key]['post'] = $post;

                    $module_information[$label][$post_key]['count'] = 1;
                } else {
                    $module_information[$label][$post_key]['count']++;
                }


            endforeach;


        endforeach;

        // we want to alphabetize our module names to make lookign
        ksort($module_information);

        return $module_information;


    }

    static function generate_background_styles_from_acf_background_options_group($group = false) {

        if (!$group) {
            return '';
        }

        $styles = [];

        if ($group['image']):
            $styles['background-image'] = "url({$group['image']['url']})";

            if ($group['position']) {
                $styles['background-position'] = $group['position'];
            }

            if ($group['size']) {
                $styles['background-size'] = $group['size'];
            }

            if ($group['repeat']) {
                $styles['background-repeat'] = $group['repeat'];
            }
        endif;

        $opacity = isset($group['opacity']) ? $group['opacity'] : 1;

        $hex_color = Palette::get_hex_color_from_role($group['color']);

        if (array_key_exists("color", $group)) {
            $styles['background-color'] = Palette::hex2rgba($hex_color, $opacity);
        }

        if (array_key_exists("blending", $group)) {
            $styles['background-blend-mode'] = $group['blending'];
        }

        return $styles;

    }

    static function ispb_module_templates($template = false) {


        $return = ['default' => 'Default'];

        if (!$template) {
            return $return;
        }

        $templates = Acme::get_templates("module_templates/$template");

        foreach ($templates as $key => $value)
            $return[$key] = $value;

        return $return;

    }

    static function get_post_types_for_page_builder_field($surpess_defaults = false) {

        $post_types = Acme::get_is_post_types($surpess_defaults);

        $return = [];

        foreach ($post_types as $name => $label) {
            $return[$name] = $label;
        }

        return $return;


    }

    static function get_searchwp_engines_for_page_builder_filter_object() {

        $swp_options = get_option("searchwp_settings");

        $return = [];

        if (!$swp_options) {
            return $return;
        }

        foreach ($swp_options['engines'] as $name => $option) {
            $return[$name] = $name;
        }

        return $return;

    }

    static function get_menus_for_acf_module() {

        $return = [];
        $menus = get_terms(['taxonomy' => 'nav_menu',]);

        if ($menus) {
            foreach ($menus as $menu) {
                $return[$menu->slug] = $menu->name;
            }
        }

        return $return;

    }

    static function get_taxonomies_for_page_builder_select_menu() {

        /**
         * simple function to grab the defined taxonomies and output
         * them as a key/value pair for a page builder module selection
         */


        $taxonomies = ACF_Helper::get_repeater('custom_taxonomies');

        $return = [];


        if (!$taxonomies)
            return $return;

        foreach ($taxonomies as $tax)
            $return[$tax['term']] = $tax['name'];

        return $return;

    }

}
