<?

namespace TSD_Infinisite;

class Palette
{

    public $roles = [
        'primary',
        'secondary',
        'tertiary',
        'accent',
        'warning',
        'success',
        'alert',
        'white',
        'gray',
        'black',
    ];

    public $mods = [
        'xxdark',
        'xdark',
        'dark',
        'light',
        'xlight',
        'xxlight'
    ];

    public $is_defaults = [
        'primary' => '#00a2a4',
        'secondary' => '#f24900',
        'tertiary' => '#666666',
        'accent' => '#e5001c',
        'warning' => '#ffae00',
        'success' => '#3adb76',
        'alert' => '#dd3333',
        'white' => '#fefefe',
        'gray' => '#cacaca',
        'black' => '#0a0a0a',
    ];

    public $shades = [];

    public function __construct()
    {

        $this->get_palette();

    }

    public static function update_from_acf_save()
    {

        // bail early if no ACF data
        if (empty($_POST['acf']))
            return;

        $global_colors = $_POST['acf']['field_is_global_colors_palette'];

        if (!$global_colors) return;

        $strip_out_field_name_from_acf_field = [];

        foreach ($global_colors as $role => $value)
            $strip_out_field_name_from_acf_field[substr($role, 31)] = $value;

        $palette = new Palette();

        foreach ($global_colors as $db_name => $value):
            $role = substr($db_name, 31);
            $palette->generate_new_shades_for_role($role, $value);
        endforeach;

    }

    public function get($role)
    {
        /*
         * returns role color value
         */

        foreach ($this->shades as $potential => $color)
            if ($role == $potential)
                return $color;

    }

    public function set($role)
    {
        /*
         * sets role color value
         */
    }

    public function generate($role)
    {
        /*
         * uses base role color to generate
         * and save single color role
         */
    }

    public function generate_new_shades_for_role($role, $color)
    {

        $shades_for_database = [];

        // error_log("generating new shades for $role");


        $shades_for_database[] = [
            'role' => $role,
            'value' => $color
        ];

        foreach ($this->mods as $mod):


            $dark_mod = strpos($mod, 'dark');
            $light_mod = strpos($mod, 'light');

            $lighten_color = $dark_mod === false ? true : false;


            $color_mod_strength = 0;

            if ($lighten_color) $color_mod_strength = $light_mod;
            if (!$lighten_color) $color_mod_strength = $dark_mod;

            $color_mod_value = 0;

            switch ($color_mod_strength):
                case 0:
                    $color_mod_value = 25;
                    break;
                case 1:
                    $color_mod_value = 50;
                    break;
                case 2:
                    $color_mod_value = 75;
                    break;

            endswitch;


            if (!$lighten_color)
                $color_mod_value = 0 - $color_mod_value;

            $scss_code_to_compile = ".style { color: scale-color($color, \$lightness: $color_mod_value%);}";

            // error_log($color);
            // error_log($scss_code_to_compile);
            $scss = new \Leafo\ScssPhp\Compiler();
            $scss->addImportPath(INFINISITE_URI . '/assets/acme/');

            $compiled_style_to_adjust = $scss->compile($scss_code_to_compile);
            // $debug .= $compiled_style_to_adjust;

            $updated_color_value = substr($compiled_style_to_adjust, 18, 7);

            if (strpos($updated_color_value, ';'))
                $updated_color_value = substr($updated_color_value, 0, 4);

            // error_log("updated: " . $updated_color_value);

            $shades_for_database[] = [
                'role' => "{$role}_{$mod}",
                'value' => $updated_color_value
            ];

            $this->shades["{$role}_$mod"] = $updated_color_value;


            // error_log('base color in place: ' . $role . ' ' . $mod);


        endforeach;

        foreach ($shades_for_database as $shade)
            \update_option('options_is_pb_global_color_palette_' . $shade['role'], $shade['value']);

    }

    public function get_palette()
    {

        /*
         * check if the palette exists.
         * if so, get & set
         * if not, create
         */


        $default_values = !$this::has_palette_been_set() ? true : false;

        // build the shades
        $this->build_shades($default_values);

        // retreive the shade values from the database
        $this->get_shades();

        $this->update_utility_classes();


    }

    public function get_shades()
    {

        /*
         * this function assumes that the shades are in the database
         * but the function in question is being called too early
         * for typical acf to work? idk.
         */


        // not sure why we need to do this
        if (!$this->roles) return;

        foreach ($this->roles as $role):
            $value = get_option("options_is_pb_global_color_palette_$role");
            $this->shades[$role] = $value;

            // we don't generate shades of white or black.
            if ($role == 'white' || $role == 'black')
                continue;

            foreach ($this->mods as $mod):
                $value = get_option("options_is_pb_global_color_palette_{$role}_$mod");
                $this->shades["{$role}_$mod"] = $value;
            endforeach;
        endforeach;

        $this->insert_overrides_into_shades();

    }

    public static function has_palette_been_set()
    {

        /*
         * right now, i'm going to be proving this by
         * checking against the primary color
         */

        $primary = 'options_is_pb_global_color_palette_primary_light';

        return get_option($primary) ? true : false;

    }

    public function build_shades($use_is_default_values = false)
    {


        /*
         * automatic shading!! we are going to begin by starting our compiler
         * then, inside the color mod loop, we are going to do some text wrangling
         * to determine weather to darken or lighten, and by how much. we then
         * apply those values in a string, and pass it to the compiler. lastly,
         * we put those in the
         */

        $scss = new \Leafo\ScssPhp\Compiler();
        $scss->setFormatter('Leafo\ScssPhp\Formatter\Compact');
        $scss_code_to_compile = "";

        $colors_for_database = [];
        $this->get_shades();
        $colors = $this->shades;

        if ($use_is_default_values):
            // error_log('IS Color Palette not found, creating default set');
            $this->build_default_shades();
            return true;
        endif;


        return true;


    }

    private function build_default_shades()
    {

        // error_log('building default shades');

        foreach ($this->is_defaults as $role => $color):

            $palette_colors_for_database[] = [
                'role' => $role,
                'value' => $color
            ];

            $this->shades[$role] = $color;

            // we don't generate shades of white or black.
            if ($role == 'white' || $role == 'black')
                continue;

            $this->generate_new_shades_for_role($role, $color);

        endforeach;


        foreach ($palette_colors_for_database as $shade)
            \update_option('options_is_pb_global_color_palette_' . $shade['role'], $shade['value']);

        $this->insert_overrides_into_shades();

        // error_log('added default base palette to database');
    }

    public function insert_overrides_into_shades()
    {

        $acf_user_color_role_overrides = ACF_Helper::get_repeater("is_pb_global_colors");

        if (!$acf_user_color_role_overrides) return;

        foreach ($acf_user_color_role_overrides as $override)
            $this->shades[$override['role']] = $override['value'];

    }

    public function update_utility_classes()
    {


        $is_global_foundation_colors = '';


        foreach ($this->shades as $role => $value)
            $is_global_foundation_colors .= "\${$role}: {$value};\n";

        $is_global_foundation_color_map = "\n\n\$is_global_palette : (\n";


        foreach ($this->shades as $role => $value)
            $is_global_foundation_color_map .= "\t{$role}: {$value},\n";

        $is_global_foundation_color_map .= ");";

        $is_global_foundation_file_content = "$is_global_foundation_colors $is_global_foundation_color_map";

        // and here we update the stylesheet.
        $file = INFINISITE_URI . 'assets/acme/is_variables.scss';
        if (file_exists($file))
            file_put_contents($file, $is_global_foundation_file_content);

        // and here we dump it in the child theme so their app.scss can access it
        $file = THEME_URI . 'assets/scss/_is_variables.scss';
        if (file_exists($file))
            file_put_contents($file, $is_global_foundation_file_content);

        // error_log('is class update');

        // we're going to boot up the compiler here
        $scss = new \Leafo\ScssPhp\Compiler();
        $scss->addImportPath(INFINISITE_URI . '/assets/acme/');

        $utility_scss_filepath = INFINISITE_URI . '/assets/acme/is_utility_classes.scss';
        $utility_styles = file_get_contents($utility_scss_filepath);

        // error_log('updating utility styles');
        // error_log($is_global_foundation_file_content);

        $compiled_utility_styles = $scss->compile($utility_styles);

        $is_utility_stylesheet_filepath = INFINISITE_URI . '/assets/css/is_utility_classes.css';

        file_put_contents($is_utility_stylesheet_filepath, $compiled_utility_styles);


    }

    public static function get_hex_color_from_role($role)
    {

        $value = get_option("options_is_pb_global_color_palette_$role");

        return $value;

    }

	public static function get_is_palette() {

    	// retreives all the roles and values from the database

        $cache = DB::function_cache('Palette::get_is_palette');

        if($cache !== false) return $cache;

		$return = [];

		$palette_roles = [
			"primary",
			"secondary",
			"tertiary",
			"accent",
			"warning",
			"success",
			"alert",
			"white",
			"gray",
			"black",
		];

		$color_mods = [
			'xxdark',
			'xdark',
			'dark',
			'light',
			'xlight',
			'xxlight'
		];

		$base = 'options_is_pb_global_color_palette_';

		foreach ( $palette_roles as $role ):
			$name            = $base . $role;
			$value           = get_option( $name );
			$return[ $role ] = $value;
			if ( $role == 'white' || $role == 'black' ) {
				continue;
			}
			foreach ( $color_mods as $mod ):
				$name                   = $base . $role . "_" . $mod;
				$value                  = get_option( $name );
				$return["{$role}_$mod"] = $value;
			endforeach;
		endforeach;

		return DB::function_cache('Palette::get_is_palette', $return);

	}


	static function insert_IS_palette_overrides() {

		/*
         * we're going to check for the primary color first. if that value isn't
         * set, we're initalizing the plugin and we need to slip in our bootstrapped
         * values into the database
         */

		$override_color_count = get_option( "options_is_pb_global_colors" );

		$color_option_name_base = "options_is_pb_global_color_palette_";

		$custom_role_colors = [];

		for ( $i = 0; $i < $override_color_count; $i ++ ):

			$role  = get_option( "options_is_pb_global_colors_{$i}_role" );
			$value = get_option( "options_is_pb_global_colors_{$i}_value" );

			$custom_role_colors[] = $role;

			\update_option( $color_option_name_base . $role, $value );

		endfor;

		\update_option( "ispb_global_color_pallete_role_overrides", serialize( $custom_role_colors ) );

	}


	static function check_IS_palette_for_blanks_and_rebuild() {

        DB::clear_palette_cache();

		$roles_as_of_last_build = unserialize( get_option( "ispb_global_color_pallete_role_overrides" ) );

		$current_role_overrides = DB::acf_cache( "is_pb_global_colors" );
		if ( ! $current_role_overrides ) {
			return;
		}

		$overrides = [];

		foreach ( $current_role_overrides as $override ) {
			$overrides[] = $override['role'];
		}

		/*
         * we check for diffs, which tells us which fields are in the old values
         * that aren't in the new
         * TODO: make sure this is true
         *
         * we then set the these diff values to be red
         */
		$diff = array_diff( $overrides, $roles_as_of_last_build );
		if ( ! $diff ) {
			return;
		}

		foreach ( $diff as $modded_role ) {
			\update_option( "options_is_pb_global_color_palette_$modded_role", '#F00' );
		}


	}

	static function get_colors() {

        $cache = DB::function_cache('Palette::get_is_palette');

        if(!empty($cache)):
            return $cache;
        endif;

//        Flash::error('palette not found');

        $primary = get_option( "options_is_pb_global_color_palette_primary" );

		// if we don't have the first color defined, we don't have any of them,
		// so we need to build the palette

		// this should only take place on plugin init

		if ( ! $primary ):
			$color = new Palette();
			$color->build_shades( 1 );
		endif;


		/*
         * right now, this really is just an alias to the get_is_palette
         * which is a legacy of the is palette not coming in until the end
         * and being shoehorned in and now a core part of the whole thing
         *
         */

		Palette::insert_IS_palette_overrides();


		// if we don't have primary, we're initing i think

		if ( $primary ) {
			return Palette::get_is_palette();
		}

		Flash::error('error in palette::get_colors');

		return [];

	}


	static function hex2rgba( $color, $opacity = false ) {

		$default = 'rgb(0,0,0,0)';

		//Return default if no color provided - transparent background
		if ( empty( $color ) ) {
			return $default;
		}


		if ( ! $color ) {
			return $default;
		}


		if ( substr( $color, 0, 1 ) != '#' ):

			$color = Palette::get_palette_color_by_role_name( $color );
			$color = $color['value'];

		endif;

		//Sanitize $color if "#" is provided
		if ( $color[0] == '#' ) {
			$color = substr( $color, 1 );
		}

		//Check if color has 6 or 3 characters and get values
		if ( strlen( $color ) == 6 ) {
			$hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
		} elseif ( strlen( $color ) == 3 ) {
			$hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
		} else {
			return $default;
		}

		//Convert hexadec to rgb
		$rgb = array_map( 'hexdec', $hex );

		//Check if opacity is set(rgba or rgb)
		if ( $opacity ) {
			if ( abs( $opacity ) > 1 ) {
				$opacity = 1.0;
			}
			$output = 'rgba(' . implode( ",", $rgb ) . ',' . $opacity . ')';
		} else {
			$output = 'rgb(' . implode( ",", $rgb ) . ')';
		}

		//Return rgb(a) color string
		return $output;
	}


	static function get_is_global_color_roles() {

		// TODO: rewrite me using the global get_colors() function

		$return = [];

		$global_color_roles = [
			'primary',
			'secondary',
			'tertiary',
			'accent',
			'warning',
			'success',
			'alert',
			'white',
			'gray',
			'black',
		];

		$mods    = [ 'light', 'dark', 'xlight', 'xdark', 'xxlight', 'xxdark' ];
		$exclude = [ 'white', 'black' ];

		foreach ( $global_color_roles as $color ):
			if ( in_array( $color, $exclude ) ) {
				continue;
			}
			foreach ( $mods as $mod ) {
				$return["{$color}_{$mod}"] = "$color - $mod";
			}
		endforeach;

		return $return;

	}

	static function build_get_colors_into_background_options_select_choices() {

		$return = [];

		foreach ( Palette::get_colors() as $role => $value ):

			$original_role = $role;

			if ( isset( $color['choices'][ $value ] ) ):
				$current_label            = $color['choices'][ $value ];
				$new_label                = $role;
				$return[ $original_role ] = "$current_label / $new_label ";
				continue;
			endif;
			$return[ $original_role ] = $role;
		endforeach;

		return $return;
	}



	static function get_palette_color_by_role_name( $role_name = false ) {

		$return = false;

		if ( ! $role_name ) {
			return $return;
		}

		$color_lookup = Palette::get_colors();

		$chosen_color = '#ffffff';

		foreach ( $color_lookup as $role => $value ):
			if ( $role != $role_name ) {
				continue;
			}

			$chosen_color = $global_color;

		endforeach;

		return $chosen_color;

	}


}