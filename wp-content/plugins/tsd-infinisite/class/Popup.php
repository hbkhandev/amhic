<?

namespace TSD_Infinisite;

class Popup
{

    public $html;

    private $config = [];
    private $set_acf = [];


    public function __construct($id = false)
    {

        return;

        $this->build_config($id);
        $this->get_popup_set();
        $this->build_html();

        $this->prepare_scripts();
    }


    private function build_config($id)
    {
        $this->config['id'] = $id;

        $this->config['set_name'] = get_post_meta($this->config['id'], 'popup_modal', 1);
        $this->get_popup_acf();

    }

    private function get_popup_acf()
    {
        $popup_modals = get_field("is_global_popup_modals", "options");

        if (!$popup_modals) return;


        $temp_acf_set = false;

        foreach ($popup_modals as $c => $set):
            $test_slug = Acme::slugify($set['title']);
            if ($this->config['set_name'] != $test_slug) continue;
            $temp_acf_set = $set;
        endforeach;

        $this->config['acf'] = $temp_acf_set;

    }

    private function build_html()
    {

        Acme::dbg($this->set_acf);
        foreach ($this->set_acf as $popup_acf):

            Acme::dbg($popup_acf);
            $title = Acme::slugify($popup_acf['title']);



            $this->html .= "
                <div class='is-popup-modal' id='$title-popup-modal'>
                    <h3>{$popup_acf['title']}</h3>
                    {$popup_acf['content']}
                </div>
        
        ";

        endforeach;


    }

    private function prepare_scripts(){
        add_action( 'wp_enqueue_scripts', array($this, 'enqueue_scripts') );
    }

    public function enqueue_scripts(){
        wp_enqueue_script( 'jquery-ui-dialog' );
    }

    static function register_global_popup_options()
    {

        // creates the top level options page
        Popup::register_layout_builder_page();
        $iss_field_group_fn = Popup::build_global_popup_option_field_group();
        $iss_field_group = apply_filters("iss_update_layout_builder", $iss_field_group_fn);
        acf_add_local_field_group($iss_field_group);

    }

    static function register_layout_builder_page()
    {

        acf_add_options_sub_page([
            'page_title' => "InfiniSite Popup Modals",
            'menu_title' => "Popups",
            'menu_slug' => "is-popup-sets",
            'parent_slug' => 'is-display-settings',
            'wp_admin_page_variable' => "popup_settings",
            'capability' => 'activate_plugins',
        ]);

    }

    static function build_global_popup_option_field_group()
    {


        $gpo_title = [
            'key' => 'field_global_popup_title',
            'label' => 'Title',
            'name' => 'title',
            'type' => 'text'
        ];

        $gpo_content = [
            'key' => 'field_global_popup_content',
            'label' => 'Content',
            'name' => 'content',
            'type' => 'wysiwyg'
        ];

        $gpo_trigger_class = [
            'key' => 'fiel1d_global_popup_trigger_elements',
            'label' => 'Trigger Elements',
            'instructions' => 'jQuery Selector of elements that open this popup modal when clicked',
            'name' => 'trigger_elements',
            'type' => 'text'
        ];

        $gpo_modal_fields = [
            $gpo_title,
            $gpo_content,
            $gpo_trigger_class
        ];

        $gpo_field_set = [
            'key' => 'field_global_popup_modals_popup_group_repeater',
            'label' => 'Popup Modals',
            'name' => 'is_global_popup_modals',
            'type' => 'repeater',
            'layout' => 'block',
            'button_label' => 'Add Popup Modal',
//            'instructions' => '',
            'sub_fields' => $gpo_modal_fields
        ];

        // our builder here is a repeater with one "set"
        $gpo_fields = [
            $gpo_field_set
        ];

        $gpo_field_group = [
            'key' => 'field_global_popup_modals_field_group',
            'type' => 'flexible_content',
            'title' => 'Global Popup Options',
            'fields' => $gpo_fields,
            'location' => [
                [
                    [
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'is-popup-sets',
                    ]
                ],
            ],
        ];

        return $gpo_field_group;

    }

}