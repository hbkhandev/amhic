<?

namespace TSD_Infinisite;

class Post_Create_Form
{

    private $acf_field = [];
    private $config = [];

    private $POST = [];

    private $input_html = '';
    private $form_html = '';
    private $flash_messages = [];
    private $flash_message_html = '';


    private $has_new_post = false;

    public function __construct($config)
    {

        $this->config($config);

        $this->create_new_draft_post();
        $this->get_flash_message();
        $this->prepare_flash_messages();
        $this->build_form();

        \Kint::dump($this);

    }

    private function config($config){
        $this->config = $config;
        $this->POST = \sanitize_post($_POST);
        $this->has_new_post = !is_null($_POST['is_post_create_form']);

        $_pt = $this->config['post_type'];
        $this->acf_field = get_field("is_cpt_{$_pt}_custom_meta_fields", "options");

    }


    private function create_new_draft_post()
    {


        if (is_null($this->POST['is_post_create_form']))
            return;

        $_post = [
            'post_title' => 'User Suggested Post',
            'post_type' => $this->config['post_type'],
            'meta_input' => $this->POST['is_post_create_form']
        ];


        $new_post_id = \wp_insert_post($_post);

        $success = is_a(get_post($new_post_id), 'WP_Post');

        if($success)
            $this->flash_messages[] = ['ok' => 'new post created'];


    }


    private function get_flash_message()
    {

        if ($this->POST == []) return;

//        if($this->POST['is_post_create_form'])

        if (!$this->has_new_post)
            $this->flash_messages[] = ['ok' => 'no new post needed'];


//        $this->flash_message = 'No Message';

    }

    private function prepare_flash_messages()
    {

        if ($this->flash_messages == []) return;

        foreach ($this->flash_messages as $status_array):
            foreach ($status_array as $status => $message)
                $this->flash_message_html .= "
                    <div class='callout'>
                        $status : $message
                    </div>";
        endforeach;


    }

    private function build_form()
    {
        foreach ($this->acf_field as $acf_field):

            $db_key = $acf_field['preset_type'] == 'custom' ? $acf_field['meta_key'] : $acf_field['preset_type'];

            $_input = "
                <label for=''>{$acf_field['label']}</label>
                <input type='text' placeholder='{$acf_field['label']}' name='is_post_create_form[{$db_key}]' />
            ";

            $this->input_html .= $_input;

        endforeach;


        $html = "

            $this->flash_message_html

            <form method='post'>
            
                <input type='hidden' name='is_post_create_form'>
            
                $this->input_html
            
                <input type='submit' value='{$this->config['form_submit_value']}'>
            
            </form>
        ";


        $this->form_html = $html;

    }


    public function get_html()
    {


        return $this->form_html;

    }

}