<?php

namespace TSD_Infinisite;

class Post_Type {

    public $_pt = [];
    public $name = '';
    public $label = '';

    public function __construct($_pt)
    {

        $this->_pt = \get_post_type_object($_pt);
        $this->set_config();

    }



    private function set_config(){

        $this->name = $this->_pt->name;
        $this->label = $this->_pt->label;

    }


}