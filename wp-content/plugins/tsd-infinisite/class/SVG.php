<?

namespace TSD_Infinisite;

use SVG\SVGImage;

class SVG {

    private $acf;
    private $filepath;
    private $palette;

    private $object;
    private $document;
    private $path;

    private $path_classes = [];

    public function __construct($icon) {

        $this->palette = new Palette();

        if (is_array($icon))
            $this->build_mefya_svg_from_acf($icon);

        if (is_string($icon))
            $this->build_mefya_svg_from_filepath($icon);

        $this->document = $this->object->getDocument();
        $this->path = $this->document->getChild(0);


        $this->document->removeAttribute('width');
        $this->document->removeAttribute('height');

        return $this;


    }

    private function build_mefya_svg_from_acf($icon) {

        $this->acf = $icon;

        $_site_root = \site_url();
        $_folder_path = str_replace($_site_root, '', $this->acf['url']);

        $this->filepath = $_SERVER['DOCUMENT_ROOT'] . $_folder_path;

        $this->object = SVGImage::fromFile($this->filepath);

    }


    private function build_mefya_svg_from_filepath($icon) {

        if (!strpos($icon, '/')):
            if (file_exists(THEME_URI . "components/svg/{$icon}.svg")):
                $icon = THEME_URI . "components/svg/{$icon}.svg";
            elseif (file_exists(PARENT_URI . "components/svg/{$icon}.svg")):
                $icon = PARENT_URI . "components/svg/{$icon}.svg";
            elseif (file_exists(INFINISITE_URI . "components/svg/{$icon}.svg")):
                $icon = INFINISITE_URI . "components/svg/{$icon}.svg";
            else:
                $icon = INFINISITE_URI . "components/svg/not-found.svg";
            endif;
        endif;


        $this->filepath = $icon;
        $this->object = SVGImage::fromFile($this->filepath);
    }


    public function html() {
        // meyfa/php-svg object outputs the string when called directly

        $_class = '';

        foreach ($this->path_classes as $value)
            $_class .= "$value ";

        $this->path->setAttribute('class', $_class);

        return $this->object;
    }

    public function set_width($width) {
        $this->document->setAttribute('width', $width);
    }

    public function set_height($height) {
        $this->document->setAttribute('height', $height);
    }

    public function set_fill($color_role = 'gray') {

        $_color = $this->palette->get($color_role);

        $this->path->setStyle('fill', $_color);


    }

    public function set_stroke($color_role = 'black', $width = 1) {

        $_color = $this->palette->get($color_role);

        $this->path->setStyle('stroke', $_color);
        $this->path->setStyle('stroke-width', $width);

        $this->path->setAttribute('transform', 'scale(.95)');
        $this->path->setAttribute('transform-origin', 'center');


    }

    public function set_hover_fill($color_role = 'gray') {

        $this->path_classes[] = "{$color_role}-fill-hover";
        $this->path_classes[] = "is-svg-hover";

    }

}