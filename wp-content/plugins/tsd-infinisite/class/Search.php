<?

/**
 * this guy lives two lives. his static funtions are to set up cpt pages
 * his oop code is to serve as an api for search related functions
 */

namespace TSD_Infinisite;

class Search {

    private $swp_engines = [];

    public function __construct() {

        if (!class_exists('SWP_Query'))
            return;

        $this->set_engines();

    }


    private function set_engines() {
        /**
         * this is the function in which we harvest data from the searchwp settings
         * in order to build out our default set of reference fields
         */

        $swp_options = get_option("searchwp_settings");

        if (!$swp_options)
            return;

        foreach ($swp_options['engines'] as $name => $post_types):

            $_post_types_for_search_engine = [];

            foreach ($post_types as $_label => $_pt):

                if (is_string($_pt))
                    continue;
                if (!$_pt['enabled'])
                    continue;
                $_post_types_for_search_engine[$_label] = new Post_Type($_label);

            endforeach;

            $_label = ucwords(str_replace('_', ' ', $name));

            // if we have the word "search" on the end of the name, lets go ahead and remove from the search bar
            if (substr($_label, -6, 6) == "Search")
                $_label = substr($_label, 0, -7);

            $this->swp_engines[$name] = ['label'      => $_label,
                                         'name'       => $name,
                                         'post_types' => $_post_types_for_search_engine];

        endforeach;

    }

    public function get_engines() {

        return $this->swp_engines;

    }

    public function update_engine(string $e, $atts = []) {

        $this->swp_engines[$e] = $atts;

    }

    public static function register_option_page_acf_group() {

        $option_page_config = DB::function_cache('Search::register_option_page_acf_group');

        if ($option_page_config === false)
            $option_page_config = DB::function_cache('Search::register_option_page_acf_group', Search::build_option_page_config());

        acf_add_local_field_group($option_page_config);

    }


    public static function register_option_page_menu_link() {

        acf_add_options_sub_page(['page_title'             => "Search Config",
                                  'menu_title'             => "Search Config",
                                  'parent_slug'            => 'is-display-settings',
                                  'capability'             => 'activate_plugins',
                                  'wp_admin_page_variable' => 'search']);

    }

    private static function build_option_page_config() {

        $return = ['key'      => 'group_is_search_options',
                   'title'    => 'IS Global Search Options',
                   'style'    => 'seamless',
                   'fields'   => Search::build_option_fields(),
                   'location' => [[['param'    => 'options_page',
                                    'operator' => '==',
                                    'value'    => 'acf-options-search-config',]],],];


        return $return;

    }

    public static function build_option_fields() {


        $post_types = Acme::get_post_types();
        $kint = @\Kint::dump($post_types);

        $template_choices = Acme::get_templates('twig/post_excerpts/');

        //        print "<div style='padding: 0 0 0 180px'>$kint</div>";

        $roadmap = ['key'     => 'field_is_search_roadmap',
                    'label'   => '',
                    'name'    => 'roadmap',
                    'type'    => 'message',
                    'message' => "
                <h2>Roadmap</h2>
                <pre>
  We need:
    Default SWP Engine (swp is going to be a mu-plugin)
    Posts per page
    Posts per row
      Desktop tablet and mobile
    Default excerpt
    Default excerpt per post type
    Category based excerpt override
                </pre>
            ",];


        $default_template = ['key'     => 'field_is_search_default_excerpt_template',
                             'name'    => 'is_default_search_result_excerpt_template',
                             'label'   => 'Default Excerpt Template',
                             'type'    => 'select',
                             'ui'      => 1,
                             'choices' => $template_choices];


        $acf_fields = [$roadmap,
                       $default_template];

        $acf_fields[] = ['key'     => 'field_is_post_type_search_excerpt_post_per_row_label',
                         'label'   => '',
                         'name'    => 'post_per_row',
                         'type'    => 'message',
                         'message' => "<h2>Posts Per Row</h2>",];

        $_post_per_row_count = [1 => 1,
                                2 => 2,
                                3 => 3,
                                4 => 4,
                                5 => 5,
                                6 => 6,
                                7 => 7,
                                8 => 8,
                                9 => 9];

        $post_per_row_small = ['key'     => 'field_is_search_excerpt_post_per_row_small',
                               'name'    => 'small',
                               'label'   => 'Small',
                               'type'    => 'select',
                               'ui'      => 1,
                               'wrapper' => ['width' => 25],
                               'choices' => $_post_per_row_count];

        $post_per_row_medium = ['key'        => 'field_is_search_excerpt_post_per_row_medium',
                                'name'       => 'medium',
                                'label'      => 'Medium',
                                'type'       => 'select',
                                'ui'         => 1,
                                'allow_null' => 1,
                                'wrapper'    => ['width' => 25],
                                'choices'    => $_post_per_row_count];

        $post_per_row_large = ['key'        => 'field_is_search_excerpt_post_per_row_large',
                               'name'       => 'large',
                               'label'      => 'Large',
                               'type'       => 'select',
                               'ui'         => 1,
                               'allow_null' => 1,
                               'wrapper'    => ['width' => 25],
                               'choices'    => $_post_per_row_count];

        $post_per_row_xlarge = ['key'        => 'field_is_search_excerpt_post_per_row_xlarge',
                                'name'       => 'xlarge',
                                'label'      => 'Xlarge',
                                'type'       => 'select',
                                'ui'         => 1,
                                'allow_null' => 1,
                                'wrapper'    => ['width' => 25],
                                'choices'    => $_post_per_row_count];

        $posts_per_row_fields = [$post_per_row_small,
                                 $post_per_row_medium,
                                 $post_per_row_large,
                                 $post_per_row_xlarge,];

        $acf_fields[] = ['key'        => 'field_is_post_type_search_excerpt_post_per_row_group',
                         'label'      => 'Posts Per Row',
                         'name'       => 'is_post_type_search_post_per_row',
                         'type'       => 'group',
                         'sub_fields' => $posts_per_row_fields];

        $acf_fields[] = ['key'     => 'field_post_type_template_override',
                         'label'   => '',
                         'name'    => 'template_override',
                         'type'    => 'message',
                         'message' => "<h2>Post Type Excerpt</h2>",];

        if ($post_types):
            foreach ($post_types as $pt):


                $acf_fields[] = ["key"        => "field_is_search_{$pt['post_type']}_excerpt_template",
                                 "name"       => "is_{$pt['post_type']}_search_result_excerpt_template",
                                 "label"      => "{$pt['name']} Excerpt Template",
                                 "type"       => "select",
                                 'allow_null' => 1,
                                 "ui"         => 1,
                                 'choices'    => $template_choices,
                                 'wrapper'    => ['width' => 33]];

            endforeach;
        endif;


        return $acf_fields;

    }


}