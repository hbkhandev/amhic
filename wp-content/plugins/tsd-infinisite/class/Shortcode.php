<?

namespace TSD_Infinisite;

class Shortcode {

    public function __construct() {

		self::create('is_row_foundation_tab_header');
		
    }


    public static function create($name, $vars = []) {

        add_shortcode($name, function($atts, $content, $tag) use ($name, $vars) {

            $data = ['atts' => $atts, 'content' => $content, 'tag' => $tag];

            $filename = "components/shortcodes/{$name}.php";

            return Acme::get_file($filename, $data);
        });

    }


}