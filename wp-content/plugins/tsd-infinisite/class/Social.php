<?

namespace TSD_Infinisite;

use Facebook\Facebook;

class Social {

    function __construct() {

        add_action( 'rest_api_init', [ $this, 'add_endpoints' ] );

    }

    public static function register_global_options() {

        $social_media_field_group = self::create_global_options();

        acf_add_local_field_group( $social_media_field_group );
        self::build_api_connection_tabs();


    }


    private static function build_api_connection_tabs() {

        // $cache = DB::function_cache('Social::build_api_connection_tabs');

        // if($cache !== false) return $cache;

        if ( ! is_admin() )
            return;


        $accounts = Social::get_global_accounts();

        if ( ! $accounts )
            return;

        $account_fields = [];

        foreach ( $accounts as $c => $account ):

            $tab = ACF::get_tab( 'field_239420384' . $c, $account['username'] );

            $fields = self::create_oauth_fields( $account );

            $account_fields = array_merge( [ $tab ], $fields, $account_fields );

        endforeach;


        $api_connection_field_group = [ 'key'      => 'group_is_global_api_info',
                                        'title'    => 'Connection Info',
                                        'fields'   => $account_fields,
                                        'location' => [ [ [ 'param'    => 'options_page',
                                                            'operator' => '==',
                                                            'value'    => 'acf-options-social-media' ] ] ] ];


        acf_add_local_field_group( $api_connection_field_group );


    }


    private static function create_oauth_fields( $account, $config = [] ) {

        $name    = $account['username'];
        $network = $account['network'];


        $redirect_uri_lookup = [ 'facebook'  => site_url() . "/wp-json/is_api/v1/social_media_auth",
                                 'twitter'   => "",
                                 'instagram' => site_url() . '/wp-json/is_api/v1/instagram_code_add',
                                 'linkedin'  => "" ];


        $return = [];

        $client_id_field     = [ 'key'     => "field_{$network}_{$name}_app_client_id",
                                 'name'    => "{$network}_{$name}_client_id",
                                 'label'   => 'Client ID',
                                 'type'    => 'text',
                                 'wrapper' => [ 'width' => 50 ] ];
        $client_secret_field = [ 'key'     => "field_{$network}_{$name}_app_client_secret",
                                 'name'    => "{$network}_{$name}_client_secret",
                                 'label'   => 'Client Secret',
                                 'type'    => 'text',
                                 'wrapper' => [ 'width' => 50 ] ];
        $auth_code_field     = [ 'key'     => "field_{$network}_{$name}_app_auth_token",
                                 'name'    => "{$network}_{$name}_auth_code",
                                 'type'    => 'text',
                                 'label'   => 'Authorization Code',
                                 'wrapper' => [ 'width' => 50 ] ];
        $access_token_field  = [ 'key'     => "field_{$network}_{$name}_app_access_token",
                                 'name'    => "{$network}_{$name}_access_token",
                                 'type'    => 'text',
                                 'label'   => 'Access Token',
                                 'wrapper' => [ 'width' => 50 ] ];


        $client_id     = get_option( "options_{$network}_{$name}_client_id" );
        $client_secret = get_option( "options_{$network}_{$name}_client_secret" );
        // TODO: we need a network url lookup here
        $redirect_uri = $redirect_uri_lookup[ $network ];
        $auth_code    = get_option( "options_{$network}_{$name}_auth_code" );
        $access_token = get_option( "options_{$network}_{$name}_access_token" );

        $auth_link_ready = $client_id && $client_secret && $redirect_uri;

        $state = json_encode( [ 'st'       => 'YourStateHere',
                                'network'  => $network,
                                'username' => $name, ] ); // Pass the current network and username, so JSOI API 'social_media_auth' would know which access code/token is for.  // 'test_state_value';


        $auth_link_url_lookup = [ //https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow/
                                  'facebook'  => "https://www.facebook.com/v4.0/dialog/oauth?client_id={$client_id}&redirect_uri={$redirect_uri}&state={$state}",
                                  'twitter'   => "https://api.twitter.com/oauth/authorize/",
                                  'instagram' => "https://api.instagram.com/oauth/authorize/?client_id={$client_id}&redirect_uri={$redirect_uri}&response_type=code",
                                  //https://docs.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow?context=linkedin/context
                                  'linkedin'  => "https://www.linkedin.com/oauth/v2/authorization" ];

        // TODO: we need a network url lookup here
        $auth_link_url = $auth_link_url_lookup[ $network ];

        $auth_html = "
            <p><a href='$auth_link_url' target='_blank'
                onclick=\"window.open('$auth_link_url', 
                                     'newwindow', 
                                     'width=345,height=465,top=250,left=650'); 
                          return false;\"
            >Click here to get Auth Code!</a></p>
        ";

        $more_info_needed_html = "
        
            <p>We need more information, please fill out the fields below and then update the page. <a href=\"#\" onClick=\"MyWindow=window.open('$auth_link_url','MyWindow', 'width=800,height=600' ); return false;\">Click Here</a></p>
        
        ";

        $redirect_uri_notice = "<p>In order to work properly, your {$network} app needs to have a redirect uri of<br />$redirect_uri</p>";

        $message_html = $auth_link_ready ? $auth_html : $more_info_needed_html;

        $message_html = $redirect_uri_notice . $message_html;

        if ( $auth_code ):
            // we have our code, we're ready to request the token

            $data = [ 'client_id'     => $client_id,
                      'client_secret' => $client_secret,
                      'grant_type'    => 'authorization_code',
                      'redirect_uri'  => $redirect_uri,
                      'code'          => $auth_code, ];
            // TODO: we need a network url lookup here
            $url     = 'https://api.instagram.com/oauth/access_token';
            $request = Account::curl( $url, $data );

            if ( isset( $request->access_token ) )
                $get_access_token_html = "<p>Here's your token, put it below: $request->access_token</p>";


            if ( isset( $request->code ) ):
                if ( $request->code === 400 )
                    $get_access_token_html = $request->error_message;
            endif;

            $message_html = $get_access_token_html;
        endif;
        $access_token_received_html = "
        
            <p>Looks like you're good to go! To refresh, delete the code and access token values and start the auth process over.</p>
        
        ";

        if ( $access_token )
            $message_html = $access_token_received_html;


        $auth_link = [ 'key'     => "field_{$network}_auth_link",
                       'name'    => "{$network}_auth_link",
                       'label'   => 'Authorize',
                       'type'    => 'message',
                       'message' => $message_html ];

        $return[] = $auth_link;
        $return[] = $client_id_field;
        $return[] = $client_secret_field;
        $return[] = $auth_code_field;
        $return[] = $access_token_field;

        return $return;

    }

    public static function create_global_options() {

        $smg_username = [ 'key'   => 'field_5a29673e15429',
                          'label' => 'Username',
                          'name'  => 'username',
                          'type'  => 'text' ];

        $smg_network = [ 'key'     => 'field_5a29673e15462',
                         'label'   => 'Network',
                         'name'    => 'network',
                         'type'    => 'select',
                         'ui'      => 1,
                         'choices' => [ 'facebook'  => "Facebook",
                                        'twitter'   => "Twitter",
                                        'instagram' => "Instagram",
                                        'linkedin'  => "Linkedin",
                                        'youtube'   => "Youtube",
                                        'pinterest' => "Pinterest",
                                        'email'     => "Email",
                                        'phone'     => "Phone", ] ];

        $smg_url = [ 'key'   => 'field_5a29673e154d3',
                     'label' => 'Url',
                     'name'  => 'url',
                     'type'  => 'url', ];

        $social_media_group_fields = [ $smg_username,
                                       $smg_network,
                                       $smg_url ];

        $social_media_repeater = [ 'key'        => 'field_5a29673e15623',
                                   'name'       => 'social_media_accounts',
                                   'type'       => 'repeater',
                                   'layout'     => 'table',
                                   'sub_fields' => $social_media_group_fields ];


        $social_media_fields = [ $social_media_repeater ];


        $social_media_field_group = [ 'key'      => "field_5a147275e53df",
                                      'title'    => "Accounts",
                                      'location' => [ [ [ 'param'    => 'options_page',
                                                          'operator' => '==',
                                                          'value'    => 'acf-options-social-media', ] ], ],
                                      'fields'   => $social_media_fields ];

        return $social_media_field_group;

    }

    public static function get_global_accounts() {

        $networks = ACF_Helper::get_repeater( "social_media_accounts" );

        return $networks;

    }


    public static function get_account_info_by_username( $username = false ) {
        if ( ! $username )
            return 'username required';

        $networks = Social::get_global_accounts();

        foreach ( $networks as $n )
            if ( $n['username'] == $username )
                return $n;

    }


    static function get_social_media_networks_for_header_and_footer() {

        $return = [];

        $sm_networks = Social::get_global_accounts();

        if ( $sm_networks ) {
            foreach ( $sm_networks as $network ) {
                $return[ $network['username'] ] = "{$network['username']} ({$network['network']})";
            }
        }

        return $return;


    }


    static function prep_social_media_fields_for_templating( $networks = [] ) {

        if ( $networks === null )
            return false;

        /**
         * this function bridges the gap between the acf select menu on the header
         * and footer options page with the social media definitions on the social
         * media account setup page. the issue is that while the field in the header
         * footer select menu is populated based on the information entered in the
         * social media accounts - but once it is set, it's not tied to it. so, if
         * a value on the social media page were to be updated or deleted, the
         * value stored in the header and footer won't update. so, we make the index
         * record the username - which is of course not a perfect solution.
         *
         * TODO: if there is more than one username, check the network as a backup
         *
         * so anyways - we use this function to grab the information from the database
         * and attach it to the information that we pass to the header and footer social
         * media template rendering functions
         */

        $updated_networks['template'] = $networks['template'];


        if ( ! $networks['select'] ) {
            return $networks;
        }

        foreach ( $networks['select'] as $username ) {
            $updated_networks['select'][ $username ] = Social::get_account_info_by_username( $username );
        }

        return $updated_networks;

    }

}
