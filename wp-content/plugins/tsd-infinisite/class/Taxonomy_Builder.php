<?

namespace TSD_Infinisite;

class Taxonomy_Builder {

	static function register_taxonomy_builder() {

		$taxonomy_builder_field_group = self::build_taxonomy_builder_field_group();

		// do the filter thing

		// register fields with acf
		acf_add_local_field_group( $taxonomy_builder_field_group );
	}

	private static function build_taxonomy_builder_field_group() {

		$taxonomy_repeater_field_name       = [
			'key'      => 'field_is_taxonomies_repeater_name',
			'label'    => 'Name',
			'type'     => 'text',
			'name'     => 'name',
			'required' => 1,
			'wrapper'  => [
				'width' => 33
			]
		];
		$taxonomy_repeater_field_term       = [
			'key'      => 'field_is_taxonomies_repeater_term',
			'label'    => 'Term',
			'type'     => 'text',
			'name'     => 'term',
			'required' => 1,
			'wrapper'  => [
				'width' => 33
			]
		];
		$taxonomy_repeater_field_post_types = [
			'key'      => 'field_is_taxonomies_repeater_post_types',
			'label'    => 'Post Types',
			'type'     => 'select',
			'ui'       => 1,
			'multiple' => 1,
			'name'     => 'post_types',
			'choices'  => Page_Builder::get_post_types_for_page_builder_field(),
			'required' => 1,
			'wrapper'  => [
				'width' => 33
			]
		];


		$taxonomy_repeater_fields = [
			$taxonomy_repeater_field_name,
			$taxonomy_repeater_field_term,
			$taxonomy_repeater_field_post_types,
		];


		$taxonomy_builder_fields = [
			[
				'key'          => 'field_is_taxonomies_repeater',
				'label'        => 'Custom Taxonomies',
				'name'         => 'custom_taxonomies',
				'type'         => 'repeater',
				'button_label' => 'Add Taxonomy',
				'layout'       => 'block',
				'sub_fields'   => $taxonomy_repeater_fields
			]
		];

		$field_group = [
			'key'      => 'field_is_taxonomy_builder_group',
			'title'    => 'Taxonomy Builder',
			'location' => [
				[
					[
						'param'    => 'options_page',
						'operator' => '==',
						'value'    => 'is-tax-options',
					]
				],
			],
			'fields'   => $taxonomy_builder_fields
		];

		return $field_group;

	}

	static function register_built_taxonomies() {

		/**
		 * This function reads the taxonomy fields we have set in
		 * the acf options page and then builds them into the taxonomy
		 * terms, as well as calls the function that sets up the
		 * acf options page for each given taxonomy
		 */

		$taxonomies = ACF_Helper::get_repeater('custom_taxonomies');


		if ( ! $taxonomies ) {
			return;
		}

		foreach ( $taxonomies as $taxonomy ):

			$tax_args = [
				'label'              => $taxonomy['name'],
				'hierarchical'       => get_field( "{$taxonomy['term']}_hierarchical", 'options' ),
				'show_admin_column'  => get_field( "{$taxonomy['term']}_admin_column", 'options' ),
				'show_in_quick_edit' => get_field( "{$taxonomy['term']}_quick_edit", 'options' ),
				'show_in_rest' => get_field( "{$taxonomy['term']}_rest_api", 'options' ),
			];

			\register_taxonomy(
				$taxonomy['term'],
				$taxonomy['post_types'],
				$tax_args
			);
		endforeach;

		Taxonomy_Builder::build_taxonomies_acf_options( $taxonomies );

	}

	private static function build_taxonomies_acf_options( $taxonomies = [] ) {

		/**
		 *
		 * This function will build out the acf fields that we are going to
		 * use as the options panels.
		 *
		 * @param $taxonomies       list of taxonomy terms from prior function
		 *
		 */


		if ( ! count( $taxonomies ) ) {
			return;
		}


		foreach ( $taxonomies as $tax ):



			$tax_options_text = [
				'key'   => 'field_293847928374982374',
				'name'  => 'test',
				'label' => $tax['name'],
				'type'  => 'text',
			];

			$field_group_fields = [
				$tax_options_text
			];

			$field_group_arguments = [
				'key'                   => "field_is_taxonomy_options_{$tax['term']}",
				'title'                 => 'Term Options',
				'location'              => [
					[
						[
							'param'    => 'term',
							'operator' => '==',
							'value'    => $tax['term'],
						]
					]
				],
				'position'              => 'normal',
				'style'                 => 'default',
				'label_placement'       => 'top',
				'instruction_placement' => 'label',
				'active'                => 1,
				'fields'                => $field_group_fields
			];

			acf_add_local_field_group( $field_group_arguments );

		endforeach;


	}

}
