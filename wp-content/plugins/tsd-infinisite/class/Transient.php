<?

namespace TSD_Infinisite;


class Transient {

    public $prefixes = ['is_acf_post', 'isacf', 'isfn'];


    public static function get_all() {

        global $wpdb;

        $t = new Transient();

        foreach ($t->prefixes as $prefix):

            print "<h2>$prefix</h2>";

            $items = $wpdb->get_results("select * from wp_options where option_name like '_transient_{$prefix}%'");

            $op = '';

            foreach ($items as $item)
                $op .= Acme::get_file("components/debug/transient/row.php", ['item' => $item]);

            print "<div class='grid-x grid-padding-x small-up-4 align-stretch'>$op</div>";


        endforeach;

    }

}