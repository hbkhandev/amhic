<?

/*
 * this file is not meant to be used outside of the wpbakery plugin env
 */


namespace TSD_Infinisite;

if ( class_exists( 'WPB_CORE_HELPER' ) )
    return;

if ( ! INFINISITE_URI )
    define( "INFINISITE_URI", plugin_dir_path( __FILE__ ) );
if ( ! THEME_URI )
    define( "THEME_URI", get_theme_file_path() . '/' );
if ( ! PARENT_URI )
    define( "PARENT_URI", get_template_directory_uri() . '/' );


class WPB_Core_Helper {

    public static function get_excerpt_templates( $dir ) {
        return self::get_files( $dir );
    }

    public static function get_files( $directory ) {


        $return = [];

        if ( is_string( $directory ) ) {
            $directory = [ $directory, $directory, $directory ];
        }

        foreach ( $directory as $filepath ) {
            if ( substr( $filepath, 0, 1 ) == '/' ) {
                $filepath = substr( $filepath, 1 );
            }
        }

        $use_slash = substr( $directory[0], - 1, 1 ) == '/';
        $url_slash = $use_slash ? '' : '/';


        $plugin_php_templates_filepath = INFINISITE_URI . $directory[0] . $url_slash . "*";
        $plugin_php_templates          = glob( $plugin_php_templates_filepath );

        $theme_php_templates_filepath = THEME_URI . $directory[1] . $url_slash . "*";
        $theme_php_templates          = glob( $theme_php_templates_filepath );

        $parent_php_templates_filepath = PARENT_URI . $directory[2] . $url_slash . "*";
        $parent_php_templates          = glob( $parent_php_templates_filepath );

        $templates = array_merge( $plugin_php_templates, $theme_php_templates, $parent_php_templates );


        // attaching the templates to the submenu template module selector

        if ( ! $templates ) {
            return $return;
        }

        $plugin_subdirs = array_filter( glob( $plugin_php_templates_filepath ), 'is_dir' );
        $theme_subdirs  = array_filter( glob( $theme_php_templates_filepath ), 'is_dir' );
        $parent_subdirs = array_filter( glob( $parent_php_templates_filepath ), 'is_dir' );

        $subdir_filepaths = array_merge( $plugin_subdirs, $theme_subdirs, $parent_subdirs );

        foreach ( $subdir_filepaths as $subdir_filepath ):

            $val   = explode( '/', $subdir_filepath );
            $label = array_pop( $val );

            $files        = glob( $subdir_filepath . '/*' );
            $subdir_files = [];

            foreach ( $files as $file ):

                $pos           = strpos( $file, '/wp-content' );
                $template_path = substr( $file, $pos );

                $val                            = explode( '/', $file );
                $file_label                     = array_pop( $val );
                $subdir_files[ $template_path ] = self::underscore( $file_label );
            endforeach;

            $return[ $label ] = $subdir_files;

        endforeach;


        foreach ( $templates as $template ):

            $pos           = strpos( $template, '/wp-content' );
            $template_path = substr( $template, $pos );


            $info                     = explode( "/", $template );
            $filename                 = array_pop( $info );
            $return[ $template_path ] = self::underscore( $filename );

        endforeach;


        return $return;
    }

    static function underscore( $str, array $noStrip = [] ) {
        // non-alpha and non-numeric characters become spaces
        $str = preg_replace( '/[^a-z0-9' . implode( "", $noStrip ) . ']+/i', ' ', $str );
        $str = trim( $str );
        $str = str_replace( " ", "_", $str );
        $str = strtolower( $str );

        return $str;
    }

    static function template_filename_encoder( $item = '', $user_config = [] ) {

        $defaults = [ 'encode' => true ];

        if ( $user_config === false )
            $user_config = [ 'encode' => false ];

        $config = \wp_parse_args( $user_config, $defaults );

        $to_fix = [ 'tsdistemplatedot'        => '.',
                    'tsdistemplatespace'      => ' ',
                    'tsdistemplatedash'       => '-',
                    'tsdistemplateunderscore' => '_',
                    'tsdistemplateslash'      => '/', ];

        if ( $config['encode'] == true )
            foreach ( $to_fix as $find => $replace )
                $item = str_replace( $find, $replace, $item );

        if ( ! $config['encode'] )
            foreach ( $to_fix as $find => $replace )
                $item = str_replace( $replace, $find, $item );

        return $item;

    }

}
