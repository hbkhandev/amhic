<?
namespace TSD_Infinisite;


class WPBakeryShortCode_is_wpb_post_collection extends \WPBakeryShortCodesContainer {
}

class WPBakeryShortCode_is_wpb_hero extends \WPBakeryShortCodesContainer {
}

use Facebook\Facebook;

class WPB_Modules {


    public static function create_modules() {

        add_action( "vc_after_init", function() {

            vc_add_shortcode_param( 'post_selector', function( $settings, $value ) {
                $vars     = [ 'settings' => $settings, 'value' => $value ];
                $filepath = '/components/wpb/modules/post_selector_draggable.php';

                return Acme::get_file( $filepath, $vars );
            }, INFINISITE_JS . '/wpb_post_selector.js' );

            vc_add_shortcode_param( 'is_post_per_row', function( $settings, $value ) {
                $vars     = [ 'settings' => $settings, 'value' => $value ];
                $filepath = '/components/wpb/modules/is_post_per_row.php';

                return Acme::get_file( $filepath, $vars );
            }, INFINISITE_JS . '/wpb_post_per_row.js' );

            vc_add_shortcode_param( 'is_padding_and_margin', function( $settings, $value ) {
                $vars     = [ 'settings' => $settings, 'value' => $value ];
                $filepath = '/components/wpb/modules/is_padding_and_margin.php';

                return Acme::get_file( $filepath, $vars );
            }, INFINISITE_JS . '/wpb_padding_and_margin.js' );


            self::create_custom_fields();

            self::create_hero_module();
            self::create_query_display_module();
            self::create_cover_image_module();
            self::create_modal_display_module();

            self::update_vc_post_grid();
            self::init_shortcode_api();

        } );


    }

    private static function create_custom_fields() {
        \vc_add_shortcode_param( 'tsd_wpb_posts_per_row', [ new WPB_Modules(), 'tsd_wpb_posts_per_row_display_fn' ] );

    }


    private static function check( $size = '', $atts = [] ) {

        if ( array_key_exists( $size, $atts ) )
            return $atts[ $size ];

    }

    private static function posts_per_row_convert( $atts ) {


        $_p = [ self::check( 'posts_per_row_mobile', $atts ),
                self::check( 'posts_per_row_tablet', $atts ),
                self::check( 'posts_per_row_desktop', $atts ) ];


        $op = "small-up-{$_p[0]} medium-up-{$_p[1]} large-up-{$_p[2]}";
        if ( array_key_exists( 'posts_per_row_large_desktop', $atts ) ) {
            $_p[] = $atts['posts_per_row_large_desktop'];
            $op   .= " xlarge-up-{$_p[3]}";
        };

        return $op;

    }

    public static function tsd_post_archive_shortcode_fn( $atts, $content = null ) {

        $loop     = $atts['loop'];
        $template = $atts['template'];
        $query    = WPB_Acme::convert_loop_shortcode_to_query_object( $loop );

        $ppr  = self::posts_per_row_convert( $atts );
        $html = '';

        if ( strpos( $loop, 'size:1' ) ):
            $post = new IS_Post( $query->posts[0] );

            return $post->render_php_template( $template );
        endif;


        foreach ( $query->posts as $post ):

            $post           = new IS_Post( $post );
            $excerpt_render = $post->render_php_template( $template );

            $html .= "<div class='cell'>$excerpt_render</div>";
        endforeach;

        $spacing = '';

        if ( array_key_exists( 'spacing', $atts ) )
            $spacing = str_replace( ',', ' ', $atts['spacing'] );

        $op = "<div class='grid-x grid-padding-x $ppr $spacing'> $html</div>";

        return $op;

    }

    public static function create_query_display_module() {


        $templates = WPB_Acme::get_templates( 'twig/query_excerpts' );


        \vc_map( [ "name"     => "Query Display",
                   "base"     => "is_wpb_query_display",
                   "category" => "InfiniSite Modules",
                   "params"   => [ [ "type"        => "dropdown",
                                     'value'       => $templates,
                                     "heading"     => "Template",
                                     "param_name"  => "template",
                                     "description" => "Choose template to display" ],
                                   [ "type"        => "loop",
                                     "heading"     => "Posts",
                                     "param_name"  => "wpb_is_post_archive",
                                     "description" => "Select your posts to display" ] ] ] );


        add_shortcode( "is_wpb_query_display", [ new WPB_Modules(), 'tsd_query_archive_shortcode_fn' ] );

    }

    public static function create_modal_display_module() {


        $button_templates = WPB_Acme::get_templates( 'components/modal/button' );
        $modal_templates  = WPB_Acme::get_templates( 'components/modal/content' );

        $button_colors = [ 'default' => 'default' ];
        $colors        = [ 'primary', 'secondary', 'tertiary', 'success', 'warning', 'error', 'gray' ];
        $shades        = [ 'xxdark', 'xdark', 'dark', 'light', 'xlight', 'xxlight' ];

        foreach ( $colors as $color ):
            $button_colors["{$color}"] = $color;
            foreach ( $shades as $shade )
                $button_colors["{$color} {$shade}"] = "{$color}_{$shade}";
        endforeach;

        \vc_map( [ "name"     => "Modal",
                   "base"     => "is_wpb_modal_display",
                   "category" => "InfiniSite Modules",
                   "params"   => [ [ "type"             => "dropdown",
                                     'value'            => $button_templates,
                                     "heading"          => "Button Template",
                                     "param_name"       => "button_template",
                                     "edit_field_class" => "vc_col-xs-4",
                                     "description"      => "Button template" ],
                                   [ "type"             => "dropdown",
                                     'value'            => $modal_templates,
                                     "heading"          => "Content Template",
                                     "param_name"       => "content_template",
                                     "edit_field_class" => "vc_col-xs-4",
                                     "description"      => "Button template" ],
                                   [ "type"             => "dropdown",
                                     'value'            => $button_colors,
                                     "heading"          => "Button Color",
                                     "param_name"       => "button_color",
                                     "edit_field_class" => "vc_col-xs-2", ],
                                   [ "type"             => "colorpicker",
                                     "value"            => "",
                                     "heading"          => "Manual Button Color",
                                     "param_name"       => "manual_button_color",
                                     "edit_field_class" => "vc_col-xs-2", ],
                                   [ "type"             => "textfield",
                                     "heading"          => "Button Text",
                                     "param_name"       => "button_text",
                                     "description"      => "What should the button say?",
                                     "edit_field_class" => "vc_col-xs-4", ],
                                   [ "type"             => "textarea_html",
                                     "heading"          => "Modal Content",
                                     "param_name"       => "content",
                                     "description"      => "What should the modal say?",
                                     "edit_field_class" => "vc_col-xs-8", ] ] ] );


        add_shortcode( "is_wpb_modal_display", [ new WPB_Modules(), 'tsd_modal_display_shortcode_fn' ] );

    }

    public static function tsd_modal_display_shortcode_fn( $atts, $content = null ) {

        extract( $atts );

        if ( ! isset( $button_color ) )
            $button_color = 'primary';

        if ( ! isset( $manual_button_color ) )
            $manual_button_color = false;

        $button_html  = Acme::get_file( $button_template, [ 'text'                => $button_text,
                                                            'button_color'        => $button_color,
                                                            'manual_button_color' => $manual_button_color ] );
        $content_html = Acme::get_file( $content_template, [ 'content' => $content ] );


        $id = uniqid( 'wpb_reveal_module_' );

        $return = "
                <span data-open='$id' class='wpb-module-button'>
                    $button_html
                </span>
                <div class='reveal wpb-module-reveal' id='$id' data-reveal data-editor-style>
                    $content_html
                </div>
        ";

        return $return;
    }


    public static function tsd_query_archive_shortcode_fn( $atts, $content = null ) {

        $query = WPB_Acme::convert_loop_shortcode_to_query_object( $atts['wpb_is_post_archive'] );
        $html  = Acme::get_file( $atts['template'], [ 'query' => $query ] );

        return $html;

    }

    public static function create_cover_image_module() {


        $templates = WPB_Acme::get_templates( 'twig/cover_image' );

        \vc_map( [ "name"     => "Cover Image",
                   "base"     => "is_wpb_cover_image",
                   "category" => "InfiniSite Modules",
                   "params"   => [ [ "type"        => "attach_image",
                                     "heading"     => "Image",
                                     "param_name"  => "image",
                                     "description" => "Image used for backgound" ],
                                   [ "type"        => "dropdown",
                                     'value'       => $templates,
                                     "heading"     => "Template",
                                     "param_name"  => "template",
                                     "description" => "Choose template to display" ],
                                   [ "type"        => "textarea_html",
                                     "heading"     => "Content",
                                     "param_name"  => "content",
                                     "description" => "Enter Content" ] ] ] );


        add_shortcode( "is_wpb_cover_image", [ new WPB_Modules(), 'tsd_cover_image_shortcode_fn' ] );

    }

    private static function process_shortcode_vars( $atts, $content = null ) {


        $vars = [];

        foreach ( $atts as $key => $value ):

            switch ( $key ):

                case 'image':

                    $vars[ $key ] = Acme::get_image_info_by_id( $value );

                    break;

                default:
                    $vars[ $key ] = $value;
                    break;
            endswitch;
        endforeach;

        if ( $content )
            $vars['content'] = $content;

        return $vars;


    }


    public static function tsd_cover_image_shortcode_fn( $atts, $content = null ) {

        $vars = self::process_shortcode_vars( $atts, $content );

        $html = Acme::get_file( $atts['template'], $vars );

        return $html;

    }

    public static function create_hero_module() {


        \vc_map( [ "name"           => "Hero",
                   "base"           => "is_wpb_hero",
                   "category"       => "InfiniSite Modules",
                   "is_container"   => true,
                   "js_view"        => 'VcColumnView',
                   'php_class_name' => '\TSD_Infinisite\WPBakeryShortCode_is_wpb_hero',
                   "as_parent"      => [ 'only' => 'is_wpb_hero_content,is_wpb_hero_loop,is_wpb_hero_image,is_wpb_hero_gallery,is_wpb_hero_title' ],
                   "params"         => [ [ "type"        => "dropdown",
                                           'value'       => WPB_Acme::get_templates( 'module_templates/hero' ),
                                           "heading"     => "Template",
                                           "param_name"  => "template",
                                           "description" => "Choose template to display" ] ] ] );


        \vc_map( [ "name"             => "Title",
                   "base"             => "is_wpb_hero_title",
                   'as_child'         => [ 'only' => 'is_wpb_hero' ],
                   'js_view'          => 'VcISCustomView',
                   'html_template'    => dirname( __FILE__ ) . '/vc_templates/test_element.php',
                   'admin_enqueue_js' => INFINISITE_DIR . '/assets/js/wpb_custom_markup.js',
                   'custom_markup'    => Acme::get_file( 'components/wpb/admin_shortcode_markup/hero/title.php' ),
                   "params"           => [ [ "type"        => "textfield",
                                             "heading"     => "Purpose",
                                             "param_name"  => "purpose",
                                             "description" => "What is this element used for in the hero?" ],
                                           [ "type"        => "textfield",
                                             "heading"     => "Title",
                                             "param_name"  => "title",
                                             "description" => "Enter Content For Use" ] ] ] );


        \vc_map( [ "name"             => "Content",
                   "base"             => "is_wpb_hero_content",
                   'as_child'         => [ 'only' => 'is_wpb_hero' ],
                   'js_view'          => 'VcISCustomView',
                   'admin_enqueue_js' => INFINISITE_DIR . '/assets/js/wpb_custom_markup.js',
                   'custom_markup'    => Acme::get_file( 'components/wpb/admin_shortcode_markup/hero/content.php' ),
                   "params"           => [ [ "type"        => "textfield",
                                             "heading"     => "Purpose",
                                             "param_name"  => "purpose",
                                             "description" => "What is this element used for in the hero?" ],
                                           [ "type"        => "textarea_html",
                                             "heading"     => "Content",
                                             "param_name"  => "content",
                                             "description" => "Enter Content For Use" ] ] ] );


        \vc_map( [ "name"   => "Image",
                   "base"   => "is_wpb_hero_image",
                   "params" => [ [ "type"        => "textfield",
                                   "heading"     => "Purpose",
                                   "param_name"  => "purpose",
                                   "description" => "What is this element used for in the hero?" ],
                                 [ "type"        => "attach_image",
                                   "heading"     => "Image",
                                   "param_name"  => "image",
                                   "description" => "Enter Image For Use" ] ] ] );


        \vc_map( [ "name"   => "Gallery",
                   "base"   => "is_wpb_hero_gallery",
                   "params" => [ [ "type"        => "textfield",
                                   "heading"     => "Purpose",
                                   "param_name"  => "purpose",
                                   "description" => "What is this element used for in the hero?" ],
                                 [ "type"        => "attach_images",
                                   "heading"     => "Gallery",
                                   "param_name"  => "gallery",
                                   "description" => "Enter Images For Use" ] ] ] );


        \vc_map( [ "name"   => "Featured Posts",
                   "base"   => "is_wpb_hero_loop",
                   "params" => [ [ "type"        => "textfield",
                                   "heading"     => "Purpose",
                                   "param_name"  => "purpose",
                                   "description" => "What is this element used for in the hero?" ],
                                 [ "type"        => "loop",
                                   "heading"     => "Posts",
                                   'param_name'  => 'loop',
                                   "description" => "Select Featured Posts Here" ] ] ] );


        add_shortcode( "is_wpb_hero", [ new WPB_Modules(), 'tsd_hero_shortcode_fn' ] );

        add_shortcode( "is_wpb_hero_title", [ new WPB_Modules(), 'tsd_hero_title_shortcode_fn' ] );
        add_shortcode( "is_wpb_hero_gallery", [ new WPB_Modules(), 'tsd_hero_gallery_shortcode_fn' ] );
        add_shortcode( "is_wpb_hero_content", [ new WPB_Modules(), 'tsd_hero_content_shortcode_fn' ] );
        add_shortcode( "is_wpb_hero_image", [ new WPB_Modules(), 'tsd_hero_image_shortcode_fn' ] );
        add_shortcode( "is_wpb_hero_loop", [ new WPB_Modules(), 'tsd_hero_loop_shortcode_fn' ] );

    }

    private static function eval_nested_module_inline_editor( $content ) {
        $t = $content;

        $dom     = new \DOMDocument();
        $content = $dom->loadHTML( do_shortcode( $content ) );

        //        \Kint::dump( $dom );
        $classname = 'vc_element';
        $query     = "//*[@class='$classname']";
        $xpath     = new \DOMXPath( $dom );
        $results   = $xpath->query( $query );

        $data_for_template = [];

        for ( $i = 0; $i < $results->length; $i ++ ):
            $item                = $results->item( $i )->nodeValue;
            $formatted_content   = "[" . substr( $item, 0, - 1 ) . "]";
            $data                = json_decode( $formatted_content );
            $data_for_template[] = $data;
            $html_for_output     = self::inline_editor_nested_content_eval( $data );
            $newdom              = new \DOMDocument();
            @$newdom->loadHTML( $html_for_output );
            $new_elem      = $newdom->documentElement;
            $imported_elem = $dom->importNode( $new_elem, 1 );

            $results->item( $i )->nodeValue = '';
            $results->item( $i )->insertBefore( $imported_elem );
        endfor;

        $dbg = @\Kint::dump( $t );

        return "<div class='my-5'>{$dom->saveHTML()}</div>";
    }

    private static function inline_editor_nested_content_eval( $data ) {
        $return = '';
        $elems  = [];

        foreach ( $data as $d ):

            $group   = [];
            $group[] = "<p>$d->type</p>";

            if ( property_exists( $d, 'title' ) )
                $group[] = "<h2>$d->title</h2>";

            if ( property_exists( $d, 'content' ) )
                $group[] = "<p>$d->content</p>";

            if ( property_exists( $d, 'purpose' ) )
                $group[] = "<p>purpose: $d->purpose</p>";

            if ( property_exists( $d, 'image' ) )
                $group[] = "<img src='{$d->image->sizes->medium}' />";

            $elems[] = $group;

        endforeach;

        foreach ( $elems as $group ):
            $row_content = '';
            foreach ( $group as $item )
                $row_content .= "<div class='cell shrink'>$item</div>";
            $return .= "<div class='grid-x grid-padding-x grid-padding-y align-bottom pb-2'>$row_content</div>";

        endforeach;

        return $return;
    }

    public static function tsd_hero_shortcode_fn( $atts, $content = null, $tag = '' ) {

        // return 'disabled';

        if ( vc_is_inline() )
            return self::eval_nested_module_inline_editor( $content );

        // our content block is only going to be made up of components
        // that know to return json formatted content appended by
        // a comma - that will set us up for our json decode, so we
        // can pass our variables into templates.

        $pre_html = do_shortcode( $content );

        // we hit an issue - you can't json decode multiple values in a single string
        // so, we put a comma at the end of all our return values, and wrap that in
        // the square brackets - it won't evaluate with a trailing comma though, so
        // we chop that off

        $formatted_shortcode_return = "[" . substr( $pre_html, 0, - 1 ) . "]";
        $data_from_shortcodes       = json_decode( $formatted_shortcode_return );

        $data_for_template = [];

        if ( $data_from_shortcodes )
            foreach ( $data_from_shortcodes as $data )
                $data_for_template[ $data->type ][] = $data;

        if ( $atts === '' )
            return 'No hero template file set';

        if ( ! array_key_exists( 'template', $atts ) )
            return 'No hero template file set';

        $html = Acme::get_file( $atts['template'], [ 'module' => $data_for_template, 'content' => $content ] );


        return $html;
    }


    public static function tsd_hero_content_shortcode_fn( $atts, $content = null ) {

        // this function serves as a midpoint between the nested module
        // and the template file - we compile an object from the module
        // data that contains all the data need by the template

        $return_object = [ 'type'    => 'content',
                           'content' => $content ];

        return wp_json_encode( $return_object ) . ',';

    }

    public static function tsd_hero_image_shortcode_fn( $atts, $content = null ) {

        // this function serves as a midpoint between the nested module
        // and the template file - we compile an object from the module
        // data that contains all the data need by the template

        $img = Acme::get_image_info_by_id( $atts['image'] );

        $return_object = [ 'type'  => 'image',
                           'image' => $img ];

        return wp_json_encode( $return_object ) . ',';

    }

    public static function tsd_hero_gallery_shortcode_fn( $atts, $content = null ) {

        // this function serves as a midpoint between the nested module
        // and the template file - we compile an object from the module
        // data that contains all the data need by the template

        $ids     = explode( ',', $atts['gallery'] );
        $gallery = [];

        foreach ( $ids as $id )
            $gallery[] = Acme::get_image_info_by_id( $id );

        $return_object = [ 'type'    => 'gallery',
                           'gallery' => $gallery ];

        return wp_json_encode( $return_object ) . ',';


    }

    public static function tsd_hero_title_shortcode_fn( $atts, $content = null ) {

        // this function serves as a midpoint between the nested module
        // and the template file - we compile an object from the module
        // data that contains all the data need by the template

        $return_object = [ 'type' => 'title' ];

        if ( $atts && array_key_exists( "title", $atts ) )
            $return_object['title'] = $atts['title'];

        return wp_json_encode( $return_object ) . ',';


    }

    public static function tsd_hero_loop_shortcode_fn( $atts, $content = null ) {

        // this function serves as a midpoint between the nested module
        // and the template file - we compile an object from the module
        // data that contains all the data need by the template

        $query = WPB_Acme::convert_loop_shortcode_to_query_object( $atts['loop'] );

        $return_object = [ 'type'  => 'query',
                           'query' => $query,
                           'posts' => $query->posts ];

        return wp_json_encode( $return_object ) . ',';

    }



    public static function tsd_hero_post_collection_fn( $atts, $content = null ) {

        if ( ! array_key_exists( 'template', $atts ) )
            return 'Please set a template in the page editor';

        ob_start();

        $pam    = explode( ",", $atts['padding_and_margin'] );
        $pam_op = '';

        foreach ( $pam as $c => $bool ):

            if ( $bool === 'false' )
                continue;

            $type = $c <= 1 ? 'padding' : 'margin';
            $dir  = $c % 2 === 0 ? 'x' : 'y';

            $pam_op .= "grid-{$type}-{$dir} ";

        endforeach;

        $ppr    = explode( ",", $atts['post_per_row'] );
        $b      = [ 'small-up-', 'medium-up-', 'large-up-', 'xlarge-up-' ];
        $ppr_op = '';

        foreach ( $ppr as $c => $pr ):
            if ( $c === 0 && ! $pr )
                $pr = 1;
            if ( ! $pr )
                continue;
            $ppr_op .= "{$b[$c]}{$pr} ";
        endforeach;


        $p  = $atts['posts'];
        $p  = substr( $p, 0, - 1 );
        $p  = str_replace( "``", "\"", $p );
        $a  = explode( "|", $p );
        $op = '';

        $t_str = $atts['template'];
        $t_pos = strpos( $t_str, "/twig" );
        $t_fp  = substr( $t_str, $t_pos );


        foreach ( $a as $post ) {
            $post = json_decode( $post );
            $post = new IS_Post( $post->ID );

            $op .= "<div class='cell'>{$post->render_php_template($t_fp)}</div>";
        }

        print "<div class='grid-x {$ppr_op} {$pam_op} is-post-collection'>$op</div>";

        return ob_get_clean();

    }


    public static function tsd_post_collection_title_shortcode_fn( $atts, $content = null ) {

        // this function serves as a midpoint between the nested module
        // and the template file - we compile an object from the module
        // data that contains all the data need by the template

        $return_object = [ 'type'  => 'title',
                           'title' => $atts['title'] ];

        return wp_json_encode( $return_object ) . ',';


    }

    public static function tsd_post_collection_loop_shortcode_fn( $atts, $content = null ) {

        // this function serves as a midpoint between the nested module
        // and the template file - we compile an object from the module
        // data that contains all the data need by the template

        $query = WPB_Acme::convert_loop_shortcode_to_query_object( $atts['loop'] );

        $return_object = [ 'type'  => 'query',
                           'query' => $query,
                           'posts' => $query->posts ];

        return wp_json_encode( $return_object ) . ',';

    }



    public static function create_social_media_module() {

        $usernames = [ '-- Choose A Username --' => '' ];
        $accounts  = ACF_Helper::get_repeater( 'social_media_accounts' );
        if ( $accounts && is_array( $accounts ) ) {
            foreach ( $accounts as $account ) {
                $usernames[ $account['username'] ] = $account['username'] . '#' . $account['network'];
            }
        }
        $templates = WPB_Acme::get_templates( 'twig/social_media_posts' );

        \vc_map( [ "name"     => "Social Media Posts",
                   "base"     => "is_wpb_social_media_posts",
                   "category" => "InfiniSite Modules",
                   "params"   => [ [ "type"        => "dropdown",
                                     'value'       => $usernames,
                                     "heading"     => "Username",
                                     "param_name"  => "username",
                                     "description" => "Choose username" ],
                                   [ "type"        => "dropdown",
                                     'value'       => $templates,
                                     "heading"     => "Template",
                                     "param_name"  => "template",
                                     "description" => "Choose template to display" ] ] ] );


        add_shortcode( "is_wpb_social_media_posts", [ new WPB_Modules(), 'is_wpb_social_media_posts_shortcode' ] );

    }

    // Add Shortcode
    public function is_wpb_social_media_posts_shortcode( $atts ) {

        // Attributes
        $atts = shortcode_atts( [ 'username' => '',
                                  'template' => '', ], $atts, 'is_wpb_social_media_posts' );

        tsd_write_log( $atts );

        $html = '';

        if ( $username = $atts['username'] ) {

            $username = explode( '#', $username );
            $network  = '';

            $accounts = ACF_Helper::get_repeater( 'social_media_accounts' );
            if ( $accounts && is_array( $accounts ) ) {
                foreach ( $accounts as $account ) {
                    if ( strtolower( $account['network'] ) == strtolower( $username[1] ) && $account['username'] == $username[0] ) {
                        $username = $account['username'];
                        $network  = strtolower( $account['network'] );
                        break;
                    }
                }
            }


            $result = self::is_wpb_social_media_builder( $network, $username );

            if ( $result['errors'] ) {
                $html = 'ERROR: <br>';
                foreach ( $result['errors'] as $error ) {
                    $html .= $error . '<br>';
                }
            }
            else {
                $html = Acme::get_file( $atts['template'], $result );
            }
        }

        return $html;
    }

    public function is_wpb_social_media_builder( $network, $username ) {

        if ( ! $network )
            return;

        $result = [ 'errors' => [] ];

        switch ( $network ) {
            case 'facebook':

                $appID          = get_option( "options_facebook_{$username}_app_id" );
                $appSecret      = get_option( "options_facebook_{$username}_app_secret" );
                $appAccessToken = get_option( "options_facebook_{$username}_access_token" );

                if ( $appID && $appSecret && $appAccessToken ) {

                    if ( ! session_id() ) {
                        session_start();
                    }

                    $fb = new Facebook( [ 'app_id'                => $appID, // Replace {app-id} with your app id
                                          'app_secret'            => $appSecret,
                                          'default_graph_version' => 'v3.2', ] );

                    try {
                        // Returns a `Facebook\FacebookResponse` object
                        $response = $fb->get( '/' . $username . '/feed', $appAccessToken );
                    } catch ( FacebookResponseException $e ) {
                        $result['errors'][] = 'Graph returned an error: ' . $e->getMessage();
                        exit;
                    } catch ( FacebookSDKException $e ) {
                        $result['errors'][] = 'Facebook SDK returned an error: ' . $e->getMessage();
                        exit;
                    }
                    $result['data'] = $response->getGraphEdge()->asArray();
                }

                break;
            default:
                # code...
                break;
        }

        return $result;
    }


    public static function update_vc_post_grid() {

        $group_title = 'InfiniSite Options';

        $fields = [ [ "type"             => "is_post_per_row",
                      "heading"          => "Posts Per Row",
                      "param_name"       => "post_per_row",
                      'group'            => $group_title,
                      "edit_field_class" => "vc_col-xs-6" ],
                    [ "type"             => "is_padding_and_margin",
                      "heading"          => "Padding and Margin",
                      "param_name"       => "padding_and_margin",
                      'group'            => $group_title,
                      "edit_field_class" => "vc_col-xs-4" ],
                    [ "type"             => "dropdown",
                      "heading"          => "Horizontal Alignment",
                      "param_name"       => "horizontal_alignment",
                      'value'            => [ 'Left'    => 'left',
                                              'Center'  => 'center',
                                              'Right'   => 'right',
                                              'Justify' => 'justify',
                                              'Spaced'  => 'spaced' ],
                      'group'            => $group_title,
                      "edit_field_class" => "vc_col-xs-3" ],
                    [ "type"             => "dropdown",
                      "heading"          => "Vertical Alignment",
                      "param_name"       => "vertical_alignment",
                      'value'            => [ 'Top'    => 'top',
                                              'Middle' => 'middle',
                                              'Bottom' => 'bottom' ],
                      'group'            => $group_title,
                      "edit_field_class" => "vc_col-xs-3" ] ];

        vc_add_params( 'vc_basic_grid', $fields );
    }

    public static function init_shortcode_api() {

        $init_api = function( $atts, $content, $tag ) {


            if ( array_key_exists( 0, $atts ) )
                if ( $atts[0] === 'permalink' )
                    return get_permalink( $atts['post_id'] );


            // we have to do 2 because we use the type at the end,
            // so that would be 1 but then we also dynamically add
            // the id of the post as an att, sot thats 2
            $data_type = $atts['type'];

            $fn_name = "format_$data_type";
            $api     = new WPB_Grid_Builder_API( $atts, $content, $tag, $atts['post_id'] );


            $defined_fn = method_exists( $api, $fn_name );

            if ( ! $defined_fn )
                return \wpautop( "No defined function for the $data_type tag {$atts['post_id']}" );

            return $api->$fn_name( $atts, $content, $tag );

        };

        \add_shortcode( "is_meta", $init_api );


    }

}


class WPB_Grid_Builder_API {

    public $post;
    public $meta_key;
    public $content;
    public $atts;

    public function __construct( $atts, $content, $tag, $post ) {

        $this->post    = new IS_Post( $post );
        $this->tag     = $atts['tag'];
        $this->content = $this->post->get( $this->tag );
        $this->atts    = $atts;


    }


    public function format_text() {
        return "{$this->content}";
    }

    public function format_date() {
        $date   = new \DateTime( $this->content );
        $format = $this->atts['format'];
        if ( ! $format )
            $format = 'mdy';

        return "{$date->format($format)}";
    }

    public function format_link() {
        return "hello world!";
    }

}


// helper functions!

class WPB_Acme {

    public $groups;
    public $count = 0;

    public function __construct( $modules ) {

        $this->types  = array_keys( $modules );
        $this->groups = $modules;
        $this->count  = count( $modules );

    }


    static function convert_loop_shortcode_to_query_object( $loop_string ) {

        $loop  = explode( "|", $loop_string );
        $query = [];

        $force_post_type_any = false;

        foreach ( $loop as $item ):

            $temp = explode( ":", $item );

            $key   = $temp[0];
            $value = $temp[1];

            switch ( $key ):

                case 'order_by':
                    $key = 'orderby';
                    break;

                case 'by_id':
                    $key                 = 'post__in';
                    $value               = explode( ",", $value );
                    $force_post_type_any = true;
                    break;

                case 'size':
                    $value = $value === 'All' || $value === 'all' ? - 1 : $value;
                    $key   = 'posts_per_page';
                    break;

                case 'post_type':
                    $_pt   = explode( ',', $temp[1] );
                    $value = $_pt;
                    break;


            endswitch;

            $query[ $key ] = $value;


        endforeach;

        if ( $force_post_type_any )
            $query['post_type'] = 'any';

        $return_query = new \WP_Query( $query );

        return $return_query;
    }


    static function get_templates( $filepath ) {

        // wpbakery doesn't support acf's awesome nested option formats
        // ... at least as far as i can tell.
        // so - we need to flatten some arrays


        $files = Acme::get_templates( $filepath );

        $new_files = [ '#' => '-- Chose A File --' ];

        foreach ( $files as $filepath => $label ):

            if ( is_string( $label ) ):
                $new_files[ $filepath ] = $label;
                continue;
            endif;

            if ( is_array( $label ) ):

                foreach ( $label as $sub_filepath => $sub_label ):

                    $arr = explode( '/', $sub_filepath );
                    $dir = array_slice( $arr, - 2, 1 );

                    if ( $dir[0] == 'twigs' )
                        continue;

                    $new_files[ $sub_filepath ] = "[{$dir[0]}] $sub_label";

                endforeach;

            endif;


        endforeach;

        return array_flip( $new_files );


    }

    public function get_amount( $name, $atts = [] ) {


        // TODO: this.
        \Kint::dump( $this->groups );


    }


    public function get( $name, $atts = [] ) {


        // right now, this is only configured to find
        // the first instance of a field. so, we couldn't
        // run this function to retrieve a second instance
        // of a given module


        if ( ! $this->count )
            return null;

        if ( ! in_array( $name, $this->types ) )
            return null;


        foreach ( $this->groups as $group )
            foreach ( $group as $module )
                if ( $module->type === $name ):
                    // this fires if the module has been placed,
                    // but no value has been saved.
                    if ( ! property_exists( $module, $name ) )
                        return null;

                    return $module->$name;
                endif;


        return false;


    }

    public function has( $name, $atts = [] ) {

        // right now, this is only configured to find
        // the first instance of a field. so, we couldn't
        // run this function to check a second instance
        // of a given module

        if ( ! $this->count )
            return false;

        foreach ( $this->groups as $group )
            foreach ( $group as $module )
                if ( $module->type === $name )
                    return true;

        return false;
    }

}
