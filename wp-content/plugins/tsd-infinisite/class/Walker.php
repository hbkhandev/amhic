<?
/**
 * this file is for holding all
 */


namespace TSD_Infinisite;

class IS_Walker {


    public static function get_walkers() {

        /**
         * returns an array for use in a select menu
         *
         * key : walker class name
         * value : human readable name
         */


        $walkers = ['Display_Parent_Branch'                  => 'Parent Submenu Branch',
                    'drilldown_menu_walker'                  => 'Drilldown',
                    'accordion_menu_walker'                  => 'Accordion',
                    'nested_menu_walker'                     => 'Nested Menu',
                    'dropdown_menu_walker'                   => 'Dropdown Menu',
                    'annapolis_overlay_menu_tiles_with_icon' => 'Annapolis Overlay Tiles',
                    'top_menu_bar_walker'                    => '"Top Menu Bar"',];


        return $walkers;


    }


}


class Display_Parent_Branch extends \Walker_Nav_Menu {

    var $target_id = false;


    function __construct($target_id = false) {


        $this->target_id = $target_id;
    }

    function walk($items, $depth, ...$args) {

        if (\is_search())
            return false;

        $args = array_slice(func_get_args(), 2);
        $args = $args[0];
        $parent_field = $this->db_fields['parent'];
        $target_id = $this->target_id;
        $filtered_items = [];
        $target_page = false;


        // if the parent is not set, set it based on the post
        if (!$target_id) {
            global $post;

            foreach ($items as $item) {
                if (gettype($post) == 'object' && $item->object_id == $post->ID) {
                    $target_id = $item->ID;
                    $target_page = $item->object_id;
                }
            }
        }


        // if there isn't a parent, do a regular menu
        if (!$target_id):
            // if we're on a page that isn't on the menu, we hide it.
            return false;
        endif;

        // get the top nav item
        $target_id = $this->top_level_id($items, $target_id);

        // only include items under the parent
        foreach ($items as $item) {
            if (!$item->$parent_field)
                continue;

            $item_id = $this->top_level_id($items, $item->ID);

            if ($item_id == $target_id) {
                $filtered_items[] = $item;
            }


        }


        // if our brand doesn't have any children, we don't want to display it

        if (count($filtered_items) == 0)
            return false;


        $top_level_post_id = get_post_meta($target_id, '_menu_item_object_id', true);
        $top_level_post = get_post($top_level_post_id);

        $show_parent_link = true;

        $return = '';

        if (\is_front_page())
            $show_parent_link = false;


        if ($show_parent_link)
            $return .= "<p 
            class='no-margin is-menu-branch-walker-title' 
            style='line-height: 1.9'>
                    {$top_level_post->post_title}
            </p>";

        $return .= parent::walk($filtered_items, $depth, $args);

        return $return;
    }

    // gets the top level ID for an item ID
    function top_level_id($items, $item_id) {
        $parent_field = $this->db_fields['parent'];

        $parents = [];
        foreach ($items as $item) {
            if ($item->$parent_field) {
                $parents[$item->ID] = $item->$parent_field;
            }
        }

        // find the top level item
        while (array_key_exists($item_id, $parents)) {
            $item_id = $parents[$item_id];
        }

        return $item_id;
    }

}


class drilldown_menu_walker extends \Walker_Nav_Menu {

    function start_lvl(&$output, $depth = 0, $args = []) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"vertical menu nested\">\n";
    }
}

class accordion_menu_walker extends \Walker_Nav_Menu {

    function start_lvl(&$output, $depth = 0, $args = []) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"vertical menu nested\">\n";
    }
}

class nested_menu_walker extends \Walker_Nav_Menu {

    function start_lvl(&$output, $depth = 0, $args = []) {
        $indent = str_repeat("\t", $depth);
        $class = 'vertical menu nested';

        $output .= "\n$indent<ul class=\"$class\">\n";
    }
}

class dropdown_menu_walker extends \Walker_Nav_Menu {

    function start_lvl(&$output, $depth = 0, $args = []) {
        $indent = str_repeat("\t", $depth);
        $class = 'vertical menu nested';

        $output .= "\n$indent<ul class=\"$class\">\n";
    }
}

class button_menu extends \Walker_Nav_Menu {

    function start_el(&$output, $item, $depth = 0, $args = [], $id = 0) {

        $output .= "<div id='menu-item-$item->ID'>";
        $attributes = '';
        !empty($item->attr_title) and $attributes .= ' title="' . esc_attr($item->attr_title) . '"';
        !empty($item->target) and $attributes .= ' target="' . esc_attr($item->target) . '"';
        !empty($item->xfn) and $attributes .= ' rel="' . esc_attr($item->xfn) . '"';
        !empty($item->url) and $attributes .= ' href="' . esc_attr($item->url) . '"';
        $title = apply_filters('the_title', $item->title, $item->ID);
        $item_output = "<a $attributes class='button secondary black-text'>" . $title . '</a>';
        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }
}


class annapolis_overlay_menu_tiles_with_icon extends \Walker_Nav_Menu {

    public $c = 0;

    function start_el(&$output, $item, $depth = 0, $args = [], $id = 0) {

        $icon = get_field("icon", $item->ID);

        $icon = get_field("fontawesome_icon", $item->ID);
        $weight = get_field("fontawesome_icon_weight", $item->ID);

        $weight = $icon && !$weight ? 'fas' : $weight;

        $icon_html = $icon ? "<i class='$weight fa-$icon'></i>" : '';

        $classes = empty($item->classes) ? [] : (array)$item->classes;
        $class_names = join(' cell menu-item-tile-container ', apply_filters('nav_menu_css_class', array_filter($classes), $item));
        !empty ($class_names) and $class_names = ' class="' . esc_attr($class_names) . '"';
        $output .= "<div id='menu-item-$item->ID' $class_names>";
        $attributes = '';
        !empty($item->attr_title) and $attributes .= ' title="' . esc_attr($item->attr_title) . '"';
        !empty($item->target) and $attributes .= ' target="' . esc_attr($item->target) . '"';
        !empty($item->xfn) and $attributes .= ' rel="' . esc_attr($item->xfn) . '"';
        !empty($item->url) and $attributes .= ' href="' . esc_attr($item->url) . '"';
        $title = apply_filters('the_title', $item->title, $item->ID);
        $item_output = $args->before . "<a $attributes>" . $icon_html . $args->link_before . $title . '</a></div>' . $args->link_after . $args->after;
        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
        $this->c++;
    }

}

class top_menu_bar_walker extends \Walker_Nav_Menu {
    function start_lvl(&$output, $depth = 0, $args = []) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"vertical menu\" data-submenu>\n";
    }
}

class Accordion_Walker extends \Walker_Nav_Menu {

    public function start_lvl(&$output, $depth = 0, $args = []) {
        global $walker_counter;

        if (!isset($walker_counter[$depth]))
            $walker_counter[$depth] = 0; else
            $walker_counter[$depth] = $walker_counter[$depth]++;

        $classes = ['accordion'];

        $class_names = join(' ', apply_filters('nav_menu_submenu_css_class', $classes, $args, $depth));
        $class_names = $class_names ? 'class="' . esc_attr($class_names) . '"' : '';

        $output .= "<div class='accordion-content start_lvl' data-tab-content>";
        $output .= "<ul $class_names data-accordion data-multi-expand='true' data-allow-all-closed='true'>";
    }

    public function end_lvl(&$output, $depth = 0, $args = []) {
        global $walker_counter;
        $count = $walker_counter[$depth];
        $uid = "accordion_{$depth}_{$count}";

        $output .= "</ul>";
        $output .= "</div>";
    }


    function start_el(&$output, $item, $depth = 0, $args = [], $id = 0) {
        $title = $item->title;
        $permalink = $item->url;
        $font_size = $depth == 0 ? 24 : 16;

        switch (substr($permalink, 1)):

            case "parent":
            case "day":
                $output .= "<li class='accordion-item parent-box' data-accordion-item>";
                $output .= "<a href='#' class='accordion-title' style='font-size: {$font_size}px'>$title</a>";
                break;

            case "title":
                $output .= "<h3>$title</h3>";
                break;

            case "time":
                $output .= "<h5 class='gray-text'>$title</h5>";
                break;

            case "spacer":
                $output .= "<div class='spacer small'></div>";
                break;

            case "message":
                $output .= "<div class='card'><div class='card-section'><p>$title</p></div></div>";
                break;

            default:
                $class_meta = get_post_meta($item->ID, 'link_class', 1);
                $template = $class_meta ? $class_meta : 'basic';
                $output .= \TSD_Infinisite\Acme::get_file("components/accordion/event/$template.php", ['post' => \TSD_Infinisite\IS_Post::db_get($item->object_id)]);
                break;
        endswitch;
    }

    function end_el(&$output, $item, $depth = 0, $args = []) {

        $permalink = $item->url;
        $level_is_tab = $permalink && $permalink == '#';
        $output .= $level_is_tab ? "</li>" : "";
    }

}

//class add_xfn_as_link_class extends \Walker_Nav_Menu
//{
//    function start_el(&$output, $item, $depth, $args)
//    {
//        global $wp_query;
//        $indent = ($depth) ? str_repeat("\t", $depth) : '';
//        $class_names = $value = '';
//        $classes = empty($item->classes) ? array() : (array)$item->classes;
//        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item));
//        $class_names = ' class="' . esc_attr($class_names) . '"';
//
//        $output .= $indent . '<li id="menu-item-' . $item->ID . '"' . $value . $class_names . '>';
//
//        $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
//        $attributes .= !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
//        $attributes .= !empty($item->xfn) ? ' class="' . esc_attr($item->xfn) . '"' : '';
//        $attributes .= !empty($item->url) ? 'href="' . esc_attr($item->url) . '"' : '';
//
//
//        $description = !empty($item->description) ? '<span>' . esc_attr($item->description) . '</span>' : '';
//
//
//        $item_output = $args->before;
//        $item_output .= '<a' . $attributes . '><span data-hover="' . $item->title . '">';
//        $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID);
//        $item_output .= $description . $args->link_after;
//        $item_output .= '</a>';
//        $item_output .= $args->after;
//
//        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
//    }
//}