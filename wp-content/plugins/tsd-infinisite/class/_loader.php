<?
include('acf-field-table.php');
include('Broadside.php');
include('Data_Moves.php');
include('Hack.php');
include('WP_Backend_Modification.php');
include('Admin_Notice.php');
include('Maintenance.php');
include('Walker.php');
include('Acme.php');
include('Flash.php');
include('Nav_Menu.php');
include('Search.php');
include('Header.php');
include('Footer.php');
include('Launcher_Icon.php');
include('CPT.php');
include('Post_Type.php');
include('Template.php');
include('Social.php');
include('Account.php');
include('Taxonomy_Builder.php');
include('Foundation.php');
include('Boilerplate.php');
include('ACF_Module.php');
include('Layout.php');
include('Page_Builder.php');
include('Sidebar.php');
include('Popup.php');
include('ACF.php');
include('ACF_Helper.php');
include('Option.php');
include('IS_Editor.php');
include('Off_Canvas_Menu.php');
include('IS_Post_Archive.php');
include('IS_Post_Excerpt.php');
include('Font_Awesome.php');
include('Calendar.php');
include('Google_Map.php');
include('IS_Test_Data.php');
include('IS_Post.php');
include('IS_Query.php');
include('IS_Term.php');
include('IS_Term_Query.php');
include('SVG.php');
include('Icon_Library.php');
include('Debug.php');
include('Error_Handler.php');
include('IS_Media.php');
include('Shortcode.php');
include('DB.php');
include('WPB_Modules.php');
include('API_V0.php');
include('WPB_Core_Helper.php');

if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {

    add_action('woocommerce_loaded', function() {
        include('IS_WooCommerce.php');
    });

} else {
    include('IS_WooCommerce.php');
}
