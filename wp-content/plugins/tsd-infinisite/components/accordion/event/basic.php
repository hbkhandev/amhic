<? $title = $post->get("title") ? $post->get("title") : $post->post_title ?>

<div class="grid-x grid-padding-x grid-padding-y">
    <div class="cell auto">
        <h6 class="no-margin"><?= $title ?></h6>
        <? if ($post->get("location_name")): ?>
            <p class="gray-text"><?= $post->get("location_name") ?></p>
        <? endif ?>
        <? if ($post->get("excerpt")): ?>
            <div class="card">
                <div class="card-section">
                    <?= $post->get("excerpt") ?>
                </div>
            </div>
        <? endif ?>
    </div>
    <div class="cell shrink">
        <a href="<?= $post->permalink ?>" class="button small hollow no-margin">Learn More</a>
    </div>
</div>
