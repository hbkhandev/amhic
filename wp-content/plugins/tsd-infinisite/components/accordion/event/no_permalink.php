<? $title = $post->get("title") ? $post->get("title") : $post->post_title ?>

<div class="grid-x grid-padding-x grid-padding-y">
    <div class="cell auto">
        <h6 class="gray-text"><?= $title ?></h6>
        <div class="spacer small"></div>
        <? if ($post->get("excerpt")): ?>
            <div class="card">
                <div class="card-section">
                    <?= $post->get("excerpt") ?>
                </div>
            </div>
        <? endif ?>
    </div>
</div>
