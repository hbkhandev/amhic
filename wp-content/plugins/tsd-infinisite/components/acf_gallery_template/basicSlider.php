<? $id = uniqid( 'mobile_gallery_' ) ?>
<?php $solo = count( $gallery ) === 1 ?>
<div class="tsd-mobile-gallery-container">
    <? if ( $solo ): ?>
        <a href="<?= $gallery[0]['sizes']['large'] ?>" data-fancybox='<?= $id ?>_mobileGallery'>
            <img src="<?= $gallery[0]['sizes']['large']; ?>" alt="">
        </a>
    <? else: ?>
    <?= slider_notification() ?>
        <div id="<?= $id ?>" class="tsd-mobile-gallery owl-carousel">
            <? foreach ( $gallery as $img ): ?>
                <a href="<?= $img['sizes']['large'] ?>" data-fancybox='<?= $id ?>_mobileGallery'>
                    <img src="<?= $img['sizes']['large']; ?>" alt="">
                </a>
            <? endforeach ?>
        </div>

        <script>
            $(function () {
                $("#<?= $id ?>").owlCarousel({
                    items: 1,
                    autoHeight: true,
                    loop: true,
                });
            })
        </script>

    <? endif ?>
</div>
