<div class="grid-x grid-padding-x">
    <? foreach($gallery as $img): ?>

    <div class="cell auto">
        <img src="<?= $img['sizes']['large']; ?>" alt="">
    </div>

    <? endforeach ?>
</div>
