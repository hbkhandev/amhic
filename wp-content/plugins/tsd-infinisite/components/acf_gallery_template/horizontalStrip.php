<? $id = uniqid( 'portfolio_gallery_' ) ?>
<?php if ( count( $gallery ) <= 4 ): ?>
    <div class="grid-x grid-padding-x">
        <? foreach ( $gallery as $c => $img ): ?>
            <div class="cell auto" style="flex: <?= $img['width'] / $img['height'] ?>">
                <a href="<?= $img['sizes']['large'] ?>" data-fancybox="gallery">
                    <img src="<?= $img['sizes']['medium']; ?>" />
                </a>
            </div>


        <? endforeach ?>
    </div>
<? else: ?>

    <?= slider_notification() ?>
    <div style="max-width: 100%">
        <div id="<?= $id ?>" class="tsd-portfolio-gallery owl-carousel" style="max-width: 100%;">
            <? foreach ( $gallery as $img ): ?>
                <a href="<?= $img['sizes']['large'] ?>" data-fancybox='mobileGallery'>
                    <img src="<?= $img['sizes']['large']; ?>" alt="">
                </a>
            <? endforeach ?>
        </div>
    </div>

    <script>
        $(function () {
            $("#<?= $id ?>").owlCarousel({
                items: 3,
                autoHeight: true,
                loop: true,
                margin: 30
            });
        })
    </script>

<? endif ?>




