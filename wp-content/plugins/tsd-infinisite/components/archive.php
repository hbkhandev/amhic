<?

global $wp_query;

$current_page = $wp_query->query_vars['paged'];

$module_args = [
    'module' => [
        'acf_fc_layout' => 'hero',
        'image' => \TSD_Infinisite\Acme::get_image_info_by_id(1599),
        'title' => get_the_archive_title()
    ]
];

$module_args['module']['content_blocks'][0]['wysiwyg'] = "Page $current_page";

$hero = new \TSD_Infinisite\ACF_Module($module_args);

?>

<span data-editor-style="">
    <?= $hero->get_html() ?>
    <div class="spacer"></div>
    <div class="grid-container">


        <div class="grid-x small-up-1 medium-up-2 large-up-3 grid-padding-x grid-padding-y">
            <? foreach ($wp_query->posts as $post): ?>
                <?
                $custom_template = get_field(
                    "is_{$post->post_type}_search_result_excerpt_template", "option");
                $template = $custom_template == '' ? $default_template : $custom_template;
                $post_archive_output = \TSD_Infinisite\IS_Post_Archive::build_twig_template($post, $template);
                ?>
                <div class="cell">
                    <?= $post_archive_output ?>
                </div>
            <? endforeach ?>
        </div>
        <div class="grid-x small-12 grid-padding-x grid-paddig-y">
        <div class="cell text-center">
            <div class="spacer"></div>
            <?= \TSD_Infinisite\Acme::paginate($wp_query) ?>
            <div class="spacer large"></div>
        </div>
    </div>
    </div>
</span>