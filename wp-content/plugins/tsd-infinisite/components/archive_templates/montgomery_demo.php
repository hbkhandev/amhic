<div class="grid-container">

    <div class="grid-x">
        <div class="cell small-12">

            <h6><?= $obj->name ?> Archive</h6>


            <?
            $custom_posts = [];

            $query = new \TSD_Infinisite\IS_Query([
                'post_type' => $this->config['post_type'],
                'tax_query' => [
                    [
                        'taxonomy' => $obj->taxonomy,
                        'field' => 'slug',
                        'terms' => [$obj->slug]
                    ]
                ]
            ]);

            foreach ($query->posts as $post) {
                $custom_posts[] = $post->ID;
            }

            $module = [
                'module' => [
                    'acf_fc_layout' => 'collapsible_content',
                    'custom_posts' => $custom_posts,
                    'post_types' => [$this->config['post_type']],
                    'template' => 'default',
                    'display_options_post_style' => 'excerpts',
                    'display_options_excerpts' => [
                        'template' => "/wp-content/themes/odca_child/twig/post_excerpts/dca_report_excerpt_list.php",
                        'posts_per_page' => -1,
                        'posts_per_row' => [
                            'small' => 1
                        ],
                        'cell_padding' => [
                            'y' => 1
                        ]
                    ],
                    'tiers' => [
                        [
                            'title' => '',
                            'category' => 'topic',
                            'display_style' => 'vertical_tab',
                            'vertical_tab_options' => [
                                'launcher_template' => "/wp-content/plugins/tsd-infinisite/components/collapsible_content/branches/vertical_tab/launcher/montgomery_demo.php",
                                'content_template' => "/wp-content/plugins/tsd-infinisite/components/collapsible_content/branches/vertical_tab/content/montgomery_demo.php",
                                'container_template' => "/wp-content/plugins/tsd-infinisite/components/collapsible_content/branches/vertical_tab/container/montgomery_demo.php",
                            ],
                        ],
                    ]
                ],
            ];


            $ccm_test = new \TSD_Infinisite\ACF_Module($module);
            print $ccm_test->get_html();

            ?>

        </div>
    </div>

</div>