<div class="is_query_calendar_slider_year owl-carousel" id="is_query_calendar_<?= $id ?>">
    <?php
    foreach ( $event_lookup as $year => $months ):
        ?>
        <div class="slide">
            <div class="grid-x align-middle">
                <div class="cell shrink year-prev">
                    <i class="fas fa-arrow-alt-circle-left"></i>
                </div>
                <div class="cell auto text-center">
                    <h2><?= $year ?></h2>
                </div>
                <div class="cell shrink year-next">
                    <i class="fas fa-arrow-alt-circle-right"></i>
                </div>
            </div>

            <div class="is_query_calendar_slider_month owl-carousel">
                <?
                foreach ( $months as $month_num => $events ):
                    // Use the factory to get your period
                    $month = $factory->getMonth( $year, $month_num );

                    ?>

                    <div class="slide">
                        <h4><?= $month ?></h4>

                        <div class="grid-x small-up-7" style="border: 1px solid gray">


                            <? $c = 0 ?>
                            <?php foreach ( $month as $week ): ?>

                                <? if ( $c == 0 ): ?>
                                    <?php foreach ( $week as $day ): ?>
                                        <div class="cell">
                                            <p class="day_name"><?= $day->getBegin()->format( 'D' ) ?> </p>
                                        </div>
                                    <?php endforeach ?>
                                <? endif ?>


                                <? $this_months_events = $factory->getEvents( $month ); ?>

                                <?php foreach ( $week as $day ): ?>
                                    <div class="cell <?= ! $month->includes( $day ) ? 'gray_xxlight-background' : '' ?>">
                                        <? if ( $month->includes( $day ) ): ?>
                                            <p style="margin:0">
                                                <?= $day->getBegin()->format( 'j' ) ?>
                                            </p>

                                            <?php foreach ( $this_months_events->find( $day ) as $daily_event ): ?>
                                                <p>
                                                    <?= $daily_event->post->post_title ?>
                                                </p>
                                            <?php endforeach ?>
                                        <? endif ?>

                                    </div>
                                <?php endforeach ?>
                                <? $c ++ ?>
                            <?php endforeach ?>

                        </div>

                        <div class="grid-x">
                            <div class="cell shrink month_prev">
                                <p>previous</p>
                            </div>
                            <div class="cell auto">
                            </div>
                            <div class="cell shrink month_next">
                                <p>next</p>
                            </div>
                        </div>

                    </div>
                <? endforeach; ?>
            </div>
        </div>
    <? endforeach; ?>

</div>


<script>

    function init_is_query_calendar_<?= $id ?>() {
        const cal = $("#is_query_calendar_<?= $id ?>");
        if (!cal.length) return false;

        const month = cal.find(".is_query_calendar_slider_month"),
            args = {
                items: 1,
                autoHeight: true
            };

        month.owlCarousel(args);
        cal.owlCarousel(args);

    }

    $(window).load(function () {
        init_is_query_calendar_<?= $id ?>();
    })
</script>
