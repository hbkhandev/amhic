<?php


$_years = \range($this->get_first_event_date()['year'], $this->get_last_event_date()['year']);
$content = '';


foreach ($_years as $year):


    $year_obj = $this->factory->getYear($year);
    $events = $this->factory->getEvents($year_obj);


    $content .= "<h3>$year</h3>";

    foreach ($year_obj as $month):

        $month_has_events = $this->factory->getEvents($month)->count();
        if (!$month_has_events) continue;


        $content .= "<h4>$month</h4>";

        $days_html = '';


        foreach ($month as $week):


            foreach ($week as $day):

                if (!$month->includes($day)):
                    $days_html .= "<div class='cell not-in-month'></div>";
                    continue;
                endif;

                $day_content = $day->getBegin()->format('j');

                $day_content .= "<hr />";

                $day_events = $events->find($day);
                foreach ($day_events as $day_event):
                    ob_start();
                    $post = $day_event->post;
                    $fields = $day_event->fields;
                    include($_SERVER['DOCUMENT_ROOT'] . $this->config['templates']['event']);
                    $day_content .= ob_get_clean();

                endforeach;

                $days_html .= "<div class='cell'>$day_content</div>";

            endforeach;

        endforeach;

        $month_content = "<div class='grid-x small-up-7'>$days_html</div>";
        $content .= $month_content;

    endforeach;
endforeach;

print $content;
