<? $first_open = $launcher->acf_options['first_open'] ?>
<? $multi_expand = $launcher->acf_options['multi_expand'] ? 'data-multi-expand="true"' : '' ?>
<? $allow_all_closed = $launcher->acf_options['allow_all_closed'] ? 'data-allow-all-closed="true"' : '' ?>


<ul class="accordion" data-accordion <?= $multi_expand ?> <?= $allow_all_closed ?> >


	<? foreach ( $launcher->child_content as $c => $content ): ?>
		<?
        $id = "{$launcher->branch_id}_{$c}";
		$encoded_title = array_keys( $launcher->branch )[ $c ];
		$term = \TSD_Infinisite\Collapsible_Content::get_term_from_encoded_string( $encoded_title );
		?>


        <li class="accordion-item <?= $c == 0 && $first_open ? 'is-active' : '' ?>" <?= $launcher->branch[ $encoded_title ]['disabled'] ?>
            data-accordion-item>

			<? include( $_SERVER['DOCUMENT_ROOT'] . $launcher->acf_options['launcher_template'] ) ?>

            <div class="accordion-content" data-tab-content>
				<?= $content ?>
            </div>
        </li>


	<? endforeach ?>

</ul>