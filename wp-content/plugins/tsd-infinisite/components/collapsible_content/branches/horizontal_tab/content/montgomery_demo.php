<div class="tabs-content" data-tabs-content="<?= $launcher->branch_id ?>">

	<? foreach ( $launcher->child_content as $c => $content ): ?>
		<? $element_id = "{$launcher->branch_id}_{$c}"; ?>

        <div class="tabs-panel <?= $launcher->is_active( $c ) ? 'is-active' : '' ?>" id="<?= $element_id ?>">

			<?= $launcher->show_loosies( $c ) ?>
			<?= $content ?>
        </div>


	<? endforeach; ?>

</div>