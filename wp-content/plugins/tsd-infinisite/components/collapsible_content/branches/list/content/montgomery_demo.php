<div class="list-content">
	<? foreach ( $launcher->child_content as $c => $content ): ?>
		<? $encoded_title = array_keys( $launcher->branch )[ $c ] ?>
		<? $term = \TSD_Infinisite\Collapsible_Content::get_term_from_encoded_string( $encoded_title ); ?>
        <div class="child_content" <?= $launcher->branch[ $encoded_title ]['disabled'] ?>>
		<? $id = "{$launcher->branch_id}_{$c}" ?>
        <h5><?= $term->name ?></h5>
		<?= $content ?>
        </div>
	<? endforeach; ?>
</div>