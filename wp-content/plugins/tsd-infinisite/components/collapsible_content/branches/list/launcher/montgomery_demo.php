<div class="cc-list-launcher">
    <h5>In this section:</h5>
	<? $c = 0 ?>
	<? foreach ( $launcher->branch as $encoded_title => $branch ): ?>
		<? $term = \TSD_Infinisite\Collapsible_Content::get_term_from_encoded_string( $encoded_title ); ?>
		<? $id = "{$launcher->branch_id}_{$c}" ?>
        <div class="list_title" <?= $launcher->branch[ $encoded_title ]['disabled'] ?>>
            <h6 class=""><?= $term->name ?></h6>
			<? $c ++ ?>
        </div>
	<? endforeach ?>
</div>
