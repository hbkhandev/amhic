<? foreach ( $launcher->child_content as $c => $content ): ?>
	<? $id = "{$launcher->branch_id}_{$c}" ?>


    <div class="reveal" id="<?= $id ?>" data-reveal>

        <div class="grid-x">
            <div class="cell small-auto medium-auto">

				<? if ( $launcher->show_navigation ): ?>
                    <i class="far fa-arrow-circle-left" data-open="<?= $launcher->parent_id ?>"></i>
				<? endif ?>

            </div>
            <div class="cell small-shrink medium-shrink" data-close>
                <i class="far fa-times-circle"></i>
            </div>
        </div>

		<?= $launcher->ID ?>
		<?= $content ?>
    </div>

<? endforeach ?>
