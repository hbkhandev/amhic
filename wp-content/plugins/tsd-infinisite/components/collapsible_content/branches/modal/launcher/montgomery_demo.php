<? if ($launcher->show_navigation): ?>

<div class="button-group">
    <button class="small button" data-open="<?= $launcher->parent_id ?>">Go Back</button>
    <p class="no-margin">
        <small><?= $launcher->parent_id ?></small>
    </p>
</div>

<? endif ?>



<div class="button-group">

	<? $c = 0 ?>
	<? foreach ( $launcher->branch as $encoded_title => $branch ): ?>
		<? $id = "{$launcher->branch_id}_{$c}" ?>

		<? $term = \TSD_Infinisite\Collapsible_Content::get_term_from_encoded_string( $encoded_title ); ?>

        <button data-open="<?= $id ?>" class="button" <?= $launcher->branch[ $encoded_title ]['disabled'] ?>>
			<?= $term->name ?><br>
        </button>
		<? $c ++ ?>
	<? endforeach ?>
</div>