<div class="cc-vertical-tabs-content branch tabs-content" data-tabs-content="<?= $launcher->branch_id ?>">
	<? foreach ( $launcher->child_content as $c => $content ): ?>
		<? $id = "{$launcher->branch_id}_{$c}"; ?>
        <div class="tabs-panel <?= $launcher->is_active($c) ? 'is-active' : '' ?>" id="<?= $id ?>"
             data-branch-id="<?= $id ?>">
            <?= $launcher->show_loosies($c) ?>
			<?= $content ?>
        </div>
	<? endforeach; ?>
</div>