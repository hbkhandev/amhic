<div class="cc-vertical-tab-launcher branch">
    <ul class="vertical-tab menu vertical" data-tabs id="<?= $launcher->branch_id ?>">
		<? $c = 0 ?>
		<? foreach ( $launcher->branch as $encoded_title => $branch ): ?>
			<? $term = \TSD_Infinisite\Collapsible_Content::get_term_from_encoded_string( $encoded_title ); ?>
			<? $id = "{$launcher->branch_id}_{$c}" ?>
            <li class="tabs-title <?= $launcher->is_active($c) ? 'is-active' : '' ?>" <?= $branch['disabled'] ?>>
                <a href="#<?= $id ?>">
					<?= $term->name ?>
                </a>
            </li>
			<? $c ++ ?>
		<? endforeach ?>
    </ul>
</div>
