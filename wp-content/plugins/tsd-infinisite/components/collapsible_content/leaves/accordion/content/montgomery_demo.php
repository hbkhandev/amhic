<? $first_open = $launcher->acf_options['first_open'] ?>
<? $multi_expand = $launcher->acf_options['multi_expand'] ? 'data-multi-expand="true"' : '' ?>
<? $allow_all_closed = $launcher->acf_options['allow_all_closed'] ? 'data-allow-all-closed="true"' : '' ?>
<ul class="accordion" data-accordion <?= $multi_expand ?> <?= $allow_all_closed ?> >

    <? foreach ($launcher->query->posts as $c => $post): ?>
        <? $post_template = $_SERVER['DOCUMENT_ROOT'] . $launcher->templates['excerpt_template']; ?>

        <li class="accordion-item no-padding <?= $c == 0 && $first_open ? 'is-active' : '' ?>" data-accordion-item>

            <? include($_SERVER['DOCUMENT_ROOT'] . $launcher->templates['launcher_template']) ?>

            <div class="accordion-content" data-tab-content>
                <?= \TSD_Infinisite\Acme::get_file($post_template, ['post' => $post]) ?>
            </div>
        </li>


    <? endforeach ?>

</ul>