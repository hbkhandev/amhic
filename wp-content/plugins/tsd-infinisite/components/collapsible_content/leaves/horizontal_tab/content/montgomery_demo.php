<div class="cc-horizontal-tab-content leaves tabs-content" data-tabs-content="<?= $launcher->branch_id ?>">
	<? foreach ( $launcher->query->posts as $c => $post ): ?>
		<? $post_template = $_SERVER['DOCUMENT_ROOT'] . $launcher->templates['excerpt_template']; ?>
		<? $id = "{$launcher->branch_id}_{$c}"; ?>

        <div class="tabs-panel <?= $c == 0 ? 'is-active' : '' ?>" id="<?= $id ?>"
             data-branch-id="<?= $id ?>">
			<?= $post->render_php_template($post_template) ?>
        </div>


	<? endforeach; ?>
</div>