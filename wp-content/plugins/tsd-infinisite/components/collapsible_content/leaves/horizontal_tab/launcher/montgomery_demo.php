<div class="cc-horizontal-tab-launcher leaves">
    <ul class="center-text tabs" data-tabs id="<?= $launcher->branch_id ?>">
		<? $c = 0 ?>
		<? foreach ( $launcher->query->posts as $c => $post ): ?>
			<? $id = "{$launcher->branch_id}_{$c}" ?>
            <li class="tabs-title <?= $c == 0 ? 'is-active' : '' ?> admin-label" data-branch-id="<?= $id ?>">
                <a href="#<?= $id ?>">
					<?= $post->post_title ?>
                </a>
            </li>
		<? endforeach ?>
    </ul>
</div>
