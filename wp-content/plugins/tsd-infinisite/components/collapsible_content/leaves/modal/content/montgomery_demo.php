<? foreach ( $launcher->query->posts as $c => $post ): ?>
	<? $id = "{$launcher->branch_id}_{$c}" ?>


    <div class="reveal" id="<?= $id ?>" data-reveal>

        <div class="grid-x">
            <div class="cell small-auto medium-auto">

				<? if ( $launcher->show_navigation ): ?>
                    <i class="far fa-arrow-circle-left" data-open="<?= $launcher->parent_id ?>"></i>
				<? endif ?>

            </div>
            <div class="cell small-shrink medium-shrink" data-close>
                <i class="far fa-times-circle"></i>
            </div>
        </div>
		<? $post_template = $_SERVER['DOCUMENT_ROOT'] . $launcher->templates['excerpt_template']; ?>
		<?= $post->render_php_template( $post_template ) ?>

    </div>

<? endforeach ?>
