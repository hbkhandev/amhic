<div class="cc-vertical-tab-container leaves">
    <div class="grid-x">
        <div class="small-12 medium-3 cell">
			<?= $launcher->launcher_html ?>
        </div>
        <div class="small-12 medium-9 cell">
			<?= $launcher->content_html ?>
        </div>
    </div>
</div>
