<div class="cc-vertical-tab-launcher leaves">
    <ul class="vertical-tab menu vertical" data-tabs id="<?= $launcher->branch_id ?>">
		<? $c = 0 ?>
		<? foreach ( $launcher->query->posts as $c => $post ): ?>
			<? $id = "{$launcher->branch_id}_{$c}" ?>
            <li class="tabs-title <?= $c == 0 ? 'is-active' : '' ?> admin-label" data-branch-id="<?= $id ?>">
                <a href="#<?= $id ?>">
					<?= $post->post_title ?>
                </a>
            </li>
		<? endforeach ?>
    </ul>
</div>
