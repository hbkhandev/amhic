function init_data_viz_fn(is_viz_id) {

    var raw_data = d3.select(is_viz_id).attr('data-tsv');
    var data = d3.tsvParseRows(raw_data, function (d) {
        return d;
    });


    var colors = $(is_viz_id).data("palette");


    var titles = data.map(function (d) {
        return d;
    });


    var config = {
        title: "",
        primary: "secondary",
        secondary: "secondary_dark",
        tertiary: "secondary_xdark"
    };


    var colorRange = d3.scaleOrdinal()
        .range([colors[config.primary], colors[config.secondary], colors[config.tertiary]])
        .domain([titles]);


    var chart = bb.generate({
        data: {
            columns: data,
            type: "bubble",
            labels: true,
            color: function (color, d) {
                return colorRange(d);
            }
        },
        bubble: {
            maxR: 60
        },
        axis: {
            x: {
                type: "category"
            },
            y: {
                max: 450
            }
        },
        tooltip: {
            format: {
                title: function (d) {
                    return false;
                },
                value: function (value, ratio, id) {
                    return value;
                }
            }
        },
        bindto: is_viz_id
    });
}