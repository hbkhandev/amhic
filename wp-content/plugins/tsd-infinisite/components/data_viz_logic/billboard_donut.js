var colors;

function init_data_viz_fn(id) {

    var element = d3.select(id),
        raw_data = element.attr('data-tsv'),
        user_config_raw = element.attr('data-config'),
        colors = JSON.parse(element.attr("data-palette")),
        user_config = {};


    if (user_config_raw)
        user_config = JSON.parse(user_config_raw);


    // this is the function where we peel off the categories and the
    // custom colors, to get just the data we want
    // TODO: we should be standardizing the names for categories we don't want to appear

    var data = d3.tsvParse(raw_data, function (d, i) {
        return d;
    });

    var data_rows = d3.tsvParse(raw_data, function (d, i) {
        var obj = Object.keys(d).map(function (e) {
            if (e === 'color_role' || e === 'Categories') return null;
            return d[e];
        })
            .filter(function (e) {
                return e
            });

        obj.unshift(d.Categories);

        return obj;

    });

    delete data_rows.columns;

    var categories = d3.tsvParse(raw_data, function (d) {
        return d.Categories;
    });

    var custom_colors = d3.tsvParse(raw_data, function (d) {
        return d.color_role;
    });

    var formats = {};

    d3.tsvParse(raw_data, function (d) {
        formats[d.Categories] = d.format;
    });

    delete formats.columns;

    var config = {
        id: id,
        innerRadius: 50,
        color_scale_start: "secondary_light",
        color_scale_stop: "secondary_dark",
        arc_padding: 2,
        value_format: false,
        value: {
            format: false,
            prefix: false,
            suffix: false
        },
        padding: {
            top: 20,
            bottom: 20,
            left: 20,
            right: 20
        },
        tooltip: {
            title: false
        }
    };

    for (var property in user_config) {
        if (user_config.hasOwnProperty(property)) {
            config[property] = user_config[property];
        }
    }


    var color_args = {
        start: config.color_scale_start,
        end: config.color_scale_stop,
        steps: categories.length,
        custom_colors: custom_colors,
        domain: categories
    };

    // console.log(color_args, data);

    var colorRange = build_color_range(color_args);


    var chart = bb.generate({
        data: {
            columns: data_rows,
            type: "pie",
            order: "asc",
            color: function (color, d) {
                // console.log(color, d);
                return colorRange(d);
            }
        },
        pie: {
            title: "",
            innerRadius: config.innerRadius,
            padding: config.arc_padding,
            label: {
                ratio: 10
            }
        },
        padding: {
            top: config.padding.top,
            right: config.padding.right,
            bottom: config.padding.bottom,
            left: config.padding.left
        },
        tooltip: {
            format: {
                title: function () {
                    return config.tooltip.title;
                },
                value: function (value, ratio, id) {


                    var format = formats[id];

                    if (format === "%")
                        return Math.round(ratio * 100) + "%";


                    if (format === "% of Previous") {

                        // console.log(value, ratio, id, formats, formats[id]);
                        // this is the ratio of the rest of the values in the graph
                        var value_to_compare = 1 - ratio;

                        // console.log(value_to_compare, ratio);

                        return Math.round(ratio / value_to_compare * 100) + "% of Previous Year";

                    }

                    if (config.value.format) {
                        var format = d3.format(config.value.format);
                        value = format(value);
                    }

                    if (config.value.prefix) {
                        value = config.value.prefix + value;
                    }

                    if (config.value.suffix) {
                        value = value + config.value.suffix;
                    }

                    return value;
                }
            }
        },
        onresize: function () {
            // can we figure out how to make it resize?
        },
        bindto: id
    });

    w = $(id).parent().width();
    h = w;

    chart.resize({
        width: w,
        height: h
    });


    function build_color_range(fn_config) {

        var colors = JSON.parse(d3.select(id).attr("data-palette"));

        var start_color = colors[fn_config.start];
        var stop_color = colors[fn_config.end];

        var colorInterpolator = d3.interpolateRgb(start_color, stop_color);

        var steps = fn_config.steps;

        // creates the default color array, if no overrides are specified, these will be the colors used

        var colorArray = d3.range(0, (1 + 1 / steps), 1 / (steps - 1)).map(function (d) {
            return colorInterpolator(d)
        });

        // creating the custom color array

        var custom_colors = fn_config.custom_colors.map(function (d) {

            if (!d) return d;

            // if the string makes a valid color (is a hex color) - we get the value and return it
            var d3_color = d3.color(d);

            if (d3_color !== null) {
                return d3_color.toString();
            }

            // if that doesn't work, we SHOULD be dealing with a color role

            if (typeof (colors[d] === 'string'))
                return colors[d];

            return false;


        });


        // splicing in the custom colors

        for (var i in custom_colors) {
            if (custom_colors.hasOwnProperty(i)) {

                var color = custom_colors[i];

                if (color) {
                    colorArray[i] = custom_colors[i];
                }

            }
        }

        var colorRange = d3.scaleOrdinal()
            .range(colorArray)
            .domain(fn_config.domain);

        return colorRange;

    }

}


