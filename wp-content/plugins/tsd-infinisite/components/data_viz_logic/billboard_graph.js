function build_graph(id) {

    var elem = d3.select(id),
        raw_data = elem.attr('data-tsv'),
        colors = JSON.parse(elem.attr("data-palette")),
        data = d3.tsvParse(raw_data, function (d) {
            return d;
        }),
        categories = d3.tsvParse(raw_data, function (d) {
            return d.Categories;
        }),
        custom_colors = d3.tsvParse(raw_data, function (d) {
            if (d.color_role === '') return false;
            return d.color_role;
        }),
        user_config_raw = elem.attr("data-config"),
        user_config = {};


    delete custom_colors.columns;


    // removing Categories and color_role from the graph column labels


    data.columns = data.columns
        .map(function (d) {
            if (d === 'Categories') return null;
            if (d === 'color_role') return null;
            return d;
        })
        .filter(function (e) {
            return e
        });


    if (user_config_raw)
        user_config = JSON.parse(user_config_raw);

    elem.classed("billboard", 1);
    elem.classed("billboard_graph", 1);

    var groups = data.columns;
    delete data.columns;

    var config = {
        color_array: {
            start: "secondary_dark",
            end: "secondary_light"
        },
        value: {
            prefix: "",
            suffix: ""
        },
        categories: function (i) {
            return groups[i];
        }
    };

    for (var property in user_config) {
        if (user_config.hasOwnProperty(property)) {
            config[property] = user_config[property];
        }
    }

    var color_args = {
        start: config.color_array.start,
        end: config.color_array.end,
        steps: categories.length,
        custom_colors: custom_colors,
        domain: categories
    };

    // going to replace the below with this
    var colorRange = build_color_range(color_args);

    var barData = data.map(function (d, e) {

        var barGoup = Object.keys(d)
            .map(function (j) {
                if (j === 'Categories') return null;
                if (j === 'color_role') return null;
                return d[j];
            })
            .filter(function (e) {
                return e
            });

        // billboard wants the category name at the beginning of the
        // data array for labelling purposes
        barGoup.unshift(d.Categories);

        return barGoup;
    });

    var chart = bb.generate({
        data: {
            columns: barData,
            type: "bar",
            labels: true,
            order: "asc",
            color: function (color, d) {


                if (typeof d === 'object') {
                    // data point

                    if (d.value)
                        return colorRange(d.id);

                    // default graph element
                    return color;
                }

                return colorRange(d);
            },
        },
        padding: {
            bottom: 50
        },
        bar: {
            width: {
                max: 40,
                ratio: .9
            },
            padding: 3
        },
        tooltip: {
            format: {
                value: function (value, ratio, id) {

                    return value;
                }
            }
        },
        axis: {
            x: {
                tick: {
                    format: function (d) {
                        return config.categories(d);
                    },
                    width: "100%"
                }
            },
            y: {
                padding: {
                    top: 100,
                    bottom: 100
                },
                tick: {
                    format: function (d) {

                        if (d === 0) return d;


                        var value = d;


                        if (config.value.format) {
                            var format = d3.format(config.value.format);
                            value = format(d);
                        }

                        if (config.value.prefix) {
                            value = config.value.prefix + value;
                        }

                        if (config.value.suffix) {
                            value = value + config.value.suffix;
                        }

                        return value;
                    }
                }
            }
        },
        bindto: is_viz_id
    });

    var w = $(is_viz_id).width();
    var h = w * .5;

    chart.resize({
        width: w,
        height: 450
    });


}


build_graph(is_viz_id);


function build_color_range(fn_config) {

    var colors = JSON.parse(d3.select(is_viz_id).attr("data-palette"));

    var start_color = colors[fn_config.start];
    var stop_color = colors[fn_config.end];

    var colorInterpolator = d3.interpolateRgb(start_color, stop_color);

    var steps = fn_config.steps;

    // creates the default color array, if no overrides are specified, these will be the colors used

    var colorArray = d3.range(0, (1 + 1 / steps), 1 / (steps - 1)).map(function (d) {
        return colorInterpolator(d)
    });

    // creating the custom color array

    var custom_colors = fn_config.custom_colors.map(function (d) {

        if (!d) return d;

        // if the string makes a valid color (is a hex color) - we get the value and return it
        var d3_color = d3.color(d);

        if (d3_color !== null) {
            return d3_color.toString();
        }

        // if that doesn't work, we SHOULD be dealing with a color role

        if (typeof(colors[d] === 'string'))
            return colors[d];

        return false;


    });


    // splicing in the custom colors

    for (var i in custom_colors) {
        if (custom_colors.hasOwnProperty(i)) {

            var color = custom_colors[i];

            if (color) {
                colorArray[i] = custom_colors[i];
            }

        }
    }

    var colorRange = d3.scaleOrdinal()
        .range(colorArray)
        .domain(fn_config.domain);

    return colorRange;

}
