// set the dimensions and margins of the graph
var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;


var graph_canvas = d3.select(is_viz_id).append("svg")
    .attr("width", "100%")
    .attr("height", height + margin.top + margin.bottom)
    // 920 473
    .attr('viewBox', '0 0 920 500')
    .append("g")
    .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");


var data = d3.select(is_viz_id).attr('data-tsv');


/**
 * formats the data used for each of the cells,
 * breaks them up into rows
 */

var tsv_data = d3.tsvParse(data, function (d) {
    return d3.entries(d).map(function (entry, i) {
        // converts value from string to integer
        return {
            'key': entry.key,
            'value': entry.value
        }
    });
});




/**
 * organize graph data in a way i like
 */

var graph_info = {
    "titles": [],
    "data": []
};

all_data = [];

tsv_data.forEach(function (row_data) {


    row_data.forEach(function (cell_data, cell_index) {

        if (cell_index == 0) {
            graph_info.titles.push(cell_data.value);
            return true;
        }

        cell_index = cell_index - 1;

        // JS is more strict than php and we can't push stuff to a non existent array.
        if (!graph_info.data[cell_index])
            graph_info.data.push([]);

        graph_info.data[cell_index].push(+cell_data.value);
        all_data.push(+cell_data.value);

    });


});

// console.log(tsv_data);

/**
 * set the max params for the graph
 */

var max_data = d3.max(all_data);
var data_length = graph_info.titles.length;


/**
 * domain : the highest / lowest values shown  : DATA
 * range : the higest / lowest point shown     : VISUAL
 **/



var x_scale = d3.scaleBand()
// on a bar graph - the x domain is the values of the x axis
// the range function just outputs an array of equally distributed values
    .domain(graph_info.titles)
    .rangeRound([0, width]);

var y_scale = d3.scaleLinear()
// y scale range is always upside down
// because of how svg coords work
    .domain([0, max_data])
    .range([height, 0]);


// v axis
var vAxis = d3.axisLeft()
    .scale(y_scale)
    .ticks(5)
    .tickPadding(5);

// h axis
var hAxis = d3.axisBottom()
    .scale(x_scale)
    .ticks(data_length)
    .tickPadding(5);

/**
 * generating myself a little scaling color palette here
 */

var colors = d3.scaleLinear()
    .domain([0, data_length])
    .range(['orangered', 'orange']);

// v guide
var vGuide = graph_canvas
    .append('g')
    .attr('class', 'v_guide');

// binds itself to the element
vAxis(vGuide);
vGuide.attr("transform", "translate(-3,0)");

// h guide
var hGuide = graph_canvas
    .append('g')
    .attr('class', 'h_guide');

// binds itself to the element
hAxis(hGuide);
hGuide.attr("transform", "translate(0," + parseFloat(height + 3) + ")");


var duration = 1300;
var delay = 300;


// console.log(graph_info);

graph_canvas.selectAll('.bar_graph_bar');


var area = d3.area()
    .x(function (d, i) {
        return x_scale(graph_info.titles[i])
    })
    .y1(function (d, i) {
        return y_scale(d)
    })
    .y0(y_scale(0));


graph_info.data.forEach(function(set, index){
    graph_canvas
        .append('path')
        .attr('d', area(set))
        .attr("stroke", "orange")
        .attr("fill", "rgba(0,0,0,.25)");
});


// get the data
