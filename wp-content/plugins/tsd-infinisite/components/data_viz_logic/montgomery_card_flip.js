// hard coded data until we move into IS
var manual_data = [
    {
        icon: 'http://placehold.it/75x75',
        label: 'Test Name Blah Blah',
        backside: '566'
    },
    {
        icon: 'http://placehold.it/50x50',
        label: 'Second Blah Blah',
        backside: '123'
    }
];

var raw_data = d3.select(is_viz_id).attr('data-tsv');

/**
 * formats the data used for each of the cells,
 * breaks them up into rows
 */

var data = d3.tsvParse(raw_data, function (d) {
    return {
        'icon': d.icon,
        'label': d.label,
        'backside': d.backside
    }
});





var info = {
    card_height: 165
};


var container = $(is_viz_id)
    .addClass("is_montgomery")
    .addClass("is_montgomery_card_flip")
    .addClass("small-up-2 medium-up-4 large-up-6")
    .addClass("grid-padding-x")
    .addClass("grid-padding-y")
    .addClass("grid-x");

// change this when we move into IS

data.forEach(function (d) {

    var cell = $("<div class='cell'></div>");
    var card = $("<div class='flip_container'></div>");
    var front_content = $("<div class='frontside-text'>").text(d.label);
    var back_content = $("<div class='backside-content'>").text(d.backside);
    var icon = $("<img />").attr('src', d.icon);


    var card_front = $("<div class='front cursor_pointer gray_xxlight-background-hover'></div>")
        .append(icon)
        .append(front_content);
    var card_back = $("<div class='back'></div>")
        .append(back_content);

    // build out the card
    card
        .append(card_front)
        .append(card_back)
        .css("height", info.card_height + 'px')

    // attach the card to the cell
    cell
        .append(card);

    // attach the cell to the container
    container
        .append(cell);

    // activate the card flip
    card
        .flip({
            trigger : 'hover'
        });

});

var prevCard = false;

$('body').on('flip:done', function(e) {
  // console.log('flip:done', e);

  var currCard = $(e.target);

  if (prevCard) {
    var flip = prevCard.data('flip-model');
    console.log('flip.isFlipped', flip.isFlipped);

    if (flip.isFlipped) {
      // prevCard.flip(false);
      prevCard.find('> .back').css('transform', 'rotateY(-180deg)');
      prevCard.find('> .front').css('transform', 'none');
    }
  }

  prevCard = $(e.target);
});
