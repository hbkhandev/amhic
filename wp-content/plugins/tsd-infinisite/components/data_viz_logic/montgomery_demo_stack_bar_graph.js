function init_data_viz_fn(is_viz_id) {

    var raw_data = jQuery(is_viz_id).attr('data-tsv');

    var json_raw = jQuery(is_viz_id).attr('data-config');

    var json_config = json_raw ? JSON.parse(jQuery(is_viz_id).attr('data-config')) : {};

    var data = d3.tsvParse(raw_data, function (d) {
        return d
    });


    var data_for_graph_pre = d3.tsvParse(raw_data, function (d) {
        return Object.keys(d).map(function (e) {
            if (e.charAt(0) === "_") return null;
            return d[e];
        }).filter(function (x) {
            return x
        });
    });


    var config = d3.tsvParse(raw_data, function (d) {
        return Object.keys(d).map(function (e) {
            if (e.charAt(0) !== "_") return null;
            return d[e];
        }).filter(function (x) {
            return x
        });
    });

    var categories = data.columns.map(function (d) {
        if (d.charAt(0) === '_') return null;
        if (d === 'category') return null;
        return d;
    }).filter(function (x) {
        return x
    });

    var graph_columns = data_for_graph_pre.map(function (d) {

        return Object.keys(d).map(function (e) {
            if (e.charAt(0) === "_") return null;
            return d[e];
        }).filter(function (x) {
            return x
        });

    });

    var groups = data.map(function (d) {
        return d.category;
    }).filter(function (x) {
        return x
    });

    var colors = {};

    data.forEach(function (d) {
        if (d._color === 'transparent') {
            colors[d.category] = 'rgba(0,0,0,0)';
            return true;
        }
        colors[d.category] = palette[d._color];
    });

    var hide_from_legend = data.map(function (d) {
        if (d.category.charAt(0) === '_') return d.category;
        return false;
    }).filter(function (x) {
        return x
    });


    var chart_args = {
        data: {
            columns: graph_columns,
            order: false,
            groups: [groups],
            type: "bar",
            labels: false,
        },
        axis: {
            x: {
                categories: categories,
                type: 'category',
                height: 50
            },
            y: {

            }
        },
        padding: {
            top: 10
        },
        bindto: is_viz_id,
        grid: {
            x: {},
            y: {
                ticks: 5
            },
            lines: {
                // front: false
            }
        },
        tooltip: {
            show: false
        },
        legend: {
            hide: hide_from_legend
        },
        color: {}
    };

    jQuery.extend(true, chart_args, json_config.chart_args);


    if (data.columns.includes('_color')) {

        var colors = [],
            tiles = [];


        data.forEach(function (d) {

            var has_pattern = d._pattern !== "" && d.hasOwnProperty("_pattern");

            var color_to_use,
                pattern_to_use;

            color_to_use = palette[d._color];
            pattern_to_use = noPattern();


            if(has_pattern){
                var _fn_name = d._pattern + "Pattern";
                color_to_use = "rgba(0,0,0,0)";

                // we use the window call because we need
                // to call a function name programatically


                if(typeof window[_fn_name](palette[d._color]) === "function"){
                    pattern_to_use = window[_fn_name](palette[d._color]);
                } else {
                    console.log("pattern function not found")
                }


            }

            if (d._color === "transparent") {
                // if the color is transparent, no matter what,
                // we don't want anything to show
                color_to_use = 'rgba(0,0,0,0)';
                pattern_to_use = noPattern();
            }

            colors.push(color_to_use);
            tiles.push(pattern_to_use);


        });


        chart_args.color.pattern = colors;
        chart_args.color.tiles = function(){ return tiles };
    }

    var chart = bb.generate(chart_args);


}

function noPattern() {


    // just give an empty pattern to the elements that aren't going to use it
    return d3.select(document.createElementNS(d3.namespaces.svg, "pattern"))
        .attr("patternUnits", "userSpaceOnUse")
        .attr("width", "1")
        .attr("height", "1")
        .node();


}


function dotPattern(fillColor) {
    var pattern = d3.select(document.createElementNS(d3.namespaces.svg, "pattern"))
        .attr("patternUnits", "userSpaceOnUse")
        .attr("width", "9")
        .attr("height", "9");

    var g = pattern
        .append("g");

    g
        .append("circle")
        .style("fill", fillColor || "#00f")
        .attr("cx", "4.5")
        .attr("cy", "4.5")
        .attr("r", "3");

    return pattern.node();
}

function linePattern(fillColor) {
    var pattern = d3.select(document.createElementNS(d3.namespaces.svg, "pattern"))
        .attr("patternUnits", "userSpaceOnUse")
        .attr("width", "10")
        .attr("height", "10");

    var g = pattern
        .append("g");

    g
        .append("polygon")
        .style("fill", fillColor || "#00f")
        .attr("points", "10,7 3,0 0,0 0,3 7,10 10,10");
    g
        .append("polygon")
        .style("fill", fillColor || "#00f")
        .attr("points", "10,3 10,0 7,0");
    g
        .append("polygon")
        .style("fill", fillColor || "#00f")
        .attr("points", "0,7 0,10 3,10");

    return pattern.node();
}