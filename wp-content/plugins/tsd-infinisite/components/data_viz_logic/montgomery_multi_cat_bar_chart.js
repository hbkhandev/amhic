// todo: how to make the margin of the viewbort correspond to the size of the legend?


var aspect_ratio = .56,
    svg = d3.select(is_viz_id).append('svg').attr("width", "100%"),
    margin = {top: 5, right: 5, bottom: 25, left: 18},
    svg_width = parseFloat(svg.style('width')),
    svg_height = parseFloat(svg.style('width')) * aspect_ratio,
    width = svg_width - margin.left - margin.right,
    height = svg_height - margin.top - margin.bottom,
    viewbox_width = svg_width + margin.left + margin.right,
    viewbox_height = svg_height + margin.top + margin.bottom,
    viewbox_top = margin.top * -1,
    viewbox_left = margin.left * -1;

svg.attr("viewBox", viewbox_left + " " + viewbox_top + " " + viewbox_width + " " + viewbox_height);

var svg_content = svg.append("g")
    .attr("transform", "translate(" +
        margin.left + ", " + margin.top + ")")

var config = {
    title_column_name: "Title",
    legend: {
        min_width: 150,
        background: "rgba(255,255,255,.35)",
        padding: {
            top: 5,
            bottom: 5,
            left: 5,
            right: 5,
        },
    }
};


var raw_data = d3.select(is_viz_id).attr('data-tsv');
var data = d3.tsvParse(raw_data, function (d) {

    /**
     * we need to get the title column, set the rest
     * of the grouped values programatically
     */

    var return_me = {};

    // it's important that we set the title and remove it
    // so it doesn't get included in the dynamic row building
    return_me.title = d[config.title_column_name];
    delete(d[config.title_column_name]);

    d3.entries(d).forEach(function (data) {
        return_me[data.key] = +data.value;
    });

    return return_me;

});

var groups = data.columns.slice(1);

var reverse_groups = groups.reverse();

// the x axis for the grouped elements
var x0 = d3.scaleBand()
    .rangeRound([0, width])
    .paddingInner(0.1);

// the x axis for each of the elements within its group
var x1 = d3.scaleBand()
    .padding(0.02);

var y = d3.scaleLinear()
    .rangeRound([height, 0]);

var z = d3.scaleOrdinal()
    .range(["#761445", "#A92177", "#D15DA2"]);

var z_reverse = d3.scaleOrdinal()
    .range(["#D15DA2", "#A92177", "#761445"]);


var max_val_array = data.map(function (d) {
    var val = groups.map(function (key) {
        return d[key];
    });
    return d3.max(val);
});

var max_val = d3.max(max_val_array);


// sets the domain for the x value labels
x0.domain(data.map(function (d) {
    return d.title;
}));
// sets the domain for the subfields
x1.domain(groups).rangeRound([0, x0.bandwidth()]);
y.domain([0, max_val])
    .nice();

var height_adj = height + 1;

// setting and placing the x axis
var x_axis = svg_content.append("g")
    .attr("class", "axis")
    // adjusts the placement
    .attr("transform", "translate(0," + height_adj + ")")
    // creates the visual element
    .call(d3.axisBottom(x0));

x_axis
    .selectAll('path, .tick line')
    .attr('stroke', z(2))
    .attr("opacity", .25);

x_axis
    .selectAll('text')
    .style('font-family', '"IBM Plex Sans"')
    // z() is our color palette
    .attr('fill', z(2))
    .attr('font-size', 16);

// setting and placing the y axis
var y_axis = svg_content.append("g")
    .attr("class", "axis")
    // creates the visual element
    .call(d3.axisLeft(y).ticks(null, "s"))
    // puts itin place
    .attr("transform", "translate(-1, 0)");

// coloring the y axis labels
y_axis
    .selectAll('text')
    .attr("font-weight", "300")
    .attr('fill', z(2));
// coloring the y axis
y_axis
    .selectAll('path, .tick line')
    .attr('stroke', z(2))
    .attr("opacity", .25);


var bars_in_graph = svg_content.append("g")
    .attr("class", "bars_in_graph")
    .selectAll("g.bars_in_graph")
    .data(data)
    .enter().append("g")
    .attr("class", function (d, i) {
        return 'bar_group_' + i;
    })
    .attr("transform", function (d) {
        return "translate(" + x0(d.title) + ",0)";
    })
    .selectAll("rect")
    // here we are shifting from looping through the general
    // data array to look through each specific bar
    .data(function (d) {
        return groups.map(function (group) {
            return {key: group, value: d[group]}
        })
    })
    .enter().append("rect")
    .attr("x", function (d) {
        return x1(d.key);
    })
    .attr("y", function (d) {
        return y(d.value);
    })
    .attr("width", x1.bandwidth())
    .attr("height", function (d) {
        return height - y(d.value);
    })
    .attr("fill", function (d, i) {
        return z(i);
    });


// putting this here so we get the right position in the layers, but
// we're setting variables dynamically, so we need to do that after
// we ave the elements in place that we need
var legend_background = svg_content.append('g').append('rect');


var legend = svg_content.append("g")
    .attr("transform", function (d, i) {

        // 30 is the line-height of the legend
        var v_adj = groups.length * -30;
        var h_adj = parseFloat(width - 180);

        // we aren't moving the legend off the top of the graph
        v_adj = 7;

        return "translate(" + h_adj + ", " + v_adj + ")";
    })
    .attr('class', 'legend_group')
    .attr("font-family", "sans-serif")
    .attr("font-size", 10)
    .attr("text-anchor", "end")
    .selectAll("g.legend_group")
    .data(groups)
    .enter().append("g")
    .attr('class', function (d, i) {
        return 'label_' + d + i;
    })
    .attr("transform", function (d, i) {
        return "translate(0, " + i * 30 + ")";
    });

var legend_dots = legend.append("circle")
    .attr("r", 10)
    .attr("fill", function (d, i) {
        return z(i);
    });


var legend_text = legend.append("text")
    .attr("text-anchor", "start")
    .attr("font-size", 16)
    .attr('class', "font_weight_600")
    .attr("fill", function (d, i) {
        return z(i);
    })
    .attr("transform", function (d, i) {
        return "translate(20, 5)";
    })
    .text(function (d) {
        return d;
    });

var legend_bbox = svg_content.select(".legend_group").node().getBBox();
var legend_transform = d3.select('.legend_group').attr('transform');


var legend_height = legend_bbox.height + config.legend.padding.top + config.legend.padding.bottom;
var legend_width = legend_bbox.width + config.legend.padding.left + config.legend.padding.right;


if (legend_width < config.legend.min_width) legend_width = config.legend.min_width;

var legend_x = legend_bbox.x - config.legend.padding.left;
var legend_y = legend_bbox.y - config.legend.padding.right;

legend_background
    .attr('transform', legend_transform)
    .attr('height', legend_height)
    .attr('width', legend_width)
    .attr('x', legend_x)
    .attr('y', legend_y)
    .attr('fill', config.legend.background);

