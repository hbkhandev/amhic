function init_data_viz_fn(is_viz_id) {


    var jquery_obj = jQuery(is_viz_id),
        div_id = jquery_obj.attr("id"),
        raw_data = jquery_obj.attr('data-tsv'),
        json_raw = jquery_obj.attr('data-config'),
        json_config = json_raw ? JSON.parse(jquery_obj.attr('data-config')) : {},
        palette = JSON.parse(d3.select(is_viz_id).attr("data-palette"));


    var data = d3.tsvParseRows(raw_data, function (d) {
        return d;
    });


    var data_for_graph = d3.tsvParse(raw_data, function (d) {

        return Object.keys(d).map(function (e) {
            if (e.substr(0, 1) == '_') return null;
            return d[e];
        }).filter(function (x) {
            return x
        });

    });

    delete data_for_graph.columns;

    /*

     {
     "First One" : primary,
     "Second One" : secondary
     }

     */


    var colors_for_graph = {};

    d3.tsvParse(raw_data, function (d) {
        colors_for_graph[d.Categories] = palette[d._color_role];
    });

    // console.log(colors_for_graph);

    var groups = data_for_graph.map(function (d) {
        return d[0]
    }).reverse();

    var types = {};

    var category_labels = data[0].map(function (d) {


        if (d === 'Categories') return null;
        if (d.charAt(0) === "_") return null;

        return d;

    }).filter(function (e) {
        return e
    });


    var chart_args = {
        data: {
            columns: data_for_graph,
            type: 'area',
            groups: [groups],
            colors: colors_for_graph,
            order: null
        },
        padding: {
            left: 65,
            right: jquery_obj.width() * .166,
            top: 25
        },
        axis: {
            x: {
                categories: category_labels,
                type: 'category',
                height: 60,
                padding: {
                    left: -.5,
                    right: -.5
                },
                label: {
                    position: 'outer-center'
                }
            },
            y: {
                label: {
                    position: 'outer-middle'
                }
            }
        },
        point: {
            show: false
        },
        legend: {
            item: {
                tile: {
                    width: 25,
                    height: 12
                }
            }
        },
        bindto: is_viz_id
    };

    jQuery.extend(true, chart_args, json_config.chart_args);


    var chart = bb.generate(chart_args);


    jquery_obj.find("svg").addClass("montgomery_stack_line_graph");

    d3.selectAll("#" + div_id + ' .bb-grid-lines').attr("clip-path", null);
    d3.selectAll("#" + div_id + ' .bb-ygrid-line line').attr("stroke-dasharray", "5 3");


}