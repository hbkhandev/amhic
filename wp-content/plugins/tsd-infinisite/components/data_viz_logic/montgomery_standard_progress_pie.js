function create_pie(d, row) {


    var cell = $("<div class='cell is_progress_pie_container'></div>");
    var id = "is_progress_pie_" + Math.round(Math.random() * 1000000);
    cell.attr("id", id);
    row.append(cell);

    var max_time = 3500;
    var percentage = d.Percentage / 100;
    var adj_time = max_time * percentage;

    var args = {
        color: colors[d.Color],
        duration: adj_time,
        easing: 'easeOut',
        strokeWidth: 7,
        trailColor: "#eee",
        svgStyle: {
            width: "80%",
            display: "block",
            margin: "auto"
        }
    };

    var circle = new ProgressBar.Circle("#" + id, args);



    var waypoint = new Waypoint({
        offset: 'bottom-in-view',
        element: cell.get(0),
        handler: function() {
            circle.animate(d.Percentage / 100);
        }
    });


    // this allows us to refer to the cell in future elements
    return cell;


}


function create_pie_label(pie, d) {

    var container = $("<div class='is_montgomery progress_pie_label' data-editor-style></div>");

    var percentage = $("<h2 class='percentage'></h2>");
    var type = $("<h3 class='type'></h3>");

    percentage.text(d.Percentage + "%");
    type.text(d.Label);

    percentage
        .addClass(d.Color + "-text")
        .addClass("no-margin");

    type.addClass(d.Color + "-text");


    container.append(percentage);
    container.append(type);

    pie.append(container);

}

var colors;

function init_data_viz_fn(is_viz_id) {

    var raw_data = jQuery(is_viz_id).attr('data-tsv');

    var data = d3.tsvParse(raw_data, function (d) {
        return d;
    });

    var row = $("<div class='grid-x grid-padding-x grid-padding-y small-up-1 medium-up-2'></div>");

    colors = JSON.parse(d3.select(is_viz_id).attr("data-palette"));


    jQuery(is_viz_id).append(row);

    data.forEach(function (d) {
        var pie = create_pie(d, row);
        create_pie_label(pie, d);
    });


}