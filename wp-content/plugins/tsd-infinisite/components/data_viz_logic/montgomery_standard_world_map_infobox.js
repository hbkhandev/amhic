function init_data_viz_fn(is_viz_id) {

    var raw_data = d3.select(is_viz_id).attr('data-tsv');
    var svg_map = d3.select("#continents");
    var jquery_obj = jQuery(is_viz_id);


    var colors = JSON.parse(d3.select(is_viz_id).attr("data-palette"));

    /**
     * formats the data used for each of the cells,
     * breaks them up into rows
     */

    var data = d3.tsvParse(raw_data, function (d) {
        return d
    });

    var legend_lookup = [
        [0, 12.5],
        [12.51, 25],
        [25.1, 50],
        [50.1, 100],
        [100.1, 200],
        [200.1, 999999],
    ];
    var categories = data.map(function (d) {
        return d.country_code;
    });

    var legend_color_domain = Object.keys(legend_lookup).map(function (d) {
        return +d
    });

    var custom_colors = Object.keys(legend_lookup).map(function (d) {
        return ''
    });


    var color_args = {
        start: 'secondary_xxlight',
        end: 'secondary_dark',
        steps: legend_color_domain.length,
        domain: legend_color_domain,
        custom_colors: ['#DBE7A3','#B4D460','#71C495','#52A98E','#367C4F','#13391E']
    };


    var colorRange = build_color_range(color_args, is_viz_id);


    jquery_obj.find("svg").addClass("robinson_map_countries");


    data.forEach(function (d) {

        var obj = $("#" + d.country_code);


        var c = 0;

        legend_lookup.map(function (l) {

            if (between(+d.projects, l[0], l[1])) {
                obj.css('fill', colorRange(c));
            }

            c++;
        });

        var hover_box_position = d.hover_box_position === '' ? 'top' : d.hover_box_position;


        Opentip.styles.is_montgomery_standard_world_map_popup_style = {
            extends: "alert",
            stem: true,
            stemLength: 20,
            stemBase: 40,
            tipJoint: "bottom center",
            background: '#fff',
            borderColor: colors.primary,
            borderRadius: 0
        };


        var openTipArgs = {
            title: false,
            style: "is_montgomery_standard_world_map_popup_style"
        };

        var tooltip_content = "" +
            "<div class='grid-x cgs-map-tooltip' data-editor-style>" +
            "<div class='cell small-12'><h6 class='no-padding primary-text data'>" + d.label + "</h6><hr class='small'></div>" +
            "</div>" +
            "<div class='grid-x cgs-map-tooltip' data-editor-style>" +
            "<div class='cell small-auto medium-auto'>" +
            "<p class='gray_xdark-text amount'>" + d.capacity + "</p>" +
            "<p class='gray_dark-text caption'>Total proposed capacity</p>" +
            "</div>" +
            "<div class='cell small-auto medium-auto'>" +
            "<p class='gray_xdark-text amount'>" + d.projects + "</p>" +
            "<p class='gray_dark-text caption'>Total proposed number of projects</p>" +
            "</div>" +
            "</div>";

        var tip = new Opentip(obj, tooltip_content, openTipArgs);



    });



}


function build_color_range(fn_config, is_viz_id) {


    var colors = JSON.parse(d3.select(is_viz_id).attr("data-palette"));

    var start_color = colors[fn_config.start];
    var stop_color = colors[fn_config.end];

    var colorInterpolator = d3.interpolateRgb(start_color, stop_color);

    var steps = fn_config.steps;

    // creates the default color array, if no overrides are specified, these will be the colors used

    var colorArray = d3.range(0, (1 + 1 / steps), 1 / (steps - 1)).map(function (d) {
        return colorInterpolator(d)
    });

    // creating the custom color array

    var custom_colors = fn_config.custom_colors.map(function (d) {

        if (!d) return d;

        // if the string makes a valid color (is a hex color) - we get the value and return it
        var d3_color = d3.color(d);

        if (d3_color !== null) {
            return d3_color.toString();
        }

        // if that doesn't work, we SHOULD be dealing with a color role

        if (typeof(colors[d] === 'string'))
            return colors[d];

        return false;


    });


    // splicing in the custom colors

    for (var i in custom_colors) {
        if (custom_colors.hasOwnProperty(i)) {

            var color = custom_colors[i];

            if (color) {
                colorArray[i] = custom_colors[i];
            }

        }
    }

    var colorRange = d3.scaleOrdinal()
        .range(colorArray)
        .domain(fn_config.domain);

    return colorRange;

}
