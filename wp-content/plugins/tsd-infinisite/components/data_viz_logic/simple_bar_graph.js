// set the dimensions and margins of the graph
var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;


var simple_bar_graph_svg = d3.select(is_viz_id).append("svg")
    .attr("width", "100%")
    .attr("height", height + margin.top + margin.bottom)
    // 920 473
    .attr('viewBox', '0 0 920 500')
    .append("g")
    .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");


var data = d3.select(is_viz_id).attr('data-tsv');
var tsv_data = d3.tsvParse(data, function (d) {
    return d3.entries(d).map(function (entry) {
        // converts value from string to integer
        return +entry.value;
    });
});

var max_data = d3.max(tsv_data[0]);
var data_length = tsv_data[0].length;

/**
 * domain : the highest / lowest values shown  : DATA
 * range : the higest / lowest point shown     : VISUAL
 **/


var x_scale = d3.scaleBand()
// on a bar graph - the x domain is the values of the x axis
// the range function just outputs an array of equally distributed values
    .domain(tsv_data.columns)
    .rangeRound([0, width]);

var y_scale = d3.scaleLinear()
// y scale range is always upside down
// because of how svg coords work
    .domain([0, max_data])
    .range([height, 0]);


// v axis
var vAxis = d3.axisLeft()
    .scale(y_scale)
    .ticks(5)
    .tickPadding(5);

// h axis
var hAxis = d3.axisBottom()
    .scale(x_scale)
    .ticks(data_length)
    .tickPadding(5);

var colors = d3.scaleLinear()
    .domain([0, data_length])
    .range(['orangered', 'orange']);

// v guide
var vGuide = simple_bar_graph_svg
    .append('g')
    .attr('class','v_guide');

// binds itself to the element
vAxis(vGuide);
vGuide.attr("transform", "translate(-3,0)");

// h guide
var hGuide = simple_bar_graph_svg
    .append('g')
    .attr('class','h_guide');

// binds itself to the element
hAxis(hGuide);
hGuide.attr("transform", "translate(0," + parseFloat(height + 3) + ")");


var duration = 1300;
var delay = 300;


simple_bar_graph_svg.selectAll('.bar_graph_bar')
    .data(tsv_data[0])
    .enter().append("rect")
    .attr("width", x_scale.bandwidth())
    .attr("height", 0)
    .attr("x", function (d, i) {
        return x_scale(tsv_data.columns[i]);
    })
    .attr("y", height)
    .attr("fill", function (d, i) {
        return colors(i);
    });

simple_bar_graph_svg.selectAll('rect').transition()
    .attr("height", function (d) {
        return y_scale(max_data - d);
    })
    .attr("y", function (d, i) {
        return height - y_scale(max_data - d)
    })
    .duration(duration)
    .delay(function (d, i) {
        return i * delay
    })
    .ease(d3.easeBounce);

// get the data
