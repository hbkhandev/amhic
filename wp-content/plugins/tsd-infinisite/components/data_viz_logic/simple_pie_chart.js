// set the dimensions and margins of the graph
var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 750 - margin.left - margin.right,
    height = 700 - margin.top - margin.bottom;


var pie_chart_svg = d3.select(is_viz_id).append("svg")
    .attr("width", "100%")
    .attr('viewBox', '0 0 600 600')
    .append("g")
    .attr("transform",
        "translate(300, 300)");


var data = d3.select(is_viz_id).attr('data-tsv');
var tsv_data = d3.tsvParse(data, function (d) {
    return d3.entries(d).map(function (entry) {
        // converts value from string to integer
        return +entry.value;
    });
});

var max_data = d3.max(tsv_data[0]);
var data_length = tsv_data[0].length;


var colors = d3.scaleLinear()
    .domain([0, data_length])
    .range(['orangered', 'orange']);

var r = 300;

var arc = d3.arc()
    .innerRadius(r * .66)
    .outerRadius(r);

var pie = d3.pie()
    .value(function (d) {
        return d;
    });

var arcs = pie_chart_svg.selectAll('.arc')
    .data(pie(tsv_data[0]))
    .enter()
    .append('g')
    .attr('class', 'arc')
    .attr('fill', function (d, i) {
        return colors(i);
    });

arcs.append('path')

    .transition().delay(function (d, i) {
    return i * 500;
}).duration(500)
    .attrTween('d', function (d) {
        var i = d3.interpolate(d.startAngle + 0.1, d.endAngle);
        return function (t) {
            d.endAngle = i(t);
            return arc(d)
        }
    })
