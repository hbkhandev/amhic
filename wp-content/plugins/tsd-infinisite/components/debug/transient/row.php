<? $name = substr($item->option_name, 11) ?>


<div class="cell full-height transient-item" data-name="<?= $name ?>">
    <div class="card height">

        <div class="card-section">
            <h6 class="no-margin text-center"><?= $name ?></h6>
        </div>
    </div>
</div>
