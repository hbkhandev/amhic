<div class="bg-blue-800">
    <footer class="bg-white mx-auto max-w-screen-xl">
        <div class="flex items-center">
            <? if ( $img ): ?>
                <div class="logo flex-initial w-64">
                    <a href="<?= site_url() ?>">
                        <img src="<?= $img['sizes']['medium'] ?>" alt="Header Logo" />
                    </a>
                    <? if ( $social_media ): ?>
                        <?= $social_media ?>
                    <? endif ?>
                </div>
            <? endif ?>

            <div class="content flex-auto flex">
                <? if ( $primary_content ): ?>
                    <div class="secondary-content flex-1">
                        <?= $primary_content ?>
                    </div>
                <? endif ?>

                <? if ( $secondary_content ): ?>
                    <div class="secondary-content flex-1">
                        <?= $secondary_content ?>
                    </div>
                <? endif ?>
            </div>

            <div class="menus flex-initial flex flex-column">
                <? if ( $primary_menu ): ?>
                    <div class="primary-menu-area">
                        <? wp_nav_menu( [ 'menu'       => $primary_menu,
                                          'items_wrap' => '<ul id="%1$s" class="%2$s" data-dropdown-menu>%3$s</ul>', ] ) ?>
                    </div>
                <? endif ?>
                <? if ( $secondary_menu ): ?>
                    <div class="primary-menu-area">
                        <? wp_nav_menu( [ 'menu'       => $secondary_menu,
                                          'items_wrap' => '<ul id="%1$s" class="%2$s" data-dropdown-menu>%3$s</ul>', ] ) ?>
                    </div>
                <? endif ?>
                <? if ( $cta_menu ): ?>
                    <div class="primary-menu-area">
                        <? wp_nav_menu( [ 'menu'       => $cta_menu,
                                          'menu_class' => '',
                                          'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', ] ) ?>
                    </div>
                <? endif ?>
            </div>

        </div>

    </footer>
</div>
