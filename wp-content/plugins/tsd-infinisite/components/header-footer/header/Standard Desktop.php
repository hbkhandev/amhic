<div class="bg-blue-800">
    <div class="bg-white mx-auto max-w-screen-xl">
        <div class="flex items-center">

            <? if ( $img ): ?>
                <div class="logo flex-initial w-64">
                    <a href="<?= site_url() ?>">
                        <img src="<?= $img['sizes']['medium'] ?>" alt="Header Logo" />
                    </a>
                </div>
            <? endif ?>


            <div class="menus flex-auto flex flex-column">
                <? if ( $primary_menu ): ?>
                    <div class="primary-menu-area">
                        <? wp_nav_menu( [ 'menu'       => $primary_menu,
                                          'menu_class' => 'flex',
                                          'items_wrap' => '<ul id="%1$s" class="%2$s align-right" data-dropdown-menu>%3$s</ul>',
                                          'link_class' => 'text-red-500' ] ) ?>
                    </div>
                <? endif ?>

                <? if ( $secondary_menu ): ?>

                    <div class="secondary">

                        <?
                        $walker = $secondary_menu_branch_toggle ? new TSD_Infinisite\Display_Parent_Branch() : false;

                        wp_nav_menu( [ 'menu'       => $secondary_menu,
                                       'menu_class' => 'dropdown menu',
                                       'items_wrap' => '<ul id="%1$s" class="%2$s align-right" data-dropdown-menu>%3$s</ul>',
                                       'walker'     => $walker ] ) ?>

                    </div>

                <? endif ?>

                <? if ( $social_media ): ?>
                    <div class="cell small-shrink medium-shrink social-media">
                        <?= $social_media ?>
                    </div>
                <? endif ?>
            </div>


            <? if ( $overlay_menu ): ?>


                <div class="cell small-shrink medium-shrink no-padding overlay-menu-launcher">
                            <span class="fa-2x cursor_pointer no_outline"
                                  data-open="overlay-menu-modal"
                                  aria-controls="overlay-menu-modal" aria-haspopup="true" tabindex="0">
                        <?= $overlay_menu_launcher_icon ?>
                            </span>
                </div>


                <? include( $_SERVER['DOCUMENT_ROOT'] . $overlay_menu_template ); ?>


            <? endif ?>


            <? if ( $offcanvas_menu ): ?>

                <div class="cell shrink no-padding offcanvas-menu-launcher">
                            <span class="fa-2x cursor_pointer no_outline"
                                  data-toggle="offCanvas"
                            >
                                <?= $offcanvas_menu_launcher_icon ?>
                            </span>
                </div>

            <? endif ?>

        </div>

    </div>
</div>

<? if ( $large_menu )
    include( $_SERVER['DOCUMENT_ROOT'] . $large_menu_template )
?>
