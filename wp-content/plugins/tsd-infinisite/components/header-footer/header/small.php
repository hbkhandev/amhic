<? $img = get_field("header_logo", "option") ?>
<div class="grid-x medium-header align-middle grid-padding-x">
    <div class="cell">
        <div class="spacer xsmall"></div>
    </div>
    <? if ($img): ?>
        <div class="cell auto logo">
            <a href="<?= site_url() ?>">
                <img src="<?= $img['sizes']['medium'] ?>" alt="Header Logo"/>
            </a>
        </div>
    <? endif ?>

    <div class="cell shrink mobile-menu-trigger-container text-right">
        <span class="fa-2x cursor_pointer no_outline"
              data-toggle="offCanvas"
        >
            <?= $offcanvas_menu_launcher_icon ?>
        </span>
    </div>

    <div class="cell">
        <div class="spacer xsmall"></div>
    </div>


    <? if ($social_media): ?>
        <div class="grid-x">
            <div class="cell small-12 auto social-media-area text-center">
                <?= $social_media ?>
            </div>

            <div class="cell">
                <div class="spacer xsmall"></div>
            </div>
        </div>
    <? endif ?>


</div>
