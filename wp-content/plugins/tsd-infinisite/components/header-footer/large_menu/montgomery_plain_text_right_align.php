<div class="grid-x fluid is_montgomery montgomery_plain_text_right_align">
    <div class="cell">

        <?

        $large_menu_walker = $large_menu_branch_toggle ?
            new TSD_Infinisite\Display_Parent_Branch() :
            false;

        wp_nav_menu([
            'menu' => $large_menu,
            'items_wrap' => '<ul id="%1$s" class="%2$s align-right">%3$s</ul>',
            'walker' => $large_menu_walker
        ]);
        ?>
    </div>
</div>