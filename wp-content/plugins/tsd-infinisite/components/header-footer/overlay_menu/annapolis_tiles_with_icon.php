<? $logo_html = \TSD_Infinisite\Header::get_responsive_logo_img('medium'); ?>

<div class="
        reveal full is-desktop-header-reveal-menu is-annapolis_tiles_with_icon flex align-center-middle no-padding
    "
     id="overlay-menu-modal"
     data-reveal
     data-v-offset="0"
     data-animation-in="fade-in"
     data-animation-out="fade-out"
>
    <div class="full-height flex align-center-middle flex-column">

        <div class="grid-container">
            <div class="grid-x">
                <div class="cell">
                    <div class="spacer xsmall"></div>
                </div>
            </div>

            <div class="grid-x align-justify align-middle  launcher-icons">
                <div class="cell shrink logo">
                    <a href="<?= site_url() ?>">
                        <?= $logo_html ?>
                    </a>
                </div>
                <div class="cell shrink icon">
                    <i class="fa fa-bars fa-2x secondary_dark-text" data-close></i>
                </div>
            </div>
            <div class="grid-x">
                <div class="cell">
                    <div class="spacer xsmall"></div>
                </div>
            </div>
        </div>


        <div class="menu-body">
            <div class="grid-container">

                <div class="grid-x">

                    <div class="cell menu-container">
                        <? wp_nav_menu(['menu'       => $overlay_menu,
                                        'menu_class' => 'small-up-2 medium-up-3 grid-x',
                                        'items_wrap' => '<div id="%1$s" class="%2$s">%3$s</div>',
                                        'walker'     => new \TSD_Infinisite\annapolis_overlay_menu_tiles_with_icon()]) ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>