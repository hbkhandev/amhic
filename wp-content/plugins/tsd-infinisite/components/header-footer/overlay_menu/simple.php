<div class="reveal large is-desktop-header-reveal-menu"
     id="overlay-menu-modal"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out"
>
    <div class="grid-y">
        <div class="cell">
            <div class="grid-x">
                <div class="cell auto">

                    <h2>Example Site Menu</h2>
                </div>
                <div class="cell shrink">
                    close me
                </div>
            </div>
        </div>
        <div class="cell">
            <div class="grid-y">
                <div class="cell">
                    <? wp_nav_menu([
                        'menu' => $overlay_menu,
                        'menu_class' => 'align-center dropdown menu is-overlay-menu',
                        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="cell">
            <p class="no-margin" data-close>
                <i class="far fa-times-circle fa-2x white-text"></i>
            </p>
        </div>
    </div>
</div>