<div class="reveal full is-desktop-header-reveal-menu montgomery_vertical_simple_menu vertical_simple_menu"
     id="overlay-menu-modal"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out"
>
    <div class="grid-y full-height container">
        <div class="cell small-shrink medium-shrink">
            <div class="grid-x align-right">
                <div class="cell small-shrink medium-shrink close-container">
                    <i class="fal fa-times cursor_pointer closer fa-2x primary-text" data-close></i>
                </div>
            </div>
        </div>
        <div class="cell small-auto medium-auto menu-container">
            <? wp_nav_menu([
                'menu' => 'sitemap',
                'menu_class' => 'is_vertical_simple_menu',
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            ]) ?>
        </div>

    </div>
</div>


<p>test</p>
