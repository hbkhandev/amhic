<form class="is-standard-header-form-search" action="<?= get_site_url() ?>">
    <ul class="menu align-middle">
        <li><input type="search" name="s" placeholder="Search"></li>
        <li><input type="submit" class="button"/></li>
    </ul>
</form>