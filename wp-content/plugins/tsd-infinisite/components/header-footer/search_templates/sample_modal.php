<?
$modal_id = uniqid('is_search_sample_modal_');
$swp_engines = new TSD_Infinisite\Search();
$current_engine = isset($_GET['swpengine']) ? esc_attr($_GET['swpengine']) : 'default'

?>

<div class="cell">
    <i class="fas fa-search is_sample_modal is_search_launcher" data-open="<?= $modal_id ?>"></i>
</div>


<div class="reveal" id="<?= $modal_id ?>" data-reveal>
    <h2>Search</h2>
    <form class="" action="<?= get_site_url() ?>">
        <div class="grid-x grid-padding-x">

            <div class="cell auto">
                <label for="s">Query</label>
                <input type="search" name="s" placeholder="Search">
            </div>
            <div class="cell shrink">
                <label for="swpengine">Search Engine</label>
                <select name="swpengine" id="">
                    <?php foreach ($swp_engines as $engine => $engine_settings) : ?>
                        <option value="<?php echo esc_attr($engine); ?>"
                            <?php selected($current_engine, $engine); ?>>
                            <?php echo isset($engine_settings['searchwp_engine_label']) ? esc_html($engine_settings['searchwp_engine_label']) : 'Default'; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="grid-x">
            <div class="cell text-center">
                <input type="submit" class="button"/>
            </div>
        </div>
    </form>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>