<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <title><?= TSD_Infinisite\Acme::browser_title() ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, user-scalable=no">
    <?php wp_head(); ?>


    <!-- Font embed code goes here -->

</head>
<body <? body_class() ?>>

<div class="off-canvas position-right" id="offCanvas" data-off-canvas>
    <?= TSD_Infinisite\Header::default_offcanvas() ?>
</div>

<div class="off-canvas-content" data-off-canvas-content data-editor-style>
<? // rest of the site lives here ?>