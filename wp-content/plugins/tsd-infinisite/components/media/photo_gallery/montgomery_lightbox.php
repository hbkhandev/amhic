<div class="grid-x grid-padding-x grid-padding-y small-up-3">

    <? foreach($gallery as $c => $image): ?>

        <div class="cell">
            <img src="<?= $image->sizes['medium'] ?>" alt="" />
        </div>

    <? endforeach ?>

</div>