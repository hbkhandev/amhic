<? $_engine = $this->filter_acf['searchwp_engine'] ?>
<? $_keyword = $this->filter_acf['keyword_search'] ?>
<? $_filter_title = $this->filter_acf['filter_title'] == '' ? "Filter & Search" : $this->filter_acf['filter_title']; ?>

<form action="" class="is_post_excerpt_filter grid-container full-height">

    <div class="grid-y full-height grid-padding-y">

        <div class="cell">

            <h3 class="open-sans"><?= $_filter_title ?></h3>
            <div class="is_divider primary"></div>

            <? if ($_keyword): ?>
                <input type="hidden" name="<?= $this->id_tag ?>[engine]" value="<?= $_engine ?>"/>

                <div class="spacer xsmall"></div>
                <div class="grid-x align-middle is_post_excerpt_filter_keyword_search">
                    <div class="cell shrink">
                        <i class="fa fa-search gray_xlight-text"></i>
                    </div>
                    <div class="cell auto space-left space-right">
                        <input
                                type="text"
                                placeholder="Keyword Search"
                                name="<?= $this->id_tag ?>[keyword]"
                                class="no-margin"
                                value="<?= $this->config['keyword'] ?>"
                        >
                    </div>
                </div>
            <? endif ?>

        </div>

        <div class="cell">
            <div class="grid-x">

                <? foreach ($taxonomy_info_for_is_post_filter as $c => $term_group): ?>

                    <?
                    $type = $term_group['config']['post_archive_filter_repeater_display_type'];
                    $multiple = $term_group['config']['post_archive_filter_repeater_allow_multiple'];
                    $form_name = "{$this->id_tag}[filters][{$c}][{$term_group['taxonomy']->name}][]";

                    $selected_filter = $_GET[$this->id_tag]['filters'][$c];

                    ?>


                    <div class="cell small-12">

                        <div class="padding-box small gray_xlight-background">
                            <label class="gray_dark-text space-left">
                                <strong>
                                    <?= $term_group['taxonomy']->label ?>
                                </strong>
                            </label>
                        </div>

                        <div class="padding-box small">
                            <? if ($type == 'select'): ?>

                                <select
                                        name="<?= $form_name ?><?= $multiple ? '[]' : '' ?>"
                                    <?= $multiple ? 'multiple' : '' ?>
                                >
                                    <option selected disabled> ---</option>
                                    <? foreach ($term_group['terms'] as $term): ?>

                                        <?
                                        $selected = '';

                                        if ($selected_filter)
                                            foreach ($selected_filter as $selected_terms)
                                                foreach ($selected_terms as $selected_term)
                                                    if ($selected_term == $term->slug)
                                                        $selected = 'selected';
                                        ?>

                                        <option
                                                name="<?= $term->slug ?>"
                                                value="<?= $term->slug ?>"
                                                id="<?= $term->taxonomy ?>_<?= $term->slug ?>"
                                            <?= $selected ?>
                                        >
                                            <?= $term->name ?>
                                        </option>
                                    <? endforeach ?>
                                </select>

                            <? endif ?>
                            <? if ($type == 'list'): ?>


                                <ul class="is_post_excerpt_filter_selector menu">

                                    <? foreach ($term_group['terms'] as $term): ?>

                                        <?
                                        $checked = '';

                                        if ($selected_filter)
                                            foreach ($selected_filter as $selected_terms)
                                                foreach ($selected_terms as $selected_term)
                                                    if ($selected_term == $term->slug)
                                                        $checked = 'checked';
                                        ?>


                                        <li>

                                            <input
                                                    type="<?= $multiple ? 'checkbox' : 'radio' ?>"
                                                    name="<?= $form_name ?>"
                                                    value="<?= $term->slug ?>"
                                                    id="<?= $this->id_tag ?>_<?= $term->taxonomy ?>_<?= $term->slug ?>"
                                                <?= $checked ?>
                                            >
                                            <label
                                                    for="<?= $this->id_tag ?>_<?= $term->taxonomy ?>_<?= $term->slug ?>"
                                            >
                                                <?= $term->name ?>
                                            </label>
                                        </li>


                                    <? endforeach ?>

                                </ul>
                            <? endif ?>
                        </div>
                    </div>

                <? endforeach ?>

            </div>
        </div>

        <div class="cell">
            <input type="submit" class="button small primary_xdark-background expanded no_margin"
                   value="Update Filters">
        </div>

    </div>
</form>

