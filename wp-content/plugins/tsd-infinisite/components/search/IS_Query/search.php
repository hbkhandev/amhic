<form action="<?= $url ?>" method="post" id="<?= $id ?>"
      class="grid-y is_query_object_search_form">

    <p>test</p>

    <? if ( $keyword_search ): ?>
        <div class="cell">
            <label for="keyword_search"><?= $keyword_label ?></label>
            <input type="text" name="<?= $id ?>_query" id="keyword_search">
        </div>
    <? endif ?>

    <? foreach ( $taxes as $tax ): ?>
        <div class="cell">
            <div class="grid-x">
                <div class="cell shrink">
                    <label for="select_<?= $tax->name ?>"><?= $tax->label ?></label>
                </div>
                <div class="cell auto">
                    <select id="select_<?= $tax->name ?>" name="<?= $id ?>_tax_<?= $tax->name ?>">
                        <option value="<?= $id ?>_null">--Select--</option>
                    </select>
                </div>
            </div>
        </div>
    <? endforeach ?>

    <input type="hidden" name="<?= $id ?>_search_form" value="true" />

    <div class="cell">
        <input type="submit" value="<?= $search_label ?>" class="button">
    </div>
</form>
