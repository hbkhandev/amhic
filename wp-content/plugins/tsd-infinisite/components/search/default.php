<?php

$posts = $this->search_query->posts;


$default_template = get_field("is_default_search_result_excerpt_template", "options");

$sizes = ['small','medium','large','xlarge'];
$posts_per_row = [];

foreach($sizes as $size)
    $posts_per_row[$size] = get_field("is_post_type_search_post_per_row_$size", "options");


if (!$posts_per_row)
    $posts_per_row = [1, 1, 1, 1];
$post_per_row_class = '';

foreach ($posts_per_row as $size => $count):
    if ($count === '')
        continue;
    $post_per_row_class .= "{$size}-up-{$count} ";
endforeach;

$search_term = isset($_GET['swpquery']) ? $_GET['swpquery'] : $_GET['s'];
$found_posts = $this->search_query->found_posts;


?>

<span data-editor-style>

    <div class="spacer"></div>

    <div class="grid-container">

        <div class="grid-x grid-padding-x grid-padding-y <?= $post_per_row_class ?>">
            <? foreach ($posts as $post): ?>
                <?
                if (get_class($post) === 'WP_Post')
                    $post = new \TSD_Infinisite\IS_Post($post);
                $custom_template = get_field("is_{$post->post_type}_search_result_excerpt_template", "option");
                $template = $custom_template == '' ? $default_template : $custom_template;
                $post_archive_output = $post->render_php_template($template);
                ?>
                <div class="cell">
                    <?= $post_archive_output ?>
                </div>

            <? endforeach ?>
        </div>

        <div class="grid-x grid-padding-x grid-padding-y">
            <div class="cell small-12 text-center">
                <div class="spacer"></div>
                <?= \TSD_Infinisite\Acme::paginate($this->search_query) ?>
                <div class="spacer large"></div>
            </div>
        </div>

    </div>

</span>