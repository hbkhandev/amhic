<div class="grid-container">
    <div class="grid-x">
        <div class="small-12 cell">
            <div class="spacer"></div>
        </div>
        <div class="small-12 cell">
            <h2>No search results found.</h2>
            <p>We were unable to find any posts to match your query.</p>
        </div>
    </div>
</div>

