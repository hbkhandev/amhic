<?php
$username = $atts['username'];

ob_start();

$appID = get_option("options_facebook_{$username}_client_id");
$appSecret = get_option("options_facebook_{$username}_client_secret");
$appAccessToken = get_option("options_facebook_{$username}_access_token");

if ( $appID && $appSecret && $appAccessToken ) {

	if (!session_id()) {
		session_start();
	}

	$fb = new Facebook\Facebook([
		'app_id' => $appID, // Replace {app-id} with your app id
		'app_secret' => $appSecret,
		'default_graph_version' => 'v3.2',
	]);

	try {
			// Returns a `Facebook\FacebookResponse` object
			$response = $fb->get(
				'/' . $username . '/feed',
				$appAccessToken
			);
	} catch (Facebook\Exceptions\FacebookResponseException $e) {
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
	} catch (Facebook\Exceptions\FacebookSDKException $e) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
	}
	$posts = $response->getGraphEdge()->asArray();

	?>
	<div class="is-facebook-feed-posts">
	<?php
		if ( $posts ) {
			echo '<ul>';
			foreach( $posts as $post ) {
				if ( array_key_exists( 'message', $post ) )
					echo '<li>' .  $post['created_time']->format('F j, Y') . ' - ' . $post['message'] . '</li>';
			}
			echo '</ul>';
		}
	?>
	</div>
	<?php
}		
$output = ob_get_contents();
ob_end_clean();

echo $output;