<?
$row_info = \TSD_Infinisite\Acme::get_row_info_by_id(get_the_ID());
$toc_rows = [];


foreach ($row_info['class'] as $row_index => $class):

    $classes = explode(' ', $class);

    if (in_array("is_toc", $classes))
        $toc_rows[] = $row_index;


endforeach;

?>

<div class="tab-links">
    <ul>
        <? foreach ($toc_rows as $c => $row): ?>
            <li>
                <a class="tab-link <?= $c == 0 ? 'active' : '' ?>"
                   href="#<?= $row_info['id'][$row] ?>">
                    <?= $row_info['title'][$row] ?>
                </a>
            </li>
        <? endforeach ?>
    </ul>
</div>
