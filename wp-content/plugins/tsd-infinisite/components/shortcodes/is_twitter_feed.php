<?

use Abraham\TwitterOAuth\TwitterOAuth;


$username = "Twitter Test Account";


$c_key = get_option("options_twitter_{$username}_client_id");
$c_sec = get_option("options_twitter_{$username}_client_secret");
$a_key = get_option("options_twitter_{$username}_auth_code");
$a_sec = get_option("options_twitter_{$username}_access_token");


$twitter = new TwitterOAuth($c_key, $c_sec, $a_key, $a_sec);


$content = $twitter->get("statuses/user_timeline", ["count"           => 1,
                                                    "exclude_replies" => true,
                                                    'screen_name'     => 'group_asset']);

$post = $content[0];
?>

<div class="grid-y">
    <h5 class="text-center">
        <a href="https://twitter.com/group_asset" target="_blank">
            <i class="fab fa-twitter"></i> @group_asset
        </a>
    </h5>
    <hr>
    <blockquote class="twitter-tweet"><a href="https://twitter.com/group_assets/status/<?= $post->id?>"></a></blockquote>
</div>

