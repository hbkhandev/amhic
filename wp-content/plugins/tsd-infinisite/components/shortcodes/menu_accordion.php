<?php

$GLOBALS['walker_counter'] = [];
$menu = wp_nav_menu(['menu'       => 59,
                     'menu_class' => 'accordion is-menu-accordion',
                     'items_wrap' => '<ul id="%1$s" class="%2$s" data-accordion data-multi-expand="true" data-allow-all-closed="true">%3$s</ul>',
                     'walker'     => new \TSD_Infinisite\Accordion_Walker()]);

