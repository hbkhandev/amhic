<div class="wpb_element_wrapper vc_custom-element-container">
    <h4 class="wpb_element_title">Hero Content - {{{ params.purpose }}}</h4>
    {{{ params.content }}}
</div>