<div class="is_padding_and_margin">
    <input placeholder="value for ppr"
           name="<?= esc_attr($settings['param_name']) ?>"
           class="wpb_vc_param_value <?= esc_attr($settings['param_name']) ?> <?= esc_attr($settings['type']) ?>_field"
           type="hidden"
           value="<?= esc_attr($value) ?>">

    <div class="vc_col-sm-3">
        <input type="checkbox" name="padding_x" class="pam_entry padding"><br>
        <label for="padding_x">Padding X</label>
    </div>
    <div class="vc_col-sm-3">
        <input type="checkbox" name="padding_y" class="pam_entry padding"><br>
        <label for="padding_y">Padding Y</label>
    </div>
    <div class="vc_col-sm-3">
        <input type="checkbox" name="margin_x" class="pam_entry margin"><br>
        <label for="margin_x">Margin X</label>
    </div>
    <div class="vc_col-sm-3">
        <input type="checkbox" name="margin_y" class="pam_entry margin"><br>
        <label for="margin_y">Margin Y</label>
    </div>

</div>