<div class="is_wpb_post_per_row">
    <input placeholder="value for ppr"
           name="<?= esc_attr($settings['param_name']) ?>"
           class="wpb_vc_param_value <?= esc_attr($settings['param_name']) ?> <?= esc_attr($settings['type']) ?>_field"
           type="hidden"
           value="<?= esc_attr($value) ?>">

    <div class="vc_col-sm-3">
        <input type="text" placeholder="small" name="small" class="ppr_entry small">
    </div>
    <div class="vc_col-sm-3">
        <input type="text" placeholder="medium" name="medium" class="ppr_entry medium">
    </div>
    <div class="vc_col-sm-3">
        <input type="text" placeholder="large" name="large" class="ppr_entry large">
    </div>
    <div class="vc_col-sm-3">
        <input type="text" placeholder="xlarge" name="xlarge" class="ppr_entry xlarge">
    </div>

</div>