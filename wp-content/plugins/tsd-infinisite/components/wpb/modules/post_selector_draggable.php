<? $id = uniqid('post_selector_draggable_');
$pt = get_post_types(['public' => 'true',]);
?>
<div class="container is_wpb_post_selector_draggable" id="<?= $id ?>">
    <div class="vc_col-sm-4">
        <select name="post_type" id="post_type_select" class="form-control">
            <option value="#">Select a Post Type</option>
            <? foreach ($pt as $key => $val): ?>
                <option value="<?= $key ?>"><?= $val ?></option>
            <? endforeach ?>
        </select>
    </div>
    <div class="vc_col-sm-8">
        <p>&nbsp;</p>
        <input
               name="<?= esc_attr($settings['param_name']) ?>"
               class="wpb_vc_param_value <?= esc_attr($settings['param_name']) ?> <?= esc_attr($settings['type']) ?>_field"
               type="hidden"
               value="<?= esc_attr($value) ?>">
    </div>
    <div class="vc_col-sm-6">
        <p>posts to select</p>
        <hr>
        <ul class="posts-sort-from sortable"  style="border: 1px solid #ddd; min-height: 20px; padding: 10px">
        </ul>
    </div>
    <div class="vc_col-sm-6">
        <p>selected posts</p>
        <hr>
        <ul class="posts-sort-to sortable" style="border: 1px solid #ddd; min-height: 20px; padding: 10px">
        </ul>
    </div>
</div>

<style>
    ul.posts-sort-from li,
    ul.posts-sort-to li {
        border: 1px solid gray;
        cursor: pointer;
    }
</style>