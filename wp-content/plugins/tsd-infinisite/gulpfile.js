const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const postcss = require('gulp-postcss');
const gutil = require('gutil');
const webpack = require('webpack-stream');
const webpackConfig = require('./webpack.config.js');

const sassPaths = [
    'bower_components/foundation-sites/scss',
    'bower_components/motion-ui/src'
];

gulp.task('scripts', function () {
    return gulp.src('assets/src/js/app.js')
        .pipe(webpack(webpackConfig))
        .on('error', function handleError() {
            this.emit('end'); // Recover from errors
        })
        .pipe(gulp.dest('assets/dist/'));
});

gulp.task('sass', function () {
    return gulp.src(['assets/scss/*.scss', "!assets/scss/_*.scss"])
        .pipe($.sourcemaps.init())
        .pipe($.sass({
            includePaths: sassPaths,
            outputStyle: 'compressed' //options; expanded, nested, compact, compressed
        })
            .on('error', function (err) {
                gutil.log(err);
                this.emit('end');
            }))
        .pipe($.autoprefixer({
            browsers: ['last 10 versions', 'ie >= 10', 'ios 6']
        }))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest('assets/dist'));
});


gulp.task("wpbakery_breakpoints", function(){
    return gulp.src([ 'assets/plugins/js_composer/assets/less/js_composer.less' ])
        .pipe($.sourcemaps.init())
        .pipe($.less({
            includePaths: sassPaths,
            outputStyle: 'compressed' //options; expanded, nested, compact, compressed
        })
            .on('error', $.sass.logError))
        .pipe($.autoprefixer({
            browsers: ['last 10 versions', 'ie >= 10', 'ios 6']
        }))
        .pipe($.sourcemaps.write())
        .pipe($.rename('js_composer.min.css'))
        .pipe(gulp.dest('assets/plugins/js_composer/assets/css/'));

})

gulp.task('default', ['sass'], function () {
    gulp.watch(['assets/scss/**/*.scss'], ['sass']);
    // gulp.watch(['assets/src/js/**/*.js'], ['scripts']);
    gulp.watch(['assets/plugins/js_composer/assets/less/lib/bower/bootstrap3/less/variables.less'], ['wpbakery_breakpoints']);
});
