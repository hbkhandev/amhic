<?php
$start_with_first_open = $this->get('start_with_first_open');

$allow_multiple_open = $this->get('allow_multiple_open');
if ($allow_multiple_open)
    $allow_multiple_open = "data-multi-expand='true'";

$allow_all_closed = $this->get('allow_all_closed');
if ($allow_all_closed)
    $allow_all_closed = "data-allow-all-closed='true'";

?>

<div class="is-accordion">
    <div class="grid-x">
        <div class="small-12 cell">

            <ul class="accordion" data-accordion <?= $allow_all_closed ?> <?= $allow_multiple_open ?>>
                <? foreach ($this->get('blade') as $c => $blade): ?>
                    <li
                            class="accordion-item <?= $c == 0 && $start_with_first_open ? 'is-active' : '' ?>"
                            data-accordion-item
                    >
                        <a href="#" class="accordion-title"><?= $blade['title'] ?></a>

                        <div class="accordion-content" data-tab-content>
                            <?= $blade['content'] ?>
                        </div>
                    </li>
                <? endforeach; ?>
            </ul>

        </div>
    </div>
</div>


