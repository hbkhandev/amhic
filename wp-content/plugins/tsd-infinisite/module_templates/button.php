<?
    $config = $this->get('config');
    $group_buttons = count($this->get('buttons')) != 1;
    $alignment = $group_buttons ? "align-{$config['alignment']}" : "text-{$config['alignment']}";
    $config['classes'] .= " {$config['size']}";

    $group_classes = $group_buttons ? $config['classes'] : "";
    $button_classes = $group_buttons ? '' : $config['classes'];

    $group = $group_buttons ? 'button-group' : '';
    $hollow = $config['hollow'] ? 'hollow' : '';
    $color = $config['color_select'];
    $button_color_class = $config['hollow'] ? "hollow $color $color-text" : "$color";
?>

<div class="grid-x ispb-button-module">
    <div class="cell <?= $alignment ?>">
        <div class="<?= "$group $alignment $group_classes" ?>">
            <? foreach ($this->get("buttons") as $button): ?>
                <? $link = $button['link'] ?>
                <a href="<?= $link['url'] ?>"
                   class="button <?= $button_color_class ?> <?= $button_classes ?> <?= $hollow ?>"
                   target="<?= $link['target'] ?>">
                    <?= $link['title'] ?>
                </a>
            <? endforeach ?>
        </div>
    </div>
</div>