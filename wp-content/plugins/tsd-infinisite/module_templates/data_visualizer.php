<?
$script = $this->get('visualization_logic');
$id = uniqid('is_data_viz_');

$csv_file = $this->get('csv_data');
$csv_file_data = false;
$csv_from_file = '';

if ($csv_file)
    $csv_from_file = preg_replace("/\xEF\xBB\xBF/", "", file_get_contents($csv_file['url']));

$data_for_graph = $csv_file ? $csv_from_file : \TSD_Infinisite\Acme::table_to_tsv_format($this->get('data'));

$map_svg = $this->get('map_svg');

if ($map_svg)
    $map_svg = preg_replace("/\xEF\xBB\xBF/", "", file_get_contents($map_svg['url']));


$json_config_field = \TSD_Infinisite\ACF::get_value($this->module, 'json_config');
$user_json_config = '';
$json_debug = false;

if ($json_config_field):
    $json_config_url = $json_config_field['url'];
    $user_json_config = file_get_contents($json_config_url);
    $json_debug = json_decode($user_json_config);
endif;

$palette = new TSD_Infinisite\Palette();

?>

<div
        id="<?= $id ?>"
        class="tsd_is_data_viz_container"
        data-tsv="<?= $data_for_graph ?>"
        data-palette="<?= htmlspecialchars(json_encode($palette->shades)); ?>"
        data-config='<?= $user_json_config ?>'
>
    <?= $map_svg ?>

</div>

<? if (is_user_logged_in()): ?>

    <p class="text-center">
        <button class="arrow_link gray_light-text" data-open="exampleModal-<?= $id ?>">View Data</button>
    </p>

    <div class="reveal large" id="exampleModal-<?= $id ?>" data-reveal data-editor-style>
        <h3>Module Data</h3>
        <div class="spacer small"></div>

        <? $data = $this->get('data') ?>

        <? if ($data): ?>
            <table>
                <? if ($data['header']): ?>
                    <thead>
                    <? foreach ($data['header'] as $header_cell): ?>
                        <th>
                            <?= $header_cell['c'] ?>
                        </th>
                    <? endforeach ?>
                    </thead>
                <? endif ?>
                <? if ($data['body']): ?>
                    <? foreach ($data['body'] as $row): ?>
                        <tr>
                            <? foreach ($row as $cell): ?>
                                <td>
                                    <?= $cell['c'] ?>
                                </td>
                            <? endforeach ?>
                        </tr>
                    <? endforeach ?>
                <? endif ?>
            </table>
        <? endif ?>

        <? if ($json_debug): ?>
            <h4>JSON User Config Data</h4>
            <? Kint::dump($json_debug) ?>
        <? endif ?>

        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>


<? endif ?>

<script type="text/javascript">

    function build_color_range(fn_config) {

        var colors = JSON.parse(d3.select(<?= $id ?>).attr("data-palette"));

        var start_color = colors[fn_config.start];
        var stop_color = colors[fn_config.end];

        var colorInterpolator = d3.interpolateRgb(start_color, stop_color);

        var steps = fn_config.steps;

        // creates the default color array, if no overrides are specified, these will be the colors used

        var colorArray = d3.range(0, (1 + 1 / steps), 1 / (steps - 1)).map(function (d) {
            return colorInterpolator(d)
        });

        // creating the custom color array

        var custom_colors = fn_config.custom_colors.map(function (d) {

            if (!d) return d;

            // if the string makes a valid color (is a hex color) - we get the value and return it
            var d3_color = d3.color(d);

            if (d3_color !== null) {
                return d3_color.toString();
            }

            // if that doesn't work, we SHOULD be dealing with a color role

            if (typeof(colors[d] === 'string'))
                return colors[d];

            return false;


        });


        // splicing in the custom colors

        for (var i in custom_colors) {
            if (custom_colors.hasOwnProperty(i)) {

                var color = custom_colors[i];

                if (color) {
                    colorArray[i] = custom_colors[i];
                }

            }
        }

        var colorRange = d3.scaleOrdinal()
            .range(colorArray)
            .domain(fn_config.domain);

        return colorRange;

    }

    function between(obj, a, b) {
        var min = Math.min.apply(Math, [a, b]),
            max = Math.max.apply(Math, [a, b]);
        return obj > min && obj < max;
    }

    var palette = JSON.parse(d3.select(<?= $id ?>).attr("data-palette"));
    palette.transparent = "rgba(0,0,0,0)";

    $(function(){
        $.getScript("<?= $script ?>", function(x, y, z){
            init_data_viz_fn(<?= $id ?>);
        })
    });


</script>