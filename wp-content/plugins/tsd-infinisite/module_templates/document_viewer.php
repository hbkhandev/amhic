<div class="is-document_viewer">
    <div class="grid-x align-center">
        <div class="cell small-12">

            <div class="responsive-embed">
                <iframe src="https://docs.google.com/viewerng/viewer?url=<?= $this->get('document')['url'] ?>&embedded=true"></iframe>
            </div>

            <div class="spacer"></div>

            <p class="text-center">
                <a href="<?= $this->get('document')['url'] ?>" class="button secondary large white-text" download>
                    <i class="far fa-download space-right"></i> Download File
                </a>
            </p>
        </div>
    </div>
</div>