<? $id = uniqid('video_player_') ?>

    <div class="is-embed">
        <div class="grid-x">
            <div class="small-12 cell">
                <?
                $embed = $this->get("embed");
                $pos = strpos($embed, 'feature=oembed');
                $embed = substr_replace($embed, '&autoplay=1&rel=0', $pos + 14, 0);
                ?>
                <div id="<?= $id ?>" class="responsive-embed no-margin <?= $this->get('aspect_ratio') ?>">
                    <? if ($this->get("poster")): ?>
                        <div class="img-container">
                            <img src="<?= $this->get('poster')['sizes']['large'] ?>" alt="Video Poster"
                                 class="full-width" />
                        </div>
                        <!-- <?= $embed ?> -->
                    <? else: ?>
                        <?= $this->get("embed") ?>
                    <? endif ?>
                </div>
            </div>
        </div>
    </div>

<? if ($this->get("poster")): ?>
    <script type="text/javascript">


        $(function () {
            var videos = $("#<?= $id ?>");

            videos.on("click", function () {
                var elm = $(this),
                    conts = elm.contents(),
                    le = conts.length,
                    ifr = null;

                for (var i = 0; i < le; i++) {
                    if (conts[i].nodeType == 8) ifr = conts[i].textContent;
                }

                elm.addClass("player").html(ifr);
                elm.off("click");
            });
        });

    </script>
<? endif ?>