<div class="is-simple_content">
    <div class="grid-x">
        <div class="small-12 cell">
            <?= do_shortcode("[gravityform id={$this->get('form')} title=false description=false ajax=true]") ?>
        </div>
    </div>
</div>