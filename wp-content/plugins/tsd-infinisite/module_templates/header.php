<?
$elem = $this->get('element');
$has_link = is_array($this->get('link'));
$link = $this->get('link');

$link_opener = $has_link ? "<a href='{$link['url']}' title='{$link['title']}' target='{$link['target']}'>" : '';
$link_closer = $has_link ? "</a>" : "";

$flex_alignment = '';
$text_alignment = '';


if ($this->get('alignment')):
    $flex_alignment = "align-{$this->get('alignment')}";
    $text_alignment = "text-{$this->get('alignment')}";
endif;



$color = $this->get('color_select');

if($color)
    $color = $color . "-text";


$class = $this->get('class');

?>

<div class="is-header">
    <div class="grid-x">
        <div class="cell">
            <<?= $elem ?> class='module-element <?= $text_alignment ?> <?= $flex_alignment ?> <?= $class ?> <?= $color ?>'>
                <?= $link_opener ?>
                    <?= $this->get('text') ?>
                <?= $link_closer ?>
            </<?= $elem ?>>
        </div>
    </div>
</div>
