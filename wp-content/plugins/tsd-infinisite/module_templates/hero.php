<?
$title = $this->get('title') ? $this->get('title') : get_the_title();
$content_blocks = $module->get("content_blocks");

?>

<div class="grid-container padding-top padding-bottom">
    <div class="grid-x">
        <div class="cell auto">
            <h3><?= $title ?></h3>
        </div>
        <div class="cell shrink">
            <h3><small>Infinisite Default Hero</small></h3>
        </div>
        <div class="cell">

            <? if ($content_blocks): ?>

                <? foreach ($content_blocks as $block): ?>

                    <div class="card">
                        <div class="card-section">
                            <?= $block['wysiwyg'] ?>
                        </div>
                    </div>

                <? endforeach ?>
            <? endif ?>

        </div>
    </div>
</div>