<?

use TSD_Infinisite\WPB_Acme;
use TSD_Infinisite\Acme;

$module = new WPB_Acme($module);

$title = $module->get("title") ?? get_the_title();
$content = $module->get("content");

// if the title equals none, set it to a string with a single space
$title !== "{none}" ?: $title = ' ';

?>

<div class="bait_interior_hero flex align-center-middle">
    <div class="container full-width" style="position: relative;">
        <?= Acme::element('h1', $title); ?>
        <?= Acme::element('div', $content, 'content'); ?>
    </div>
</div>
