<?
use SVG\SVGImage;

include INFINISITE_URI . 'assets/acme/foundation-5-brand-lookup.php';
$has_link = is_array($this->get('link'));
$link = $this->get('link');

$link_opener = $has_link ? "<a href='{$link['url']}' title='{$link['title']}' target='{$link['target']}'>" : '';
$link_closer = $has_link ? "</a>" : "";
?>
<div class="is-icon">
    <div class="grid-x">
        <div class="small-12 cell text-center">
            <? $library = in_array($this->get('icon'), $foundation_5_brands) ? 'fab' : 'fas'; ?>
            <?= $link_opener ?>
            <? if ($this->get('icon')): ?>
                <i
                        class="
                        <?= $library ?>
                        fa-<?= $this->get('icon') ?>
                        <?= $this->get('size') ?>
                        <?= $this->get('animation_effects') ?>
                        <?= $this->get('rotate_flip') ?>
                        <?= $this->get('color_select') ?>-text
                    "
                    ></i>
            <? else: ?>
                <? if(\TSD_Infinisite\Acme::can_user_see_debug()): ?>
                    <p>icon with custom svg turned off due to vendor conflict. ping gregg on glip and tell him of this message that he wrote.</p>
                    <? endif ?>
                <?
                // todo: turn this back on
//                $icon = SVGImage::fromFile($this->get('svg')['url']);
//                $doc = $icon->getDocument();
//                $icon_path = $doc->getChild(0);
//                $icon_path->setStyle('fill', '#0000FF');
//                print $icon;
                ?>
            <? endif ?>
            <? if ($this->get('caption')): ?>
                <div class="spacer xsmall"></div>

                <p class="s
                mall <?= $this->get('color_select') ?>-text">
                    <?= $this->get('caption') ?></p>
            <? endif ?>
            <?= $link_closer ?>
        </div>
    </div>
</div>