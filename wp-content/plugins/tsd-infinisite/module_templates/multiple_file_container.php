<div class="is-document_viewer">
    <div class="grid-x">
        <div class="cell small-12">
            <div class="card">
                <div class="card-section">
                    <ul class="fa-ul">
                        <? foreach ($this->get('file_container') as $file): ?>
                            <? $file = $file['file']; ?>
                            <li>
                                <i class="fa-li fa-file-pdf fal primary-text fa-sm"></i>
                                <a href="<?= $file['url'] ?>">
                                    <?= $file['title'] ?>
                                </a>
                            </li>
                        <? endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>