<p>default template post collection</p>

<? Kint::dump($data) ?>

<? $col_lookup = [3, 6, 3, 9, 12]; ?>


<div class="isotope-row">
    <div class="grid-x grid-padding-x grid-padding-y">

        <? foreach ($data as $c => $module): ?>


            <div class="cell small-<?= $col_lookup[$c] ?>">
                <div class="card">
                    <div class="card-section">

                        <h2><?= $module->type ?></h2>
                        <? if ($module->content): ?>
                            <?= wpautop($module->content); ?>
                            <?= wpautop($module->content); ?>
                            <?= wpautop($module->content); ?>
                            <?= wpautop($module->content); ?>
                            <?= wpautop($module->content); ?>
                            <?= wpautop($module->content); ?>
                        <? endif ?>
                        <? if ($module->posts): ?>

                            <h4>Posts</h4>

                            <? foreach ($module->posts as $post): ?>
                                <p><?= $post->post_title ?></p>

                            <? endforeach ?>

                        <? endif ?>

                        <? if ($module->image): ?>

                            <img src="<?= $module->image->url ?>" alt="">


                        <? endif ?>

                    </div>

                </div>
            </div>

        <? endforeach ?>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        $(".isotope-row").isotope({
            itemSelector: '.cell',
            layoutMode: 'masonry'
        })
    })
</script>