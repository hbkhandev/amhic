<?
$img = $this->get("image");
if (!$img) return;

$width = $this->get('full_width') ? '100%' : 'auto';
$img_src = isset($img['sizes']) ? $img['sizes']['large'] : $img['url'];
$link_html = TSD_Infinisite\Acme::get_link_html_from_acf_module($this->get('link'));


if (substr($img_src, -3, 3) == 'svg'):

    $svg = file_get_contents($img_src);

    print $svg;

    return;
endif;

?>
<div class="is-single_image">
    <div class="grid-x">
        <div class="small-12 cell">
            <?= $link_html[0] ?>
            <img
                    src="<?= $img_src ?>"
                    style="width: <?= $width ?>"
                    alt="<?= $img['alt'] ?>"
            >
            <?= $link_html[1] ?>
        </div>
    </div>
</div>