<?
$divider_colors = '';

$display_divider = is_array($this->get('divider_color'));

if ($display_divider)
    foreach ($this->get('divider_color') as $color)
        $divider_colors .= "$color ";
?>

<div class="is-spacer grid-x">
    <div class="cell">
        <div class="spacer <?= $this->get('size') ?> flex align-<?= $this->get('divider_position') ?>">
            <? if ($display_divider): ?>
                <div class="is_divider <?= $divider_colors ?>"></div>
            <? endif ?>
        </div>
    </div>
</div>