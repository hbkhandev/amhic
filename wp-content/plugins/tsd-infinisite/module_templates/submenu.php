<?

if (!file_exists($this->get('template'))):

    if (TSD_Infinisite\Acme::can_user_see_error_message())
        print "<h2>Included File Missing</h2>";

    return;

endif;

include($this->get('template'));