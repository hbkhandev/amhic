<?
$content = $this->module['table_content'];
$header = $content['header'];
$body = $content['body'];
?>
<div class="cell">
    <table class="is_pb_table_layout">

        <? if ($header): ?>

            <thead>
            <? foreach ($header as $content): ?>
                <td><p class="no-margin"><?= $content['c'] ?></p></td>
            <? endforeach ?>
            </thead>

        <? endif ?>

        <? if (count($body)): ?>
            <tbody>

            <? foreach ($body as $row): ?>
                <tr>
                    <? foreach ($row as $content): ?>
                        <td><p class="no-margin"><?= $content['c'] ?></p></td>
                    <? endforeach ?>
                </tr>
            <? endforeach ?>

            </tbody>
        <? endif ?>
    </table>
</div>
