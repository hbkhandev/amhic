<? $cell_count = $this->get('cells_per_row') ? $this->get('cells_per_row') : 3 ?>

<div class="is-text_row">
    <div class="grid-x grid-padding-x grid-padding-y medium-up-<?= $cell_count ?>">
        <? foreach ($this->get('columns') as $text_row): ?>
            <div class="cell">
                <?= $text_row['content'] ?>
            </div>
        <? endforeach ?>
    </div>
</div>