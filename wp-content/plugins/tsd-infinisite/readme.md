# TSD Infinisite
* Tags: pagebuilder, layout builder
* Requires at least: 4,9
* Tested up to: 4.9
* Requires PHP: 5.2.4
* License: GPLv2 or later
* License URI: http://www.gnu.org/licenses/gpl-2.0.html
* Stable tag: trunk
* Contributors: tsdgregg

## Purpose

Business-oriented rapid-deployment content management with advanced structural, templating and styling tools.

## Core Overrides

* /vendor/leafo/scssphp/src/Compiler.php:3122 -> $nextIsRoot = false;
* /bower_components/foundation-sites/scss/components/_breadcrumbs.scss:52 -> $breadcrumbs-item-separator-item-rtl: 'bs-rtl' !default;


### Description
This plugin powers a page building system. It uses Advanced Custom Fields to create a page builder, and a post type builder.