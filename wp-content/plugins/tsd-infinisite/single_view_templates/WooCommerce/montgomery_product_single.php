<?
// $post    = new \TSD_Infinisite\IS_Post( get_post( get_the_ID() ) );
// $fields  = $post->fields;
$header  = $this->header;
$sidebar = $this->sidebar;
$footer  = $this->footer;
$product = new \TSD_Infinisite\IS_Product($this->acf_fields->get_ID());
$cats = $product->post->terms['product_cat'];

$product_category_links = '';

foreach($cats as $c => $cat):
    $product_category_links .= "<a href='#'>{$cat->name}</a>";
    if($c + 1 != count($cats))
        $product_category_links .= ", ";
endforeach;

$gallery_template = INFINISITE_URI . 'components/media/photo_gallery/montgomery_lightbox.php';


?>

<div class='is-page-builder-wrapper grid-y' data-editor-style>

    <div class="cell">
        <div class="spacer"></div>
    </div>

    <div class='cell'>
        <div class='grid-x grid-padding-x'>
            <div class="cell small-12 medium-4 large-3">
                <img src="<?= $product->image->sizes['medium'] ?>" alt="">
                <?= $product->apply_gallery_template($gallery_template) ?>
            </div>

            <div class="cell small-12 medium-8 large-9">
                <h2><?= $product->get_title() ?></h2>
                <h6><?= $product->get_price_html() ?></h6>

                <?= apply_filters("the_content", $product->get_description()) ?>
                <?= $product->add_to_cart_form() ?>

                <hr>

                <p><?= $product->get_sku() ?></p>

                <p>Categories: <?= $product_category_links ?></p>

                <hr>

                <?= $product->review_form() ?>

                <?php


                global $post;

                $post = get_post($this->acf_fields->get_ID);

                \setup_postdata($post);

                the_title();

                print have_comments() ? 1 : 2;


                \wp_reset_postdata($post);

                the_title();



                ?>


            </div>

        </div>
    </div>

</div>