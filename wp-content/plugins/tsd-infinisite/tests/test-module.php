<?php
/**
 * Class PageBuilderTest
 *
 * @package Tsd_Infinisite
 */

/**Å
 * Sample test case.
 */
class ModuleTest extends WP_UnitTestCase
{

    public function setUp()
    {
//        $this->markTestSkipped('skipped all page builder tests for loading time.');

        parent::setUp();


        $this->post = $this->factory->post->create_and_get();
        $this->cpt_post = $this->factory->post->create_and_get();

        $this->image = $this->factory->attachment->create_and_get();
        $this->svg = $this->factory->attachment->create_and_get();

        $this->image->sizes = TSD_Infinisite\Acme::fudge_image_sizes_for_testing();


        $test_data_acf_page_builder = $this->get_standard_page_builer_test_with_all_layouts();
        $test_data_cpt = $this->get_cpt_meta_fields();

        // build out the page builder data for the primary test post
        update_field('field_ispb_row_repeater', $test_data_acf_page_builder, $this->post->ID);

        // attach each of the test cpt data to our cpt post
        foreach ($test_data_cpt as $key => $value)
            update_field($key, $value, $this->cpt_post->ID);

        $this->assertTrue(true);
    }

    public static function msg($msg)
    {
        fwrite(STDERR, print_r($msg, TRUE));
    }

    private static function add_test_data($acf)
    {
        foreach ($acf as $type => $module):

            if (array_key_exists('image', $module)):
                $module['image']['link'] = [
                    'title' => 'Link Title Here',
                    'target' => '',
                    'url' => 'http://topshelfdesign.net',
                ];
            endif;


            if (array_key_exists('image', $module)):

                $module['image'] = [
                    'url' => '#'
                ];


            endif;


            if (array_key_exists('images', $module)):


                $module['images'] = [
                    [
                        'url' => 'test'
                    ],
                    [
                        'url' => 'test'
                    ]
                ];


                foreach ($module['images'] as $c => $image):

                    $module['images'][$c]['link'] = [
                        'title' => 'Link Title Here',
                        'target' => '',
                        'url' => 'http://topshelfdesign.net',
                    ];

                endforeach;
            endif;

            $_template = INFINISITE_URI . "module_templates/{$module['acf_fc_layout']}.php";

            $acf_module = new \TSD_Infinisite\ACF_Module([
                'module' => $module,
                'module_template' => $_template
            ]);

            $acf_module->get_html();


        endforeach;
    }

    function test_module_to_acf()
    {

        $acf = self::get_sample_module_acf();
        $acf = self::add_test_data($acf);


        $this->assertTrue(true);
    }

    function test_create_page_builder_layouts()
    {


        $layouts = TSD_Infinisite\Page_Builder::get_page_builder_layouts();

        // TODO: write about all the awful ways this could go wrong


        $is_valid = true;


        $this->assertTrue($is_valid);

    }

    function get_sample_module_acf()
    {

        // this function has been moved to the test data class

        $modules = new \TSD_Infinisite\IS_Test_Page_Builder_Modules();
        return $modules->page_builder_modules;

    }

    function get_standard_page_builer_test_with_all_layouts()
    {

        $acf = self::get_sample_module_acf();
        return [
            [
                'field_ispb_cell_repeater' => [
                    [
                        'field_ispb_cell_acf_fc_container' => $acf,
                        'field_ispb_cell_config_background_options' => [
                            'field_ispb_cell_config_background_options_custom_color' => '#00ff00',
                            'field_ispb_cell_config_background_options_color' => '#0000ff',
                            'field_ispb_cell_config_background_options_image' => $this->image->ID,
                            'field_ispb_cell_config_background_options_blending' => 'multiply',
                            'field_ispb_cell_config_background_options_opacity' => '.5',
                        ],
                        'field_ispb_cell_config_col_width' => [
                            'field_ispb_cell_config_col_width_mobile' => 12,
                            'field_ispb_cell_config_col_width_tablet' => 4,
                            'field_ispb_cell_config_col_width_desktop' => 4,
                            'field_ispb_cell_config_col_width_large_desktop' => '',
                        ],
                        'field_ispb_cell_config_vertical_self_alignment' => 'middle',
                    ],

                ],

                'field_ispb_row_config_background_options' => [
                    'field_ispb_row_config_background_options_custom_color' => '#003300',
                    'field_ispb_row_config_background_options_color' => '#0000cc',
                    'field_ispb_row_config_background_options_image' => $this->image->ID,
                    'field_ispb_row_config_background_options_blending' => 'overlay',
                    'field_ispb_row_config_background_options_opacity' => '.25',
                ],
                'field_ispb_row_dimensions' => [
                    'field_ispb_row_config_max_width_width' => '1200px',
                    'field_ispb_row_config_max_width_height' => '80%',
                ],
                'field_ispb_row_flex_align' => [
                    'field_ispb_row_flex_align_horizontal' => 'left',
                    'field_ispb_row_flex_align_vertical' => 'top',
                ],
                'field_ispb_row_spacers' => [
                    'field_ispb_row_spacers_top' => 'medium',
                    'field_ispb_row_spacers_bottom' => 'medium',
                ]

            ]


        ];
    }


    function get_cpt_meta_fields()
    {

        return [
            'name' => 'Name',
            'title' => 'Title',
            'image' => $this->image->ID,
            'embed' => "<iframe width='640' height='360' src='https://www.youtube.com/embed/bKLDOyYTXMo?feature=oembed' frameborder='0' gesture='media' allow='encrypted-media' allowfullscreen></iframe>",
            'file' => [
                'ID' => '8',
                'id' => '8',
                'title' => 'Jesper Reeh - CV',
                'filename' => 'Jesper-Reeh-CV.pdf',
                'url' => 'http://localhost:8888/wp-content/uploads/2017/12/Jesper-Reeh-CV.pdf',
                'alt' => '',
                'author' => '1',
                'description' => '',
                'caption' => '',
                'name' => 'jesper-reeh-cv',
                'date' => '2017-12-26 12:43:14',
                'modified' => '2017-12-26 12:43:14',
                'mime_type' => 'application/pdf',
                'type' => 'application',
                'icon' => 'http://localhost:8888/wp-includes/images/media/document.png',
            ],

            'identification' => 'Identification',
            'link' => [
                'title' => 'Link Text Here',
                'url' => 'http://google.com',
                'target' => '_blank',
            ],

            'subtitle' => 'Subtitle',
            'start_date' => '20/12/2017',
            'end_date' => '22/12/2017',
            'location' => [
                'address' => '485 Lonsdale St, Melbourne VIC 3000, Australia',
                'lat' => '-37.81388214027176',
                'lng' => '144.95824813842773',
            ],

            'address_line_1' => 'Address Line 1',
            'address_line_2' => 'Address Line 2',
            'author' => 'Author',
            'excerpt' => '<p>Post Excerpt</p>',

            'source' => 'Source',
            'hours' => 'Hours',
            'phone_number' => '1231231231',
            'email_address' => 'email@address.com',
            'fax_number' => '1231231232',
            'facebook' => 'http://facebook.com',
            'linkedin' => 'http://linkedin.com',
            'twitter' => 'http://twitter.com',
            'youtube' => 'http://youtube.com',
            'instagram' => 'http://instagram.com',
        ];


    }

}
