<?
/*
Plugin Name:  TSD InfiniSite
Plugin URI:   http://infinisite.wpengine.com/
Description:  TSD Page Builder Plugin
Version:      0.75
Author:       Top Shelf Digital
Author URI:   https://topshelfdesign.net/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

define( 'INFINISITE_DIR', plugins_url( '/', __FILE__ ) );
define( "INFINISITE_URI", plugin_dir_path( __FILE__ ) );
define( "THEME_URI", get_theme_file_path() . '/' );
define( "THEME_DIR", get_stylesheet_directory_uri() . '/' );
define( "PARENT_DIR", get_template_directory() . '/' );
define( "PARENT_URI", get_template_directory_uri() . '/' );

add_filter( 'qm/output/file_path_map', function( $map ) {
    $map['/nas/content/live/tauc'] = '/Users/gregg/Documents/projects/tauc-is';

    return $map;
} );

require INFINISITE_URI . 'assets/plugin-update-checker-4.8/plugin-update-checker.php';
$repo_url        = 'https://bitbucket.org/tsddevelopment/infinisite-source';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker( $repo_url, __FILE__, 'tsd-infinisite' );


$myUpdateChecker->setAuthentication( [ 'consumer_key'    => '7QEXa6JTCHkjqG3m6D',
                                       'consumer_secret' => 'SYT4yefeHykQVLFQZ8WmgqwfzbnfpYtx', ] );


// 1. customize ACF path
add_filter( 'acf/settings/path', 'my_acf_settings_path' );

function my_acf_settings_path( $path ) {
    // update path
    return INFINISITE_DIR . '/assets/plugins/acf/';
}

// Enable the option show in rest
add_filter( 'acf/rest_api/field_settings/show_in_rest', '__return_true' );

// Enable the option edit in rest
add_filter( 'acf/rest_api/field_settings/edit_in_rest', '__return_true' );

// 2. customize ACF dir
add_filter( 'acf/settings/dir', 'my_acf_settings_dir' );

function my_acf_settings_dir( $dir ) {
    // update path
    return INFINISITE_DIR . '/assets/plugins/acf/';
}


// 3. Hide ACF field group menu item
//add_filter( 'acf/settings/show_admin', '__return_false' );


// 4. Include Embedded Plugins
include_once( 'assets/plugins/acf/acf.php' );

$temp_wpb_flag = true;

if ( $temp_wpb_flag ):

    include_once( 'assets/plugins/js_composer/js_composer.php' );
    $wpb_installed = function_exists( 'vc_map' );
    define( "WPBAKERY_INSTALLED", $temp_wpb_flag );
endif;

// 5. Include Composer and our Classess
include_once( 'vendor/autoload.php' );
include_once( 'class/_loader.php' );

include_once( "assets/acme/shortcodes.php" );

use TSD_Infinisite\Boilerplate;
use TSD_Infinisite\AdminNotice;


// enable api functions

new TSD_Infinisite\API_V0();

//new \TSD_Infinisite\Shortcode();


// 6. Define our Constants
Boilerplate::define_constants();


function IS_Activation() {

    /**
     * PLUGIN ACTIVATION
     *
     * What should we be doing when the plugin is activated?
     *
     * Verifying integrity of header and footer templates
     * Verifying integrity of all post archive modules used on the site!
     *
     */

    // we need to get the palette and update the stylesheet
    // Acme::set_is_colo// r_palette();
    // $palette = new \TSD_Infinisite\Palette();
    // Data_Moves::cpt_from_repeater_to_option_sub_page();
    // Acme::update_is_stylesheets();
}


register_activation_hook( __FILE__, 'IS_Activation' );


add_action( "init",  'tsd_infinisite_init_cpt' );

function tsd_infinisite_init_cpt(){
    TSD_Infinisite\CPT::register_built_post_types();
}


add_action( "acf/init", 'tsd_infinisite_register_custom_options', 20 );

function tsd_infinisite_register_custom_options() {


    $page     = array_key_exists( 'page', $_GET ) ? $_GET['page'] : false;
    $admin    = is_admin();
    $frontend = ! $admin;


    $display_settings = $admin && 'is-display-settings' === $page;

    $register_sm_functions = $frontend || $page == 'acf-options-social-media';

    $register_header_settings = $page == 'acf-options-header';

    $cpt_sub_page = substr( $page, 0, 21 ) == 'acf-options-post-type';

    //    $register_taxonomy =


    if ( ! function_exists( 'acf_add_options_page' ) )
        return;


    TSD_Infinisite\CPT::register_post_type_builder();
    TSD_Infinisite\Option::register_acf_global_options_pages();

    if ( $admin ):

        // TODO: refactor these into their own objects

        TSD_Infinisite\Search::register_option_page_acf_group();
        TSD_Infinisite\Search::register_option_page_menu_link();
        TSD_Infinisite\Sidebar::register_global_sidebar_options();
    endif;
    //    TSD_Infinisite\Popup::register_global_popup_options();

    if ( $display_settings )
        TSD_Infinisite\Option::create_global_options();


    if ( ! $frontend ):
        // TODO: determine if/when/how these are required on the frontend
        TSD_Infinisite\Taxonomy_Builder::register_taxonomy_builder();


    endif;


    TSD_Infinisite\Taxonomy_Builder::register_built_taxonomies();

    TSD_Infinisite\Option::add_meta_fields_to_media_post_type();
    TSD_Infinisite\Option::register_image_thumbnail_sizes();


    TSD_Infinisite\Editor::remove_editor_from_page_and_post_type();

    TSD_Infinisite\Nav_Menu::register_nav_menu_item_meta_fields();


}

// add_filter( 'wp_nav_menu_objects', 'TSD_Infinisite\Nav_Menu::is_check_nav_items_for_custom_functionality', 10, 2 );


/**
 *
 * actions and filters - most of the plugin work is done by hooking
 * into the WP actions to create our customized functions
 *
 */

add_action( 'wp_enqueue_scripts', 'TSD_Infinisite\Boilerplate::enqueue_assets' );
add_action( 'wp_enqueue_scripts', 'TSD_Infinisite\Editor::enqueue_editor_styles' );
add_action( 'admin_enqueue_scripts', 'TSD_Infinisite\Boilerplate::admin_assets' );
add_action( 'admin_enqueue_scripts', 'is_bulk_tax_editor_load_scripts' );

add_action( 'after_setup_theme', 'TSD_Infinisite\Nav_Menu::register_menus' );

add_action( "admin_head-nav-menus.php", 'TSD_Infinisite\Nav_Menu::add_IS_menu_meta_boxes' );

add_action( "wp_enqueue_scripts", 'tsd_ininisite_deregister_scripts' );


function tsd_ininisite_deregister_scripts() {

    if ( is_admin() )
        return;

    // if "is login" page
    if ( in_array( $GLOBALS['pagenow'], [ 'wp-login.php', 'wp-register.php' ] ) )
        return;

    // holy plugin incompatability, batman!
    wp_deregister_script( 'google-map-api' );
    wp_deregister_script( 'google-infobox' );
    wp_deregister_script( 'datepicker' );
    wp_deregister_script( 'd3' );
    wp_deregister_script( 'd3-parse' );
    wp_deregister_script( 'd3plus-text' );

    wp_deregister_script( 'jquery' );
    wp_enqueue_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js', [], '2.2.4', 1 );


    wp_deregister_script( 'is_js' );
    wp_enqueue_script( 'is_js', INFINISITE_JS . '/app.js', [], false, 1 );

}

// Add the IS links to the top admin bar
add_action( 'admin_bar_menu', 'TSD_Infinisite\WP_Backend_Modification::add_IS_admin_toolbars', 100 );


// Add the IS stylesheets to the WYSIWYG editor
if ( is_admin() )
    add_action( 'admin_init', 'TSD_Infinisite\Editor::admin_init_fn' );

add_action( 'admin_notices', [ AdminNotice::getInstance(), 'displayAdminNotice' ] );


/**
 *
 * allow svg upload - this isn't the right way to do this,
 * but its all i've got for right now. we should be sanitizing
 *
 */

function is_mime_types( $mimes ) {
    $mimes['svg'] = 'image/svg+xml';
    $mime_types['json'] = 'application/javascript';


    return $mimes;
}

add_filter( 'upload_mimes', 'is_mime_types' );


/**
 * PLUGIN DEBUG PAGE AREA
 *
 * TODO: clean this up a bit.
 */

if ( isset( $_GET['is_debug'] ) ) {
    add_action( 'wp_loaded', function() {


        $debug = new \TSD_Infinisite\Debug();

        if ( $debug->is_active() ) {
            print $debug->get_output();
            die();
        }

    } );

}
/**
 *
 * Callback for SearchWP engine choice dropdown. Swaps out engine config for
 * chosen engine at runtime.
 *
 * @param $settings
 * @param $query
 *
 * @return mixed
 */
function my_searchwp_engine_dropdown_handler( $settings, $query ) {
    $engines         = SWP()->settings['engines'];
    $selected_engine = isset( $_GET['swpengine'] ) ? esc_attr( $_GET['swpengine'] ) : 'default';
    if ( isset( $engines[ $selected_engine ] ) ) {
        $settings = $engines[ $selected_engine ];
    }

    return $settings;
}

add_filter( 'searchwp_engine_settings_default', 'my_searchwp_engine_dropdown_handler', 10, 2 );

// TODO: refactor me!!


function my_acf_google_map_api( $api ) {

    $api['key'] = 'AIzaSyCT7y_yTozrvJeAmdcd3Q7EQfgXmu4LfHY';

    return $api;

}

add_filter( 'acf/fields/google_map/api', 'my_acf_google_map_api' );

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );


/**
 * this filter function is for ordering posts by their last name, it's used
 * by the post_archive class
 */


add_filter( 'posts_orderby', function( $orderby, \WP_Query $q ) {
    if ( 'wpse_last_word' === $q->get( 'orderby' ) && $get_order = $q->get( 'order' ) ) {
        if ( in_array( strtoupper( $get_order ), [ 'ASC', 'DESC' ] ) ) {
            global $wpdb;
            $orderby = " SUBSTRING_INDEX( {$wpdb->posts}.post_title, ' ', -1 ) " . $get_order;
        }
    }

    return $orderby;
}, PHP_INT_MAX, 2 );


/**
 * this allows us to put post excerpts in yoast seo
 */


function set_is_post_excerpt() {
    global $post;

    return strip_tags( get_field( "excerpt", $post->ID ) );
}

function is_yoast_seo_tag_update() {
    wpseo_register_var_replacement( '%%post_excerpt%%', 'set_is_post_excerpt', 'advanced', 'help text here' );
}

add_action( 'wpseo_register_extra_replacements', 'is_yoast_seo_tag_update' );


remove_post_type_support( 'product', [ 'editor' ] );


// disable gutenberg for posts
add_filter( 'use_block_editor_for_post', '__return_false', 10 );

// disable gutenberg for post types
add_filter( 'use_block_editor_for_post_type', '__return_false', 10 );


if ( WPBAKERY_INSTALLED )
    add_action( 'vc_before_init', '\TSD_Infinisite\WPB_Modules::create_modules' );


// ############

// include_once( 'components/shortcodes/is_social_media_posts.php' );

/**
 * # tsd_write_log()
 * Write text to a log file on the theme's directory
 */
if ( ! function_exists( 'tsd_write_log' ) ) {
    function tsd_write_log( $msg ) {
        $file = dirname( __FILE__ ) . '/tsd_log.log';

        $date = date( 'm/d/Y h:i:s a' );

        if ( is_array( $msg ) || is_object( $msg ) ) {
            $msg = print_r( $msg, true );
        }

        $msg = '[ ' . $date . ' ]: ' . $msg;
        $msg .= "\r\n\r\n";

        // Write the contents to the file,
        // using the FILE_APPEND flag to append the content to the end of the file
        // and the LOCK_EX flag to prevent anyone else writing to the file at the same time
        file_put_contents( $file, $msg, FILE_APPEND | LOCK_EX );
    }
}

add_action( 'wp_ajax_tsd_update_post_term', 'tsd_update_post_term' );
// add_action( 'wp_ajax_nopriv_tsd_update_post_term', 'tsd_update_post_term' ); // ---> Disable if the ajax should only be called with a user logged in.
function tsd_update_post_term() {

    if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {

        if ( ! check_ajax_referer( 'is-bulk-tax-editor-nonce', 'nonce' ) ) {
            wp_send_json_error( 'Invalid security token sent.' );
            die();
        }

        $result = [];

        $post_id  = isset( $_POST['pid'] ) ? $_POST['pid'] : 0;
        $tax_name = isset( $_POST['taxname'] ) ? $_POST['taxname'] : '';
        $term_id  = isset( $_POST['termid'] ) ? (int) $_POST['termid'] : 0;
        $status   = isset( $_POST['status'] ) ? (bool) $_POST['status'] : false;

        if ( term_exists( $term_id, $tax_name ) ) {
            if ( $status ) {
                $result = wp_set_object_terms( $post_id, $term_id, $tax_name, true );
            }
            else {
                $result = wp_remove_object_terms( $post_id, $term_id, $tax_name );
            }
        }

        $is_error = is_wp_error( $result );

        echo json_encode( [ 'status' => ! $is_error ? 'ok' : $result->get_error_message(),
                            'data'   => ! $is_error ? $result : '', ] );

    }

    die();

}

function is_bulk_tax_editor_load_scripts() {
    $screen = get_current_screen();

    if ( 'infinisite-options_page_is-bulk-tax-editor' == $screen->id ) {

        /* wp_enqueue_style( 'datatables-net', 'https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css' );
        wp_enqueue_script( 'datatables-net', 'https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js', array(), null, true  ); */

        /* wp_enqueue_style( 'datatables-checkbox', '//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css', array('datatables-net') );
        wp_enqueue_script( 'datatables-checkbox', '//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js', array('datatables-net'), null, true  ); */

        wp_enqueue_style( 'is_bulk_tax_editor', INFINISITE_DIR . '/assets/css/is_bulk_tax_editor.css' );
        wp_enqueue_script( 'is_bulk_tax_editor', INFINISITE_DIR . '/assets/js/is_bulk_tax_editor.js', [ 'jquery' ], null, true );
        wp_localize_script( 'is_bulk_tax_editor', 'is_bulk_tax_editor', [ 'ajax_url' => admin_url( 'admin-ajax.php' ),
                                                                          'nonce'    => wp_create_nonce( 'is-bulk-tax-editor-nonce' ), ] );
    }

}

add_action( 'admin_menu', function() {

    add_submenu_page( 'is-display-settings', 'Bulk Tax Editor', 'Bulk Tax Editor', 'manage_options', 'is-bulk-tax-editor', 'is_bulk_tax_editor' );

}, 99 );

function is_bulk_tax_editor() {

    $pt = \TSD_Infinisite\Acme::get_post_types();
    ?>
    <h1>Bulk Tax Editor</h1>
    <div class="wrap is-bulk-tax-editor-wrap">
        <table class="form-table" role="presentation">
            <tbody>
            <tr>
                <th scope="row">Select Post Types</th>
                <td>
                    <div class="row clearfix">
                        <div class="col col-4">
                            <select name="tax_editor_pt" multiple>
                                <?php
                                foreach ( $pt as $post_type ) {
                                    echo '<option value="' . $post_type['post_type'] . '">' . $post_type['name'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th scope="row">Taxonomy/Terms/Posts</th>
                <td>
                    <div id="tax_editor_tax_list"></div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <?php
}


add_action( 'wp_ajax_is_bulk_tax_editor_get_tax_and_posts', 'is_bulk_tax_editor_get_tax_and_posts' );
// add_action( 'wp_ajax_nopriv_is_bulk_tax_editor_get_tax', 'is_bulk_tax_editor_get_tax' ); // ---> Disable if the ajax should only be called with a user logged in.
function is_bulk_tax_editor_get_tax_and_posts() {

    if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {

        if ( ! check_ajax_referer( 'is-bulk-tax-editor-nonce', 'nonce' ) ) {
            wp_send_json_error( 'Invalid security token sent.' );
            die();
        }

        $result = [];

        $post_types = isset( $_POST['post_types'] ) ? $_POST['post_types'] : [];

        if ( $post_types ) {

            $post_type_ctr = 0;
            foreach ( $post_types as $post_type ) {

                $post_type_obj = get_post_type_object( $post_type );

                $result[] = [ 'type'       => $post_type_obj->name,
                              'label'      => $post_type_obj->labels->name,
                              'taxonomies' => [] ];

                $taxonomy_objects = get_object_taxonomies( $post_type, 'objects' );
                $taxonomy_list    = [];
                $tax_ctr          = 0;
                foreach ( $taxonomy_objects as $tax ) {

                    $taxonomy_list[] = $tax->name;

                    $terms = get_terms( [ 'taxonomy'   => $tax->name,
                                          'hide_empty' => false, ] );

                    $result[ $post_type_ctr ]['taxonomies'][] = [ 'label' => $tax->label,
                                                                  'name'  => $tax->name,
                                                                  'terms' => [], ];

                    if ( $terms && ! is_wp_error( $terms ) ) {
                        foreach ( $terms as $term ) {

                            $result[ $post_type_ctr ]['taxonomies'][ $tax_ctr ]['terms'][] = [ 'id'   => $term->term_id,
                                                                                               'slug' => $term->slug,
                                                                                               'name' => $term->name, ];

                        }
                    }
                    $tax_ctr ++;

                }

                $result[ $post_type_ctr ]['posts'] = is_get_posts_by_type( $post_type, $taxonomy_list );

                $post_type_ctr ++;
            }
        }

        echo json_encode( [ 'status' => 'ok',
                            'data'   => $result, ] );

    }

    die();

}

function is_get_posts_by_type( $post_type, $taxonomy_list ) {

    $result = [];

    if ( $post_type ) {

        // WP_Query arguments
        $args = [ 'post_type'      => [ $post_type ],
                  'post_status'    => [ 'publish' ],
                  'posts_per_page' => - 1, ];

        // The Query
        $posts = new WP_Query( $args );

        // The Loop
        if ( $posts->have_posts() ) {
            while ( $posts->have_posts() ) {
                $posts->the_post();

                $post_id = get_the_ID();

                $active_terms = [];
                // Get all the current term of the post

                foreach ( $taxonomy_list as $taxonomy ) {
                    if ( $post_terms = get_the_terms( $post_id, $taxonomy ) ) {
                        foreach ( $post_terms as $post_term ) {
                            $active_terms[] = $post_term->slug;
                        }
                    }
                }

                $result[] = [ 'id'           => get_the_ID(),
                              'title'        => get_the_title(),
                              'active_terms' => $active_terms, ];

            }
        }

        // Restore original Post Data
        wp_reset_postdata();

    }

    return $result;

}


// Allow SVG
add_filter( 'wp_check_filetype_and_ext', function( $data, $file, $filename, $mimes ) {

    global $wp_version;
    if ( $wp_version !== '4.7.1' ) {
        return $data;
    }

    $filetype = wp_check_filetype( $filename, $mimes );

    return [ 'ext'             => $filetype['ext'],
             'type'            => $filetype['type'],
             'proper_filename' => $data['proper_filename'] ];

}, 10, 4 );

function cc_mime_types( $mimes ) {
    $mimes['svg'] = 'image/svg+xml';

    return $mimes;
}

add_filter( 'upload_mimes', 'cc_mime_types' );


// this will allow the heartbeat to load, wpe seems to cause an error with query monitor

add_filter( 'wpe_heartbeat_allowed_pages', function( $pages ) {
    global $pagenow;
    $pages[] =  $pagenow;
    return $pages;
});
