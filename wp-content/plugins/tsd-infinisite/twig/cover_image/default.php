<div class="cover-image white-text">
    <img src="<?= $image['sizes']['large'] ?>" alt="" class="full_width">
    <div style="position: absolute; bottom: 10px; left: 10px; background: rgba(0,0,0,.8);">
        <?= $content ?>
    </div>
</div>