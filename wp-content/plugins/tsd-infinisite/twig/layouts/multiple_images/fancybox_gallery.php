<?
$images = $module['images'];

if (!$images) return;

$gallery_id = uniqid('afrm_past_events_gallery_');

$thumbnail_size = 'large';

$images_per_row = 3;

?>


<div class="grid-x small-up-1 medium-up-2 large-up-<?= $images_per_row ?> grid-padding-x grid-padding-y">

    <? foreach ($images as $c => $image): ?>
        <a
                class="cell small-12 medium-<?= $images_per_row ?>"
                href="<?= $image['sizes']['large'] ?>"
                data-fancybox="<?= $gallery_id ?>"
        >
            <img src="<?= $image['sizes'][$thumbnail_size] ?>"/>
        </a>

    <? endforeach ?>

</div>

<script>

    function init_gallery() {

        $(".afrm_past_events_gallery").owlCarousel({
            items: 1,
            loop: false,
            autoHeight: true
        });

    }

    $(function () {
        init_gallery();
    });
</script>