<?
$images = $module['images'];

if (!$images) return;

$gallery_id = uniqid('afrm_past_events_gallery_');

$thumbnail_size = 'large';

$images_per_row = 3;

?>

<div class="three_image_row_carousel_fancybox">
    <div class="owl-carousel space-bottom">
        <? foreach ($images as $c => $image): ?>
            <div class="slide">
                <a href="<?= $image['url'] ?>"
                   data-fancybox="<?= $gallery_id ?>"
                   class="slide_img"
                   style="background-image: url(<?= $image['sizes']['large'] ?>);"
                >
                </a>
            </div>
        <? endforeach ?>
    </div>
</div>

<script>

    function init_three_image_row_carousel_fancybox() {

        const galleries = jQuery(".three_image_row_carousel_fancybox");

        if (!galleries.length) return false;

        galleries.each(function () {

            const container = jQuery(this),
                gallery = container.find(".owl-carousel");

            gallery.owlCarousel({
                loop: true,
                margin: 12,
                stagePadding: 12,
                responsive: {
                    0: {
                        items: 1
                    },
                    800: {
                        items: 3
                    }

                }
            });

        });

    }


    jQuery(function () {
        init_three_image_row_carousel_fancybox();
    })

</script>