<div class="cell text-center">

    <? if ($post->get("image")): ?>
        <a href="<?= $post->permalink ?>">
            <img src="<?= $post->get("image")['sizes']['medium'] ?>" alt="">
        </a>
    <? endif ?>

    <h4><?= $post->get("name") ?></h4>
    <h5><?= $post->get("title") ?></h5>

</div>