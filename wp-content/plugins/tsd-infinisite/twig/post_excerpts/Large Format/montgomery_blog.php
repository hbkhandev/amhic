<div class="cell is-large-format-blog-excerpt">
    <div class="card">
        <div class="card-section">
            <h2><?= $post->get("title") ?></h2>

            <? if ($post->get("excerpt"))
                print $post->get("excerpt") ?>

            <div class="text-center">
                <a href="<?= $post->permalink ?>" class="button">Read More</a>
            </div>
        </div>
    </div>
</div>
