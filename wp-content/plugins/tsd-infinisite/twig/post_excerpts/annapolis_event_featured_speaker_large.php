<?
$post = new TSD_Infinisite\IS_Post($post);
$fields = $post->fields;
?>

<div class="cell is-annapolis annapolis_event_featured_speaker_large">

    <div class="grid-x grid-padding-x">
        <div class="cell small-8 medium-3">
            <? if ($fields->get("image")): ?>
                <img src="<?= $fields->get("image")['sizes']['large'] ?>" alt="" style="width: 100%; display: block;"/>
            <? endif ?>
        </div>

        <div class="cell small-auto medium-auto content">

            <h3 class="no-margin secondary-text"><?= $post->post_title ?></h3>

            <hr class="small">

            <div class="spacer xsmall"></div>
            <?= $fields->output("set", "h6") ?>
            <?= $fields->get("content") ?>
        </div>
    </div>
</div>
