<? $post = new \TSD_Infinisite\IS_Post($post) ?>
<? $link = $fields->get_link_html() ?>


<div class="cell">

    <div class="grid-x align-middle">
        <? if ($fields->get("image")): ?>
            <div class="grid small-12 medium-3">
                <?= $link[0] ?>
                <img src="<?= $fields->get("image")['sizes']['medium'] ?>" alt="">
                <?= $link[1] ?>
            </div>
        <? endif ?>

        <div class="cell small-12 medium-9">
            <div class="card">
                <div class="card-section">
                    <? if ($fields->get("start_date")): ?>
                        <h6><?= $fields->get_start_date()->format('F jS, Y') ?></h6>
                    <? endif ?>
                    <h2>
                        <?= $link[0] ?>
                        <?= $post->post_title ?>
                        <?= $link[1] ?>
                    </h2>

                    <?= $fields->get("excerpt") ?>

                    <a href="<?= $fields->get("url") ?>" class="arrow_link" target="blank">Read It Now</a>

                </div>
            </div>
        </div>

    </div>

</div>
