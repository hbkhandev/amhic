<? $post = new \TSD_Infinisite\IS_Post($post) ?>
<? $link = $fields->get_link_html() ?>


<div class="cell montgomery_news_excerpt_standard">

    <div class="card">
        <div class="card-section">
            <?= $link[0] ?>
            <img src="<?= $fields->get("image")['sizes']['medium'] ?>" alt="" class="image_center">
            <?= $link[1] ?>
            <? if ($fields->get("start_date")): ?>
                <h6><?= $fields->get_start_date()->format('F jS, Y') ?></h6>
            <? endif ?>
            <h3>
                <?= $link[0] ?>
                <?= $post->post_title ?>
                <?= $link[1] ?>
            </h3>

            <a href="<?= $fields->get("url") ?>" class="arrow_link" target="blank">Read It Now</a>

        </div>

    </div>

</div>
