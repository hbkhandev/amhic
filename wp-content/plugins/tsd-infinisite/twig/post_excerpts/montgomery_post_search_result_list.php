<? $post = new \TSD_Infinisite\IS_Post($post) ?>
<div class="cell montgomery_post_search_result_list">
    <h2><?= $post->post_title ?></h2>
    <?= $post->get("excerpt") ?>
    <? if ($post->permalink): ?>
        <a href="<?= $post->permalink ?>">Read More</a>
    <? endif ?>
</div>