<? $post = new \TSD_Infinisite\IS_Post($post) ?>


<div class="cell montgomery_news_excerpt_standard">

    <div class="grid-x grid-padding-x align-bottom">
        <div class="cell small-12 medium-auto">

            <? if ($fields->get("start_date")): ?>
                <h6 class="gray_light-text"><?= $fields->get_start_date()->format('F j, Y') ?></h6>
            <? endif ?>

            <h2 class="no-margin">
                <?= $post->post_title ?>
            </h2>
        </div>

        <div class="cell small-shrink medium-shrink">
            <? if ($fields->get("file")): ?>
                <p class="small no-margin">
                    <a href="<?= $fields->get("file")['url'] ?>" class="no_underline primary_xdark-text" target="blank">Download</a>
                </p>
            <? endif ?>
        </div>

        <div class="cell small-12">
            <hr>
        </div>
    </div>


</div>
