<div class="cell small-12 is_post_slider is_post_archive_query_module">
    <div class="owl-carousel is-website-slider-query-module">

        <? foreach ($this->query->posts as $post): ?>
            <?
            $fields = get_fields($post->ID);
            $image = $fields['image'];
            $excerpt = $fields['excerpt'];

            ?>
            <div class="grid-x grid-padding-x align-middle">

                <div class="cell auto">
                    <img src="<?= $image['sizes']['large'] ?>" alt="">
                </div>

                <div class="cell auto">
                    <h3><?= $post->post_title ?></h3>
                    <?= $excerpt ?>
                </div>
            </div>

        <? endforeach ?>

    </div>
</div>