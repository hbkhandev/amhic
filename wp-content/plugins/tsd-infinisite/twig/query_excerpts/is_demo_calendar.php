<?
$factory = new CalendR\Calendar;
$posts = $this->query->posts;

$event_collection = new \CalendR\Event\Provider\Basic();
$factory->getEventManager()->addProvider('event_collection', $event_collection);


$event_lookup = [];

foreach ($posts as $c => $post):

    $start_date = get_field("start_date", $post->ID);

    if (!$start_date) continue;

    $date = new DateTime($start_date);

    $event_lookup[$date->format("Y")][$date->format("n")][] = $post;

    $new_event = new \TSD_Infinisite\Calendr_Event($c, $date, $date);
    $new_event->post = $post;
    $new_event->fields = get_fields($post->ID);
    $event_collection->add($new_event);

endforeach;

// sorting the event lookup
foreach ($event_lookup as $year => $events)
    krsort($event_lookup[$year]);

krsort($event_lookup);

$html_id_tag = uniqid("slider_");


?>

<div class="cell">
    <? Kint::dump($this->query) ?>
    <? Kint::dump($factory) ?>
</div>


<div class="cell shrink">
    <? foreach ($posts as $post): ?>
        <? $timestamp = get_field("start_date", $post->ID) ?>
        <? $date = new DateTime($timestamp) ?>

        <p><?= $post->post_title ?> - <?= $date->format("F jS, Y") ?></p>
    <? endforeach ?>
</div>

<div class="is_montgomery is_montgomery_basic_event_calendar cell auto" id="<?= $html_id_tag ?>">

    <div class="is_demo_calendar_slider_year owl-carousel">
        <?php
        foreach ($event_lookup as $year => $months):
            ?>
            <div class="slide">
                <div class="grid-x align-middle">
                    <div class="cell shrink year-prev">
                        <i class="fas fa-arrow-alt-circle-left"></i>
                    </div>
                    <div class="cell auto text-center">
                        <h2><?= $year ?></h2>
                    </div>
                    <div class="cell shrink year-next">
                        <i class="fas fa-arrow-alt-circle-right"></i>
                    </div>
                </div>

                <div class="is_demo_calendar_slider_month owl-carousel">
                    <?
                    foreach ($months as $month_num => $events):
                        // Use the factory to get your period
                        $month = $factory->getMonth($year, $month_num);

                        ?>

                        <div class="slide">
                            <h4><?= $month ?></h4>

                            <div class="grid-x small-up-7" style="border: 1px solid gray">


                                <? $c = 0 ?>
                                <?php foreach ($month as $week): ?>

                                    <? if ($c == 0): ?>
                                        <?php foreach ($week as $day): ?>
                                            <div class="cell">
                                                <p class="day_name"><?= $day->getBegin()->format('D') ?> </p>
                                            </div>
                                        <?php endforeach ?>
                                    <? endif ?>


                                    <? $this_months_events = $factory->getEvents($month); ?>

                                    <?php foreach ($week as $day): ?>
                                        <div class="cell <?= !$month->includes($day) ? 'gray_xxlight-background' : '' ?>">
                                            <? if ($month->includes($day)): ?>
                                                <p>
                                                    <?= $day->getBegin()->format('j') ?>
                                                </p>

                                                <?php foreach ($this_months_events->find($day) as $daily_event): ?>
                                                    <p>
                                                        <?= $daily_event->post->post_title ?>
                                                    </p>
                                                <?php endforeach ?>
                                            <? endif ?>

                                        </div>
                                    <?php endforeach ?>
                                    <? $c++ ?>
                                <?php endforeach ?>

                            </div>

                            <div class="grid-x">
                                <div class="cell shrink month_prev">
                                    <p>previous</p>
                                </div>
                                <div class="cell auto">
                                </div>
                                <div class="cell shrink month_next">
                                    <p>next</p>
                                </div>
                            </div>

                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        <? endforeach; ?>

    </div>
</div>

<script>
    function init_calendar_slider() {

        var slider_container = $("#<?= $html_id_tag ?>");

        var year_slider = $("#<?= $html_id_tag ?> .is_demo_calendar_slider_year").owlCarousel({
            items: 1,
            touchDrag: false,
            mouseDrag: false,
        });


        var month_slider = slider_container.find(".is_demo_calendar_slider_month").owlCarousel({
            items: 1
        });

        var month_next = slider_container.find(".month_next");
        var month_prev = slider_container.find(".month_prev");

        var year_next = slider_container.find(".year_next");
        var year_prev = slider_container.find(".year_prev");

    }

    $(function () {
        init_calendar_slider();
    });

</script>