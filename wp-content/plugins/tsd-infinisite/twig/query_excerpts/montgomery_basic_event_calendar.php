<?
$factory = new CalendR\Calendar;
$posts = $this->query->posts;

$event_collection = new \CalendR\Event\Provider\Basic();
$factory->getEventManager()->addProvider('event_collection', $event_collection);


$event_lookup = [];

foreach ($posts as $c => $post):

    $start_date = get_field("start_date", $post->ID);

    if (!$start_date) continue;

    $date = new DateTime($start_date);

    $event_lookup[$date->format("Y")][$date->format("n")][] = $post;

    $new_event = new \TSD_Infinisite\Calendr_Event($c, $date, $date);
    $new_event->post = $post;
    $new_event->fields = get_fields($post->ID);
    $event_collection->add($new_event);

endforeach;

// sorting the event lookup
foreach ($event_lookup as $year => $events)
    krsort($event_lookup[$year]);

krsort($event_lookup);

$html_id_tag = uniqid("slider_");

$years = array_keys($event_lookup);

?>

<div class="is_montgomery is_montgomery_basic_event_calendar cell small-12 medium-auto" id="<?= $html_id_tag ?>">

    <div class="grid-x align-middle">
        <div class="cell shrink year_prev">
            <i class="fas fa-arrow-alt-circle-left"></i>
        </div>
        <div class="cell auto slider-title text-center">
            <h3><?= $years[0] ?></h3>
        </div>
        <div class="cell shrink year_next">
            <i class="fas fa-arrow-alt-circle-right"></i>
        </div>
    </div>


    <div class="is_demo_calendar_slider_year owl-carousel">


        <?php
        foreach ($event_lookup as $year => $months):
            ?>

            <div class="slide year_slide" data-year="<?= $year ?>">
                <div class="is_demo_calendar_slider_month owl-carousel">


                    <?
                    foreach ($months as $month_num => $events):
                        // Use the factory to get your period
                        $month = $factory->getMonth($year, $month_num);

                        ?>

                        <div class="slide month_slide">


                            <div class="grid-x month-pager align-middle">
                                <div class="cell small-3 month_prev">
                                    <h6 class="no-margin">&laquo; Next</h6>
                                </div>
                                <div class="cell auto text-center">
                                    <h4 class="no-margin"><?= $month ?></h4>
                                </div>
                                <div class="cell small-3 month_next text-right">
                                    <h6 class="no-margin">Previous &raquo;</h6>
                                </div>
                            </div>


                            <div class="grid-x small-up-7" style="border: 1px solid gray">


                                <? $c = 0 ?>
                                <?php foreach ($month as $week): ?>

                                    <? if ($c == 0): ?>
                                        <?php foreach ($week as $day): ?>
                                            <div class="cell">
                                                <p class="day_name"><?= $day->getBegin()->format('D') ?> </p>
                                            </div>
                                        <?php endforeach ?>
                                    <? endif ?>


                                    <? $this_months_events = $factory->getEvents($month); ?>

                                    <?php foreach ($week as $day): ?>
                                        <div class="cell <?= !$month->includes($day) ? 'gray_xxlight-background' : '' ?>">
                                            <? if ($month->includes($day)): ?>
                                                <p>
                                                    <?= $day->getBegin()->format('j') ?>
                                                </p>

                                                <?php foreach ($this_months_events->find($day) as $daily_event): ?>
                                                    <p>
                                                        <?= $daily_event->post->post_title ?>
                                                    </p>
                                                <?php endforeach ?>
                                            <? endif ?>

                                        </div>
                                    <?php endforeach ?>
                                    <? $c++ ?>
                                <?php endforeach ?>

                            </div>

                        </div>
                    <? endforeach; ?>


                </div>
            </div>
        <? endforeach; ?>
    </div>

</div>

<script>
    function init_calendar_slider() {

        var slider_container = $("#<?= $html_id_tag ?>");

        var slider_title = slider_container.find(".slider-title");

        var year_slider = slider_container.find(".is_demo_calendar_slider_year");

        var year_next = slider_container.find(".year_next");
        var year_prev = slider_container.find(".year_prev");

        var month_sliders = slider_container.find(".is_demo_calendar_slider_month");

        var months_next = month_sliders.find(".month_next");
        var months_prev = month_sliders.find(".month_prev");


        year_slider.owlCarousel({
            items: 1,
            touchDrag: false,
            mouseDrag: false
        });


        month_sliders.owlCarousel({
            items: 1
        });

        stopOwlPropagation(month_sliders);

        year_next.click(function () {
            year_slider.trigger("next.owl.carousel");
        });
        year_prev.click(function () {
            year_slider.trigger("prev.owl.carousel");
        });

        months_next.click(function (e) {
            var this_months_calendar = $(this).parents(".is_demo_calendar_slider_month");
            this_months_calendar.trigger("next.owl.carousel");
        });

        months_prev.click(function (e) {
            var this_months_calendar = $(this).parents(".is_demo_calendar_slider_month");
            this_months_calendar.trigger("prev.owl.carousel");
        });


        year_slider.on("translated.owl.carousel", function (event) {

            var element = $(event.target);
            var current_slide = element.find("> .owl-stage-outer > .owl-stage > .owl-item.active > .year_slide");
            var current_year = current_slide.attr('data-year');
            slider_title.find("h3").text(current_year);

        });


    }

    $(function () {
        init_calendar_slider();
    });

    function stopOwlPropagation(element) {
        $(element).on('to.owl.carousel', function (e) {
            e.stopPropagation();
        });
        $(element).on('next.owl.carousel', function (e) {
            e.stopPropagation();
        });
        $(element).on('prev.owl.carousel', function (e) {
            e.stopPropagation();
        });
        $(element).on('destroy.owl.carousel', function (e) {
            e.stopPropagation();
        });
    }


</script>