<?php
$calendar = new \TSD_Infinisite\Calendar($this->query->posts);

$map = new \TSD_Infinisite\Google_Map([
    'height' => '600px'
]);

foreach ($this->query->posts as $post):
    $fields = new \TSD_Infinisite\ACF_Helper($post);
    $map->add_marker($fields->get("location"));
endforeach;

?>

<div class="cell">
    <h1>Advanced API Test</h1>

    <div class="grid-x grid-padding-x">
        <div class="cell small-12 medium-4 large-shrink">
            <?php foreach ($this->query->posts as $post): ?>
                <? $fields = new \TSD_Infinisite\ACF_Helper($post); ?>
                <h2><?= $post->post_title ?></h2>
                <p><?= $fields->formatted_date_range() ?></p>
            <? endforeach; ?>
        </div>
        <div class="cell small-12 medium-4 large-auto">
            <?= $map->get_html(); ?>
        </div>
        <div class="cell small-12 medium-4 large-auto">
            <?= $calendar->get_output('month') ?>
        </div>
    </div>


</div>