<?
$map = new \TSD_Infinisite\Google_Map([
    'height' => '600px'
]);

foreach ($this->query->posts as $post):
    $fields = new \TSD_Infinisite\ACF_Helper($post);
    $map->add_marker($fields->get("location"));
endforeach;

?>

<div class="cell">
    <?= $map->get_html(); ?>
</div>