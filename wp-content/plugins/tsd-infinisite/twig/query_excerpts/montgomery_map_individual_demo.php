<div class="cell">


    <div class="grid-x small-up-1 medium-up-2 grid-padding-x">

        <?php foreach ($this->query->posts as $post): ?>
            <? $fields = new \TSD_Infinisite\ACF_Helper($post); ?>
            <div class="cell">
                <h2><?= $post->post_title ?></h2>
                <p><?= $fields->formatted_date_range() ?></p>
                <?= $fields->get_basic_map() ?>
            </div>
        <? endforeach; ?>
    </div>

</div>
