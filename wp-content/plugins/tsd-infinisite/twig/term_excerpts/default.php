<div class="card">
    <div class="card-section">
        <h3><?= $term->name ?></h3>
        <?= wpautop( $term->description ) ?>
    </div>
</div>
