$(window).load(function () {
    let c = $(".tsd-project-carousel");
    if (!c.length) return;
    const myCarousel = new Carousel({
        container: document.querySelector('.tsd-project-carousel'),
        items: document.querySelectorAll('.slider__item'),
        displayControls: true,
        controlsContainer: document.querySelector('.slider__controls'),
        textControls: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
        autoplay: true,
        autoplayTime: 3500
    });
});
