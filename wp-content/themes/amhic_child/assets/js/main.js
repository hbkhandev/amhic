jQuery(function (){
    jQuery(window).scroll(function() {
        var scroll = jQuery(window).scrollTop();
        if (scroll >= 150) {
            jQuery("nav").addClass("active");
        } else {
            jQuery("nav").removeClass("active");
        }
    });
});