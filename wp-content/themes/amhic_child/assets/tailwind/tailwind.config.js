// tailwind.config.js
module.exports = {
    important: true,
    // mode: 'jit', // Just-In-Time Compiler
    // purge: {
    //     enabled: false,
    //     content: [
    //         '*.php',
    //         '././template-parts/**/*.php',
    //         // doesn't work
    //         './././././plugins/tsd-infinisite/assets/plugins/js_composer/include/classes/shortcodes/paginator/class-vc-pageable.php',
    //     ],
    // },
    theme: {
        extend: {
            colors: {
                transparent: 'transparent',
                white: '#FFFFFF',
                gray: {
                    100: '#f4f4f4',
                    200: '#B8B8B8',
                    300: '#F9F9F9',
                    400: '#707070',
                    700: '#333333',
                    800: '#2D2D2E',
                    900: '#1F1F1F',
                },
                red: {
                    500: '#E50019',
                    700: '#C32026',
                    800: '#7B1010',
                    'DEFAULT': '#E50019',
                    'dark': '#C32026'
                },
                yellow: {
                    500: '#FFF67B'
                },
                green: {
                    600: '#186C08'
                },
                blue: {
                    100: '#e0f3f9',
                    200: '#B8DDFD',
                    300: '#2BB1D9',
                    500: '#3250C7',
                    600: '#24398E',
                    700: '#1B2C6D',
                    800: '#173064',
                    'light': '#2bb1d9',
                    'dark': '#1B2C6D'
                },
                '24398E': '#24398E',
                'FFFFFF': '#FFFFFF',
                '173064': '#173064',
                'C32026': '#C32026',
                '707070': '#707070',
                '3250C7': '#3250C7',
                'F9F9F9': '#F9F9F9',
                'E8ECFA': '#E8ECFA',
                'B8B8B8': '#B8B8B8',
                'F7F7F7': '#F7F7F7',
                '186C08': '#186C08',
                '2BB1D9': '#2BB1D9',
                'F7FAFC': '#F7FAFC',
                'B8DDFD': '#B8DDFD',
                'E50019': '#E50019',
                '1F1F1F': '#1F1F1F',
                '003E92': '#003E92',
            },
            fontFamily: {
                'sans': ['"proxima-nova",sans-serif']
            },
            boxShadow: {
                amhshadow1: '3px 3px 9px #00000029',
                amhshadow2: '0px 3px 6px #00000029'
            },
            minHeight: {
                '0': '0',

                'md': '300px',
                'lg': '400px',
                'xl': '500px',
                '2xl': '600px',
                'full': '100%',
            }
        },
    },
    darkMode: false, // or 'media' or 'class'
    variants: {},
    plugins: [],
};
