<footer class="white-text-children pt-5 pb-4">
    <div class="grid-container no-padding">
        <div class="row">
            <div class="spacer small"></div>
        </div>
        <div class="grid-container">
            <div class="grid-x">
                <div class="cell">
                    <? if ( $primary_content ): ?>
                        <div class="cell auto">
                            <div class="secondary-content">
                                <?= $primary_content ?>
                            </div>
                        </div>
                    <? endif ?>

                </div>
            </div>
            <div id="footer-bottom" class="grid-x align-middle">

                <? if ( $secondary_content ): ?>
                    <div class="cell shrink content pr-5">
                        <?= $secondary_content ?>
                    </div>
                <? endif ?>

                <div class="cell auto">
                    <? if ( $primary_menu ): ?>
                        <div class="primary-menu-area">
                            <? wp_nav_menu( [ 'menu'       => $primary_menu,
                                              'menu_class' => 'dropdown menu white-text-children align-right',
                                              'items_wrap' => '<ul id="%1$s" class="%2$s" data-dropdown-menu>%3$s</ul>', ] ) ?>
                        </div>
                    <? endif ?>
                </div>


            </div>
        </div>
        <div class="row">
            <div class="spacer small"></div>
        </div>
    </div>
</footer>
