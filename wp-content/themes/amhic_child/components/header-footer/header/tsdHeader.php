<?php
if ( $primary_menu ):
    $header_args = [ 'menu'       => $primary_menu,
                     'menu_class' => 'dropdown menu',
                     'echo'       => false,
                     'items_wrap' => '<ul id="%1$s" class="%2$s align-right" data-dropdown-menu>%3$s</ul>',
                     'walker'     => new TSD_Infinisite\top_menu_bar_walker() ];
    $header_menu = wp_nav_menu( $header_args );
endif;
?>

<div class="bg-red-500 grid-container is-standard-desktop-header py-4" is-editor-style>
    <div class="grid-x medium-header align-middle">

        <? if ( $img ): ?>
            <div class="cell logo">
                <a href="<?= site_url() ?>">
                    <img src="<?= $img['sizes']['thumbnail'] ?>" alt="Header Logo" />
                </a>
            </div>
        <? endif ?>


        <div class="cell shrink menus">
            <? if ( $primary_menu ): ?>
                <div class="cell primary-menu-area">
                    <?= $header_menu ?>
                </div>
            <? endif ?>
        </div>

    </div>
</div>
