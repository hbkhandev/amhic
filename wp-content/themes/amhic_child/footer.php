<!-- Parallax -->
<!-- <section class="_parallax h-[670px] <?php if(is_page_template('template-parts/temp-home.php')){ echo 'mt-[-150px]'; } ?>" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/component-13–1.png"> -->
    <!-- Empty section for image. -->
<!-- </section> -->
<!-- Footer -->
<footer>
    <div class="container mx-auto relative z-10">
        <div class="footer-logo text-center mb-[70px]">
            <a href="<?php echo home_url(); ?>" class="block text-center">
                <img class="m-auto" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/footer-logo.png">
            </a>
        </div>
        <h3 class="footer-heading">The benefits choice for education, research, and public service nonprofit organizations</h3>
        <div class="footer-cols">
            <div class="footer-copyright-text">
                <img class="m-auto" src="<?php echo get_stylesheet_directory_uri();?>/assets/img/before-copyright.png">
                <p class="col-span-11 relative top-[8px]">© Copyright 2021 AMHIC. All Rights Reserved.<br/> Site created by Top Shelf Design</p>
            </div>
            <div class="footer-nav">
                <div class="_holder pt-[20px]">
                    <ul class="desktop-footer-nav">
                        <!-- <li><a href="#">GLOSSARY</a> </li> -->
                        <li class=""><a href="http://amhic.local/site-map/">SITE MAP</a> </li>
                        <li><a href="http://amhic.local/terms/">TERMS</a> </li>
                        <li><a href="http://amhic.local/privacy/">PRIVACY</a> </li>
                        <li>
                            <a href="#" target="_blank">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/linkedin.png" alt="" />
                            </a>
                        </li>
                    </ul>
                    <ul class="mobile-footer-nav md:hidden">
                        <!-- <li><a href="#">GLOSSARY</a> </li> -->
                        <li><a href="http://amhic.local/terms/">TERMS</a> </li>
                        <li><a href="http://amhic.local/privacy/">PRIVACY</a> </li>
                        <li class=""><a href="http://amhic.local/site-map/">SITE MAP</a> </li>
                        <li>
                            <a href="#" target="_blank">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/linkedin.png" alt="" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>