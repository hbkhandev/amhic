<? include_once( "class/_loader.php" );
function tsd_load_scripts() {
    wp_enqueue_style('tsd_slick', get_stylesheet_directory_uri() . '/assets/css/slick.css', '', '', '');
    wp_enqueue_style('tsd_slick_theme', get_stylesheet_directory_uri() . '/assets/css/slick-theme.css', '', '', '');
    wp_enqueue_script('tsd_jquery', get_stylesheet_directory_uri() . '/assets/js/jquery-3.6.0.min.js', '', '', true  );
    wp_enqueue_script('parallax', get_stylesheet_directory_uri() . '/assets/js/libs/parallax.min.js', array('jquery'), '', true  );
    wp_enqueue_script('main', get_stylesheet_directory_uri() . '/assets/js/main.js', array('jquery'), '', true  );
    wp_enqueue_script('tsd_slick_script', get_stylesheet_directory_uri() . '/assets/js/slick.min.js', array('jquery'), null, true  );
} // Load scripts in footer
add_action('wp_enqueue_scripts', 'tsd_load_scripts');
/**
 * Slick Slider Footer Script
 */
function slick_slider_footer_script(){
    ?>
    <script>
        jQuery(document).ready(function($){

            jQuery('.partner_slider').slick({
                dots: false,
                infinite: true,
                speed: 500,
                arrows: true,
            });

            jQuery('.home_slider').slick({
                dots: false,
                vertical: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                verticalSwiping: true,
                nextArrow: $('.previous-arrow-up'),
                prevArrow: $('.next-arrow-down'),
            });

            jQuery('.testimonials_slider').slick({
                dots: false,
                infinite: true,
                speed: 500,
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                nextArrow: $('.testimonial-next-arrow'),
                prevArrow: $('.testimonial-previous-arrow'),
            });

            jQuery('.home_slider_temp').slick({
                autoplay: false,
                dots: false,
                vertical: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                verticalSwiping: true,
                draggable: false,
                nextArrow: $('.previous-arrow-up'),
                prevArrow: $('.next-arrow-down'),
            });
            
            jQuery('.home_slider_left').slick({
                autoplay: false,
                dots: false,
                infinite: true,
                speed: 500,
                draggable: false,
            });

            jQuery('.next-arrow-down').on('click', function(e){
                jQuery('.home_slider_left .slick-next').trigger('click');
            });

            jQuery('.previous-arrow-up').on('click', function(e){
                jQuery('.home_slider_left .slick-prev').trigger('click');
            });
            
        });
    </script>
    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();
        /**
         * Responsive Navbar
         */
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>
    <?php
}
add_action('wp_footer', 'slick_slider_footer_script');
/**
 * About Page Partner Slider
 * [partner_slider]
 */
function partner_slider_func( $atts ){
    $html = '';
    /**
     * Get Partners
     */
    $args = array(
        'post_type' => 'staff',
        'tax_query' => array(
            array(
                'taxonomy' => 'staff_type',
                'field'    => 'slug',
                'terms'    => 'partner',
            ),
        ),
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'date'
    );
    $posts_array = get_posts($args);
    if($posts_array){
        $counter = 1;
        $html .= '<div class="partner_slider">';
        foreach ($posts_array as $post) : setup_postdata($post);
            if($counter%4 == 1){
                $html .= '<div>';
                    $html .= '<div class="partner-box">';
            }
            $html .= '<div class="partner-box-img"><img src="'.get_the_post_thumbnail_url($post->ID).'"><div class="partner-box-name">'.$post->post_title.'</div></div>';
            if($counter%4 == 0){
                    $html .= '</div>';
                $html .= '</div>';
            }
            $counter++;
        endforeach;
        $html .= '</div>';
    }
    return $html;
}
add_shortcode( 'partner_slider', 'partner_slider_func' );
/**
 * About Page Partner Slider Responsive
 * [partner_slider_responsive]
 */
function partner_slider_responsive_func( $atts ){
    $html = '';
    /**
     * Get Partners
     */
    $args = array(
        'post_type' => 'staff',
        'tax_query' => array(
            array(
                'taxonomy' => 'staff_type',
                'field'    => 'slug',
                'terms'    => 'partner',
            ),
        ),
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'date'
    );
    $posts_array = get_posts($args);
    if($posts_array){
        $html .= '<div class="partner_slider partner_slider_responsive">';
        foreach ($posts_array as $post) : setup_postdata($post);
            $html .= '<div>';
                $html .= '<div class="partner-box">';
                    $html .= '<div class="partner-box-img"><img src="'.get_the_post_thumbnail_url($post->ID).'"><div class="partner-box-name">'.$post->post_title.'</div></div>';
                $html .= '</div>';
            $html .= '</div>';            
        endforeach;
        $html .= '</div>';
    }
    return $html;
}
add_shortcode( 'partner_slider_responsive', 'partner_slider_responsive_func' );
/**
 * Home Page Vertical Slider
 * [home_slider]
 */
function home_slider_func( $atts ){
    $html = '';
    $html .= '<div class="home-arrow previous-arrow-up"><span>Previous</span></div>';
    $html .= '<div class="home_slider">';
    $html .= '<div class="home_slider_single"><div><p>Providing employers and their employees with high-quality and valued benefit programs.</p></div></div>';
    $html .= '<div class="home_slider_single"><div><p>Recognizing that "one size does not fit all" and that a benefits portfolio that offers choice and flexibility will best serve employees of participating organizations.</p></div></div>';
    $html .= '<div class="home_slider_single"><div><p>Embracing the diversity of employees of our participating organizations and provide benefits with tolerance for diverse religious, social, and cultural values.</p></div></div>';
    $html .= '<div class="home_slider_single"><div><p>Providing employers and their employees with high-quality and valued benefit programs.</p></div></div>';
    $html .= '<div class="home_slider_single"><div><p>Recognizing that "one size does not fit all" and that a benefits portfolio that offers choice and flexibility will best serve employees of participating organizations.</p></div></div>';
    $html .= '<div class="home_slider_single"><div><p>Embracing the diversity of employees of our participating organizations and provide benefits with tolerance for diverse religious, social, and cultural values.</p></div></div>';
    $html .= '</div>';
    $html .= '<div class="home-arrow next-arrow-down"><span>Next</span></div>';
    return $html;
}
add_shortcode( 'home_slider', 'home_slider_func' );
/**
 * Testimonials Slider
 * [testimonials_slider]
 */
function testimonials_slider_func( $atts ){
    $html = '';
    /**
     * Fetching Testimonials 
     */
    $args = array(
        'post_type' => 'testimonials',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'date'
    );
    $posts_array = get_posts($args);
    if($posts_array){
        $html .='<div class="testimonail-slider-container">';
            $html .= '<div class="testimonial-arrow testimonial-previous-arrow"><span>Previous</span></div>';
                $html .= '<div class="testimonials_slider">';
                    foreach ($posts_array as $post) : setup_postdata($post);
                        $testimonial_meta = get_post_meta($post->ID);   
                        $html .= '<div class="testimonials_slider_single">';
                            $html .= '<div class="testimonials_slider_single_content"><p>'.$testimonial_meta['content'][0].'</p></div>';
                            $html .= '<div class="testimonials_slider_single_meta">';
                                $html .= '<h3>'.$post->post_title.'</h3>';
                                $html .= '<h4>'.$testimonial_meta['bio'][0].'</h4>';
                            $html .= '</div>';
                        $html .= '</div>';
                    endforeach;
                $html .= '</div>';
            $html .= '<div class="testimonial-arrow testimonial-next-arrow"><span>Next</span></div>';
        $html .= '</div>';
    }
    return $html;
}
add_shortcode( 'testimonials_slider', 'testimonials_slider_func' );
/**
 * Testimonials Simple
 * [testimonials_simple]
 */
function testimonials_simple_func( $atts ){
    $html = '';
    /**
     * Fetching Testimonials 
     */
    $args = array(
        'post_type' => 'testimonials',
        'post_status' => 'publish',
        'posts_per_page' => 8,
        'order' => 'ASC',
        'orderby' => 'date'
    );
    $posts_array = get_posts($args);
    if($posts_array){
        $html .= '<div class="testimonial-simple-container grid grid-cols-4 gap-4">';
            foreach ($posts_array as $post) : setup_postdata($post);
                $testimonial_meta = get_post_meta($post->ID); 
                $html .= '<div class="testimonial_simple_box">';  
                    $html .= '<div class="testimonials_slider_single_meta">';
                        $html .= '<h3>'.$post->post_title.'</h3>';
                        $html .= '<h4>'.$testimonial_meta['bio'][0].'</h4>';
                    $html .= '</div>';
                $html .= '</div>';
            endforeach;
        $html .= '</div>';
    }
    return $html;
}
add_shortcode( 'testimonials_simple', 'testimonials_simple_func' );
/**
 * Events
 * [events] 
 */
function events_func( $atts ){
    $html = '';
    /**
     * Events Post Type
     */
    $args = array(
        'post_type' => 'events',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'date'
    );
    $posts_array = get_posts($args);
    if($posts_array){
        foreach ($posts_array as $post) : setup_postdata($post);
            $testimonial_meta = get_post_meta($post->ID); 
            $strint_date = $testimonial_meta['start_date'][0];
            $strint_date = strtotime($strint_date);
            $newformat = date('M d', $strint_date);
            $time = $testimonial_meta['time'][0];
            $content = $testimonial_meta['content'][0];
            $register_link = $testimonial_meta['register_link'][0];
            $html .= '<div class="events-box">';
                $html .= '<h3>'.$newformat.'</h3>';
                $html .= '<h3>'.$time.'</h3>';
                $html .= '<h3 class="title">'.$post->post_title.'</h3>';
                $html .= '<p>'.$content.'</p>';
                $html .= '<a class="link" href="'.$register_link.'">Register</a>';
            $html .= '</div>';
        endforeach;
    }
	return $html;
}
add_shortcode( 'events', 'events_func' );
/**
 * NAV Walker Class 
 */
class CSS_Menu_Walker extends Walker {

	var $db_fields = array('parent' => 'menu_item_parent', 'id' => 'db_id');
	
	function start_lvl(&$output, $depth = 0, $args = array()) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul>\n";
	}
	
	function end_lvl(&$output, $depth = 0, $args = array()) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
	}
	
	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
	
		global $wp_query;
		$indent = ($depth) ? str_repeat("\t", $depth) : '';
		$class_names = $value = '';
		$classes = empty($item->classes) ? array() : (array) $item->classes;
		
		/* Add active class */
		if (in_array('current-menu-item', $classes)) {
			$classes[] = 'active';
			unset($classes['current-menu-item']);
		}
		
		/* Check for children */
		$children = get_posts(array('post_type' => 'nav_menu_item', 'nopaging' => true, 'numberposts' => 1, 'meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $item->ID));
		if (!empty($children)) {
			$classes[] = 'has-sub';
		}
		
		$class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
		$class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';
		
		$id = apply_filters('nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args);
		$id = $id ? ' id="' . esc_attr($id) . '"' : '';
		
		$output .= $indent . '<li' . $id . $value . $class_names .'>';
		
		$attributes  = ! empty($item->attr_title) ? ' title="'  . esc_attr($item->attr_title) .'"' : '';
		$attributes .= ! empty($item->target)     ? ' target="' . esc_attr($item->target    ) .'"' : '';
		$attributes .= ! empty($item->xfn)        ? ' rel="'    . esc_attr($item->xfn       ) .'"' : '';
		$attributes .= ! empty($item->url)        ? ' href="'   . esc_attr($item->url       ) .'"' : '';
		
		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'><span>';
		$item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
		$item_output .= '</span></a>';
		$item_output .= $args->after;
		
		$output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
	}
	
	function end_el(&$output, $item, $depth = 0, $args = array()) {
		$output .= "</li>\n";
	}
}
/**
 * Home Page slider
 * [temp_slider]
 */
function home_slider_func_temp( $atts ){
    $html = '';
    $html .= '<div class="grid grid-cols-12">';
        // Left Column
        $html .= '<div class="col-span-5 px-4">';
            if( have_rows('core_values_slider', 314) ):
                // $html .= '<div class="home-arrow previous-arrow-up"><span>Previous</span></div>';
                $html .= '<div class="home_slider_left">';
                    // Loop through rows.
                    while( have_rows('core_values_slider', 314) ) : the_row();
                        $title = get_sub_field('title');
                        $tag = get_sub_field('tag');
                        $icon = get_sub_field('icon');
                            $html .= '<div class="home_slider_left_single"><div><h3 class="heading_29_36_ffffff" style="color: #fff; ">'.$title.'</h3><img src="'.$icon.'"><h2 style="color: #fff; ">'.$tag.'</h2></div></div>';
                    // End loop.
                    endwhile;
                $html .= '</div>';
                // $html .= '<div class="home-arrow next-arrow-down"><span>Next</span></div>';
            
            // No value.
            else :
                // Do something...
            endif;
        $html .= '</div>';
        // Right Column
        $html .= '<div class="col-span-7 px-4">';
            if( have_rows('core_values_slider', 314) ):
                $html .= '<div class="home-arrow previous-arrow-up"><span>Previous</span></div>';
                $html .= '<div class="home_slider_temp">';
                    // Loop through rows.
                    while( have_rows('core_values_slider', 314) ) : the_row();
                        // $title = get_sub_field('title');
                        // $tag = get_sub_field('tag');
                        $description = get_sub_field('description');
                        // $icon = get_sub_field('icon');
                            $html .= '<div class="home_slider_single"><div><p>'.$description.'</p></div></div>';
                    // End loop.
                    endwhile;
                $html .= '</div>';
                $html .= '<div class="home-arrow next-arrow-down"><span>Next</span></div>';
            
            // No value.
            else :
                // Do something...
            endif;
        $html .= '</div>';
    $html .= '</div>';
    return $html;
}
add_shortcode( 'temp_slider', 'home_slider_func_temp' );
/**
 * Employer Options
 * [employer_options]
 */
function employer_options_func( $atts ){
    $a = shortcode_atts( array(
		'page_id' => 'something'
	), $atts );
    $page_ID = $a['page_id'];
    /**
     * HTML
     */
    $html = '';
    if( have_rows('employer_options', $page_ID) ):
        $html .= '<div class="employer-options-box">';
            $html .= '<div class="heading"><h2>Employer Options</h2></div>';
            while( have_rows('employer_options', $page_ID) ) : the_row();
        
                // Get parent value.
                $title = get_sub_field('title');
                $html .= '<div class="sub-heading"><h2>'.$title.'</h2></div>';
                // Loop over sub repeater rows.
                if( have_rows('sub_options') ):
                    while( have_rows('sub_options') ) : the_row();
        
                        // Get sub value.
                        $heading = get_sub_field('heading');
                        $description = get_sub_field('description');
        
                        $html .= '<div class="sub-box-container">';
                            $html .= '<div class="sub-box-cols sub-box-col-left">';
                                $html .= '<div class="heading-container">';
                                    $html .= '<div class="heading-text">';
                                        $html .= $heading;
                                    $html .= '</div>';
                                $html .= '</div>';
                            $html .= '</div>';
                            $html .= '<div class="sub-box-cols sub-box-col-right">';
                                $html .= $description;
                            $html .= '</div>';
                        $html .= '</div>';

                    endwhile;
                endif;
            endwhile;
        $html .= '</div>';
    endif;

    return $html;
}
add_shortcode( 'employer_options', 'employer_options_func' );
/**
 * Steady Rates Shortcode 
 * [steady_rates pate_id=1]
 */
function steady_rates_func( $atts ) {
	$a = shortcode_atts( array(
		'page_id' => 'something'
	), $atts );
    $page_ID = $a['page_id'];
    $html = '';
    $html .= '<div class="steady-rates-box">';
        $html .= '<div class="steady-rates-heading">';
            $html .= '<div class="steady-rates-heading-col steady-rates-heading-col-left">';
                $html .= '<h3>Medical Plan</h3>';
            $html .= '</div>';
            $html .= '<div class="steady-rates-heading-col steady-rates-heading-col-right">';
                $html .= '<h3>Annual Rate Changes</h3>';
            $html .= '</div>';
        $html .= '</div>';
        // Check rows exists.
        if( have_rows('steady_rates', $page_ID) ):
            $count = 1;
            $html .= '<table>';
            // Loop through rows.
            while( have_rows('steady_rates', $page_ID) ) : the_row();

                // Load sub field value.
                $first = get_sub_field('first');
                $second = get_sub_field('second');
                $third = get_sub_field('third');
                $fourth = get_sub_field('fourth');
                // Do something...

                if($count == 1){
                    $html .= '<thead>';
                        $html .= '<tr>';
                            $html .= '<th></th>';
                            $html .= '<th>'.$second.'</th>';
                            $html .= '<th>'.$third.'</th>';
                            $html .= '<th>'.$fourth.'</th>';
                        $html .= '</tr>';
                    $html .= '</thead>';
                    $html .= '<tbody>';
                }else{
                    $html .= '<tr>';
                        $html .= '<td>'.$first.'</td>';
                        $html .= '<td>'.$second.'</td>';
                        $html .= '<td>'.$third.'</td>';
                        $html .= '<td>'.$fourth.'</td>';
                    $html .= '</tr>';
                }

            // End loop.
            $count++;
            endwhile;
                $html .= '</tbody>';
            $html .= '</table>';
        // No value.
        else :
            // Do something...
        endif;
    $html .= '</div>';
	return $html;
}
add_shortcode( 'steady_rates', 'steady_rates_func' );
/**
 * Alert Shortcode
 * [alert]
 */
function alert_shortcode_func($atts){
    $a = shortcode_atts( array(
		'icon' => 'something',
		'content' => 'something else',
	), $atts );

    $icon = $a['icon'];
    $content = $a['content'];
    $html = '';
    $html .= '<div class="alert bg_FFF67B grid grid-cols-6">';
        $html .= '<div class="alert-icon"><img src="'.$icon.'"></div>';
        $html .= '<div class="alert-content col-span-5">';
            $html .= '<p>'.$content.'</p>';
        $html .= '</div>';
    $html .= '</div>';

    return $html;
}
add_shortcode('alert', 'alert_shortcode_func');