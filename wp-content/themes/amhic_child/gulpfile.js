const gulp = require('gulp'),
    // sass = require('gulp-sass'),
    sass = require('gulp-sass')(require('sass')),
    postcss = require('gulp-postcss'),
    concat = require('gulp-concat'),
    tailwindcss = require('tailwindcss');

gulp.task('style', function () {
    return gulp.src('assets/scss/app.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('assets/scss'))
        .pipe(postcss([
            tailwindcss('assets/tailwind/tailwind.config.js')
        ]))
        .pipe(concat({ path: 'app.css'}))
        .pipe(gulp.dest('assets/dist'));
});

gulp.task('default', function() {
    gulp.series('style');
    // gulp.watch('assets/css/**/*.scss', gulp.series('style'));
    gulp.watch('**/*.php', gulp.series('style'));
    gulp.watch('*.html', gulp.series('style'));
    gulp.watch('*.scss', gulp.series('style'));
    gulp.watch('**/*.scss', gulp.series('style'));
    // gulp.watch('template-parts/**/*', gulp.series('style'));
});
