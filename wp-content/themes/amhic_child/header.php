<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php if(is_page_template('template-parts/temp-mem-resources.php')){ ?>
    <!-- Library Search -->
    <div class="_lSearch">
        <div class="container mx-auto text-right ">
            <div class="flex items-center justify-end">
                Here for a quick form? Use our search to scan through our resources to find just the form you’re looking for.
                <div class="ml-5">
                    <form class="relative">
                        <input required type="text" placeholder="Search Our Library" />
                        <button type="submit">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/search-solid.png" alt="" />
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<!-- Navigation -->
<nav>
    <div class="container mx-auto my-auto break-normal">
        <div class="flex items-center justify-between">
            <div class="_logo">
                <a href="<?php echo esc_url(home_url('/')); ?>">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png" alt="<?php bloginfo('name'); ?>" />
                </a>
            </div>
            <div class="_navItems">
            <div class="topnav" id="myTopnav">
                <a href="<?php echo home_url(); ?>" class="responsive-logo"><img class="" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png"></a>
                <?php 
                    wp_nav_menu(array('menu' => 2));
                ?>
                <!-- <a href="#home" class="active">Home</a>
                <a href="#news">News</a>
                <a href="#contact">Contact</a>
                <div class="dropdown">
                    <button class="dropbtn">Dropdown 
                    <i class="fa fa-caret-down"></i>
                    </button>
                    <div class="dropdown-content">
                    <a href="#">Link 1</a>
                    <a href="#">Link 2</a>
                    <a href="#">Link 3</a>
                    </div>
                </div> 
                <a href="#about">About</a> -->
                <a href="javascript:void(0);" style="font-size: 38px; color: #c22529; margin-top: 10px;" class="icon" onclick="myFunction()">&#9776;</a>
            </div>
                <?php 
                    wp_nav_menu(array(
                        'menu' => 2, 
                        'container_id' => 'tsd_menu', 
                        'walker' => new CSS_Menu_Walker()
                    ));
                ?>
                
            </div>
        </div>
    </div>
</nav>
