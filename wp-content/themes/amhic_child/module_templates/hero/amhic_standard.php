<?

use TSD_Infinisite\WPB_Acme;
use TSD_Infinisite\Acme;

$module = new WPB_Acme($module);

$title = $module->get("title") ?? get_the_title();

// if the title equals none, set it to a string with a single space
$title !== "{none}" ?: $title = ' ';

?>

<div class="_amhic_interior_hero flex items-center justify-center min-h-xl bg-blue-dark">
    <h1 class="text-5xl m-0 text-white"><?= $title ?></h1>
</div>
