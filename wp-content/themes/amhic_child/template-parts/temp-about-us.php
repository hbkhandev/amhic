<?php
/*
  * Template Name: About Us
  */
get_header(); ?>
<!-- Banner -->
<section class="_smBanner" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/about-banner.jpg">
    <div class="container mx-auto relative z-10">
        <h1>About Us</h1>
    </div>
</section>
<!-- Vision & Mission -->
<section class="bg-F7FAFC py-[60px] text-707070">
    <div class="container mx-auto relative z-10">
        <h1 class="text-center text-[50px] leading-[61px] mb-[115px]">Vision, Mission & Core Values</h1>
        <div class="grid grid-cols-2 gap-[30px]">
            <div>
                <p class="font-bold">Vision - Where we hope to be in the future</p>
                <p class="mb-12">The vision of AMHIC, A Reciprocal Association is to be the premier, self-governing insurance company for not-for-profit organizations dedicated to providing their employees with a flexible portfolio of high-quality, cost-effective and comprehensive benefits programs.</p>
                <p>Mission - Why we exist</p>
                <p>The mission of AMHIC is to provide high quality benefits to employees of small and mid-size mission-based not-for-profit organizations that are dedicated to supporting education, research and/or public service.</p>
            </div>
            <div>
                <p class="font-bold">Core Values - What binds us together</p>
                <p>Organizations participating in AMHIC embrace the following core values:</p>
                <p>We are committed to providing employers and their employees with high-quality and valued benefit programs.</p>
                <p>We recognize that "one size does not fit all" and that a benefits portfolio that offers choice and flexibility will best serve employees of participating organizations.</p>
                <p>We embrace the diversity of employees of our participating organizations and provide benefits with tolerance for diverse religious, social and cultural values.</p>
                <p>We expect AMHIC to operate with fiscal integrity, transparency and accountability and to achieve cost stability.</p>
                <p>We value exemplary customer service, responsiveness and administrative efficiency.</p>
                <p>We rely on participating organizations to make an extended commitment to serve the vision and mission of AMHIC and to cooperate in its governance.</p>
            </div>
        </div>
    </div>
</section>
<!-- Goals & History -->
<section class="text-707070">
    <div class="container mx-auto relative z-10">
        <h2 class="text-center mb-[50px]">Goals</h2>
        <div class="grid grid-cols-2 gap-[30px] mb-[160px]">
            <div>
                <p>At AMHIC, we strive to be the healthcare choice for education, research and public service-related not-for-profit organizations domiciled in the District of Columbia. We understand that attracting and retaining quality employees is more challenging than ever, and that employee benefits plans make a huge difference. Our vision? Using our member pool to lower your risks and costs, and offering plan features you may not be able to find anywhere else.</p>
            </div>
            <div>
                <p>AMHIC’s primary goals are to:</p>
                <ul class="list-disc pl-5">
                    <li>Provide not-for-profit organizations access to affordable health insurance coverage.</li>
                    <li>Achieve cost stability through increased spread of risk.</li>
                    <li>Offer flexible benefit plan designs.</li>
                    <li>Serve as benefits consultant and plan administrator.</li>
                </ul>
            </div>
        </div>
        <h2 class="text-center mb-[50px]">History</h2>
        <div class="flex justify-center">
            <div class="max-w-[50%]">
                <p>Founded in 1989, AMHIC, A Reciprocal Association was formerly known as the Washington Higher Education Secretariat Multiple Employer Trust. The program has grown from 400 participants to more than 2,000, while maintaining an extremely rich benefits program at a reasonable and competitive level of pricing.</p>
            </div>
        </div>
    </div>
</section>
<!-- Key Milestones -->
<section class="bg-F7FAFC py-[40px] text-707070 leading-6">
    <div class="container mx-auto relative z-10">
        <h3 class="text-center text-707070 text-[33px] leading-[40px] mb-[53px]">Key Milestones</h3>
        <div class="grid grid-cols-4 gap-[30px]">
            <div>
                <p>1989</p>
                <p>13 Associations enrolled in the TIAA-CREF health insurance plan with 400 participants. Program becomes commonly known as the Washington Higher Education Secretariat (WHES) Multiple Employer Trust (MET).</p>
            </div>
            <div>
                <p>1990</p>
                <p>The WHES MET partially self insures the medical plan, with the American Council on Education as the plan sponsor.</p>
            </div>
            <div>
                <p>2002</p>
                <p>WHES Trust transitions to a captive insurance company and renamed Association Mutual Health Insurance Company (AMHIC).</p>
            </div>
            <div>
                <p>2014</p>
                <p>AMHIC converts its organizational structure to a reciprocal exchange and is renamed AMHIC, A Reciprocal Association.</p>
            </div>
        </div>
    </div>
</section>
<!-- Partners -->
<section class="text-707070">
    <div class="container mx-auto relative z-10">
        <h2 class="text-center mb-[18px]">Partners</h2>
        <div class="flex justify-center mb-16">
            <div class="max-w-[50%]">
                <p>AMHIC is proud to work with premier companies that provide expertise and guidance to our management and Board of Directors.</p>
            </div>
        </div>
        <div class="_pLogo grid grid-cols-6 gap-x-[30px] gap-y-[60px] mb-[200px]">
            <div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/partner.jpg" alt="" />
            </div>
            <div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/partner.jpg" alt="" />
            </div>
            <div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/partner.jpg" alt="" />
            </div>
            <div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/partner.jpg" alt="" />
            </div>
            <div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/partner.jpg" alt="" />
            </div>
            <div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/partner.jpg" alt="" />
            </div>
            <div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/partner.jpg" alt="" />
            </div>
            <div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/partner.jpg" alt="" />
            </div>
            <div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/partner.jpg" alt="" />
            </div>
            <div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/partner.jpg" alt="" />
            </div>
            <div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/partner.jpg" alt="" />
            </div>
            <div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/partner.jpg" alt="" />
            </div>
        </div>
        <!-- Join Us -->
        <div class="text-center">
            <div class="_join">
                <h3>Join Us Today</h3>
                <form>
                    <input type="text" placeholder="Your Name" />
                    <input type="email" placeholder="Your Email" />
                    <textarea placeholder="How can we help you?"></textarea>
                    <button type="submit" class="btn">Join Us</button>
                </form>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
