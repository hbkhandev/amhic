<?php
/*
  * Template Name: Become Member
  */
get_header(); ?>
    <!-- Banner -->
    <section class="_smBanner" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/member-banner.jpg">
        <div class="container mx-auto relative z-10">
            <h1>Become<br/> A Member</h1>
        </div>
    </section>
    <!-- Benefits -->
    <section class="pt-[35px] text-707070">
        <div class="container mx-auto max-w-[1230px] mb-[65px]">
            <p class="text-[30px] text-center leading-[37px]">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est</p>
        </div>
        <div class="container mx-auto mb-[125px]">
            <div class="grid grid-cols-3 gap-[30px]">
                <div class="_bBox">
                    <div>
                        <div class="flex gap-[30px] mb-[40px] items-center">
                            <div>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/benefit-1.png" alt="" />
                            </div>
                            <div>
                                <h3>Benefit Name</h3>
                            </div>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo</p>
                    </div>
                    <a href="#">Learn More</a>
                </div>
                <div class="_bBox">
                    <div>
                        <div class="flex gap-[30px] mb-[40px] items-center">
                            <div>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/benefit-2.png" alt="" />
                            </div>
                            <div>
                                <h3>Benefit Name</h3>
                            </div>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo</p>
                    </div>
                    <a href="#">Learn More</a>
                </div>
                <div class="_bBox">
                    <div>
                        <div class="flex gap-[30px] mb-[40px] items-center">
                            <div>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/benefit-3.png" alt="" />
                            </div>
                            <div>
                                <h3>Benefit Name</h3>
                            </div>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo</p>
                    </div>
                    <a href="#">Learn More</a>
                </div>
            </div>
        </div>
        <!-- Benefits Row 1 -->
        <div class="_bRow relative mb-[140px]">
            <div class="w-[75%]">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/b1.png" class="w-full" alt="" />
            </div>
            <!-- Text -->
            <div class="_txt right-[9%]">
                <div class="_holder relative z-10">
                    <div class="p-[50px]">
                        <h3>Benefit Name</h3>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo</p>
                    </div>
                    <div class="text-center mb-6">
                        <a href="#">Register Today</a>
                    </div>
                </div>
                <div class="absolute inset-0 p-4 text-center z-2">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/b1-1.png" alt="" class="inline-block" />
                </div>
            </div>
        </div>
        <!-- Benefits Row 2 -->
        <div class="_bRow relative mb-[140px]">
            <div class="w-[100%] pl-[25%]">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/b2.png" class="w-full" alt="" />
            </div>
            <!-- Text -->
            <div class="_txt left-[9%]">
                <div class="_holder relative z-10">
                    <div class="p-[50px]">
                        <h3>Benefit Name</h3>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo</p>
                    </div>
                    <div class="text-center mb-6">
                        <a href="#">Register Today</a>
                    </div>
                </div>
                <div class="absolute inset-0 p-4 text-center z-2">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/b2-1.png" alt="" class="inline-block" />
                </div>
            </div>
        </div>
        <!-- Benefits Row 3 -->
        <div class="_bRow relative">
            <div class="w-[75%]">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/b3.png" class="w-full" alt="" />
            </div>
            <!-- Text -->
            <div class="_txt right-[9%]">
                <div class="_holder relative z-10">
                    <div class="p-[50px]">
                        <h3>Benefit Name</h3>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo</p>
                    </div>
                    <div class="text-center mb-6">
                        <a href="#">Register Today</a>
                    </div>
                </div>
                <div class="absolute inset-0 p-4 text-center z-2">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/b3-1.png" alt="" class="inline-block" />
                </div>
            </div>
        </div>
        <!-- Join Us -->
        <div class="container mx-auto mt-[215px]">
            <div class="text-center">
                <div class="_join">
                    <h3>Join Us Today</h3>
                    <form>
                        <input type="text" placeholder="Your Name" />
                        <input type="email" placeholder="Your Email" />
                        <textarea placeholder="How can we help you?"></textarea>
                        <button type="submit" class="btn">Join Us</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>