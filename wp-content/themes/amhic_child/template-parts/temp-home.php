<?php
/*
  * Template Name: Home Page
  */
get_header(); ?>
<!-- Banner -->
<section class="_banner py-[16rem] bg-blue-100" data-parallax="scroll"
         data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner-img.jpg">
    <div class="container mx-auto relative z-10">
        <div class="grid grid-cols-2 gap-[30px] items-end">
            <div>
                <h1 class="text-white text-6xl">The Premier Benefits Choice for Washington D.C. Not-For-Profits</h1>
                <h3>Join Our Consortium Today</h3>
            </div>
            <div class="text-center">
                <a href="#" class="btn mb-2">WHY AMHIC</a>
            </div>
        </div>
    </div>
</section>
<!-- Stats -->
<section class="_stats bg-blue-100 pt-0 mt-[-170px]">
    <div class="container mx-auto text-center">
        <div class="grid grid-cols-3 gap-[70px]">
            <div class="_box">
                <h2 class="mb-3">Serving D.C. Since 1990</h2>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/group-3391.png" alt="" />
                <p>Supporting members in education, research and public service</p>
            </div>
            <div class="_box">
                <h2 class="mb-3">55+ Member Nonprofits</h2>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/group-3347.png" alt="" />
                <p>Ranging from 5 to over 150 employees</p>
            </div>
            <div class="_box">
                <h2 class="mb-3">1,700+ Employees</h2>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/group-3390.png" alt="" />
                <p>And their families are covered by AMHIC benefit plans</p>
            </div>
        </div>
    </div>
</section>

<!-- Our Mission -->
<section class="_ourMission bg-blue-100">
    <div class="container mx-auto text-center">
        <div class="grid grid-cols-2 gap-[200px] _mBlock">
            <div>
                <h2 class="mb-12">Our Mission</h2>
                <p>Choice. Flexibility. Cost-effectiveness. With AMHIC you can get everything you are looking for. AMHIC
                   delivers benefits solutions that meet the needs of your employees.</p>
                <a href="#" class="button red mt-12">All Resources</a>
            </div>
            <div>
                <h2 class="mb-12">Who We Are</h2>
                <p>As a member, you can finaly provide affordable benefits to your staff and still enjoy outstanding
                   plan features, with cost stability you’ll appreciate.</p>
                <a href="#" class="button red mt-12">Determine Your Eligibility</a>
            </div>
        </div>
    </div>
</section>
<!-- Our Membership -->
<div class="flex justify-end bg-center min-h-xl"
     style="background-image:url(http://placehold.it/1600x400)">
    <div class="w-7/12 p-8 bg-white bg-opacity-50 flex flex-col justify-center">
        <h2>Our Membership</h2>
        <p>Our members are from...</p>
        <ul class="grid grid-cols-2 gap-4 pb-12">
            <li>Industry Name</li>
            <li>Industry Name</li>
            <li>Industry Name</li>
            <li>Industry Name</li>
            <li>Industry Name</li>
        </ul>
        <div>
            <a href="#" class="button red">Join Us</a>
        </div>
    </div>
</div><!-- AMHIC Delivers -->
<div class="flex">
    <div class="w-1/2 p-8 bg-white bg-opacity-50 flex flex-col justify-center">
        <div class="max-w-lg mx-auto">
            <h2 class="pb-6">AMHIC Delivers</h2>
            <p>Choice. Customer Service. Cost-effectiveness.</p>
            <div class="pb-6"></div>
            <a href="#" class="button red">Meet the Board</a>
        </div>
    </div>
    <div class="w-1/2 bg-center"
         style="
         min-height: 500px;
         background-image:url(http://placehold.it/1600x400)"></div>
</div>
<!-- Core Plans-->
<div class="bg-gray-100 py-32">
    <div class="container mx-auto">
        <h2 class="text-center mb-12">
            Our Core Plans
        </h2>

        <div class="grid grid-cols-3 gap-12">
            <img src="http://placehold.it/400x400" alt="">
            <img src="http://placehold.it/400x400" alt="">
            <img src="http://placehold.it/400x400" alt="">
        </div>

        <div class="pt-12 text-center">
            <a href="#" class="button red">See More Plans</a>
        </div>
    </div>
</div>

<!-- Testimonials-->

<div class="_homeTestimonial py-12">
    <div class="max-w-3xl mx-auto">

        <h4 class="text-3xl text-center mb-12">&ldquo;Quote here lorem ipsupm&rdquo;</h4>
        <p class="text-sm font-bold text-right mb-0 text-red-dark">Person Name</p>
        <p class="text-sm text-right mb-0 text-blue-dark">Person Job</p>

        <div class="flex justify-around mt-12">
            <a href="#" class="button red">Read Testimonials</a>
            <a href="#" class="button red">Become A Member</a>
        </div>
    </div>
</div>

<!-- Core Values -->

<div class="bg-blue-dark">
    <div class="container py-16 mx-auto">
        <div>
            <h3 class="text-white text-center text-3xl font-bold">Core Values Of Our Consortium</h3>
            <div class="text-center"><a href="#" class="button white">Read More</a></div>
        </div>

    </div>
</div>
<?php get_footer(); ?>
