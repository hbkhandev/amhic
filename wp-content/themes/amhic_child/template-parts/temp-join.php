<?php
/*
  * Template Name: Join
  */
get_header(); ?>
    <!-- Banner -->
    <section class="_smBanner" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/join-banner.jpg">
        <div class="container mx-auto relative z-10">
            <h1>Become A Member</h1>
        </div>
    </section>
    <!-- Eligibility -->
    <section class="bg-F7FAFC py-[60px] text-707070">
        <div class="container mx-auto">
            <h1 class="text-center mb-[58px] text-[50px] leading-[61px]">Determine Your Eligibility</h1>
            <div class="flex gap-[60px] mb-[80px]">
                <div class="col-span-5">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/e1.png" alt="" />
                </div>
                <div class="col-span-7">
                    <ul class="_checkList">
                        <li>Do you qualify as a not-for-profit group?</li>
                        <li>Is one of your organization’s missions to support education, research or public service?</li>
                        <li>Do you have at least five active employees? Are you domiciled in the District of Columbia?</li>
                        <li>Do you have at least 75% of all benefit-eligible employees enrolled in an employee benefit plan?</li>
                    </ul>
                </div>
            </div>
            <div class="flex gap-[60px] text-[25px] leading-[30px]">
                <div class="w-[60%]">
                    <p class="mb-[30px]">If you answered “yes” to all of the above eligibility questions, we will be happy to provide you with more information. Please complete the documents on the right, and send to:</p>
                    <p class="mb-[30px]">Rhona Byer<br/> Executive Director<br/> AMHIC, A Reciprocal Association (AMHIC)<br/> Select Benefit Plan Administrators (SBPA)</p>
                    <p>1101 17th Street, NW<br/> Suite 300<br/> Washington, DC. 20036</p>
                </div>
                <div class="w-[33%]">
                    <a href="#" class="_dBox">
                        Completed Group Application
                    </a>
                </div>
                <div class="w-[33%]">
                    <a href="#" class="_dBox">
                        Completed Census
                    </a>
                    <div class="_dSample">Download Sample</div>
                </div>
            </div>
        </div>
    </section>
    <!-- Join Us -->
    <section>
        <div class="container mx-auto">
            <div class="text-center">
                <div class="_join">
                    <h3>Join Us Today</h3>
                    <form>
                        <input type="text" placeholder="Your Name" />
                        <input type="email" placeholder="Your Email" />
                        <textarea placeholder="How can we help you?"></textarea>
                        <button type="submit" class="btn">Join Us</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>