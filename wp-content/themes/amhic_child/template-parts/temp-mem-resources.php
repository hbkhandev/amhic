<?php
/*
  * Template Name: Member Resources
  */
get_header(); ?>
    <!-- Banner -->
    <section class="_smBanner" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sm-banner.jpg">
        <div class="container mx-auto relative z-10">
            <h1>Member<br/> Resources</h1>
        </div>
    </section>
    <!-- Links Block -->
    <section class="text-center pt-[35px] pb-[80px]">
        <div class="container mx-auto relative z-10 max-w-[1330px]">
            <p class="text-[30px] leading-[37px] mb-12">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est</p>
            <ul class="_links">
                <li><a href="#">Enrollment</a> </li>
                <li><a href="#">Benefits / Forms</a> </li>
                <li><a href="#">Contact Us</a> </li>
            </ul>
        </div>
    </section>
    <!-- CTA -->
    <section class="_cta">
        <div class="container mx-auto relative z-10">
            <div class="grid grid-cols-2 gap-16 items-center">
                <div class="col-span-8">
                    <h1>Get the Enrollment Form</h1>
                    <h2>Contains all registration information and plan options<br/> for your health policy.</h2>
                </div>
                <div class="col-span-3 col-start-9">
                    <a href="#" class="btn">Download</a>
                </div>
            </div>
        </div>
    </section>
    <!-- Content -->
    <section class="text-707070 pb-[270px]">
        <div class="container mx-auto relative z-10  max-w-[1450px]">
            <div class="grid grid-cols-2 gap-[80px]">
                <div class="col-span-5">
                    <div class="_sideBar">
                        <ul class="mb-[88px]">
                            <li><a href="#">General Information</a> </li>
                            <li><a href="#">Medical Insurance</a> </li>
                            <li><a href="#" class="active">Dental Insurance</a> </li>
                            <li><a href="#">Vision Insurance</a> </li>
                            <li><a href="#">Life Insurance</a> </li>
                            <li><a href="#">Legal Benefits</a> </li>
                            <li><a href="#">Employee Assistance Program</a> </li>
                            <li><a href="#">Flexible Spending Account</a> </li>
                            <li><a href="#">Health Savings Account</a> </li>
                            <li><a href="#">Benefit Manager Information</a> </li>
                            <li><a href="#">Network Directories</a> </li>
                        </ul>
                        <div class="_bPart text-center">
                            <p>Download the AMHIC Enrollment Form</p>
                            <a href="#" class="mb-[90px]">AMHIC Enrollment Form <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/long-arrow-alt-up-solid.png" alt="" class="inline-block ml-3" /> </a>
                            <p>Questions?</p>
                            <a href="#" class="mb-4">Ask Us Below <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/long-arrow-alt-up-solid-2.png" alt="" class="inline-block ml-3" /> </a>
                        </div>
                    </div>
                </div>
                <div class="col-span-7 col-start-6">
                    <div class="_content leading-[27px]">
                        <h3 class="text-[32px] leading-[39px] mb-[39px]">Dental Insurance Benefits</h3>
                        <p>Give your employees one of two good choices from MetLife: a standard plan covering the majority of preventative and major dental services, and a premium plan with added benefits. In both plans, employees can use providers outside the MetLife Preferred Dentist Program. Employees who use the program's preferred dentists can take advantage of:</p>
                        <p>Lower out-of-pocket expenses for most preventative and major dental services.</p>
                        <p class="mb-[50px]">Typical savings of 15-45% below the average charges.</p>
                        <div class="flex items-center bg-FFF67B p-8 mb-16">
                            <div class="_icon min-w-[76px] mr-6">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/pngwing.png" alt="" />
                            </div>
                            <p class="m-0"><span class="font-bold">Note to Participating Members:</span> All AMHIC groups do not offer all AMHIC plans. Please check with your human resources department to confirm which AMHIC plans are offered by your employer.</p>
                        </div>
                        <div class="grid grid-cols-2 gap-[50px] leading-[24px]">
                            <div>
                                <h4>Premium</h4>
                                <p class="mb-[36px]">Excellent benefits including:</p>
                                <ul>
                                    <li>Higher co-insurance for basic and major services</li>
                                    <li>Lower annual deductibles than the Standard Dental Plan,</li>
                                    <li>$2,000 per-person annual dollar limit.</li>
                                    <li>Covers orthodontia (for children or adults) with a $2,000 per-person lifetime limit.</li>
                                </ul>
                                <div class="mb-16">
                                    <p><a href="#" class="_simpleBtn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/pdfpic.png" alt="" class="inline-block mr-3" /> Dental Claim Form</a></p>
                                    <p><a href="#" class="_simpleBtn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/pdfpic.png" alt="" class="inline-block mr-3" /> Dental Generic ID Card</a></p>
                                </div>
                                <h3 class="text-[32px] leading-[39px] mb-[39px]">Dental Insurance Links</h3>
                                <p><a href="#" class="_simpleBtn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/link.png" alt="" class="inline-block mr-3" /> MetLife - Select PDP Plus Network</a></p>
                            </div>
                            <div>
                                <h4>Standard</h4>
                                <p class="mb-[36px]">Help your employees cover all the basics.</p>
                                <ul class="mb-[78px]">
                                    <li>Doesn’t pay for orthodontia,</li>
                                    <li>Covers most minor and major dental work, </li>
                                    <li>Most preventative and diagnostic services.</li>
                                    <li>Annual dollar limit per person is $1,000.</li>
                                </ul>
                                <div>
                                    <p><a href="#" class="_simpleBtn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/link.png" alt="" class="inline-block mr-3" /> Dental Plan Benefits</a></p>
                                    <p><a href="#" class="_simpleBtn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/link.png" alt="" class="inline-block mr-3" /> Dental Certificate of Coverage</a></p>
                                    <p><a href="#" class="_simpleBtn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/link.png" alt="" class="inline-block mr-3" /> Certificate of Coverage - Rider</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Questions? -->
    <section class="bg-E8ECFA py-[80px]">
        <div class="container mx-auto relative z-10 text-center">
            <div class="_join">
                <h3 class="mb-[80px]">Questions?</h3>
                <form>
                    <input type="text" placeholder="Your Name" />
                    <input type="text" placeholder="Benefit Type" />
                    <input type="email" placeholder="Your Email" />
                    <textarea placeholder="Your Question"></textarea>
                    <button type="submit" class="btn">Submit</button>
                </form>
            </div>
        </div>
    </section>
<?php get_footer(); ?>