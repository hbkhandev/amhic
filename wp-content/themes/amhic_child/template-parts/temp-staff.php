<?php
/*
  * Template Name: Staff
  */
get_header(); ?>
<?= apply_filters("the_content", get_the_content()) ?>
    <!-- Banner -->
    <section class="_smBanner" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/staff-banner.jpg">
        <div class="container mx-auto relative z-10">
            <h1>AMHIC<br/> Staff</h1>
        </div>
    </section>
    <!-- AMHIC Executives -->
    <section class="bg-F7FAFC py-[70px]">
        <div class="container mx-auto">
            <h2 class="text-707070 text-center mb-[40px]">AMHIC Executives</h2>
            <div class="flex justify-center gap-[30px]">
                <div class="max-w-[50%]">
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
                <div class="max-w-[50%]">
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
            </div>
        </div>
    </section>
    <!-- Expert Guidance -->
    <section class="text-707070 text-center">
        <div class="container mx-auto max-w-[1150px] text-[30px] leading-[37px] mb-[90px]">
            <h3 class="text-707070 text-[32px]">Expert Guidance</h3>
            <p>AMHIC is guided by a board of directors comprised of distinguished CEOs and senior executives from member organizations as well as outside experts.</p>
            <p>AMHIC’s board is responsible for guiding the work of the organization and ensuring it is responsive to the needs of education, research or public service-related associations in Washington, DC.</p>
            <p>AMHIC, A Reciprocal Association</p>
        </div>
        <div class="container mx-auto">
            <h3 class="text-707070 text-[32px] mb-[40px]">Board of Directors</h3>
            <div class="grid grid-cols-4 gap-[30px] mb-[285px]">
                <div>
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
                <div>
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
                <div>
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
                <div>
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
                <div>
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
                <div>
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
                <div>
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
                <div>
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
                <div>
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
                <div>
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
                <div>
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
                <div>
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
                <div>
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
                <div>
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
                <div>
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
                <div>
                    <img src="http://placehold.it/400x550?text=Bio+Image" alt="" />
                </div>
            </div>
            <!-- Join Us -->
            <div class="text-center">
                <div class="_join">
                    <h3>Join Us Today</h3>
                    <form>
                        <input type="text" placeholder="Your Name" />
                        <input type="email" placeholder="Your Email" />
                        <textarea placeholder="How can we help you?"></textarea>
                        <button type="submit" class="btn">Join Us</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>
