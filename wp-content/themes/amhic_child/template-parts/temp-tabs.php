<?php
/**
 * Template Name: Resources Tabs
 */
?>
<section class="section-member-resources-tabs">
    <div class="container">
        <div class="grid grid-cols-3">
            
            <div class="mem-res-tabs-title col-span-1">

                <?php if( have_rows('tabs', 1109) ): $count = 1; ?>
                    <div class="tab">
                        <?php while( have_rows('tabs', 1109) ): the_row(); 
                            $title = get_sub_field('title');
                            $ID_tabs = $new_str = str_replace(' ', '', $title);
                            ?>
                            <?php if($count == 1){ ?>
                                <button class="tablinks" onclick="openCity(event, '<?php echo $ID_tabs; ?>')" id="defaultOpen"><?php echo $title; ?></button>
                            <?php } else { ?>
                            <button class="tablinks" onclick="openCity(event, '<?php echo $ID_tabs; ?>')"><?php echo $title; ?></button>
                            <?php } ?>
                        <?php $count++; endwhile; ?>
                    </div>
                <?php endif; ?>
            
                <!-- <div class="tab">
                    <button class="tablinks" onclick="openCity(event, 'GeneralInformation')" id="defaultOpen">General Information</button>
                    <button class="tablinks" onclick="openCity(event, 'MedicalInsurance')">Medical Insurance</button>
                    <button class="tablinks" onclick="openCity(event, 'DentalInsurance')">Dental Insurance</button>
                    <button class="tablinks" onclick="openCity(event, 'VisionInsurance')">Vision Insurance</button>
                    <button class="tablinks" onclick="openCity(event, 'LifeInsurance')">Life Insurance</button>
                    <button class="tablinks" onclick="openCity(event, 'LegalBenefits')">Legal Benefits</button>
                    <button class="tablinks" onclick="openCity(event, 'EmployeeAssistanceProgram')">Employee Assistance Program</button> 
                    <button class="tablinks" onclick="openCity(event, 'FlexibleSpendingAccount')">Flexible Spending Account</button>
                    <button class="tablinks" onclick="openCity(event, 'HealthSavingsAccount')">Health Savings Account</button>
                    <button class="tablinks" onclick="openCity(event, 'BenefitManagerInformation')">Benefit Manager Information</button>
                </div> -->
            </div>
            <div class="mem-res-tabs-content col-span-2">

                <?php if( have_rows('tabs', 1109) ): ?>
                    <div class="tab">
                        <?php while( have_rows('tabs', 1109) ): the_row(); 
                            $title = get_sub_field('title');
                            $content = get_sub_field('title');
                            $ID_tabs = $new_str = str_replace(' ', '', $title);
                            ?>
                            <div id="<?php echo $ID_tabs; ?>" class="tabcontent">
                                <div class="tab-content">
                                    <?php if( have_rows('content') ): ?>
                                        <?php while( have_rows('content') ): the_row(); 
                                            $description = get_sub_field('description');
                                            $show_alert = get_sub_field('show_alert');
                                            echo $description;
                                            if($show_alert === 'yes'){
                                                if( have_rows('alert') ): ?>
                                                    <?php while( have_rows('alert') ): the_row(); 
                                                        $alert_icon = get_sub_field('alert_icon');
                                                        $alert_content = get_sub_field('alert_content');
                                                        ?>
                                                        <div class="alert bg_FFF67B grid grid-cols-6">
                                                            <div class="alert-icon"><img src="<?php echo $alert_icon; ?>"></div>
                                                            <div class="alert-content col-span-5">
                                                                <p><?php echo $alert_content; ?></p>
                                                            </div>
                                                        </div>
                                                    <?php endwhile; 
                                                endif; 
                                            } ?>
                                            <?php if( have_rows('two_cols_data') ): ?>
                                                <div class="two-cols grid grid-cols-2">
                                                    <?php while( have_rows('two_cols_data') ): the_row(); 

                                                        $first_column = get_sub_field('first_column');
                                                        $second_column = get_sub_field('second_column');
                                                        ?>
                                                        <div class="two-cols-column pl-[0]">
                                                            <?php echo $first_column; ?>
                                                        </div>
                                                        <div class="two-cols-column pr-[0]">
                                                            <?php echo $second_column; ?>
                                                        </div>
                                                    <?php endwhile; ?>
                                                </div>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>

                <!-- <div id="GeneralInformation" class="tabcontent">
                    <div class="tab-content">
                        <h3>General Information</h3>
                    </div>
                </div>
                <div id="MedicalInsurance" class="tabcontent">
                    <div class="tab-content">
                        <h3>Medical Insurance</h3>
                    </div>
                </div>
                <div id="DentalInsurance" class="tabcontent">
                    <div class="tab-content">
                        <h3>Dental Insurance</h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus incidunt harum illum, quos veritatis doloremque distinctio nulla velit enim dicta sint error quo similique! Porro expedita dolores doloribus placeat error? Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus incidunt harum illum, quos veritatis doloremque distinctio nulla velit enim dicta sint error quo similique! Porro expedita dolores doloribus placeat error?</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione aperiam illo laudantium odit magni eaque ea.</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                        <div class="alert bg_FFF67B grid grid-cols-6">
                            <div class="alert-icon"><img src="http://amhic.local/wp-content/uploads/2021/07/pngwing.com@2x.png"></div>
                            <div class="alert-content col-span-5">
                                <p><strong>Note to Participating Members:</strong> All AMHIC groups do not offer all AMHIC plans. Please check with your human resources department to confirm which AMHIC plans are offered by your employer.</p>
                            </div>
                        </div>
                        <div class="two-cols grid grid-cols-2">
                            <div class="two-cols-column pl-[0]">
                                <h4>Premium</h4>
                                <p>Excellent benefits including:</p>
                                <ul>
                                    <li>Higher co-insurance for basic and major services</li>
                                    <li>Lower annual deductibles than the Standard Dental Plan,</li>
                                    <li>$2,000 per-person annual dollar limit. </li>
                                    <li>Covers orthodontia (for children or adults) with a $2,000 per-person lifetime limit.</li>
                                    <li><a href="#" class="_simpleBtn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/pdfpic.png" alt="" class="inline-block mr-3" /> Dental Claim Form</a></li>
                                    <li><a href="#" class="_simpleBtn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/pdfpic.png" alt="" class="inline-block mr-3" /> Dental Generic ID Card</a></li>
                                </ul>
                            </div>
                            <div class="two-cols-column pr-[0]">
                                <h4>Standard</h4>
                                <p>Help your employees cover all the basics.</p>
                                <ul>
                                    <li>Doesn’t pay for orthodontia,</li>
                                    <li>Covers most minor and major dental work,</li> 
                                    <li>Most preventative and diagnostic services.</li>
                                    <li>Annual dollar limit per person is $1,000.</li>
                                    <li><a href="#" class="_simpleBtn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/link.png" alt="" class="inline-block mr-3" /> Dental Plan Benefits</a></li>
                                    <li><a href="#" class="_simpleBtn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/link.png" alt="" class="inline-block mr-3" /> Dental Certificate of Coverage</a></li>
                                    <li><a href="#" class="_simpleBtn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/link.png" alt="" class="inline-block mr-3" /> Certificate of Coverage - Rider</a></li>
                                </ul>
                            </div>
                        </div>
                        <h3>Download Insurance Link</h3>
                        <ul>
                            <li><a href="#" class="_simpleBtn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/link.png" alt="" class="inline-block mr-3" /> MetLife - Select PDP Plus Network</a></li>
                        </ul>
                    </div>
                </div>
                <div id="VisionInsurance" class="tabcontent">
                    <div class="tab-content">
                        <h3>Vision Insurance</h3>
                    </div>
                </div>
                <div id="LifeInsurance" class="tabcontent">
                    <div class="tab-content">
                        <h3>Life Insurance</h3>
                    </div>
                </div>
                <div id="LegalBenefits" class="tabcontent">
                    <div class="tab-content">
                        <h3>Legal Benefits</h3>
                    </div>
                </div>
                <div id="EmployeeAssistanceProgram" class="tabcontent">
                    <div class="tab-content">
                        <h3>Employee Assistance Program</h3>
                    </div>
                </div>
                <div id="FlexibleSpendingAccount" class="tabcontent">
                    <div class="tab-content">
                        <h3>Flexible Spending Account</h3>
                    </div>
                </div>
                <div id="HealthSavingsAccount" class="tabcontent">
                    <div class="tab-content">
                        <h3>Health Saving Account</h3>
                    </div>
                </div>
                <div id="BenefitManagerInformation" class="tabcontent">
                    <div class="tab-content">
                        <h3>Benefit Manager Information</h3>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</section>