<?php 
/**
 * Template Name: Temp
 */
get_header(); ?>
<div class="
text-[3.5rem] 
leading-[60px] 
text-FFFFFF 
text-center 
font-black 
md:text-[4.5rem] 
md:leading-[87px] 
mt-[0]
pt-[50px] 
pb-[80px]
pt-[100px] 
pb-[100px]
pt-[150px] 
pr-13 
pb-[150px] 
pl-[100px]
pt-[150px] 
pr-[100px] 
pb-[150px] 
pl-[100px]
pt-[80px]
mx-0
my-[0]
px-[0]
py-0
mt-[-170px]
pt-[10px] pb-[80px]
pt-[80px] pb-[80px]
pl-[5%]
pt-[70px] pb-16
pr-[5%]
pt-[70px] pb-[170px]
min-h-[733px] 
pl-[3%]
pt-[60px] pb-[60px] pr-[10%] pl-[10%]
pb-[0] pt-[0]
pt-[60px] pb-[60px]
pt-[25px] pb-[25px]
pt-[40px] pb-[40px] mt-[80px]
pt-[66px]
mb-[60px]
pb-[10px]
mx-[-15px]
mt-10 pr-[22%] pl-[22%]
mt-[60px]
min-h-[400px] md:min-h-[733px] md:min-h-[600px] bg-center
pt-[25px] md:pr-[25px] pb-[25px] md:pl-11
md:min-h-[100vh]
bg-top
mb-[40px] mb-0
pb-0
py-[50px] bg-F7FAFC bg-FFFFFF mr-4 ml-[15px]
p-[15px] mt-[-15px] mb-4
py-[80px] text-1B2C6D mb-[50px]
md:mr-4 md:ml-[15px] mt-14 hidden
bg-blue-700
text-blue-600 font-black
text-6xl text-white text-center md:text-7xl
text-5xl text-blue-600
text-blue-700
text-7xl
text-5xl text-white font-black
text-5xl text-blue-700 font-black
text-7xl text-white font-black
"></div>
<?php get_footer(); ?>