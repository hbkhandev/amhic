<?
use TSD_Infinisite\IS_Post;
/* @var $post IS_Post */
?>
<div class="cell card border rounded p-3 shadow team-service">
    <div class="staff-box grid grid-cols-1">
        <div class="staff-media">
            <img src="<?php echo get_the_post_thumbnail_url($post->ID); ?>" class="staff-image">
        </div>
        <div class="staff-meta">
            <h3 class="person-name"><?php echo $post->post_title; ?></h3>
            <h4 class="job-title"><?php echo get_field('title', $post->ID);?></h4>
            <p class="person-email"><?php echo get_field('email', $post->ID);?></p>
            <p class="person-phone"><?php echo get_field('phone', $post->ID);?></p>
        </div>
    </div>
    <!--<h2><?/*= $post->post_title */?></h2>-->
</div>