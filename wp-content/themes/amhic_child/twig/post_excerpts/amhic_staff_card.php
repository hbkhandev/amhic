<?
use TSD_Infinisite\IS_Post;
/* @var $post IS_Post */
?>
<div class="cell card border rounded p-3 shadow team-staff">
    <div class="staff-box staff-flex ">
        <div class="staff-media col-span-1">
            <img src="<?php echo get_the_post_thumbnail_url($post->ID); ?>" class="staff-image">
        </div>
        <div class="staff-meta col-span-2">
            <h3 class="person-name"><?php echo $post->post_title; ?></h3>
            <h4 class="job-title"><?php echo get_field('title', $post->ID);?></h4>
            <div class="description"><p><?php echo get_field('description', $post->ID);?></p></div>
        </div>
    </div>
    <!--<h2><?/*= $post->post_title */?></h2>-->
</div>
