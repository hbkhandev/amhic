<?
use TSD_Infinisite\IS_Post;
/* @var $post IS_Post */
?>
<div class="cell card border rounded p-3 shadow team-testimonial">
    <div class="staff-box">
        <div class="staff-meta">
            <h3 class="person-name"><?php echo $post->post_title; ?></h3>
            <div class="description"><p><?php echo get_field('description', $post->ID);?></p></div>
        </div>
    </div>
    <!--<h2><?/*= $post->post_title */?></h2>-->
</div>