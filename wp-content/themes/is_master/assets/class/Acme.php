<?

namespace TSD;

class Acme {

    static function dbg($var, $var_dump = false)
    {

        if ($var_dump):
            print "<pre>";
            var_dump($var);
            print "</pre>";
            return;
        endif;
        $dbg = print_r($var, 1);
        print "<pre>$dbg</pre>";

    }

}