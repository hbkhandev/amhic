<?php

require_once 'assets/class/Display.php';
require_once 'assets/class/Acme.php';

add_filter( 'update_pagebuilder_fields', 'update_pagebuilder_fields_fn', 10, 1 );
function update_pagebuilder_fields_fn( $page_builder_field_group ) {

    $page_builder_field_group['location'][][] = [
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'page'
    ];

    return $page_builder_field_group;
}


add_filter( 'update_custom_post_type_meta', 'update_custom_post_type_meta_fn', 10, 1 );
function update_custom_post_type_meta_fn( $post_type_field_group ) {
    return $post_type_field_group;
}

add_filter( 'searchwp_missing_integration_notices', '__return_false' );