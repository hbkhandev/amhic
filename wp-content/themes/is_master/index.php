<?php

if (!class_exists("\TSD_Infinisite\Acme")):
    $filepath = get_template_directory() . '/components/messages/is_disabled.php';
    include($filepath);
    return;
endif;

TSD_Infinisite\Acme::header();

$standard = \TSD_Infinisite\Acme::standard_page();

$args = [];

if ($standard)
    $args['post_id'] = get_the_ID();
if (isset($_GET['s']))
    $args['s'] = $_GET['s'];
if (is_404())
    $args['post_not_found'] = 1;

$layout = new TSD_Infinisite\Layout(get_the_ID());
print $layout->get_html();

TSD_Infinisite\Acme::footer();