<?php

$post = new \TSD_Infinisite\IS_Post(get_the_id());

$title = $module['title'] ? $module['title'] : $post->post_title;

?>

<div class="card">
    <div class="card-section">
        <div class="spacer"></div>
        <h3 class="text-center no-margin"><?= $title ?></h3>
        <div class="spacer"></div>
    </div>
</div>

