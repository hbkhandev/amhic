<?php

$post = new \TSD_Infinisite\IS_Post(get_the_ID());


$module_config = ['acf_fc_layout'  => 'hero',
                  'title' => 'Blog: ' . $post->post_title,
                  'content_blocks' => [['wysiwyg' => '']],];


$hero_module = new \TSD_Infinisite\ACF_Module(['module'          => $module_config,
                                               'module_template' => THEME_URI . 'module_templates/hero/tech_demo.php',]);

?>

<?= $hero_module->get_html() ?>

<div class="grid-container">

    <div class="grid-x grid-padding-x">

        <div class="cell small-12 medium-4 large-3">

            <?= $post->get("excerpt") ?>
            <p><a href="<?= get_permalink(20) ?>">Back to Posts</a></p>


        </div>

        <div class="cell small-12 medium-8 large-9">
            <?= $this->get_row_html() ?>
        </div>
    </div>

</div>
