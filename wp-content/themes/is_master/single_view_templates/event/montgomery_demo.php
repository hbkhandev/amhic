<?php

$module_config = ['acf_fc_layout'  => 'hero',
                  'content_blocks' => [['wysiwyg' => '']],];


$hero_module = new \TSD_Infinisite\ACF_Module(['module'          => $module_config,
                                               'module_template' => THEME_URI . 'module_templates/hero/tech_demo.php',]);

$post = new \TSD_Infinisite\IS_Post(get_the_ID());

$date = $post->get_start_date()->format("F jS, Y");

$map = new \TSD_Infinisite\Google_Map(['height' => '300px']);
$map->add_marker($post->get("location"));


?>

<?= $hero_module->get_html() ?>


<div class="grid-container">

    <div class="grid-x grid-padding-x">

        <div class="cell small-12 medium-4 large-3">
            <?= $map->get_html() ?>
            <div class="spacer small"></div>
            <h6>Event Date: <?= $date ?></h6>
            <p><a href="<?= get_permalink(8) ?>">Back to Events</a></p>
        </div>

        <div class="cell small-12 medium-8 large-9">
            <?= $post->get("content") ?>
            <hr>
            <h2>Registration</h2>
            <?= do_shortcode("[gravityform id='2' title='false' description='false']") ?>
        </div>
    </div>

</div>
