<?php

$post = new \TSD_Infinisite\IS_Post(get_the_ID());
$date = $post->get_start_date()->format("F jS, Y");


$module_config = ['acf_fc_layout'  => 'hero',
                  'title' => 'Publication: ' . $post->get("title"),
                  'content_blocks' => [['wysiwyg' => '']],];


$hero_module = new \TSD_Infinisite\ACF_Module(['module'          => $module_config,
                                               'module_template' => THEME_URI . 'module_templates/hero/tech_demo.php',]);

?>

<?= $hero_module->get_html() ?>


<div class="grid-container">

    <div class="grid-x grid-padding-x">

        <div class="cell small-12 medium-4 large-3">
            <h5>Release</h5>
            <p><?= $date ?></p>

            <div class="spacer small"></div>

            <h5>Tags</h5>

            <ul>

            <? foreach($post->terms['publication_type'] as $term): ?>

                <li><?= $term->name ?></li>

            <? endforeach ?>
            </ul>

            <div class="spacer small"></div>

            <p><a href="<?= $post->get("file")['url'] ?>">Download File</a></p>
            <p><a href="<?= get_permalink(10) ?>">Back to Publications</a></p>


        </div>

        <div class="cell small-12 medium-8 large-9">
            <?= $this->get_row_html() ?>
        </div>
    </div>

</div>
