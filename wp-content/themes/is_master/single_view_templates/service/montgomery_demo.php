<?php

$post = new \TSD_Infinisite\IS_Post(get_the_ID());

$service_query = new \TSD_Infinisite\IS_Query(['post_type' => 'service']);

$module_config = ['acf_fc_layout'  => 'hero',
                  'title'          => "InfiniSite Demo Site Services",
                  'content_blocks' => [['wysiwyg' => '']],];


$hero_module = new \TSD_Infinisite\ACF_Module(['module'          => $module_config,
                                               'module_template' => THEME_URI . 'module_templates/hero/tech_demo.php',]);

?>

<?= $hero_module->get_html() ?>


<div class="grid-container">

    <div class="grid-x grid-padding-x">

        <div class="cell small-12 medium-4 large-3">
            <ul class="menu vertical">

                <? foreach ($service_query->posts as $service): ?>
                    <? $active = $service->ID == $post->ID ?>

                    <li>
                        <a href="<?= $service->permalink ?>"
                           class="<?= $active ? 'secondary-text' : '' ?>"><?= $service->post_title ?></a>
                    </li>

                <? endforeach ?>
            </ul>

        </div>

        <div class="cell small-12 medium-8 large-9">
            <h2><?= $post->post_title ?></h2>
            <?= $post->get("content") ?>
            <div class="spacer small"></div>

            <h3>Ready to Learn More?</h3>
            <?= do_shortcode("[gravityform id='1' title='false' description='false']") ?>
        </div>
    </div>

</div>
