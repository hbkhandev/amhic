<?php
$post = new \TSD_Infinisite\IS_Post(get_the_ID());

$module_config = ['acf_fc_layout'  => 'hero',
                  'title'          => 'Staff',
                  'content_blocks' => [['wysiwyg' => '']],];


$hero_module = new \TSD_Infinisite\ACF_Module(['module'          => $module_config,
                                               'module_template' => THEME_URI . 'module_templates/hero/tech_demo.php',]);


?>

<?= $hero_module->get_html() ?>

<div class="spacer"></div>

<div class="grid-container">

    <div class="grid-x grid-padding-x">

        <div class="cell small-12 medium-4 large-3">
            <div class="card">
                <? if ($post->get("image")): ?>
                    <img src="<?= $post->get('image')['sizes']['large'] ?>" alt="" class="full-width" />
                <? endif ?>

                <div class="card-section">
                    <? $email = $post->get("email_address") ?>
                    <p>Email :
                        <a href="mailto:<?= $email ?>">
                            <?= $email ?>
                        </a>
                    </p>
                </div>

            </div>
            <div class="spacer small"></div>
            <p class="text-center"><a href="<?= get_permalink(172) ?>">Back to Staff</a></p>

        </div>

        <div class="cell small-12 medium-8 large-9">
            <h2><?= $post->get("name") ?></h2>
            <?= $post->get("content") ?>
            <div class="spacer small"></div>

            <h3>Let's Start The Conversation</h3>
            <?= do_shortcode("[gravityform id='1' title='false' description='false']") ?>


        </div>
    </div>

</div>
