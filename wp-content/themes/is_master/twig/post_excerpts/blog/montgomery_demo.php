<div class="cell">
    <div class="card primary">

        <div class="card-divider">
            <a href="<?= $post->permalink ?>">
                <h5 class="no-margin">
                    <?= $post->post_title ?>
                </h5>
            </a>

            <? if ($post->get("start_date")): ?>
                <p class="gray_dark-text auto-text no-bottom-margin auto-left">
                    <small>
                        <?= $post->get_start_date()->format('F jS, Y') ?>
                    </small>
                </p>
            <? endif ?>

        </div>


        <div class="card-section">


            <div class="grid-container grid-x grid-padding-x no-padding no-margin">





                <? if ($post->get("image")): ?>
                    <div class="cell medium-3 text-center">
                        <a href="<?= $post->permalink ?>">
                            <img src="<?= $post->get("image")['sizes']['medium'] ?>" alt="<?= $post->post_title ?>">
                        </a>
                    </div>
                <? endif ?>


                <div class="cell small-12 medium-4 large-3">



                    <? if ($post->get("author_post")): ?>

                        <p>
                            Post by:
                            <? foreach ($post->get("author_post") as $c => $a): ?>

                                <? $author = new \TSD_Infinisite\IS_Post($a->ID) ?>

                                <a href="<?= $author->permalink ?>">
                                    <?= $author->get("name") ?><?= $c == count($post->get("author_post")) - 1 ? ' ' : ', ' ?>
                                </a>

                            <? endforeach ?>
                        </p>
                    <? endif ?>

                    <h6>Categories</h6>
                    <? foreach ($post->terms['category'] as $category): ?>
                        <p><?= $category->name ?></p>
                    <? endforeach ?>

                    <h6>Tags</h6>
                    <? foreach ($post->terms['post_tag'] as $c => $category): ?>
                        <span><?= $category->name ?><?= $c == count($post->terms['post_tag']) - 1 ? '' : ', ' ?></span>
                    <? endforeach ?>

                </div>

                <div class="cell auto">
                    <?= $post->get("excerpt") ?>
                    <? if ($post->permalink): ?>
                        <a href="<?= $post->permalink ?>" class="arrow_link">Read More</a>
                    <? endif ?>
                </div>

            </div>


        </div>
    </div>
</div>
