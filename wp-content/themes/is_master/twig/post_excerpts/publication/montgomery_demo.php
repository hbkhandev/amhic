<div class="cell">
    <div class="card primary">

        <div class="card-divider">
            <a href="<?= $post->permalink ?>">
                <h5 class="no-margin">
                    <?= $post->post_title ?>
                </h5>
            </a>

            <? if ($post->get("start_date")): ?>
                <p class="gray_dark-text auto-text no-bottom-margin auto-left">
                    <small>
                        <?= $post->fields->format_date('F jS, Y') ?>
                    </small>
                </p>
            <? endif ?>

        </div>


        <div class="card-section">

            <? if ($post->get("author_post")): ?>
                <p>Post By: <?= $post->get("author_post")[0]->post_title ?></p>
            <? endif ?>


            <div class="grid-container grid-x grid-padding-x no-padding no-margin align-middle">
                <? if ($post->get("image")): ?>
                    <div class="cell medium-3 text-center">
                        <a href="<?= $post->permalink ?>">
                            <img src="<?= $post->get("image")['sizes']['medium'] ?>" alt="<?= $post->post_title ?>">
                        </a>
                    </div>
                <? endif ?>

                <div class="cell auto">
                    <?= $post->get("excerpt") ?>
                    <? if ($post->permalink): ?>
                        <a href="<?= $post->permalink ?>" class="arrow_link">Read More</a>
                    <? endif ?>
                </div>

            </div>


        </div>
    </div>
</div>
