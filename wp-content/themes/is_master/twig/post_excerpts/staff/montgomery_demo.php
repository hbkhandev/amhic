<div class="cell">

    <div class="montgomery_demo excerpt card">
        <a href="<?= $fields->permalink ?>">
            <?= $fields->get_image("medium", ['class' => 'full-width']) ?>
        </a>

        <div class="card-section">


            <h3>
                <a href="<?= $fields->permalink ?>">
                    <?= $fields->get("name") ?>
                </a>
            </h3>
            <h4><?= $fields->get("title") ?></h4>

        </div>


    </div>
</div>
